from attribute_dict import AD
from image.utilities import *
import logging
import os
from math import sqrt
logger = logging.getLogger("mvapi")

import numpy as np
import cv2

__LINE_THICKNESS = 2
__COLOR_REFERENCE = (0, 0, 255)
__COLOR_SAFE = (0, 255, 0)
__COLOR_VIOLATION = (255, 0, 0)
__COLOR_EXTREME_VIOLATION = (255, 0, 0)
__COLOR_WHITE = (255,255,255)


CAGE_READY = 'ready'
CAGE_DEPLOYING = 'deploying'
CAGE_DEPLOYED = 'deployed-safely'
CAGE_VIOLATION = 'cage-violation'
EXTREME_CAGE_VIOLATION = 'extreme_cage-violation'
SAFE = 'safe_usage'

CAGE_UNSAFE_STATES = [CAGE_READY, CAGE_DEPLOYING]

def choose_cage_box(saferack_boxes, cfg):
    # TODO: choose correct saferack box using better heuristics
    # currently selects the largest bounding box

    if not len(saferack_boxes):
        return None
    elif len(saferack_boxes) == 1 and len(saferack_boxes[0]) >= 4:
        return Zone.create('cage', saferack_boxes[0])
    else:
        box = None
        for bbox in [b for b in saferack_boxes if len(b) >= 4]:
            if box is None or ((box[2]*box[3]) < (bbox[2]*bbox[3])): # check if bbox has larger area
                box = bbox
        if len(box) >= 4:
            return Zone.create('cage', box)
        else:
            return None

def select_image(filedata, preferred):
    anonymized_index = None
    for i, (filename, image) in enumerate(filedata):
        if filename == preferred:
            return load_image(image)
        elif filename == 'anonymous.jpg':
            anonymized_index = i

    if anonymized_index is not None:
        return load_image(filedata[anonymized_index][1])
    elif len(filedata) > 0:
        return load_image(filedata[0][1])

    return None

def distance(p1, p2):
    return sqrt(
        (p1.x - p2.x)**2 + (p1.y - p2.y)**2
    )
    

def operator_in_zone(operator_bboxes, cage_bbox, extents=None):
    for operator in operator_bboxes:
        if not isinstance(operator, Zone):
            operator = Zone.create('person', operator)
        if extents is not None:
            # extents should be an array of values
            x_left, x_right, y_top, y_bottom = extents
            in_x = cage_bbox.x + x_left <= operator.xmid <= cage_bbox.x2 - x_right
            in_y = cage_bbox.y + y_top <= operator.ymid <= cage_bbox.y2 - y_bottom
            return in_x and in_y
        if cage_bbox.x <= operator.xmid <= cage_bbox.x2:
            return True

    return False

def cage_violation(vgroup, vgc, egc, edgc, rd):
    # cfg_EXAMPLE = {
    #     "raised_cage_bbox": [],
    #     "deployed_cage_bbox": [], # optional
    #     "deploying_threshold": 0.5,
    #     "deployed_threshold": 0.8,
    #     "output_image": "annonymize.jpg" # optional
    # }
    try:
        logger.info(f"cage_violation cfg: {vgc}")
        bboxes = rd.output.bboxes
        filedata = rd.output.filedata
        srcfg = vgc.conditions.cage_violation
        esrcfg = vgc.conditions.extreme_cage_violation

        cage_box = choose_cage_box(bboxes.saferack_cage, srcfg)
        if cage_box is None:
            logger.exception(f"cage_violation cage_box is None")
            return

        for v in ['deploying_threshold', 'deployed_threshold']:
            if v not in srcfg or not srcfg[v]:
                logger.exception(f"cage_violation parameter {v} is not correct: {srcfg[v]}")
                return
        
        if 'extents' in srcfg and isinstance(srcfg.extents, (list, tuple)) and len(srcfg.extents) != 4:
            logger.exception("cage_violation extents misconfigured {srcfg.extents}")
            srcfg.extents = None # Don't kill processing over misconfigured extents


        if 'raised_cage_bbox' not in srcfg or len(srcfg.raised_cage_bbox) < 4:
            logger.exception(f"cage_violation raised_cage_bbox is not correct: {srcfg.raised_cage_bbox}")
            return

        anchor_box = Zone.create('anchor', srcfg.raised_cage_bbox)
        if not isinstance(anchor_box, Zone):
            logger.exception(f"cage_violation anchor_box is not correct: {srcfg.raised_cage_bbox}")
            return


        # if deployed_cage_bbox bbox isn't given, threshold is in pixels instead of percentage
        if 'deployed_cage_bbox' in srcfg:
            deployed_box = Zone.create('deployed', srcfg.deployed_cage_bbox)
            max_length = distance(anchor_box.centroid, deployed_box.centroid)
        else:
            deployed_box = None
            max_length = 1


        image = select_image(filedata, srcfg.get('output_image', 'anonymous.jpg'))
        current_length = distance(anchor_box.centroid, cage_box.centroid)
        # TODO: Projection?

        displacement = (current_length / max_length)

        operator_in_image = ('operator' in bboxes and len(bboxes.operator))
        operator_in_cage = operator_in_image and operator_in_zone(bboxes.operator, cage_box, srcfg.get('extents'))
        egc.cage_violation = 0
        egc.extreme_cage_violation = 0
        state = status = CAGE_READY

        if operator_in_cage:
            if displacement > srcfg.deployed_threshold:
                state = CAGE_DEPLOYED
                status = SAFE
            elif displacement >= srcfg.deploying_threshold:
                state = CAGE_DEPLOYING
                status = CAGE_VIOLATION
                egc.cage_violation = 1
                if displacement <= esrcfg.extreme_displacement:
                    status = EXTREME_CAGE_VIOLATION
                    egc.extreme_cage_violation = 1

        if image is not None:
            color = __COLOR_SAFE
            thickness = __LINE_THICKNESS
            if status == CAGE_VIOLATION:
                color = __COLOR_VIOLATION
            elif status == EXTREME_CAGE_VIOLATION:
                color = __COLOR_EXTREME_VIOLATION
                thickness = __LINE_THICKNESS * 2

            cv2.circle(image, anchor_box.centroid.as_int_point(), __LINE_THICKNESS+2, __COLOR_REFERENCE, thickness=-1)
            cv2.circle(image, cage_box.centroid.as_int_point(), thickness+2, color, thickness=-1)

            if deployed_box is not None:
                cv2.circle(image, deployed_box.centroid.as_int_point(), __LINE_THICKNESS+2, __COLOR_REFERENCE, thickness=-1)

            cv2.putText(image, f"{status}: {displacement:.2f}", (int(deployed_box.xmid + 10), (deployed_box.y2 + cage_box.y2)//2), cv2.FONT_HERSHEY_DUPLEX, 0.5, __COLOR_WHITE, thickness=1)

            filedata.append((f"{str(to_datetime(rd.collection_time).timestamp())}:{status}.jpg", pack_image(image)))

        edgc.update({
            "cage_status": state,
            "cage_violation": True if status in [CAGE_VIOLATION, EXTREME_CAGE_VIOLATION] else False,
            "collection_time": rd.collection_time,
            "displacement": displacement,
            "extreme_cage_violation": True if status == EXTREME_CAGE_VIOLATION else False,
            "status": status
            })

        if status not in (CAGE_READY, SAFE):
            logger.info(f"cage_violation a cage_violation has occurred: {edgc}")

    except Exception as err:
        msg = f"cage_violation.cage_violation error: {repr(err)}"
        logger.exception(msg)
        raise

#!/usr/bin/env python3

""" Configuration example on the confluence page

https://atollogy.atlassian.net/wiki/spaces/CV/pages/590774424/Configuration+for+tf+inference+classification

"""

from attribute_dict import AD
from image.utilities import *
from shell import shcmd

import logging
import os
import tensorflow as tf
import numpy as np
import cv2
import base64
import re
import time
import asyncio

class ClassificationInference:
    def __init__(self, mcfg=None):
        self.mcfg = AD(mcfg)
        self.model_name = self.mcfg.name

        self.logger = logging.getLogger("mvapi")

        self.image_size = 224

        self.load_success = None

        self.create_graph()


    def create_graph(self):
        """
        load the tensorflow model used for object classification
        """
        try:
            self.logger.warning(
                "Loading {} - {}".format(self.__class__.__name__, self.mcfg.name)
            )

            version_lookup_path = os.path.join(
                str(self.mcfg.lookup_path), str(self.mcfg.model_version)
            )
            self.graph_name = shcmd(f"find {version_lookup_path} -type f -name '*.pb'")[0][0].strip()
            with tf.gfile.FastGFile(self.graph_name, "rb") as f:
                self.graph_def = tf.GraphDef()
                self.graph_def.ParseFromString(f.read())
                _ = tf.import_graph_def(self.graph_def, name="")

            self.sess = tf.Session()
            self.load_success = True
            self.class_dic = self.setup_class_list()
            return True
        except:
            self.load_success = False
            self.logger.exception(
                "****************** Cannot load classifier model: {} ******************".format(
                    self.mcfg.name
                )
            )
            return False

    def setup_class_list(self):
        """
        load classes dictionary
        """
        class_dic = {}
        try:
            if 'classes' in self.mcfg.cfg:
                return {
                    int(k): v for k, v in self.mcfg.cfg.classes.items()
                }
            elif 'classes_path' in self.mcfg.cfg:
                return {
                    int(k): v for k, v in AD.load(self.mcfg.cfg.classes_path).items()
                }
            else:
                label_lookup_path = os.path.join(
                    self.mcfg.lookup_path,
                    str(self.mcfg.model_version),
                    self.mcfg.cfg.label_file
                )
                uid_lookup_path = os.path.join(
                    self.mcfg.lookup_path,
                    str(self.mcfg.model_version),
                    self.mcfg.cfg.uid_file
                )
                return self.inception_list_load(label_lookup_path, uid_lookup_path)
        except:
            self.logger.exception(
                "****************** Cannot load model class labels: {} ******************".format(
                    self.mcfg.name
                )
            )


    ####### Inception model load classes #######
    def inception_list_load(self, label_lookup_path, uid_lookup_path):
        """Loads a human readable English name for each softmax node.

        Args:
            label_lookup_path: string UID to integer node ID.
            uid_lookup_path: string UID to human-readable string.

        Returns:
            dict from integer node ID to human-readable string.
        """
        # Loads mapping from string UID to human-readable string
        proto_as_ascii_lines = tf.gfile.GFile(uid_lookup_path).readlines()
        uid_to_human = {}
        p = re.compile(r"[n\d]*[ \S,]*")
        for line in proto_as_ascii_lines:
            parsed_items = p.findall(line)
            uid = parsed_items[0]
            human_string = parsed_items[2]
            uid_to_human[uid] = human_string

        # Loads mapping from string UID to integer node ID.
        node_id_to_uid = {}
        proto_as_ascii = tf.gfile.GFile(label_lookup_path).readlines()
        for line in proto_as_ascii:
            if line.startswith("  target_class:"):
                target_class = int(line.split(": ")[1])
            if line.startswith("  target_class_string:"):
                target_class_string = line.split(": ")[1]
                node_id_to_uid[target_class] = target_class_string[1:-2]

        # Loads the final mapping of integer node ID to human-readable string
        node_id_to_name = {}
        for key, val in node_id_to_uid.items():
            name = uid_to_human[val]
            node_id_to_name[key] = name

        return node_id_to_name

    ################# Image Process functions list #################
    """
    crop_option
        "whole": use entire image, no crop
        "three": automatically image crop by three separate regions, left, center, right. each region keeps the square shape.
        "center": crop only center square region.
        "interest_region": regions of interest are for one target object.
    """
    def img_regularization(self, img):
        ### average RGB result from previous training and resize image
        RGB_channel_means = list(self.mcfg.cfg.RGB_MEAN)
        resize_img = cv2.resize(img, (self.image_size, self.image_size))
        img_mean = resize_img - RGB_channel_means
        return img_mean

    def crop(self, img, zone):
        ### format of zone is [x,y,w,h].
        crop_img = img[zone[1] : zone[1] + zone[3], zone[0] : zone[0] + zone[2]]
        return crop_img

    def img_center_crop(self, img):
        """
        image will be cropped with square shape on center.
        square's one side will be image's height.
        Thus, starting crop x1 will be (w / 2) - (h / 2).
        """

        h, w, _ = img.shape
        crop_img = img.copy()[0:h, int((w / 2) - (h / 2)) : int((w / 2) + (h / 2))]

        return crop_img

    def img_three_separation(self, img):
        ### image crop by left square, center square, right sqaure.
        h, w, _ = img.shape
        crop_img = []

        x1_coordinate = {
            "left": 0,
            "center": int((w / 2) - (h / 2)),
            "right": int(w - h),
        }

        for _, value in x1_coordinate.items():
            crop_img.append(img.copy()[0:h, value : value + h])

        return crop_img

    def img_dir_crop(self, img, direction, pt1, pt2):
        """
        Function to crop input img. The crops used would be in increasing size starting from left or right based on direction
        :param img: Input np.array image
        :param direction: "left" or "right"
        :param pt1: [x,y] coordinates of a point (a corner of rectangular region we want to focus on)
        :param pt2: [x,y] coordinates of an opposite point.
        :return:

        In customer config:

        "crop_option": {
            "dir": [
              "left",
              [1086, 90],
              [2822, 1555]
            ]
        """
        crop_img = []

        self.dir_crops = []

        min_x = min(pt1[0], pt2[0])
        max_x = max(pt1[0], pt2[0])

        min_y = min(pt1[1], pt2[1])
        max_y = max(pt1[1], pt2[1])

        w = max_x - min_x
        h = max_y - min_y

        scale_w = 0.1 * w
        scale_h = 0.1 * h

        if direction == 'left':
            # Biggest crop is on the left.
            # A scenario where this can be used is when we see a major portion of vehicle to the left of the image
            # and hence should have a bigger crop to the left to capture it
            #
            # First crop:   x coordinates: min_x -> min_x+h (or max_h)
            #               y coordinates: min_y -> max_y
            # Second crop:  x coordinates: min_x + 20% of width -> min_x + 80% of width
            #               y coordinates: min_y -> min_y + 75% of height
            # Third crop:   x coordinates: min_x + 50% of width -> max_x
            #               y coordinates: min_y -> min_y + 60% of height
            self.dir_crops = np.array([
                [min_x, min_y, min_x + h, max_y],
                [min_x + int(2 * scale_w), min_y, min_x + int(8 * scale_w), min_y + int(7.5 * scale_h)],
                [min_x + int(5 * scale_w), min_y, max_x, min_y + int(6 * scale_h)]], dtype=np.int64)

        elif direction == "right":
            # Biggest crop is on the right.
            # A scenario where this can be used is when we see a major portion of vehicle to the right of the image
            # and hence should have a bigger crop to the right to capture it
            #
            # First crop:   x coordinates: max_x - h(or min_x) -> max_x
            #               y coordinates: min_y -> max_y
            # Second crop:  x coordinates: min_x + 20% of width -> min_x + 80% of width
            #               y coordinates: min_y -> min_y + 75% of height
            # Third crop:   x coordinates: min_x  -> min_x + 50% of width
            #               y coordinates: min_y -> min_y + 60% of height
            self.dir_crops = np.array([
                [max_x-h, min_y, max_x, max_y],
                [min_x + int(2 * scale_w), min_y, min_x + int(8 * scale_w), min_y + int(7.5 * scale_h)],
                [min_x , min_y, min_x + int(5 * scale_w), min_y + int(6 * scale_h)]], dtype=np.int64)

        if min_x + h > w:
            self.dir_crops[0] = [min_x, min_y, max_x, max_y]

        for i in range(len(self.dir_crops)):
            crop_img.append(img.copy()[self.dir_crops[i][1]:self.dir_crops[i][3], self.dir_crops[i][0]:self.dir_crops[i][2], :])

        # Convert from [x1, y1, x2, y2] to [x1, y1, w, h]
        self.dir_crops[:, 2] = self.dir_crops[:, 2] - self.dir_crops[:, 0]
        self.dir_crops[:, 3] = self.dir_crops[:, 3] - self.dir_crops[:, 1]
        self.dir_crops = self.dir_crops.tolist()

        return crop_img

    def img_preprocess(self, pre_process, img, img_index, image_data):
        """
        image preprocess depend on different model,
        inception needs to be encoded as a string byte format.
        resnet needs to be resize and mean subtraction.
        """
        # inception format
        if pre_process == "img_encode":
            img_str_bytes = cv2.imencode(
                ".jpg", img, [int(cv2.IMWRITE_JPEG_QUALITY), 100]
            )[1].tostring()
            image_data.append(img_str_bytes)
        # resnet format
        elif pre_process == "regularization":
            image_data[img_index] = self.img_regularization(img)

        return image_data

    def img_interest_region(self, rd, color_sapce_image, image_data, pre_process):
        """
        Classify one object from several differenct zone.
        For example, lookup_zone parameter will be airplane head, body, tail.

        parameter rd: RecData
        parameter color_sapce_image: image input
        parameter image_data: image stack array
        parameter pre_process: kind of image process choose from ["img_encode", "regularization"]

        result img_stack_length: length of interest_region
        result image_data: after processing, image stack array
        """
        img_stack_length = len(rd.interest_region)
        for i, each_zone in enumerate(rd.interest_region.values()):
            crop_img = self.crop(color_sapce_image, each_zone)
            image_data = self.img_preprocess(pre_process, crop_img, i, image_data)

        return img_stack_length, image_data

    def img_crop_option(self, rd, color_sapce_image, image_data, pre_process):
        """
        Depend on crop option statement.
        image will be processed with the option.
        crop_option is ["three" , "center", "whole"]

        parameter rd: RecData
        parameter color_sapce_image: image input
        parameter image_data: image stack array
        parameter pre_process: kind of image process choose from ["img_encode", "regularization"]

        result img_stack_length: length of interest_region
        result image_data: after processing, image stack array
        """

        img_stack_length = 1
        if rd.crop_option == "three":
            crop_img_list = self.img_three_separation(color_sapce_image)
            img_stack_length = len(crop_img_list)
            for i in range(img_stack_length):
                crop_img = crop_img_list[i]
                image_data = self.img_preprocess(pre_process, crop_img, i, image_data)
        elif rd.crop_option == "center":
            crop_img = self.img_center_crop(color_sapce_image)
            image_data = self.img_preprocess(pre_process, crop_img, 0, image_data)

        elif not isinstance(rd.crop_option, str) and rd.crop_option.dir:
            # If the crop option is "dir"

            # Return the crops from color_sapce_image to be passed to inference
            crop_img_list = self.img_dir_crop(color_sapce_image, rd.crop_option.dir[0], rd.crop_option.dir[1], rd.crop_option.dir[2])
            img_stack_length = len(crop_img_list)

            # Preprocess based on whether resnet or inception based base model
            for i in range(img_stack_length):
                image_data = self.img_preprocess(pre_process, crop_img_list[i], i, image_data)

        else:
            image_data = self.img_preprocess(pre_process, color_sapce_image, 0, image_data)

        return img_stack_length, image_data

    ################# End Image Process functions #################

    def result_confidence(self, rd, classified_obj):
        isdict = isinstance(rd.group_class, dict)
        confidence_score = 0.0
        top_class = None
        for cl in classified_obj.keys():
            if cl in rd.group_class:
                score = classified_obj[cl]
                if (not isdict and score >= rd.confidence_threshold) or (isdict and score >= rd.group_class[cl]):
                    if score > confidence_score:
                        confidence_score = score
                        top_class = cl

        if not top_class and "default_class" in rd and rd.default_class:
            confidence_score = rd.confidence_threshold
            top_class = rd.default_class

        return top_class, confidence_score

    def build_prediction_dict(
        self, result_dic, confidence_list, top_k, class_top_score, stack_index, rd
    ):
        for node_id in top_k:
            if node_id not in self.class_dic:
                human_string = "Undefined_node_id"
            else:
                human_string = self.class_dic[node_id]
            score = float(confidence_list[node_id])

            ### if duplicate human readable key, keep bigger score.
            if human_string in result_dic:
                if result_dic[human_string] < score:
                    result_dic[human_string] = score
            else:
                result_dic[human_string] = score

        if "group_class" in rd and rd.group_class:
            _, score = self.result_confidence(rd, result_dic)
            if score > class_top_score:
                # save image index value to annotate image.
                rd.annotation_index = stack_index
                class_top_score = score
        else:
            score = max(result_dic.values())
            if score > class_top_score:
                rd.annotation_index = stack_index
                class_top_score = score

        return result_dic, class_top_score

    def top_class_confidence(self, rd, result_dic, annotation_label):
        """
        "subjects": {
            "num_top_predictions": 20, ← number of rank list, it will show only top number of list.
            "confidence_threshold": 0.03, ← confidence threshold
            "group_class": list input or dictionary input available
            *"group_class": ["airliner", "warplane, military plane", "wing"]
                        your tragetting classes to classify,
                        if you find a one of those classes from your rank result with higher confidence score than "confidence_threshold",
                        choose as a top_class
            *"group_class": {"airliner": 0.03, "warplane, military plane": 0.03, "wing": 0.5, "class_label": confidence threshold}
                        it is available to put each confidence threshold per each clase.
        """

        # only collect confidence score from classes list.
        if "group_class" in rd and rd.group_class:
            top_class, top_confidence = self.result_confidence(rd, result_dic)
        else:
            top_class = None
            top_confidence = max(result_dic.values())

        # use annotation label if it exists
        if annotation_label:
            rd.output[annotation_label] = top_confidence
            top_class = annotation_label
        elif not top_class:
            top_class = max(result_dic, key=result_dic.get)
            top_confidence = result_dic[top_class]

        rd.output.result = top_class

        return top_class, top_confidence

    def annotation_img_filedata(self, img, class_label, confidence_score, rd):
        if len(img.shape) == 3:
            h, w, _ = img.shape
        else:
            h, w = img.shape

        three_crop = [
            [0, 0, h, h],
            [int((w / 2) - (h / 2)), 0, h, h],
            [int(w - h), 0, h, h],
        ]

        if "interest_region" in rd:
            annotation_bbox = list(rd.interest_region.values())[rd.annotation_index]
        elif "crop_option" in rd:
            if rd.crop_option == "three":
                annotation_bbox = three_crop[rd.annotation_index]
            elif rd.crop_option == "center":
                annotation_bbox = three_crop[1]
            elif isinstance(rd.crop_option, dict) and rd.crop_option.dir:
                annotation_bbox = self.dir_crops[rd.annotation_index]
            else:
                annotation_bbox = [0, 0, w, h]
        else:
            annotation_bbox = [0, 0, w, h ]

        confidence_score = confidence_score * 100

        text_label = "{} {:.1f}%".format(class_label, confidence_score)
        boxes = {text_label: [annotation_bbox]}
        to_annotate = []
        for label, bboxes in boxes.items():
            to_annotate.extend([Zone.create(label, b, rtype=class_label, threshold=confidence_score) for b in bboxes])
        rd.output.filedata.append(('annotated.jpg',
                                    annotate(img.copy(), to_annotate, color_space=rd.color_space)))
        save_annotations(rd, to_annotate, rd.origins, rd.color_space)

        if confidence_score < rd.confidence_threshold:
            text_label = ""
            boxes = {text_label: [annotation_bbox]}
            to_anonymize = []
            for label, bboxes in boxes.items():
                to_anonymize.extend([Zone.create(label, b, rtype=class_label, threshold=confidence_score) for b in bboxes])
            rd.output.filedata.append(('anonymous.jpg',
                                    annotate(img.copy(), to_anonymize, color_space=rd.color_space)))
            save_annotations(rd, to_anonymize, rd.origins, rd.color_space)

        rd.delete("annotation_index")
        del rd.image

    ################## main ######################

    def inception_inference(self, color_sapce_image, rd):

        image_data = []
        result_dic = {}
        result_dic_crop = {}

        ### image preprocess
        if "interest_region" in rd and rd.interest_region:
            image_count, image_data = self.img_interest_region(
                rd, color_sapce_image, image_data, "img_encode"
            )
        elif "crop_option" in rd and rd.crop_option:
            image_count, image_data = self.img_crop_option(
                rd, color_sapce_image, image_data, "img_encode"
            )
        else:
            image_count = 1
            img_str_bytes = cv2.imencode(
                ".jpg", color_sapce_image, [int(cv2.IMWRITE_JPEG_QUALITY), 100]
            )[1].tostring()
            image_data.append(img_str_bytes)

        # recursive value to find which image have top confidence from image stacks
        class_top_score = 0

        for i in range(image_count):

            softmax_tensor = self.sess.graph.get_tensor_by_name("softmax:0")

            predictions = self.sess.run(
                softmax_tensor, {"DecodeJpeg/contents:0": image_data[i]}
            )
            predictions = np.squeeze(predictions)

            top_k = predictions.argsort()[-rd.num_top_predictions :][::-1]
            confidence_list = predictions

            if rd.debug:
                result_dic_crop[str(i)] = {}

                for node_id in top_k:
                    result_dic_crop[str(i)][self.class_dic.get(node_id, 'Undefined_node_id')] = float(
                        confidence_list[node_id])

            result_dic, class_top_score = self.build_prediction_dict(
                result_dic, confidence_list, top_k, class_top_score, i, rd
            )

        return result_dic, result_dic_crop

    def resnet_v2_inference(self, color_sapce_image, rd):

        result_dic = {}
        result_dic_crop = {}
        batch_size = self.mcfg.cfg.batch_size
        image_data = np.zeros((batch_size, self.image_size, self.image_size, 3))

        ### image preprocess
        if "interest_region" in rd and rd.interest_region:
            img_stack_length, image_data = self.img_interest_region(
                rd, color_sapce_image, image_data, "regularization"
            )
        elif "crop_option" in rd and rd.crop_option:
            img_stack_length, image_data = self.img_crop_option(
                rd, color_sapce_image, image_data, "regularization"
            )
        else:
            img_stack_length = 1
            image_data[0] = self.img_regularization(color_sapce_image)

        softmax_tensor = self.sess.graph.get_tensor_by_name("softmax_tensor:0")
        predictions = self.sess.run(softmax_tensor, {"input_tensor:0": image_data})
        predictions = np.squeeze(predictions)

        # recursive value to find which image have top confidence from image stacks
        class_top_score = 0

        for stack_index in range(img_stack_length):
            top_k = predictions.argsort()[stack_index][-rd.num_top_predictions :][::-1]
            confidence_list = predictions[stack_index]

            if rd.debug:
                result_dic_crop[str(stack_index)] = {}

                for node_id in top_k:
                    result_dic_crop[str(stack_index)][self.class_dic.get(node_id, 'Undefined_node_id')] = float(
                        confidence_list[node_id])

            result_dic, class_top_score = self.build_prediction_dict(
                result_dic, confidence_list, top_k, class_top_score, stack_index, rd
            )

        return result_dic, result_dic_crop

    def image_inference(self, color_sapce_image, rd):
        """
        Runs inference on an image.
        Args:
            image: Image file name.

        # Some useful tensors:
        # 'softmax, softmax_tensor:0': A tensor containing the normalized prediction across
        #   labels.
        # 'DecodeJpeg/contents, input_tensor:0': A tensor containing a string providing JPEG
        #   encoding of the image.
        # Runs the softmax tensor by feeding the image_data as input to the graph.

        Returns:
        {
            "object_class_1": confidence_score,
            "object_class_2": confidence_score,
            ...
        }
        """
        rd.annotation_index = 0

        if self.mcfg.name == "inception_imagenet":
            result_dic, result_dic_crop = self.inception_inference(color_sapce_image, rd)
            annotation_label = rd.subject
        elif self.mcfg.name == "resnet50_v2_imagenet":
            result_dic, result_dic_crop = self.resnet_v2_inference(color_sapce_image, rd)
            annotation_label = rd.subject
        else:
            result_dic, result_dic_crop = self.resnet_v2_inference(color_sapce_image, rd)
            annotation_label = None

        ### step for classification rank result added, rank_result show all list with confidence probability
        rd.output.rank_result = result_dic

        if rd.debug:
            # this will be true only when debug option is specified and set to True on config
            rd.output.rank_result_crop = result_dic_crop

        # calculate classification result with subject parameters
        return self.top_class_confidence(rd, result_dic, annotation_label)

    def run_classification(self, recData):
        """
        detect bounding boxes and confidence of objects of interest in an image
        currently we are only looking for people

        Parameters
        ----------
        Coming from customerCfg...
        "<step_container_label>": {
            "function": <funcName>,        <-- mv object recognition model name
            "color_space": "BGR",
            "input_image_indexes": [0],
            "input_crop": null|<Zone [x, y, w, h]>     #<-- used to crop input
            "input_step": <input step source>          #<-- precondition
            "region_filter": false,
            "subjects": {
                "model_name": { # Any Key need to exist. (You cannot put 'subjects' under 'sebjects' key!)
                }
            },
            "version": <version number of function>    #<-- step setup

        Returns
        -------
        a response in the following shape is sent back to the atlapi mvresults endpoint
        {
            'reason': None,
            'status': 'success',
            'subjects': {},
            'result': selected class result (using for report),
            'rank_result': {class rank result....} like [truck: confidence_score, trailor: confidence_score, ...],
        }
        """

        event_loop = asyncio.get_event_loop()
        start = event_loop.time()

        rd = recData
        if 'debug' not in rd:
            rd.debug = False
        rd.output = AD({
            "filedata": [],
            "origins": rd.origins,
            "reason": None,
            "status": "success",
            "subjects": {},
            "model_name": self.model_name
        })
        try:
            color_sapce_image = load_image(
                rd.image, color_space='rgb', crop=rd.input_crop
            )  # Loads image as rgb.
            # load_image converts to rgb format internally

            # self.logger.debug("color_sapce_image type: {}".format(type(color_sapce_image)))
            # self.logger.info("Object classification params: {}".format(rd))

            # Add brightness and resolution values to output
            check_image(color_sapce_image, rd, rd.output)

            if "num_top_predictions" not in rd:
                rd.num_top_predictions = 5

            if "confidence_threshold" not in rd:
                if "nms_threshold" in rd:
                    rd.confidence_threshold = rd.nms_threshold
                else:
                    rd.confidence_threshold = 0.4

            if "crop_option" not in rd:
                # option: "whole", "center", "three"
                rd.crop_option = "whole"

            top_class, top_confidence = self.image_inference(
                color_sapce_image, rd
            )  # imagenet classification

            rd.output.inference_time = event_loop.time() - start

            self.logger.info(
                "classified_obj: topclass: {} - infer time: {}".format(
                    top_class, rd.output.inference_time
                )
            )

            self.annotation_img_filedata(color_sapce_image, top_class, top_confidence, rd)

            rd.output.total_mvapi_time = event_loop.time() - start

            return rd

        except Exception as err:
            self.logger.exception(
                "****************** Cannot do Object Classification, node_name:{} - step_name:{} - model_name:{} Error:{}".format(
                    rd.nodename, rd.function, self.mcfg.name, repr(err)
                )
            )


    async def classify(self, recData):
        return self.run_classification(recData)

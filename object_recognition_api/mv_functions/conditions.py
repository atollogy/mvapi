
from .cage_violation import cage_violation
from attribute_dict import *
from image.utilities import *
import logging
import os
logger = logging.getLogger("mvapi")

import numpy as np
import cv2

category_defaults = AD({
    "ppe": {
        "primary": "operator",
        "conditions": {
            "hard_hat": {
                "alert": True,
                "annotation": "no hard hat",
                "descriptors": {
                    "hard_hat": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6,
                "ctype": 'individual'
            },
            "safety_vest": {
                "alert": True,
                "annotation": "no safety vest",
                "descriptors": {
                    "safety_vest": False,
                    "safety_garment": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6,
                "ctype": 'individual'
            },
        },
        "category": "ppe",
        "iou_threshold": 0,
        "output_image": "anonymous.jpg"
    },
    "cage": {
        "primary": "operator",
        "conditions": {
            "cage_violation": {
                "alert": True,
                "annotation": "cage violation",
                "descriptors": {
                    "cage_violation": True,
                },
                "deployed_threshold": 0.75,
                "deploying_threshold": 0.4,
                "enabled": True,
                "min_count": 1,
                "threshold": 0.05,
                "extreme_displacement": 0.6,
                "truck_alignment_count": 3,
                "ctype": 'individual'
            },
            "extreme_cage_violation": {
                "alert": True,
                "annotation": "extreme cage violation",
                "descriptors": {
                    "extreme_cage_violation": True
                },
                "deployed_threshold": 0.75,
                "deploying_threshold": 0.4,
                "enabled": True,
                "min_count": 1,
                "threshold": 0.05,
                "extreme_displacement": 0.6,
                "truck_alignment_count": 3,
                "ctype": 'individual'
            },
            "truck_alignment_violation": {
                "alert": True,
                "descriptors": {
                    "cage_violation": True,
                    "extreme_cage_violation": True
                },
                "enabled": True,
                "min_count": 5,
                "threshold": 0.6,
                "ctype": 'aggregate'
            }
        },
        "category": "cage",
        "iou_threshold": 0,
        "output_image": "anonymous.jpg"
    }
})


def evaluate_ppe(vgroup, vgc, egc, edgc, rd):
    try:
        bboxes = rd.output.bboxes
        ckeys = edgc.ckeys = list(vgc.conditions.keys())
        dkeys = edgc.dkeys = unique(*[ccfg.descriptors.keys() for ccfg in vgc.conditions.values()])
        subjects = []
        for key in vgc.primary.split('|'):
            sboxes = bboxes.get(key, [])
            if sboxes != None:
                subjects.extend(sboxes)

        num_subjects = len(subjects)
        if num_subjects == 0:
            return

        subjects = np.array(subjects)
        cflags = np.ones(shape=(num_subjects, len(ckeys)))
        dflags = np.zeros(shape=(num_subjects, len(dkeys)))

        for i, descriptor in enumerate(dkeys):
            descriptor_values = bboxes.get(descriptor,[])

            if descriptor_values is None or len(descriptor_values) == 0:
                continue
            elif not isinstance(descriptor_values, np.ndarray):
                descriptor_values = np.array(descriptor_values)

            if  num_subjects > 0 and len(descriptor_values) > 0:
                overlap = iou(subjects, descriptor_values)
                overlap = (overlap.max(axis=1) > vgc.get("iou_threshold", 0))
                dflags[:,i] = overlap

        annotation_names = []
        for i, (condition, ccfg) in enumerate(vgc.conditions.items()):
            annotation_names.append(ccfg.get('annotation', condition.replace("_", " ")))
            for descriptor, state in ccfg.descriptors.items():
                dindex = dkeys.index(descriptor)
                if state:
                    cflags[:,i] = np.logical_and(cflags[:, i], dflags[:,dindex])
                else:
                    cflags[:,i] = np.logical_and(cflags[:, i], np.logical_not(dflags[:,dindex]))

        egc.update(dict(**{name: int(cflags[:,i].sum()) for i, name in enumerate(ckeys)}))

        annotation_names = np.array(annotation_names)

        for bbox, flags in zip(subjects, cflags):
            label = f"{vgc.primary.split('|')[0]}"
            descriptor = False
            conditions_by_name = annotation_names[flags > 0]

            if len(conditions_by_name) > 0:
                label += f" - {', '.join(conditions_by_name)}"
                descriptor = True

            if descriptor:
                if label not in edgc.zones:
                    edgc.zones[label] = []

                edgc.zones[label].append(
                    Zone.create(
                        label,
                        bbox[:4],
                        atype="condition" if descriptor else "subject",
                        rtype=vgc.primary.split('|')[0]
                    )
                )
        action_boxes = [zn for zn in unique(*list(edgc.zones.values())) if isinstance(zn, Zone)]
        logger.info(f"activity boxes: {action_boxes}")
        rkeys = sorted([k for k, v in egc.items() if v])
        if len(rkeys):
            rd.output.filedata.append((
                f"{str(to_datetime(rd.collection_time).timestamp())}:{'_'.join(rkeys)}_ppe-violation.jpg",
                annotate(load_image(rd.image, color_space='rgb', crop=rd.input_crop), action_boxes, color_space='rgb')
            ))
        else:
            rd.output.filedata.append((
                f"{str(to_datetime(rd.collection_time).timestamp())}:ppe-violation.jpg",
                annotate(load_image(rd.image, color_space='rgb', crop=rd.input_crop), action_boxes, color_space='rgb')
            ))
    except Exception as err:
        msg = f"evaluate_violation_categories.evaluate_ppe exception {repr(err)}"
        logger.exception(msg)
        raise

def iou(bboxes1, bboxes2):
    """
        returns an array with all the iou values between bboxes1 and bboxes2
        bboxes1.shape = (3,4); bboxes2.shape = (4,4)
        return.shape = (3,4)
    """
    # [xmin, ymin, width, height]
    # [xmin, ymin, xmax, ymax]

    x11, y11, w1, h1 = np.split(bboxes1[:, :4], 4, axis=1)
    x12 = x11 + w1
    y12 = y11 + h1
    x21, y21, w2, h2 = np.split(bboxes2[:, :4], 4, axis=1)
    x22 = x21 + w2
    y22 = y21 + h2
    xA = np.maximum(x11, np.transpose(x21))
    yA = np.maximum(y11, np.transpose(y21))
    xB = np.minimum(x12, np.transpose(x22))
    yB = np.minimum(y12, np.transpose(y22))
    inter_area = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)
    boxa_area = (x12 - x11 + 1) * (y12 - y11 + 1)
    boxb_area = (x22 - x21 + 1) * (y22 - y21 + 1)
    iou = inter_area / (boxa_area + np.transpose(boxb_area) - inter_area)
    return iou

def evaluate_violation_categories(rd):
    if not rd:
        return

    e = rd.output.evaluations = AD()
    ed = rd.output.evaluation_data = AD()

    try:
        for vgroup, vgcfg in rd.output.violation.items():
            eg = e[vgroup] = AD()
            edg = ed[vgroup] = AD()

            activity_boxes = rd.regions.values()
            for category_name, cfg_category in vgcfg.categories.items():
                if category_name in category_defaults:
                    egc = eg[category_name] = AD()
                    edgc = edg[category_name] = AD()
                    edgc.zones = AD()
                    vgc = AD(category_defaults[category_name])
                    for k, v in cfg_category.deep_items():
                        vgc[k] = v
                        vgc.vgroup = vgroup
                    vgcfg.categories[category_name] = vgc

                    if 'ppe' in category_name:
                        evaluate_ppe(vgroup, vgc, egc, edgc, rd)
                        activity_boxes = unique(activity_boxes, [zn for zn in unique(*list(edgc.zones.values())) if isinstance(zn, Zone)])
                        continue
                    elif 'cage' in category_name:
                        if 'saferack_cage' in rd.output.bboxes and len(rd.output.bboxes.saferack_cage):
                            cage_violation(vgroup, vgc, egc, edgc, rd)
                        continue
                    else:
                        logger.error(f"evaluate_violation_categories error: category {category_name} not known")
                        continue
            save_annotations(rd.output, activity_boxes, rd.origins, 'rgb')
    except Exception as err:
        msg = f"evaluate_violation_categories exception for {category_name}: {repr(err)}"
        logger.exception(msg)
        raise

#!/usr/bin/env python3

from attribute_dict import AD
from image.utilities import *

# from object_recognition_api.object_detection import _tmp_recData_debug_str

from typing import List, Dict, Optional
from contextlib import contextmanager
import logging
import copy
import cv2
import numpy as np
import os
import re
import tensorflow as tf
import time
import asyncio
import json
from typing import Tuple, Dict, List, Optional, Any, Union
from atollogy.data.schemas.mv_results.mv_image_results import DetectedClass, DetectedObject, DetectedObjectGroup
from atollogy.data.schemas.mv_results.mv_inference_reqs import TFBbox

from mv_functions.social_distance import SocialDistanceATL, SocialDistanceCfg
from mv_functions.social_distance_v2 import Social_Distance_V2, Social_Distance_Cfg_V2
from mv_functions.conditions import evaluate_violation_categories


class DetectionInference():
    def __init__(self, mcfg=None):
        self.mcfg = mcfg
        self.model_name = self.mcfg.name
        self.sociald_v1 = SocialDistanceATL()

        self.logger = logging.getLogger("mvapi")

        self.load_tf_model()

        self.create_session()

        if self.load_success:
            self.createCategoryMap(self.model_pbtxt)

    def add_bbox(self, excluded, key, bbx):
        if key in excluded:
            excluded[key].append(bbx)
        else:
            excluded[key] = [bbx]

    def annotate_anonymize_imgs(self, rd, color_space_image, annotate_objects, anonymize_objects, results_bboxes):
        """
        Wrapper function to call annotate_bboxes and anonymize_bboxes
        :param rd: recdata
        :param color_space_image: Input image
        :param annotate_objects: classes to be annotated
        :param anonymize_objects: classes to be anonymized, person always anonymized
        :param results_bboxes: bounding boxes to be annotated/ anonymized
        :return:
        """

        self.annotate_boxes(rd, color_space_image.copy(), annotate_objects, results_bboxes)

        self.anonymize_boxes(rd, color_space_image.copy(), annotate_objects, anonymize_objects, results_bboxes)

    def annotate_boxes(self, rd, color_space_image, annotate_objects, results_bboxes):
        """
        Function to draw bboxes on images and anonymize objects
        :param rd: Attribute dictionary
        :param color_space_image: numpy image
        :param annotate_objects: list of classes to be annotated
        :param results_bboxes: dictionary with zones for bboxes
        :return
        """


        boxesToAnnotate = [
            zn
            for kls in annotate_objects
            if kls in results_bboxes
            for zn in results_bboxes[kls]
        ]

        if len(boxesToAnnotate) == 0:
            return

        elif rd.region_filter and len(rd.regions):
            # Add region bounding box
            boxesToAnnotate = list(rd.regions.values()) + boxesToAnnotate
        else:
            pass

        rd.output.filedata.append(('annotated.jpg', annotate(color_space_image.copy(), boxesToAnnotate, color_space='rgb')))
        save_annotations(rd.output, boxesToAnnotate, rd.origins, 'rgb')

    def anonymize_boxes(self, rd, color_space_image, annotate_objects, anonymize_objects, results_bboxes):
        """
        Function to draw bboxes on images and anonymize bboxes
        :param rd: Attribute dictionary
        :param color_space_image: numpy image
        :param annotate_objects: list of classes to be annotated
        :param anonymize_objects: list of classes to be anonymized
        :param results_bboxes: dictionary with zones for bboxes
        :return:
        """

        if "blur_boxes" in rd and rd.blur_boxes:
            anonymize_type = "blur_subject"
        else:
            anonymize_type = "anonymous_subject"

        boxesToAnnotateAndAnonymize = ([
                    Zone.create(zn.label if 'label' in zn else str(kls), zn[0:5], atype=anonymize_type, rtype=kls)
                    for kls in anonymize_objects
                    if kls in results_bboxes
                    for zn in results_bboxes[kls]
                ]
                + [
                    zn
                    for kls in annotate_objects
                    if kls not in anonymize_objects and kls in results_bboxes
                    for zn in results_bboxes[kls]
                ]
        )

        if rd.region_filter and len(rd.regions):
            # Add region bounding box
            boxesToAnnotateAndAnonymize = list(rd.regions.values()) + boxesToAnnotateAndAnonymize

        # if len(boxesToAnnotateAndAnonymize) > 0:
            # If on egde, write all images to S3
        rd.output.filedata.append(('anonymous.jpg',
                                    annotate(color_space_image.copy(), boxesToAnnotateAndAnonymize, color_space='rgb')))
        save_annotations(rd.output, boxesToAnnotateAndAnonymize, rd.origins, 'rgb')

    def createCategoryMap(self, pathToLabelFile):
        """
        Added by Aarti
        :param pathToLabelFile: Path to the .pbtxt file
        :return categories: list of dictionaries as [{'id':1, 'name' = 'person'},...]
        :return category_index: dictionary as {1:{'id':1, 'name' = 'person'},... }
        """

        with tf.gfile.GFile(pathToLabelFile, "r") as fid:
            label_map_string = fid.read()

        patt = re.compile(
            r'id: (\d+)[\s]*name: [\',"](\w+)[\',"]', re.DOTALL & re.IGNORECASE
        )
        g = patt.findall(label_map_string)

        self.__categories: List[Dict] = [{"id": int(x[0]), "name": x[1]} for x in g]

        self.__classList: List[str] = [
            x["name"] for x in self.__categories
        ]  # should be a list containing names of all classes
        # the model was trained for

        self.__category_index: Dict[int, Dict] = {}

        for cat in self.__categories:
            self.__category_index[cat["id"]] = cat

    def create_session(self):
        """
        Function to create a session object.
        :return:
        """

        config = tf.ConfigProto()
        config.allow_soft_placement = True
        # config.gpu_options.per_process_gpu_memory_fraction = (1.0 - 0.05 * self.model_cnt) / self.model_cnt
        # reserve an additional 5% per api
        config.gpu_options.allow_growth = True

        self.session = tf.Session(config=config, graph=self.detection_graph)

    def cast_pixel_locs_to_int(self, detection):
        """
        detection pixel locations are floats. Convert these to ints so they can be
        used for indexing

        Parameters
        ----------
        detection : [x1, y1, x2, y2, percent_confident]
        """
        d = detection.tolist()
        int_dect = [int(x) for x in d[:4]] + d[4]
        return  int_dect

    async def detect(self, recData):
        """
        :param recData:
        # Input Config parameters
        -------------------------------------------
        {
          "stepCfgs": {
              "gate_CB": {
                "color_space": "rgb",              # Color space of input image to mvapi
                "function": "ground_operations",   # Model used for inference
                "input_crop": null,                # used to crop input
                "input_image_indexes": [
                  0
                ],
                "input_step": "mvedge",
                "region_filter": true,    # area of interest
                "regions": {              # [x, y, w, h] coordinates of region. All bounding boxes which overlap with this area(by atleast a threshold) are annotated/considered
                  "pier_1": [             # The label of region_filter, Can specify multiple region of interest
                    320,
                    220,
                    1280,
                    640
                  ]
                },
                "annotate_after_filter": True       # If trUe, apply ROI & overlap filter and then annotate images with remaining bboxes.
                "subjects": {
                  "active": {
                    "confidence_thresh": 0.6,       # All detected boxes which overlap with region filter and have atleast this confidence of detection
                    "nms_threshold": 0.4,           # Used to remove multiple bounding boxes of same class that overlap greater than this threshold(Non Maximal Suppression)
                    "roi_ratio": {                  # Retain boxes which satisfy detected_box_area/region_filter_area > roi ratio of class. Removes small boxes of a particular class. Most likely background detections
                      "airplane": 0.2,
                      "ConveyorBelt": 0.02
                    },
                    "blur_boxes": [5, 5] or "blur_boxes": True,   # blur detected bboxes, higher number blur object more.
                    "overlap": {
                    "airplane":
                      {
                        "ConveyorBelt": null,      # Retain bounding boxes for "Conveyor belt" only if it overlaps with "airplane" bounding box. Uses image.utilities/Zone.overlaps to determine overlapa
                        "FuelTruck": null
                      }
                    },
                    "group_class": {               # Post group different class results into one represent class, bboxes and subjects number will combine into the represent class. function name is "post_group_class".
                        "truck": [
                            "truck",
                            "car",
                            "train"
                            ]
                        },
                    "social_distance": {
                        "target_objects": ["person", "operator"],
                        "perspective_transform": {
                            "origin_points" : {
                                "top_left": [x1, y1],
                                "top_right": [x2, y2],
                                "bottom_left": [x3, y3],
                                "bottom_right": [x4, y4]
                            }
                            "transform_points":{  #if "transform_points" is None or empty, then automatically calculate transform_points coordinates
                                'width': interger
                                'height': interger
                            }
                        },
                        "scale_translation": 250"
                    },
                    "states":
                      "all": [              #  To be used when a model is trained for multiple classes and need to specify a common config for all classes
                        [
                          "bbox",
                          null
                        ],
                        true,               # Interest objects => Interested in all classes if True
                        true,               # Annotate objects => Annotate all classes  if True
                        false               # Anonymize objects => Anonymize all classes if True. NOTE: "person" class is always anonymized by default to protect privacy. "person" boxes which are inside region_filter are labelled as "operator"
                      ],
                      "ConveyorBelt": [     # Conditions for a specific class in model. Make sure this name in same as that of the label.pbtxt file of the model
                        [
                          "exclude",        # Exclude certain detections of this class if they contain the next set of points
                          [821, 342],       # [x1, y1] coordinate. If multiple points given, OR condition is used to determine if box to be retained
                          [100, 200]
                        ],
                        [
                          "bbox",
                          null
                        ],
                        true,
                        true,
                        false
                      ]
                    }
                  }
                },
                "brightness": {
                  "alert": 3,               # number of out-of-bounds images before triggering alert
                  "high_threshold": 190,    # above this value the image is too bright
                  "low_threshold": 8,       # below this value the image is too dark
                  "normalize": false,       # based on history, use cv2 to adjust brightness @ normal (tb implemented)
                  "track": false,           # track brightness in history
                  "track_history_length": 2
                },
                "version": "v01"            # step setup
              }
          },
          "steps": [
            "gate_CB"
          ]
        }
        :return:
        a response in the following shape is sent back to the atlapi mvresults endpoint
        Returns: TO Database
        ----------------------
        {
          "bboxes": {           # Contains bounding box coordinates for all classes. Format : [top_left_x, top_left_y, w, h, confidence_score]
            "person": [],       # If model can detect "person" and region_filter specified, then "person" -> all people bboxes outside region_filter; "operator" -> all people bboxes inside/overlapping with region_filter
            "CargoDoor": [],
            "TugBarWheel": [],
            "PassengerDoor": [],
            "PalletLoader": [],
            "truck": [],
            "CateringTruck": [
              [
                883,
                337,
                147,
                83,
                0.9907569289207458
              ]
            ],
            "ParkWheel": [],
            "EmergencyVehicle": [],
            "PushbackTug": [],
            "Bus": [],
            "TugConnected": [],
            "LuggageTrailer": [],
            "airplane": [],
            "FuelTruck": [],
            "ConveyorBelt": [],
            "JetBridgeHood": [],
            "Stairs": [
              [
                1331,
                414,
                107,
                154,
                0.99869304895401
              ]
            ],
            "operator": []
          },
          "object_centers": [[x1,y1], [x2,y2]. [...]...],
          "reason": null,
          "status": "success",
          "subjects": {               # per class count of bboxes
            "person": 0,              # "person" count is for number of "operators"
            "CargoDoor": 0,
            "TugBarWheel": 0,
            "PassengerDoor": 0,
            "PalletLoader": 0,
            "truck": 0,
            "CateringTruck": 1,
            "ParkWheel": 0,
            "EmergencyVehicle": 0,
            "PushbackTug": 0,
            "Bus": 0,
            "TugConnected": 0,
            "LuggageTrailer": 0,
            "airplane": 0,
            "FuelTruck": 0,
            "ConveyorBelt": 0,
            "JetBridgeHood": 0,
            "Stairs": 1,
            "non-operator": 0        # "non-operator" count is for number of "persons"
          },
          "excluded": {
            "airplane": [           # bboxes NOT considered when reporting counts. These are bounding boxes excluded when failing to satisfy various filter conditions
              [
                1272,
                266,
                395,
                196,
                0.7794846296310425
              ]
            ]
          },
          "inference_time": 0.4299999999348074,         # Total time in secs for inference
          "total_mvapi_time": 1.1040000000502914        # Total time in secs for inference + post-processing
        }

        """

        event_loop = asyncio.get_event_loop()
        start = event_loop.time()

        rd = recData
        if 'debug' not in rd:
            rd.debug = False
        #self.logger.info("Input recData received:{}".format(rd))
        rd.images = []
        rd.output = AD(
            {
                "filedata": [],
                "bboxes": {},
                "reason": None,
                "status": "success",
                "subjects": {},
                "excluded": {},
                "model_name": self.model_name
            }
        )
        violation_is_configured = "violation" in rd and isinstance(rd.violation, (AD, dict))
        if violation_is_configured:
            rd.output.violation = rd.violation
        results = rd.output.subjects

        try:
            color_space_image = load_image(rd.image, color_space='rgb', crop=rd.input_crop)

            # Add brightness and resolution values to output
            check_image(color_space_image, rd, rd.output)

            if rd.social_enabled > 1:
                self.sdv2 = Social_Distance_V2(rd.nodename, rd.sd_config, color_space_image.copy())
                sd_output  = self.sdv2.apply_transformations()
                if isinstance(rd.sd_config, Social_Distance_Cfg_V2):
                    rd.sd_config = AD(rd.sd_config.dict())

                rd.output.sd_params = sd_output[0]
                rd.output.filedata.append(('undistorted.jpg', sd_output[1]))
                rd.output.filedata.append(('undistorted_with_trapezoid.jpg', sd_output[2]))

                if rd.sd_config.use_in_detection == 'undistorted':
                    color_space_image = sd_output[1].copy()
                elif rd.sd_config.use_in_detection == 'undistorted_with_trapezoid':
                    color_space_image = sd_output[2].copy()

            # Determine interest objects
            interest_objects, annotate_objects, anonymize_objects, interest_objListDict = \
                self.determine_interest_objects(rd.states)

            # Get coordinates of exclusion points
            excluded = self.get_excluded_pt_coord(rd.states)

            # TODO: to switch to DEBUG level later
            # if self.logger.isEnabledFor(logging.INFO):
            #     self.logger.info(
            #         "detect(): recData={}\n  interest_objects={}\n  interest_objListDict={}".format(
            #             _tmp_recData_debug_str(recData),
            #             interest_objects,
            #             interest_objListDict,
            #         )
            #     )
            # We are expecting the recData contains "customer_id", "gateway_id" and "nodename",
            # as depicted in "return_rest_mv_results" function

            detectedBoxes, active_learning_data = await self.detect_objects_newTF(
                color_space_image,
                interest_objListDict,
                conf_thresh=rd.confidence_thresh,
                nms_thresh=rd.nms_threshold,
            )

            # Copy over AL data before modifications to detectedBoxes
            # TODO; Handle video frames
            try:
                active_learning_data['origin'] = rd.origins[0]
                rd.output['active_learning'] = active_learning_data
            except Exception as err:
                logger.warning(f'detect failed to generate active learning output with {err}')

            self.logger.info(
                "Detected using new Tensorflow Model: {}".format(self.model)
            )

            rd.output.inference_time = event_loop.time() - start
            self.logger.info(
                "detected_boxes after inference: {} - infer time: {}".format(detectedBoxes, rd.output.inference_time)
            )

            if rd.social_enabled > 1:
                sdv2_annotated = self.sdv2.filter_and_calculate_bboxes(rd=rd, bboxes=detectedBoxes, current=color_space_image)
                try:
                    rd.output.filedata.append(('trapezoid_with_distances.jpg', sdv2_annotated))
                except Exception:
                    pass

            # self.logger.info('conf:{}, nms:{}'.format(rd.confidence_thresh,rd.nms_threshold))
            # The following lines of code sort all the detections in descending order by score and then choose
            # "max_to_keep" among them. This may result in some people not being annotated and anonymized if
            # some person detection score is not in first "max_to_keep" number of detections
            if "max_to_keep" in rd:
                detectedBoxes = self.filter_with_max_to_keep(
                    int(rd.max_to_keep), detectedBoxes, interest_objects)

            ############ run regions on the loop ############
            if rd.region_filter and len(rd.regions) > 1:
                annotation_bbox = {}
                ### if rd.regions exist more than one, it will be iterated by rd.regions loop
                for regions_label, zone_filter in rd.regions.items():
                    inference_box = AD(detectedBoxes)

                    rd.output[regions_label].bboxes, rd.output[regions_label].subjects, rd.output[regions_label].excluded = self.region_output(
                                                                region_list = zone_filter,
                                                                inference_result = inference_box,
                                                                excluded = excluded,
                                                                return_bboxes = AD(),
                                                                subject_name = AD(),
                                                                interest_objects = interest_objects
                                                            )

                    ### Post process for annotation image, it will combine each regions bboxes to one bbox
                    for key, value in rd.output[regions_label].bboxes.items():
                        if key not in annotation_bbox:
                            annotation_bbox[key] = value[:]
                        else:
                            for list_value in value:
                                if list_value not in annotation_bbox[key]:
                                    annotation_bbox[key].append(list_value)

                for operator_bbox in annotation_bbox["operator"]:
                    if operator_bbox in annotation_bbox["person"]:
                        annotation_bbox["person"].remove(operator_bbox)

                ### End post process for annotation image
                rd.output.bboxes = annotation_bbox

                object_count = {}
                for key, value in annotation_bbox.items():
                    if key == 'person':
                        object_count['non-operator'] = len(value)
                    elif key == 'operator':
                        object_count['person'] = len(value)
                    else:
                        object_count[key] = len(value)

                rd.output.subjects = object_count
            else:
                if rd.region_filter and len(rd.regions):
                    zone_filter = rd.regions[
                        rd.regions.keys()[0]
                    ]  # Zone class object created in prep config
                else:
                    zone_filter = []

                rd.output.bboxes, rd.output.subjects, rd.output.excluded = self.region_output(
                    region_list = zone_filter,
                    inference_result = detectedBoxes,
                    excluded = excluded,
                    return_bboxes = rd.output.bboxes,
                    subject_name = results,
                    interest_objects = interest_objects
                    )

            ################## social_distance logic #######################

            if rd.social_enabled == 1:
                centroids = self.sociald_v1.get_centerpoint(rd.output.bboxes, rd.sd_config.target_objects)
                if "perspective_transform" in rd.sd_config and rd.sd_config.perspective_transform:
                    if len(centroids) > 0:
                        rd.output.object_centers = self.sociald_v1.run_ppt(rd.sd_config.perspective_transform, centroids)
            if isinstance(rd.sd_config, (SocialDistanceCfg, Social_Distance_Cfg_V2)):
                rd.sd_config = AD(rd.sd_config.dict())

            ## ---------------- SHOW ROI THRESHOLD IN ANNOTATION - ONLY FOR DEBUG ---------------- ##
            # for key, entry in rd.output.bboxes.items():
            #     for zn in entry:
            #         zn.label = zn.label + "_roi={}".format(zn.area/zone_filter.area)
            ## ----------------------------------------------------------------------------------- ##

            if "group_class" in rd and rd.group_class:
                ### parameter example) group_class = {"truck": ["truck", "car", "train"]}
                ### post group different class results into one represent class
                self.post_group_class(rd.group_class, rd.output, "bboxes", [], save_grouped=True)
                self.post_group_class(rd.group_class, rd.output, "subjects", 0)


            ####################### Apply filters here ######################

            # ROI filter Bounding box area/ Region_filter area thresholding
            if "roi_ratio" in rd and rd.region_filter:
                rd.output.bboxes, rd.output.subjects, rd.output.excluded = self.process_roi_ratio(zone_filter,
                                                                                                  rd.output.bboxes,
                                                                                                  rd.output.subjects,
                                                                                                  rd.output.excluded,
                                                                                                  rd.roi_ratio)

            if "overlap" in rd and rd.overlap:
                rd.output.bboxes, rd.output.subjects, rd.output.excluded = self.parents_overlap_filter(rd.overlap, rd.output.bboxes, rd.output.subjects, rd.output.excluded)

            if "multiclass" in rd and rd.multiclass:
                rd.output.bboxes, rd.output.subjects, rd.output.excluded = self.multiclass_filter(rd.output.bboxes, rd.output.subjects, rd.output.excluded, rd.confidence_thresh, iouThreshold=rd.nms_threshold)

            if "size_filter" in rd and rd.size_filter:
                rd.output.bboxes, rd.output.subjects, rd.output.excluded = self.size_filter(
                    rd.output.bboxes,
                    rd.output.subjects,
                    rd.output.excluded,
                    min_width=rd.size_filter.get('min_width', 0),
                    max_width=rd.size_filter.get('max_width', 1920),
                    subjects=rd.size_filter.get('subjects', ["person", "operator"])
                )

            #################################################################
            self.annotate_anonymize_imgs(rd, color_space_image, annotate_objects, anonymize_objects, rd.output.bboxes)

            ######################### Violation Logic ########################
            try:
                if violation_is_configured and 'operator' in rd.output.bboxes and len(rd.output.bboxes.operator):
                    evaluate_violation_categories(rd)
            except Exception as err:
                logger.exception(f"DetectionInference.detect violation exception: {repr(err)}")
            #################################################################

            rd.output.total_mvapi_time = event_loop.time() - start

            self.logger.info(
                "results_bboxes: {}, results: {} - mvapi time: {}".format(rd.output.bboxes, results, rd.output.total_mvapi_time)
            )

            if 'image' in rd:
                del rd.image
            return rd

        except Exception as err:

            # Close session object
            self.session.close()

            self.logger.exception(
                "****************** Cannot do Object detection, node_name:{} - step_name:{} - model_name:{} Error:{}".format(
                    rd.nodename, rd.function, self.model, repr(err)
                )
            )

    async def detect_objects_newTF( self, color_space_image, interest_objListDict, conf_thresh=0.6, nms_thresh=0.4):
        """
        Added  by Aarti
         API in the same form as object_recognition.detect_objects()
         :param self.session: Loaded TensorFlow session
         :param self.detection_graph: Loaded Graph
         :param color_space_image: Loaded Image
         :param interest_objListDict: List of dictionary of objects we are interested in tracking. Ex: [{'id':1,'name':'person'}, ...]
         :param conf_thresh: Confidence threshold to filter out detections
         :param nms_thresh: Non Maximal Supression threshold to eliminate multiple detections and bounding boxes of same object
         :return: bbxes: bounding boxes and classes detected. Ex: [{'dog':[[xmin, ymin, width, height, score]]}, {}, ...].
         The detection boxes are list of lists and the detection coordinates are in int datatype to support plotting on image
         """

        with self.detection_graph.as_default():
            outputDict = AD(self.run_inference_for_single_image(color_space_image))

        # Filtering out only interest objects from all predictions
        interest_objsID = [x["id"] for x in interest_objListDict]  # ID number of class
        interest_objName = [x["name"] for x in interest_objListDict]  # Name of class

        # Active Learning w/ Least Confidence Uncertainty Sampling
        active_learning_data = None
        try:
            scores = outputDict["detection_scores"][:outputDict["num_detections"]]
            scores = np.stack([scores, 1-scores], axis=1)
            max_prob = scores.max(axis=1)
            min_prob = scores.min(axis=1)
            boxwise_uncertainty_scores = (1 - (max_prob - min_prob))**2

            active_learning_data = {
                "model_name": self.model_name,
                "model_version": self.mcfg.model_version,
                "boxwise_least_confidence_uncertainty": {
                    "max": float(boxwise_uncertainty_scores.max()),
                    "avg": float(boxwise_uncertainty_scores.mean()),
                    "sum": float(boxwise_uncertainty_scores.sum())
                },
                "origin": None # Will be filled in later
            }
        except Exception as err:
            logger.warning(f'detect_objects_newTF failed to generate active learning data with {err}')


        idx = np.isin(outputDict["detection_classes"], interest_objsID)
        outputDict["detection_scores"] = outputDict["detection_scores"][idx]
        outputDict["detection_boxes"] = outputDict["detection_boxes"][idx]
        outputDict["detection_classes"] = outputDict["detection_classes"][idx]

        height, width, _ = np.shape(color_space_image)

        # Populate bbxes
        bbxes = AD()

        for idClass in interest_objListDict:

            bbxes[idClass["name"]] = outputDict["detection_boxes"][
                np.where(outputDict["detection_classes"] == idClass["id"])
            ]  # Detection boxes are generated as [ymin, xmin, ymax, xmax]

            if bbxes[idClass["name"]].size == 0:
                bbxes[idClass["name"]] = []

            else:
                # Unnormalizing Bounding boxes
                bbxes[idClass["name"]] = np.dot(
                    bbxes[idClass["name"]],
                    np.diag(np.array([height, width, height, width])),
                )

                # Arranging in correct order . Detection boxes are generated as [ymin, xmin, ymax, xmax]
                bbxes[idClass["name"]] = bbxes[idClass["name"]][
                    :, (1, 0, 3, 2)
                ]  # Rearranging as [xmin, ymin, xmax, ymax]

                # Appending scores
                scores = outputDict["detection_scores"][
                    np.where(outputDict["detection_classes"] == idClass["id"])
                ]
                tmp_boxes = np.column_stack((bbxes[idClass["name"]], scores))

                dets, _ = self.filtered_nms(
                    tmp_boxes, conf_thresh, nms_thresh
                )  # Non Maximal Suppression per class.
                # dets -> list of ndarrays holding bounding box coordinates and score

                # sorted results converted to Zone's (highest score to lowest
                bbxes[idClass["name"]] = sorted(
                    [Zone.create(idClass["name"], d[:4], threshold=d[4], notwh=True, ptype=int, rtype=idClass["name"]) for d in dets],
                    key=lambda b: b.threshold,
                    reverse=True
                )
                #
                # for det in dets:
                #     #  Converting to [xmin, ymin, width, height] format since
                #     # image.utilities.annotate expects [xmin, ymin, width_box, height_box] format
                #
                #     det[2] = det[2] - det[0]  # Width of bbox
                #     det[3] = det[3] - det[1]  # Height of box
                #     det = self.cast_pixel_locs_to_int(det)
                #     if idClass["name"] in bbxes:
                #         bbxes[idClass["name"]].append(det)
                #     else:
                #         bbxes[idClass["name"]] = [det]

        # self.logger.info("new bbxes:{}".format(bbxes))

        return bbxes, active_learning_data

    def determine_interest_objects(self, states):
        """
        Function to determine classes we are interested in to detect, annotate and anonymize
        :param states: dictionary rd.states from consul config
        :return:
        """

        # filter possible targets to detect using:
        # <model_state>: [[possible values],  <THIS_FIELD_TRUE>, <annotate_bool>, <anonymize_bool>]
        interest_objects = [k for k, v in states.items() if v[-3]]

        # <model_state>: [[possible values], <interest_bool>, <THIS_FIELD_TRUE>, <anonymize_bool>]
        annotate_objects = [k for k, v in states.items() if v[-2]]

        # <model_state>: [[possible values], <interest_bool>, <annotate_bool>, <THIS_FIELD_TRUE>]
        anonymize_objects = [k for k, v in states.items() if v[-1]]

        # All people in images should be anonymized irrespective of if they are in interest_objects or not
        # Adding them to annotate objects for reference and comparison
        # Add 'person' object to annotate and anonymize if not already present

        if len(interest_objects) and interest_objects[0] == "all":

            dnt_annotate = set(interest_objects) - set(annotate_objects)
            dnt_anonymize = set(interest_objects) - set(anonymize_objects)

            interest_objects = self.__classList[:]  # all classes when "all"

            if len(annotate_objects) and annotate_objects[0] == 'all':
                annotate_objects = self.__classList[:]
                for x in dnt_annotate:
                    if x in annotate_objects:
                        annotate_objects.remove(x)

            if len(anonymize_objects) and anonymize_objects[0] == 'all':  # This will be empty from line 764 if consul parameter set to false
                anonymize_objects = self.__classList[:]
                for x in dnt_anonymize:
                    if x in anonymize_objects:
                        anonymize_objects.remove(x)

        # inside region person labled as a "person", outside region person labled as a "operator"
        if 'person' in annotate_objects:
            annotate_objects = list(set(annotate_objects + ["person", "operator"]))
        if 'person' in anonymize_objects:
            anonymize_objects = list(
                set(anonymize_objects + ["person", "operator"])
            )

        # Finding class id number of interest objects
        # If interest_objects == ['all'], then detect all classes

        if len(interest_objects) and interest_objects[0] != "all":
            interest_objListDict = list(
                filter(lambda x: x["name"] in interest_objects, self.__categories)
            )  # [{'id':1,'name':'person'}, ...]
        else:
            interest_objListDict = self.__categories

        # self.logger.info('new interest_obj:{}'.format(interest_objects))
        # self.logger.info('new anotat_obj:{}'.format(annotate_objects))
        # self.logger.info('new anon_obj:{}'.format(anonymize_objects))
        # self.logger.info('new interest objects:{}.{}'.format(interest_objListDict, self.model))

        return interest_objects, annotate_objects, anonymize_objects, interest_objListDict

    def filtered_nms(self, dets, conf_thresh=0.6, nms_thresh=0.4):
        """NMS with confidence threshold filtering"""

        # self.logger.info("dets before: {}".format(dets))
        if dets.shape[0] == 0:
            return []
        if isinstance(conf_thresh, AD) or not isinstance(conf_thresh, float):
            conf_thresh = 0.6
        if isinstance(nms_thresh, AD) or not isinstance(nms_thresh, float):
            nms_thresh = 0.4

        x1 = dets[:, 0]
        y1 = dets[:, 1]
        x2 = dets[:, 2]
        y2 = dets[:, 3]
        scores = dets[:, 4]

        areas = (x2 - x1 + 1) * (y2 - y1 + 1)
        order = scores.argsort()[::-1]

        keep = []
        discard = []
        while order.size > 0:
            i = order[0]
            keep.append(i)
            xx1 = np.maximum(x1[i], x1[order[1:]])
            yy1 = np.maximum(y1[i], y1[order[1:]])
            xx2 = np.minimum(x2[i], x2[order[1:]])
            yy2 = np.minimum(y2[i], y2[order[1:]])

            w = np.maximum(0.0, xx2 - xx1 + 1)
            h = np.maximum(0.0, yy2 - yy1 + 1)
            inter = w * h
            ovr = inter / (areas[i] + areas[order[1:]] - inter)

            inds = np.where(ovr <= nms_thresh)[0]
            ind_discard = np.where(ovr > nms_thresh)[0]
            discard += order[ind_discard + 1].tolist()

            order = order[inds + 1]

        excluded_dets = dets[discard, :]
        # self.logger.info("dets after:{}".format(dets[keep,:]))

        dets = [d for d in dets[keep, :] if d[4] > conf_thresh]

        # self.logger.info("excluded dets:{}".format(excluded_dets))

        return dets, excluded_dets

    def filter_with_max_to_keep(self, max_to_keep, detectedBoxes, interest_objects):
        """
        The following lines of code sort all the detections in descending order by score and then choose
        "max_to_keep" among them. This may result in some people not being annotated and anonymized if
        some person detection score is not in first "max_to_keep" number of detections

        :param max_to_keep: int value indicating maximum number of boxes to be retained
        :param detectedBoxes: bboxes from inference
        :param interest_objects: objects we are interested in
        :return: filtered detected boxes
        """

        if len(detectedBoxes) != 0:  # Only if there are bboxes detected
            filt_bbxes = {}
            temp_list = []

            # Converting to list of lists [[xmin, ymin, w, h, score, class]]
            for key, val in detectedBoxes.items():
                [x.append(key) for x in val]
                temp_list.extend(
                    val
                )  # temp_list = [[xmin, ymin, w, h, score, class]]

            temp_list = sorted(
                temp_list, key=lambda x: x[4], reverse=True
            )  # index 4 is score.
            # Sorting according to score

            if len(temp_list) >= max_to_keep:
                temp_list = temp_list[:max_to_keep]

            # Converting back to bbxes format
            for val in temp_list:
                if val[5] in filt_bbxes.keys():  # val[5] -> class
                    filt_bbxes[val[5]].append(val[:5])
                else:
                    filt_bbxes[val[5]] = [val[:5]]

            # Adding remaining key values in interest objects so that format of results is consistent with
            # {"class1":[], "class2":[[xmin, ymin, w, h, score], [xmin, ymin, w, h, score]]}
            for objClass in interest_objects:
                if objClass not in filt_bbxes.keys():
                    filt_bbxes[objClass] = []

            #################################################################
            # # Uncomment if needed. Needs testing. Added by Aarti
            # # The following lines of code within '####' does the following
            # # Keep all people detected. But choose 'max_to_keep' number of detections from remaining classes according to
            # # the highest scores. So total detections retained = no of people + 'max_to_keep' detections from other classes
            #
            #                 if len(detectedBoxes) != 0:  # Only if there are bboxes detected
            #                     filt_bbxes = {}
            #                     temp_list = []
            #                     people_list = []
            #                     total_list = []
            #
            #                     for key, val in detectedBoxes.items():
            #                         [x.append(key) for x in val]
            #                         temp_list.extend(val)  # temp_list = [[xmin, ymin, w, h, score, class]]
            #
            #                     if 'person' in self.__classList:
            #                         people_list = list(filter(lambda x: x[5] == 'person', temp_list))
            #                         temp_list = list(filter(lambda x: x[5] != 'person', temp_list))
            #
            #                     temp_list = sorted(temp_list, key=lambda x: x[4], reverse=True)  # index 4 is score.
            #                     # Sorting according to score
            #
            #                     if len(temp_list) >= max_to_keep:
            #                         temp_list = temp_list[:max_to_keep]
            #
            #                     total_list = temp_list + people_list
            #
            #                     # Converting back to bbxes format
            #                     for val in temp_list:
            #                         if val[5] in filt_bbxes.keys():  # val[5] -> class
            #                             filt_bbxes[val[5]].append(val[:5])
            #                         else:
            #                             filt_bbxes[val[5]] = [val[:5]]
            #
            #                     # Adding remaining key values in interest objects so that format of results is consistent with
            #                     # {"class1":[], "class2":[[xmin, ymin, w, h, score], [xmin, ymin, w, h, score]]}
            #                     for objClass in interest_objects:
            #                         if objClass not in filt_bbxes.keys():
            #                             filt_bbxes[objClass] = []
            #
            #                     return filt_bbxes
            #################################################################
            return filt_bbxes

    def get_excluded_pt_coord(self, states):
        """
        Function to get exclusion point coordinates from consul config. If a bounding box includes any of
        these points, it is ignored during annotation
        :param states: rd.states from consul config
        :return: list of lists containing coordinates of exclusion points
        """

        excluded = AD()
        for key, val in states.items():
            if len(val) > 4:
                for entry in val[:-4]:
                    if isinstance(entry, list) and len(entry) > 1 and entry[0] == 'exclude':
                        if key in excluded:
                            excluded[key] = excluded[key] + entry[1:]
                        else:
                            excluded[key] = entry[1:]

        # copy exclusion points for person
        if "person" in excluded:
            excluded["operator"] = excluded["person"]

        return excluded

    def load_tf_model(self):
        """
        Added by Aarti : 11th Sept 2018
        Function to load saved frozen (friends)object recognition model.
        This model been trained using TF 1.8 and the new API released by Tensorflow

         Parameters
        ----------
        self.model_cnt : the number of apis which will be run on this device. This is used
                 for saving space on the GPU for the rest of APIs. Default value is 1


        :return:
        """
        self.model = self.mcfg.name

        try:

            self.model_lookup_path()
            # Edit this to give access to new Frozen graph .pb file in S3 ??
            self.logger.warning(
                "load_tf_model: Loading {}".format(
                     self.model_name
                )
            )

            tf.reset_default_graph()
            self.detection_graph = tf.Graph()
            with self.detection_graph.as_default():
                od_graph_def = tf.GraphDef()
                with tf.gfile.GFile(
                    self.model_file, "rb"
                ) as fid:  # Needs path to frozen .pb file
                    serialized_graph = fid.read()
                    od_graph_def.ParseFromString(serialized_graph)
                    tf.import_graph_def(od_graph_def, name="")

            self.load_success = True

            self.logger.info(
                "load_tf_model: Loaded new Tensorflow Model: {}".format(
                    self.model_file
                )
            )
            # self.logger.info(
            #     "load_tf_model: str(detection_graph)={} id=(detection_graph)".format(
            #         str(self.detection_graph), id(self.detection_graph)
            #     )
            # )

        except Exception as e:
            self.load_success = False
            self.logger.exception(
                "******************* load_tf_model: CANNOT LOAD MODEL NAME={} *******************".format(
                    self.model_name
                )
            )
            self.logger.warning(str(e))

    def model_lookup_path(self):
        """
        Get the locations of .pb and .pbtxt files
        :param lookup_path: /data/mvapi_data/output/faster_rcnn_resnet101/friends_train/production_model
        :param model_version: 0
        :return:
        """
        version_lookup_path = os.path.join(self.mcfg.lookup_path, str(self.mcfg.model_version))
        try:
            for file_name in os.listdir(version_lookup_path):
                if file_name.endswith(".pb"):
                    self.model_file = os.path.join(version_lookup_path, file_name)
                    # self.logger.info("new model_file:{}".format(self.model_file))
                    # e.g.: self.model_file:
                    # /data/mvapi_data/output/faster_rcnn_resnet101/friends_train/production_model/0/frozen_inference_graph
                    # 85144.pb
                if file_name.endswith(".pbtxt"):
                    self.model_pbtxt = os.path.join(version_lookup_path, file_name)
                    # self.logger.info("new model_pbtxt: {}".format(self.model_pbtxt))
                    # e.g.: self.model_pbtxt: /data/mvapi_data/output/faster_rcnn_resnet101/
                    # friends_train/production_model/0/friends_label_map.pbtxt"
        except:
            self.logger.exception(
                "model_lookup_path: Cannot find model_file={} ".format(version_lookup_path))

    def multiclass_filter(self, bboxes, subjects, excluded, conf_thresh, iouThreshold=0.75):
        """
        Apply nms filtering along multiple classes

        :param bboxes: AD of class and bboxes
        :param subjects: count of bboxes per class
        :param excluded: AD of class and bboxes excluded from annotating
        :param conf_thresh: Confidence threshold to filter out detections
        :param iouThreshold: overlap Threshold
        :return:

        """

        num_bboxes = sum([len(x) for x in bboxes.values()])

        if num_bboxes:
            # process only if there are bounding boxes

            # Creating dictionary {"person": 1, "airplane": 2....}
            cat_n_id = {x["name"]: x["id"] for x in self.__categories}
            cat_id_n = {v:k for k, v in cat_n_id.items()}

            # Add operator class to dictionary if model can detect "person"
            if "person" in cat_n_id:
                cat_n_id["operator"] = max(cat_n_id.values()) + 1

            # Concatenate all bounding boxes across all classes. Add class id and bbox index to this list
            dets = []
            cat = []
            idx = []

            for key, val in bboxes.items():
                dets += val
                cat += [cat_n_id[key]] * len(val)
                idx += list(range(len(val)))


            # Append class if and index info to retrace the results
            dets = np.concatenate((np.array(dets), np.array(cat)[:, np.newaxis], np.array(idx)[:, np.newaxis]), axis=1)
            dets[:, 2] += dets[:, 0]  # x2
            dets[:, 3] += dets[:, 1]  # y2
            bboxes_out, excluded_bbxes = self.filtered_nms(dets, conf_thresh=conf_thresh, nms_thresh=iouThreshold)

            # Get only the class id and index of excluded bbxes
            excluded_id  = excluded_bbxes[:,-2:].astype(np.int32).tolist()

            excluded_id_dict = {}

            # group indices according to class with dictionary
            for entry in excluded_id:
                excluded_id_dict[cat_id_n[entry[0]]] = excluded_id_dict.get(cat_id_n[entry[0]], []) + [entry[1]]

            # Sort indices for each class in descending order, for popping not to disturb index of other elements
            [x.sort(reverse=True) for x in excluded_id_dict.values()]

            # Go over each item in excluded_id_dict, pop that from bboxes and add to excluded. Also update count
            for key, idlist in excluded_id_dict.items():
                for i in idlist:
                    val = bboxes[key].pop(i)
                    subjects[key] -= 1
                    if key in excluded:
                        excluded[key].append(val)
                    else:
                        excluded[key] = [val]

        return bboxes, subjects, excluded


    def size_filter(self, bboxes, output_subjects, excluded, min_width, max_width, subjects):
        for subject in subjects:
            if not subject in bboxes:
                continue

            bboxes[subject] = [bbox for bbox in bboxes[subject] if min_width <= bbox.w <= max_width]
            # TODO: update counts + excluded properly

        return bboxes, output_subjects, excluded

    def parents_overlap_filter(self, overlap_config, bboxes, subjects, excluded_bbxes):
        """
        Function to determine if child classes overlap with parent class
        :param overlap_config: child parent hierarchy
        :param bboxes: Detected bounding boxes
        :param subjects: rd.subjects in config
        :param excluded_bbxes: The excluded bounding boxes so far
        :return:
        """

        threshold_value = 0.5

        for parent_type, child_types in overlap_config.items():
            if "person" in child_types and not "operator" in child_types:
                child_types["operator"] = child_types["person"]

            for ctype in child_types:
                children = bboxes[ctype]
                for kid in children:
                    in_parent = False
                    for parent in bboxes[parent_type]:
                        if parent.contains(kid, threshold=threshold_value):
                            in_parent = True
                            break
                    if not in_parent:
                        self.add_bbox(excluded_bbxes, ctype, kid)
                        bboxes[ctype].pop(bboxes[ctype].index(kid))

        self.update_subject_count(subjects, bboxes)

        return bboxes, subjects, excluded_bbxes

    def post_group_class(
        self,
        group_class: Dict[str, any],
        recData_output: AD,
        data_key: str,
        default_value: Any,
        save_grouped=False,
    ) -> None:
        """
        group different classes into a representative class
        if an example of input like that: group_class = {"truck": ["truck", "car", "train"],
                                                        "representative_class": ["class_list_1","class_list_2",...]}
        "truck", "car", "train" result will be combine into one "truck" result.

        config:
        group_class = {"truck": ["truck", "car", "train"]}
        --------------------------------------------------
        before:
        {
        "bboxes": {"truck": [[1,2,3,4,9.0]], "car":[[1,3,3,4,9.0]], "train":[[2,2,3,4,9.0]], "person":[[0,0,100,100, 1.0]]}
        }
        after:
        {
         "bboxes": {"truck": [[1,2,3,4,9.0],[1,3,3,4,9.0],[2,2,3,4,9.0]], "car":[], "train":[], "person":[[0,0,100,100, 1.0]]},
         "grouped_class": {"car":[[1,3,3,4,9.0]], "train":[[2,2,3,4,9.0]]}
        }

        * {"object_class": [x, y, width, height, confidence_score]}
        --------------------------------------------------
        before:
        {
        "subjects": {"truck": 1, "car": 3, "train": 2,"bird": 1},
        }
        after:
        {
        "subjects": {"truck": 6, "car": 0, "train": 0,"bird": 1},
        }
        """
        if save_grouped and 'grouped_class' not in recData_output:
            recData_output.grouped_class = AD()
        # self.logger.info(f"group_class: {group_class}")
        for gclass, subclasses in group_class.items():
            for idx, subclass in enumerate(subclasses):
                idx += 1
                ### update annotation label
                # Examples of 3 generated labels within the same image:
                # "person_0[0.888]" = first "person" object with confidence score of 0.888
                # "person_1[0.999]" = second "person" object with confidence score of 0.999
                # "truck_0[0.875]" = first "truck" object with confidence score of 0.875
                if data_key == "bboxes":
                    [bb.set_label("({}_{}[{:.3f}]".format(gclass, idx, bb.threshold))
                     for sc_idx, bb in enumerate(recData_output[data_key][subclass])]
                    if subclass != gclass:
                        recData_output[data_key][gclass] += recData_output[data_key][subclass]
                        if save_grouped:
                            recData_output.grouped_class[subclass] = recData_output[data_key][subclass]
                        recData_output[data_key][subclass] = default_value

    def process_roi_ratio(self, zone_filter, output_bbxes, output_bx_count, excluded_bbxes, roi_threshold):
        """
        Function to remove bounding boxes whose area is less than threshold * region_filter
        :param zone_filter: The region in config
        :param output_bbxes: Output bounding boxes so far
        :param output_bx_count: output bbox count
        :param excluded_bbxes: Boxes excluded so far based on filters applied previously
        :param roi_threshold: min ratio between bbx area and zone filter area
        :return:
        """

        for key, th in roi_threshold.items():
            if key in output_bbxes:
                excluded_bbxes[key].extend(list(filter(lambda zn: zn.area/zone_filter.area <= roi_threshold[key], output_bbxes[key])))
                output_bbxes[key] = list(filter(lambda zn: zn.area/zone_filter.area > roi_threshold[key], output_bbxes[key]))
                output_bx_count[key] = len(output_bbxes[key])

        excluded_bbxes = {k: val for k, val in excluded_bbxes.items() if val}

        return output_bbxes, output_bx_count, excluded_bbxes

    def process_with_zone_filter(self, zone_filter, detectedBoxes):
        """
        Function to filter bounding boxes based on bounding box
        :param zone_filter: Zone object for bounding box
        :param detectedBoxes: detected boxes from inference
        :return: Filtered detectedBoxes
        """

        for kls, klsList in AD(detectedBoxes).items():
            klsList = list(filter(lambda x: len(x) >= 4, klsList))
            if zone_filter:
                if kls not in ['person', 'safety_vest', 'safety_garment', 'hard_hat']:
                    klsList = list(filter(lambda x: x[:4] in zone_filter, klsList))
                    detectedBoxes[kls] = klsList
                elif kls == 'person':
                    klsList_op = list(filter(lambda x: x[:4] in zone_filter, klsList))
                    detectedBoxes['operator'] = klsList_op

                    klsList_per = list(filter(lambda x: x[:4] not in zone_filter, klsList))
                    detectedBoxes[kls] = klsList_per
                else: # safety_vest, safety_garment, hard_hat
                    detectedBoxes[kls] = klsList
            else:
                detectedBoxes[kls] = klsList

        return detectedBoxes

    def process_with_exclusion_filter(self, excluded, detectedBoxes, results_bboxes, results, zone_filter, interest_objects):
        """
        Function to filter bounding boxes with contains any of the exclusion points in them

        :param excluded: List of lists of exclusion point coordinates
        :param detectedBoxes: bboxes from inference
        :param results_bboxes: dictionary with bounding boxes per class after filtering
        :param results: number of detections per class
        :return:
        """

        excluded_bbxes = {}

        for kls, klsList in detectedBoxes.items():

            if len(klsList) == 0:
                continue

            # Creating Zones for people. If person within region then mark as 'operator'. Else mark as 'person'
            # All people in images should be anonymized irrespective of if they are in interest_objects or not

            # The reporting side looks at results.subjects["person"] (-> int scalar) to report
            # if there are operators present or not.

            # person -> any bbox for "person" outside/no overlap with region filter(Ex: mill boundary)
            # operator -> any bbox for "person" within/overlapping with region filter
            # Summary: In database, query for
            # No of "persons" = results.subjects."nonoperator"
            # No of "operators" = results.subjects."person"
            # Bboxes for "persons" = results.bboxes."person"
            # Bboxes for "operators" = results.bboxes."operator"
            exclusion_pts = []
            if 'all' in excluded and isinstance(excluded.all, list) and len(excluded.all):
                exclusion_pts.extend(excluded.all)
            if kls in excluded and isinstance(excluded[kls], list) and len(excluded[kls]):
                exclusion_pts.extend(excluded[kls])

            zone_ret = [Zone.create(
                                    f"{kls}",
                                    [bx.x, bx.y, bx.w, bx.h],
                                    atype="subject",
                                    rtype=kls,
                                    threshold=bx[4], exclusionpts=exclusion_pts) for idx, bx in enumerate(klsList)]

            # Filter based on zone.exclude. If zone.exclude is True, goes to excluded list

            results_bboxes[kls] = list(filter(lambda x: not x.exclude, zone_ret))
            excluded_bbxes[kls] = [x for x, y in zip(klsList, zone_ret) if y.exclude]

            if zone_filter:
                if not (kls in ['person', 'operator']):
                    results[kls] = len(results_bboxes[kls])
                elif kls == 'person':
                    results['non-operator'] = len(results_bboxes[kls])
                else:
                    # kls is 'operator'
                    results['person'] = len(results_bboxes[kls])
            else:
                results[kls] = len(results_bboxes[kls])

        # Remove empty and zero entries in dictionaries
        excluded_bbxes = {k: val for k, val in excluded_bbxes.items() if val}

        # Adding remaining key values in interest objects so that format of results is consistent with
        # {"class1":[], "class2":[[xmin, ymin, w, h, score], [xmin, ymin, w, h, score]]}
        final_results_bboxes = {k: [] for k in interest_objects}
        final_results = {k: 0 for k in interest_objects}

        if zone_filter and 'person' in interest_objects:
            final_results['non-operator'] = 0
            final_results_bboxes['operator'] = []

        final_results.update(results)
        final_results_bboxes.update(results_bboxes)

        return final_results_bboxes, final_results, excluded_bbxes

    def region_output(self, region_list, inference_result, excluded, return_bboxes, subject_name, interest_objects):

        detected_by_region = self.process_with_zone_filter(region_list, inference_result)

        bboxes, subjects, excluded_bbxes = self.process_with_exclusion_filter(
                excluded, detected_by_region, return_bboxes, subject_name, region_list, interest_objects)

        return bboxes, subjects, excluded_bbxes

    def run_inference_for_single_image(self, image):
        """
        Added by Aarti
        :param self.session: Loaded session
        :param image: Loaded image
        :return:
        """

        # Get handles to input and output tensors
        with self.detection_graph.as_default():
            ops = tf.get_default_graph().get_operations()

        all_tensor_names = {output.name for op in ops for output in op.outputs}
        tensor_dict = {}
        for key in [
            "num_detections",
            "detection_boxes",
            "detection_scores",
            "detection_classes",
            "detection_masks",
        ]:
            tensor_name = key + ":0"
            if tensor_name in all_tensor_names:
                tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                    tensor_name
                )

        # Commenting the following lines of code since we do not detect masks for objects.
        # Also, the following lines use 'utils_ops' python script from Object Detection repo.

        # if 'detection_masks' in tensor_dict:
        #     # The following processing is only for single image
        #     detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        #     detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        #     # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        #     real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        #     detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        #     detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        #     detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
        #         detection_masks, detection_boxes, image.shape[0], image.shape[1])
        #     detection_masks_reframed = tf.cast(
        #         tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        #     # Follow the convention by adding back the batch dimension
        #     tensor_dict['detection_masks'] = tf.expand_dims(
        #         detection_masks_reframed, 0)

        image_tensor = tf.get_default_graph().get_tensor_by_name("image_tensor:0")

        # Create a session if object has no attribute "session" or is the session is closed
        if not hasattr(self, "session") or (hasattr(self, "session") and self.session._closed):
            self.create_session()

        # Run inference
        output_dict = self.session.run(
            tensor_dict,
            feed_dict={image_tensor: np.expand_dims(image.astype(np.uint8), 0)},
        )

        # all outputs are float32 numpy arrays, so convert types as appropriate
        output_dict["num_detections"] = int(output_dict["num_detections"][0])
        output_dict["detection_classes"] = output_dict["detection_classes"][0].astype(
            np.uint8
        )
        output_dict["detection_boxes"] = output_dict["detection_boxes"][0]
        output_dict["detection_scores"] = output_dict["detection_scores"][0]
        if "detection_masks" in output_dict:
            output_dict["detection_masks"] = output_dict["detection_masks"][0]

        return output_dict

    def update_subject_count(self, subject_count, output_bbxes):
        for sbj_key in subject_count.keys():
            box_key = sbj_key
            if sbj_key == "non-operator":
                box_key = "person"
            elif sbj_key == "person":
                box_key = "operator"
            subject_count[sbj_key] = len(output_bbxes[box_key])

#####################################################################################################
def _tmp_recData_debug_str(recData: Optional[Dict]) -> str:
    if not recData:
        return str(recData)

    # when calling this function within ObjectDetection.detect(...),
    # we are expecting the recData contains "customer_id", "gateway_id" and "nodename",
    # as depicted in "return_rest_mv_results" function

    EXCLUDED_KEY = ["image", "images"]
    file_str = io.StringIO()
    file_str.write("(\n")
    for key, value in recData.items():
        if not key in EXCLUDED_KEY:
            file_str.write("    ")
            file_str.write(key)
            file_str.write(" : ")
            file_str.write(str(value))
            file_str.write("\n")

    file_str.write(")\n")
    return file_str.getvalue()

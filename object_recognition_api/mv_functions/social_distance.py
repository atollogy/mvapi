import cv2
import numpy as np

from pydantic import BaseModel
from typing import Tuple, Dict, List, Optional, Any, Union


class CornerPoints(BaseModel):
    """
    top_left, top_right, bottom_left, bottom_right corner points
    """

    top_left: List[int]
    """
    [x,y] coordinates for top_left
    """

    top_right: List[int]
    """
    [x,y] coordinates for top_right
    """

    bottom_left: List[int]
    """
    [x,y] coordinates for bottom_left
    """

    bottom_right: List[int]
    """
    [x,y] coordinates for bottom_right
    """

class RectangularPoints(BaseModel):
    """
    width and height parameters
    """

    width: int
    height: int


class PerspectiveTransformCfg(BaseModel):
    """
    In Perspective Transformation, we can change the perspective of a given image or video
    for getting better insights about the required information.
    """

    origin_points: CornerPoints
    """
    Dictinary with four corner coordinates from original image
    top_left, top_right, bottom_left, bottom_right
    """

    transform_points: Optional[RectangularPoints]
    """
    Dictinary with four corner coordinates to transform to new perspective
    top_left, top_right, bottom_left, bottom_right
    """

    scale_translation: Optional[int] = 250
    """
    move image center to the scale translation points [250, 250]
    """

class SocialDistanceCfg(BaseModel):
    """
    Configuration that supports the logic that transform coordinates.
    Configuration for getting center points after transform coordinates.
    """

    target_objects: List[str] = ["operator", "person"]
    """
    target classes for perspective transform
    example: ["operator", "person"]
    """

    perspective_transform: Optional[PerspectiveTransformCfg]
    """
    Perspective Transformation. Advertisements. When human eyes see near things
    they look bigger as compare to those who are far away
    """


######### Perspective Transform code #########
class SocialDistanceATL():
    def get_centerpoint(self, bboxes: List[Union[int, float]], target_object: List[str]=["operator", "person"]) -> List[List[int]]:
        # bboxes = {'object': [[x, y, w, h, confidence_score],[...]]}
        # target_object: object list example: ["operator", "person"]

        bbox_list = []
        # combine target object bboxes lists
        for t in target_object:
            bbox_list += bboxes[t]

        # calculate center point from x,y,w,h
        center_list = []
        for i in range(len(bbox_list)):
            center_x = round(( bbox_list[i][2] ) / 2 + bbox_list[i][0])
            center_y = round(( bbox_list[i][3] ) / 2 + bbox_list[i][1])
            center_list.append([center_x, center_y])

        return center_list

    def order_points(self, pts: np.array) -> List[List[int]]:
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = "float32")
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # return the ordered coordinates
        return rect


    def run_ppt(self, perspective_config: PerspectiveTransformCfg, origin_centers: List[List[int]]) -> List[np.array]:
        """
        perspective_transform: {
            "origin_points" : {
                'top_left': [x1, y1],
                'top_right': [x2, y2],
                'bottom_left': [x3, y3],
                'bottom_right': [x4, y4]
            },
            "transform_points":{
                'width': interger
                'height': interger
            }
            "scale_translation": 250
        }

        *** if "transform_points" is none or empty,
        then automatically calculate transform_points coordinates

        origin_centers = [[x,y],[x2,y2]...]
        """

        origin_dict = dict(perspective_config.origin_points)
        pts1 = self.order_points(np.array(list(origin_dict.values())))

        xmin, ymin = np.amin(pts1, axis=0).astype(int)
        xmax, ymax = np.amax(pts1, axis=0).astype(int)

        if "transform_points" in perspective_config and perspective_config.transform_points:
            transform_list = [
                [xmin, ymin],
                [xmin + perspective_config.transform_points.width, ymin],
                [xmin, ymin + perspective_config.transform_points.height],
                [xmin + perspective_config.transform_points.width, ymin + perspective_config.transform_points.height],
            ]
        else:
            xmin += perspective_config.scale_translation
            ymin += perspective_config.scale_translation
            xmax += perspective_config.scale_translation
            ymax += perspective_config.scale_translation

            transform_list = [
                [xmin, ymin],
                [xmax, ymin],
                [xmin, ymax],
                [xmax, ymax],
            ]

        pts2 = self.order_points(np.array(transform_list))

        # transform matrix
        M = cv2.getPerspectiveTransform(pts1,pts2)

        # the result of the transformed coodinates
        transform_results = cv2.perspectiveTransform(np.array([origin_centers],dtype=np.float32), M)[0]
        # convert float to int array
        transform_results = np.around(transform_results, decimals=-1).astype(int).tolist()

        return transform_results
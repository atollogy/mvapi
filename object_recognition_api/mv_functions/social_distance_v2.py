
from attribute_dict import *
from image.utilities import *
import cv2
import io
import logging
logger = logging.getLogger('mvapi')
import math
import numpy as np
from planar import Vec2, Vec2Array, Polygon, Affine
from image.utilities import Line
from pydantic import BaseModel
# import shapely.geometry as s
# from shapely import affinity
from typing import Tuple, Dict, List, Optional, Any, Union


class Social_Distance_Cfg_V2(BaseModel):
    """
    Required parameters for locked Z-Y perspective transformation
    """

    center_shift: int = 0
    """
    percentage to scale top x-axis display trapezoid
    """

    dtz_top_x_adjust: float = 0
    """
    percentage to scale top x-axis display trapezoid
    """

    dtz_bottom_x_adjust: float = 0
    """
    percentage to scale bottom x-axis display trapezoid
    """

    dtz_top_y_adjust: float = 0
    """
    percentage to scale top y-axis display trapezoid
    """

    focal_length: float = 2.8
    """
    focal length of camera lens - typically 2.8 or 3.6
    """

    x_angle: float = 0
    """
    physical horizontal angle of camera in relation to the horizontal plane of the ceiling
    """

    height: float = 0
    """
    height of camera in inches from ground
    """

    rotate: float = 0
    """
    rotate image x degrees
    """

    target_objects: List[str] = ["operator", "operators", "person"]
    """
    list of object detection classes to calculate distances between
    """

    trapezoid: Optional[dict]
    """
    trapezoid shape
    """

    use_in_detection: str = 'undistorted'
    """
    specify which output file will be used in mv detction process - ['original', 'undistorted', 'transformed', 'scaled']
    """

    y_angle: float = 0
    """
    physical vertical angle of camera in relation to the horizontal plane of the ceiling
    """

    version: int = 2
    """
    version of social distancing functionality
    """


######### Perspective Transform code #########
class Social_Distance_V2(object):
    def __init__(self, nodename, cfg, original):
        self.cfg = AD({
            'ctype': 'de8cam',
            'dx_offset': 0,
            'dy_offset': 0,
            'shapes': {},
            'trapezoid_keys': ['bottom_left','bottom_right', 'top_left', 'top_right'],
            'violations': {
                'no_issue': {
                    'color': CT.white.rgb,
                    'font': cv2.FONT_HERSHEY_DUPLEX,
                    'font_scale': 0.5,
                    'range': (100, 200),
                    'vnum': '0'
                },
                'minor': {
                    'color': CT.yellow.rgb,
                    'font': cv2.FONT_HERSHEY_DUPLEX,
                    'font_scale': 0.5,
                    'range': (6, 8),
                    'vnum': '2'
                },
                'safe': {
                    'color': CT.lime_green.rgb,
                    'font': cv2.FONT_HERSHEY_DUPLEX,
                    'font_scale': 0.5,
                    'range': (8,100),
                    'vnum': '1'
                },
                'severe': {
                    'color': CT.red.rgb,
                    'font': cv2.FONT_HERSHEY_DUPLEX,
                    'font_scale': 0.5,
                    'range': (0, 4),
                    'vnum': '4'
                },
                'significant': {
                    'color': CT.orange.rgb,
                    'font': cv2.FONT_HERSHEY_DUPLEX,
                    'font_scale': 0.5,
                    'range': (4, 6),
                    'vnum': '3'
                }
            },
            'x_tan_angle': 0,
            'y_tan_angle': 0
            })
        self.images = AD()
        if isinstance(cfg, Social_Distance_Cfg_V2):
            cfg = cfg.dict()
        self.cfg.update(cfg)
        self.cfg.image_size = original.shape
        self.focals = AD({
            1280: 2.8,
            1920: 3.8,
            2560: 6.8,
            3200: 8.8,
            3840: 12.8
        })
        self.cfg.focal_length = self.focals[self.cfg.image_size[1]]
        self.add_image('original', original)
        self.calculate_parameters(nodename)

    def add_image(self, itype, img):
        self.images[itype] = AD({
                            'ratio': (img.shape[0]/img.shape[1]),
                            'shape': img.shape[:2],
                            'x': img.shape[1],
                            'y': img.shape[0]
                        })

        self.cfg.shapes[itype] = AD(self.images[itype])
        #logger.info(f"added image {itype}: {self.images[itype]}")
        self.images[itype].img = img
        return self.images[itype]

    # def analyse_trapezoid(self):
    #     oy, ox = self.images.original.shape[:2]
    #     logger.info(f"cfg before trapezoid: {self.cfg}")
    #
    #     self.cfg.trapezoid = t = AD({k: Point(*v) for k, v in self.cfg.trapezoid.items()})
    #     self.cfg.trapezoid_sides = ts =  AD({
    #         'bottom': Line(sorted([t.bottom_left, t.bottom_right])),
    #         'left': Line(sorted([t.bottom_left, t.top_left])),
    #         'right': Line(sorted([t.top_right, t.bottom_right])),
    #         'top': Line(sorted([t.top_left, t.top_right]))
    #     })
    #     self.cfg.y_angle = ((90 - abs(angle(ts.left))) + (90 - abs(angle(ts.right))))/2
    #     if angle(ts.bottom) == 0 and angle(ts.top) != 0:
    #         self.cfg.x_angle = angle(ts.top)
    #     elif (angle(ts.bottom) > 0 and angle(ts.top) < 0) or (angle(ts.bottom) < 0 and angle(ts.top) > 0):
    #         self.cfg.x_angle = (abs(angle(ts.bottom)) + abs(angle(ts.top)))/2
    #     elif (angle(ts.bottom) > 0 and angle(ts.top) > 0) or (angle(ts.bottom) < 0 and angle(ts.top) < 0):
    #         self.cfg.rotate = (angle(ts.bottom) + angle(ts.top))/2
    #     else:
    #         self.cfg.x_angle = self.cfg.rotate = 0
    #     self.cfg.display_trapezoid = AD({k: as_int_point(v) for k, v in self.cfg.trapezoid.items()})
    #     logger.info(f"cfg after trapezoid: {self.cfg}")
        # self.cfg.dtz_top_y_adjust
        # self.cfg.dtz_top_x_adjust
        # self.cfg.dtz_bottom_x_adjust
        # self.cfg.center_shift

    def apply_transformations(self):
        try:
            current = original = self.images.original
            current = deskewed = self.deskew(current)
            current = undistorted = self.undistort(current)
            # current = undistorted = self.add_image('undistorted', current.img)

            current = undistorted_with_trapezoid = self.generate_and_draw_display_trapezoid(current)
            return (self.cfg, undistorted.img,  undistorted_with_trapezoid.img)
        except Exception as err:
            logger.exception(f"Social_Distance_V2.transform_image exception: {err}")

    def build_four_point_transform_matrix(self, pts):
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordinates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")

        # compute the perspective transform matrix and then apply it
        matrix = cv2.getPerspectiveTransform(rect, dst)
        return [matrix, (maxWidth, maxHeight)]

    def calculate_distance(self, line, y_tan):
        # logger.info(line, y_tan)
        pts = line[:]
        line_angle = angle(pts)
        simple = self.simple_distance(pts)*self.cfg.pixel_distance_y
        line_tan = math.tan(math.radians(line_angle))
        logger.info(f"line simple: {simple} - angle: {line_angle} - tan: {line_tan}")
        # logger.info(f"pts before transform: {pts}")
        pts[0] = Point(pts[0][0]/y_tan, pts[0][1]*y_tan)
        pts[1] = Point(pts[1][0]/y_tan, pts[1][1]*y_tan)
        # logger.info(f"pts after transform: {pts}")

        y = self.images[self.cfg.use_in_detection].y
        dx = (pts[1].x - pts[0].x)
        dx = dx * self.cfg.pixel_distance_x
        dx = dx * self.cfg.x_axis_tan

        dy = (pts[1].y - pts[0].y)
        dy = dy * self.cfg.pixel_distance_y
        dy = dy * self.cfg.y_axis_tan
        dist = math.sqrt(dx**2 + dy**2)
        dist = dist/self.cfg.floor_ratio
        dist_in = round(dist, 2)
        dist_ft = round(dist/12, 2)
        return [[as_int_point(line[0]), as_int_point(line[1])], pts, dist_in, dist_ft]

    def calculate_parameters(self, nodename):
        if 'dw4cam' in nodename:
            self.cfg.yfv = 57.0
            self.cfg.xfv = 87
        elif 'de8cam' in nodename:
            self.cfg.yfv = 58.0
            self.cfg.xfv = 82
        else:
            self.cfg.yfv = 55.0
            self.cfg.xfv = 87.0

        self.cfg.area = self.images.original.x * self.images.original.y
        self.cfg.y_diff = 90 - (self.cfg.yfv + self.cfg.y_angle)
        self.cfg.y_diff_tan = math.tan(math.radians(self.cfg.y_diff))
        self.cfg.y_tan_angle = self.cfg.yfv/2
        self.cfg.y_axis_tan = math.tan(math.radians(self.cfg.y_tan_angle))
        self.cfg.y_adjust_angle = (self.cfg.yfv/2 + self.cfg.y_angle)
        self.cfg.y_adjust_tan = math.tan(math.radians(self.cfg.y_adjust_angle))
        self.cfg.y_offset_angle = 90 - self.cfg.y_angle - self.cfg.yfv/2
        self.cfg.y_offset_tan = math.tan(math.radians(self.cfg.y_offset_angle))

        self.cfg.deskew_tan = math.tan(math.radians(self.cfg.x_angle))
        self.cfg.x_tan_angle = self.cfg.xfv/2 - self.cfg.x_angle
        self.cfg.x_axis_tan = math.tan(math.radians(self.cfg.x_tan_angle))

        self.cfg.height_adjusted = self.cfg.y_floor = (self.cfg.height*self.cfg.y_adjust_tan)
        self.cfg.height_ratio = self.cfg.height/self.cfg.height_adjusted
        self.cfg.x_floor = (self.cfg.height * self.cfg.x_axis_tan) * 2
        self.cfg.y_floor_ft = self.cfg.y_floor/12
        self.cfg.x_floor_ft = self.cfg.x_floor/12
        self.cfg.floor_ratio = self.cfg.y_floor/self.cfg.x_floor
        self.cfg.pixel_distance_y = self.cfg.pixel_distance = self.cfg.y_floor/self.images.original.y
        self.cfg.pixel_distance_x = self.cfg.x_floor/self.images.original.x
        self.cfg.ppi_y = 1/self.cfg.pixel_distance_y
        self.cfg.ppi_x = 1/self.cfg.pixel_distance_x
        self.cfg.ppf_y = self.cfg.ppi_y*12
        self.cfg.ppf_x = self.cfg.ppi_x*12

    def deskew(self, current):
        XDT = self.cfg.deskew_tan
        if self.cfg.rotate:
            current = self.add_image('rotated', image_rotation(current.img.copy(), -self.cfg.rotate))

        y_offset = (current.y * XDT)
        logger.info(y_offset)
        self.cfg.deskew_pts = AD({
                'top_left': (0, -y_offset),
                'bottom_left': (0, current.y + y_offset),
                'top_right': (current.x, 0),
                'bottom_right': (current.x,  current.y)
            })
        (matrix, dimensions) = self.build_four_point_transform_matrix(
                                                                np.array(self.order_points(self.cfg.deskew_pts.values()),
                                                                dtype="float32")
                                                            )
        current = self.add_image('deskewed', cv2.warpPerspective(current.img.copy(), matrix, dimensions))
        return current

    def generate_and_draw_display_trapezoid(self, display):
        if 'trapezoid' in self.cfg and isinstance(self.cfg.trapezoid, (AD, dict)) and len(self.cfg.trapezoid.keys()) == 4:
            self.cfg.display_trapezoid = display_trapezoid = AD({k: as_int_point(v) for k, v in self.cfg.trapezoid.items()})
        else:
            # self.cfg.dx_offset = DXO = int((display.x - display.y)/2  math.tan(math.radians(self.cfg.y_angle - (30  - self.cfg.y_angle))))
            self.cfg.dx_offset = DXO = int((display.x - display.y)/4)
            # self.cfg.dx_offset = DXO = int(display.y/2 * self.cfg.y_axis_tan)/2
            self.cfg.dy_offset = DYO = int(display.y - (display.y*self.cfg.height_ratio +self.cfg.ppf_y*self.cfg.dtz_top_y_adjust))
            print(f"dyo: {DYO}")
            # (self.cfg.y_floor*self.cfg.floor_ratio + (self.cfg.dtz_top_y_adjust if self.cfg.dtz_top_y_adjust <= 2.5 else 2.5)*12))
            self.cfg.dx_offset_top = DXOT = int(
                (self.cfg.dx_offset*2) +
                (self.cfg.dtz_top_x_adjust * self.cfg.ppf_x)
            )
            self.cfg.dx_offset_bottom = DXOB = int(
                self.cfg.dx_offset +
                (self.cfg.dtz_bottom_x_adjust * self.cfg.ppf_x)
            )

            # logger.info(DXO,self.cfg.dtz_top_x_adjust, self.cfg.center_shift, self.cfg.ppf_x)
            self.cfg.display_trapezoid = display_trapezoid = AD({
                'top_left': (DXOT + int(self.cfg.center_shift * self.cfg.ppf_x), DYO),
                'top_right': (int(display.x - DXOT), DYO),
                'bottom_left': (DXOB, display.y),
                'bottom_right': (int(display.x - DXOB + (self.cfg.center_shift * self.cfg.ppf_x)), display.y)
            })


        undistorted_with_trapezoid = display.img.copy()
        cv2.line(undistorted_with_trapezoid, display_trapezoid.bottom_left, display_trapezoid.top_left, (0, 255, 255), 5)
        cv2.line(undistorted_with_trapezoid, display_trapezoid.top_left, display_trapezoid.top_right, (0, 255, 255), 5)
        cv2.line(undistorted_with_trapezoid, display_trapezoid.bottom_right, display_trapezoid.top_right, (0, 255, 255), 5)
        cv2.line(undistorted_with_trapezoid, display_trapezoid.bottom_left, display_trapezoid.bottom_right, (0, 255, 255), 5)

        return self.add_image('undistorted_with_trapezoid', undistorted_with_trapezoid)

    def get_vtype(self, dist):
        ni = self.cfg.violations.no_issue
        try:
            for v, vd in self.cfg.violations.items():
                low, high = vd.range
                if low <= dist < high:
                    return (v, vd.vnum, vd.color, vd.font, vd.font_scale)
        except Exception:
            return ('no_issue', ni.vnum, ni.color, ni.font, ni.font_scale)

    def filter_and_calculate_bboxes(self,
                                    rd: AD = None,
                                    bboxes: AD = None,
                                    current: np.array = None,
                                    target_objects: List[str]=["operator", "person"],
                                    ) -> AD:
        try:
            if not all([rd, bboxes]):
                plogger.error(f"filter_and_calculate_bboxes error - bboxes and/or rd null or empty")
                return current
            rd.output.pre_sd_bboxes = AD(bboxes)
            rd.output.sd_bboxes = AD({to: [] for to in target_objects})
            rd.output.sd_filtered_out_bboxes = AD({to: [] for to in target_objects})
            rd.output.object_bottom_midpoints = object_bottom_midpoints = []

            # area_threshold = (current.shape[0] * current.shape[1]) * 0.75
            for to in target_objects:
                if to in bboxes:
                    for b in bboxes[to]:
                        if not isinstance(b, Zone) or b.atype_name != 'anonymous_subject':
                            b = Zone.create(to, b[:4], atype='anonymous_subject', result=b, rtype=to, threshold=b[4])
                        if b.sd_centroid.inside_polygon(self.cfg.display_trapezoid):
                            rd.output.sd_bboxes[to].append(b)
                            rd.output.object_bottom_midpoints.append(as_int_point(b.sd_centroid))
                        else:
                            rd.output.sd_filtered_out_bboxes[to].append(b)
            rd.output.multiple_midpoints = True if len(rd.output.object_bottom_midpoints) > 2 else False
            rd.output.distances = AD({v.vnum: [] for v in self.cfg.violations.values()})
            seen=[]
            annotated = current.copy()
            for i1, v1 in enumerate(object_bottom_midpoints[:]):
                for i2, v2 in enumerate(object_bottom_midpoints[:]):
                    if v1 != v2:
                        line_labels = sorted([(v1, i1), (v2, i2)], key=lambda x: x[0][0])
                        line = [p[0] for p in line_labels]
                        line_id = f"Op{line_labels[0][1]}-Op{line_labels[1][1]}"
                        if line_id not in seen:
                            seen.append(line_id)
                            dist = self.calculate_distance(line, self.cfg.y_adjust_tan)
                            vname, vnum, vcolor, vfont, vfont_scale = self.get_vtype(dist[-1])
                            rd.output.distances[vnum].append([dist[0], dist[-1]])
                            cv2.line(annotated, as_int_point(line[0]), as_int_point(line[1]), vcolor, 2)
                            cv2.circle(annotated, as_int_point(line[0]), 3, vcolor, 3)
                            cv2.circle(annotated, as_int_point(line[1]), 3, vcolor, 3)
                            label = '{}: {:2.2f} ft'.format(vname, dist[-1])
                            mp = midpoint(line)
                            (lwidth, lheight), baseline = cv2.getTextSize(label, vfont, vfont_scale, 1)
                            cv2.rectangle(annotated, (mp[0], int(mp[1]-(lheight**vfont_scale*0.85))), (mp[0]+lwidth, mp[1]), vcolor, cv2.LINE_AA)
                            cv2.putText(annotated, label, (mp[0], mp[1]+3)  , vfont, vfont_scale, CT.black,    1, cv2.LINE_AA)
            bboxes.update(rd.output.sd_bboxes)
            return annotated
        except Exception as err:
            logger.exception(f"Social_Distance_V2.filter_and_calculate_bboxes exception: {err}")
            raise err

    def order_points(self, pts, ptype='float32'):
        pts = np.array(pts, dtype=ptype)
        # initialize a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = ptype)

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        return rect

    def simple_distance(self, points):
        p1, p2 = [Point(*p) for p in sorted(points)]
        dx = (p2.x - p1.x)
        dy = (p2.y - p1.y)
        return math.sqrt(dx**2 + dy**2)

    # def transform(self, current):
    #     self.cfg.perspective_pts = AD({
    #         'top_left': (current.x_offset, 0),
    #         'top_right': (current.x - current.x_offset, 0),
    #         'bottom_left': (-current.x_offset, current.y),
    #         'bottom_right': (current.x + current.x_offset, current.y)
    #     })
    #
    #     (self.cfg.perspective_transform, self.cfg.perspective_transform.dimensions) = self.build_four_point_transform_matrix(
    #                                                 np.array(self.cfg.perspective_pts.values(),
    #                                                 dtype = "float32")
    #                                             )
    #     self.cfg.perspective_transform = matrix
    #     return self.add_image('transformed',  cv2.warpPerspective(current.img.copy(), self.cfg.perspective_transform, self.cfg.perspective_transform.dimensions))

    def undistort(self, current):
        distCoeff = np.zeros((4,1),np.float64)
        k1 = -(self.cfg.focal_length/197500) # negative to remove barrel distortion
        # k1 = -1.0e-5
        k2 = 0
        p1 = 0.0
        p2 = 0.0

        distCoeff[0,0] = k1;
        distCoeff[1,0] = k2;
        distCoeff[2,0] = p1;
        distCoeff[3,0] = p2;

        # assume unit matrix for camera
        cam = np.eye(3,dtype=np.float32)
        cam[0,2] = current.x/2.0  # define center x
        cam[1,2] = current.y/2.0 # define center y

        if self.cfg.focal_length < 3.2:
            cam[0,0] = self.cfg.focal_length/0.45 # define focal length x
            cam[1,1] = self.cfg.focal_length/0.45 # define focal length y
        else:
            cam[0,0] = (self.cfg.focal_length/0.4) # define focal length x
            cam[1,1] = (self.cfg.focal_length/0.4) # define focal length y

        current = self.add_image('undistorted', color_space_convert(cv2.undistort(current.img.copy(), cam, distCoeff), 'bgr', 'rgb'))
        return current

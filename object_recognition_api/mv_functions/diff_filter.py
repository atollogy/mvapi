#!/usr/bin/env python3

from attribute_dict import AD
from image.utilities import *
import os
import cv2
import numpy as np
import mahotas as mh
import logging


class DiffFilter:
    def __init__(self,):
        self.logger = logging.getLogger("mvapi")

        self.img_dic = {}
        self.time_dic = {}

    def connected_pixel_threshold(self, diff_binary, lesspixel):
        """
        noise filter, remove small number of connected pixels.
        """

        # labeled: each cluster of pixel labeled by different number, changed_blobs: cluster count
        labeled, changed_blobs = mh.label(diff_binary)
        # sizes: list of each cluster pixel size
        sizes = mh.labeled.labeled_size(labeled)
        # too_small: find small pixel cluster
        too_small = np.where(sizes < lesspixel)
        # length of labeled except too small cluster
        changed_segments = changed_blobs - len(too_small[0])
        # find the largest pixel cluster
        largest_segment = max(sizes[1:]) if len(sizes) > 1 else 0
        # filtering too small pixel clusters
        filtered_img = mh.labeled.remove_regions(labeled, too_small) > 0
        # total number of diff pixel after filtering
        segment_pixels = np.count_nonzero(filtered_img)
        # total pixel number on the region
        region_pixels = diff_binary.shape[0] * diff_binary.shape[1]
        # total percentage of changed pixel after filtering small cluster
        segment_percent = (segment_pixels / region_pixels) * 100

        return (
            filtered_img,
            int(changed_blobs),
            int(changed_segments),
            int(region_pixels),
            int(largest_segment),
            int(segment_percent),
        )

    def region_of_interest(self, img_binary, padding):
        """
        Find rectangular area(region of interest) that include every pixels.
        Except black area (pixel value is 0).
        with padding distance area.
        """

        if len(img_binary.shape) == 2:
            h, w = img_binary.shape
        else:
            h, w, _ = img_binary.shape

        # find all x,y coordinates
        x_list = np.argwhere(img_binary > 0)[:, 1]
        y_list = np.argwhere(img_binary > 0)[:, 0]

        # if there is no diff image, then return empty list
        if len(x_list) == 0 or len(y_list) == 0:
            return []

        x1 = int(min(x_list) - padding if min(x_list) - padding > 0 else 0)
        x2 = int(max(x_list) + padding if max(x_list) + padding < w else w)

        y1 = int(min(y_list) - padding if min(y_list) - padding > 0 else 0)
        y2 = int(max(y_list) + padding if max(y_list) + padding < h else h)

        # return [x,y,w,h]
        return [x1, y1, x2 - x1, y2 - y1]

    def check_filter(
        self,
        subject,
        active_box,
        changed_blobs,
        changed_segments,
        largest_segment,
        segment_percent,
        time_gap,
    ):
        # setup threshold comparison condition
        condition_key = (
            subject.condition_key if "condition_key" in subject else "segment_percent"
        )
        threshold = subject.threshold if "threshold" in subject else 0
        timegap_threshold = (
            subject.timegap_threshold if "timegap_threshold" in subject else 5
        )

        if condition_key in globals():
            condition = globals()[condition_key]
        else:
            self.logger.warning(
                "condition is not exist on the list, it will apply to default set up of segment_percent"
            )
            condition = segment_percent

        # appling short circuit, if first statement satisfy it will not excute second statement
        return (condition > threshold) or (
            condition <= threshold and time_gap < timegap_threshold
        )

    def img_subtraction(self, input_img, stepCfg, subject):
        """
        background subtraction using simple image diff
        """

        # default parameters set up.
        if "pixel_threshold" in subject:
            pixel_threshold = subject.pixel_threshold
        else:
            pixel_threshold = 20

        if "lesspixel" in subject:
            lesspixel = subject.lesspixel
        else:
            lesspixel = 1000

        if "padding" in subject:
            padding = subject.padding
        else:
            padding = 0

        # Preprocess images with  gaussian filter
        blur_img = cv2.GaussianBlur(input_img, (5, 5), 0)

        img_key = stepCfg.gateway_id + "_" + stepCfg.step_name

        # Calculates the per-element absolute difference between two arrays or between an array and a scalar
        diff_img = (
            cv2.absdiff(blur_img, self.img_dic[img_key])
            if img_key in self.img_dic
            else blur_img
        )
        # Calculate time gap from previous time
        time_gap = (
            float(stepCfg.collection_time) - self.time_dic[img_key]
            if img_key in self.time_dic
            else 0
        )

        # Using pixel_threshold value convert image to binary image
        diff_img[diff_img <= pixel_threshold] = 0

        # count number of connected pixel group, and less than the value of connected_min will be filtered out
        filtered_img, changed_blobs, changed_segments, region_pixels, largest_segment, segment_percent = self.connected_pixel_threshold(
            diff_img, lesspixel
        )

        roi_region = self.region_of_interest(filtered_img, padding)

        fiter_decision = self.check_filter(
                            subject,
                            roi_region,
                            changed_blobs,
                            changed_segments,
                            largest_segment,
                            segment_percent,
                            time_gap,
                        )

        # save current image into img_dic, collection time to time_dic
        self.img_dic[img_key] = blur_img.copy()
        self.time_dic[img_key] = float(stepCfg.collection_time)

        return (
            roi_region,
            changed_blobs,
            changed_segments,
            region_pixels,
            largest_segment,
            segment_percent,
            time_gap,
            fiter_decision,
        )

    def subject_filter(self, stepCfg, subjName, subject, color_sapce_image, retValue):

        # load zone image
        h, w, _ = color_sapce_image.shape
        if "zone" not in subject or not subject.zone:
            subject.zone = [0, 0, w, h]
        subject.zone = Zone.create(subjName, subject.zone, atype="crop")
        zonecolor_sapce_image = color_space_zone(
            color_sapce_image, stepCfg.color_space, subject.color_space, subject.zone
        )

        # image diff process
        roi_region, changed_blobs, changed_segments, region_pixels, largest_segment, segment_percent, time_gap, fiter_decision = self.img_subtraction(
            zonecolor_sapce_image, stepCfg, subject
        )

        retValue.active_box = roi_region
        retValue.changed_blobs = changed_blobs
        retValue.changed_segments = changed_segments
        retValue.region_pixels = region_pixels
        retValue.largest_segment = largest_segment
        retValue.segment_percent = segment_percent
        retValue.time_gap = time_gap
        retValue.fiter_decision = fiter_decision

    def run_DiffFilter(self, stepCfg):
        retValue = AD({"status": "success", "reason": None, "filedata": []})
        try:
            color_sapce_image = load_image(
                stepCfg.filedata[stepCfg.filelist[0]],
                color_space=stepCfg.color_space,
                crop=stepCfg.input_crop,
            )

            for subjName, subject in stepCfg.subjects.items():
                if subjName in retValue:
                    raise MVError(
                        "Subject name {} already present in results".format(subjName)
                    )
                self.subject_filter(stepCfg, subjName, subject, color_sapce_image, retValue)

        except Exception as err:
            msg = "DiffFilter exception: {}".format(err)
            self.logger.exception(msg)
            retValue.status = "error"
            retValue.reason = msg

        return retValue

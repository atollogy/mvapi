#!/usr/bin/env python3
from api import Api
from attribute_dict import AD
from aws import Aws
from common import *
from image.utilities import *
from mv_functions.social_distance import SocialDistanceCfg
from mv_functions.social_distance_v2 import Social_Distance_Cfg_V2
from mv_functions.tf_inference_detection import DetectionInference, _tmp_recData_debug_str
from mv_functions.tf_inference_classification import ClassificationInference
from queuing import ATLEvent
from shell import shcmd

from email import policy
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.parser import BytesParser

import asyncio
import base64
import binascii
import copy
import cv2
from datetime import date, datetime, timedelta
import functools
import http.client as httplib
import io
import iso8601
import janus
import json
import logging
import numpy as np
import os
import pyconsul as consul
import queue
import random
import re
import requests
from sanic import Sanic
from sanic.response import json as jresp
import signal
import sys
import tensorflow as tf
import threading
from threading import Thread, Lock
import time
from time import strftime
import traceback
import types
from typing import List, Dict, Optional
import uvloop


#####################################################################################################

logger = logging.getLogger("mvapi")

#####################################################################################################


class ObjectDetection(Api):
    """
    MAIN Wrapper function for entry into mvapi models
    When adding new models, functions, the following places have to be updated:

    *** git clone https://(id)@bitbucket.org/atollogy/model_cfg.git
    1. Update model version and parameter on https://bitbucket.org/atollogy/model_cfg/src/master/
        -each model parameters: https://bitbucket.org/atollogy/model_cfg/src/master/models/model_cfg/
        -dev,stg model version config: https://bitbucket.org/atollogy/model_cfg/src/master/models/envs/dev/svcs/mvapi/load_models.json
        -prd version config: https://bitbucket.org/atollogy/model_cfg/src/master/models/envs/prd/svcs/mvapi/load_models.json
    for edge devices look at: model_cfg/models/envs/dev/svcs/mvedge or model_cfg/models/envs/prd/svcs/mvedge

    When downloading new version of model by following places:
    1. cd /opt/mvapi
    2. ./download_models

    Sync s3 trained model:
    * Execute "sync_buckets.py" to copy/upload files from atl-dev--zoo to atl-demo--zoo, atl-stg--zoo, atl-prd--zoo *
    Cross check if files have been copied (there have been sporadic failures in the past).
    If files not present, copy manually and investigate further
    """

    def __init__(
        self,
        aws=None,
        bcm=None,
        model=None,
        mcfg=None,
        name=None,
        pcnt=10,
        queue=None,
        warmup=False
    ):
        Api.__init__(self, bcm=bcm)

        self.mcfg = AD(mcfg)
        self.model = model  # e.g. "friends"
        self.name = name  # e.g. "friends0"
        self.queue = queue
        self.pcnt = pcnt
        self.late_pcnt = int(pcnt/3)
        self.should_stop = False
        self.logger = logging.getLogger("mvapi")
        self.delay = 2

        # self.logger.info("new model:{}".format(self.model))
        # self.logger.info("new name:{}".format(self.name))

        self.aws = aws
        if self.aws is not None:
            self.ext_queue = self.aws.sqs(model)
            self.ext_queue_late = self.aws.sqs(model + '_late', wait=5)
            self.mv_results_queue = self.aws.sqs("mv_results")
        else:
            self.ext_queue = None
            self.ext_queue_late = None
            self.mv_results_queue = None

        self.logger.info(
            "queues info: ext_queue=%s, ext_queue_late=%s, mv_results_queue=%s, queue=%s",
            str(self.ext_queue),
            str(self.ext_queue_late),
            str(self.mv_results_queue),
            str(self.queue)
        )

        if self.mcfg.type == 'tf_faster_rcnn':
            on_edge = self.aws is None
            self.mv_function = DetectionInference(mcfg=self.mcfg)
            if self.mv_function.load_success:
                self.mv_function = self.mv_function.detect
            else:
                self.mv_function = None
            # self.mcfg.lookup_path : /data/mvapi_data/output/faster_rcnn_resnet101/friends_train/production_model/0"
        elif self.mcfg.type == 'tf_classifier':
            self.mv_function = ClassificationInference(mcfg=self.mcfg)
            if self.mv_function.load_success:
                self.mv_function = self.mv_function.classify
            else:
                self.mv_function = None

    async def forward_to_api(self, stepCfg):
        try:
            self.logger.info("mvapi forwarding file {}".format(filename))
            self.logger.info("stepCfg: {}".format(stepCfg))
            if stepCfg.gateway_id is None:
                raise self.logger.error("Gateway ID is none")


            # Remove filedata from step output
            stepCfg.output.delete("filedata")

            im = index_metadata = AD()
            im.cameraId = 0
            im.collectionTime = stepCfg.collection_time
            im.gatewayId = stepCfg.gateway_id
            im.nodename = stepCfg.nodename
            im.steps = [AD()]
            im.steps[0].name = stepCfg.step_name
            im.steps[0].function = stepCfg.function
            im.steps[0].functionVersion = stepCfg.functionVersion
            im.steps[0].configVersion = "default"
            im.steps[0].inputParameters = {}
            im.steps[0].startTime = stepCfg.start_time
            im.steps[0].endTime = stepCfg.end_time if stepCfg.end_time else datetime.utcnow().timestamp()
            im.steps[0].output = stepCfg.output # ex: output from running tiny_friends
            im.steps[0].output.cause = stepCfg.processor_role
            multipart_msg = MIMEMultipart("related")
            mime_msg = MIMEApplication(json.dumps(index_metadata), _subtype="json")
            mime_msg[
                "Content-ID"
            ] = "<%(gateway_id)s/0/%(collection_time)s/index@at0l.io>" % (stepCfg)
            multipart_msg.attach(mime_msg)
            self.logger.info("<%(gateway_id)s/0/%(collection_time)s/index@at0l.io>" % (stepCfg))
            for filename, image in stepCfg.output.filedata:
                try:
                    img = MIMEImage(image, _subtype="jpeg")
                    img[
                        "Content-Disposition"
                    ] = 'attachment; filename="{}/{}"'.format(stepCfg.step_name, filename)
                    img["Content-ID"] = (
                        "<{}/0/{}/{}/{}@at0l.io)>".format(stepCfg.gateway_id, stepCfg.collection_time, stepCfg.step_name, filename)
                    )
                    multipart_msg.attach(img)
                except Exception as err:
                    self.logger.info("image mime encode error: {}".format(err))
                    return None, 1, err

            full_msg_string = multipart_msg.as_string(
                policy=multipart_msg.policy.clone(max_line_length=None, linesep="\r\n")
            )
            boundary = multipart_msg.get_boundary()

            # remove the generated document headers, since we manually generate the request headers
            body = re.sub(
                r"^[\S\s]*?--" + re.escape(boundary), r"--" + boundary, full_msg_string
            )

            headers = {
                "Content-Type": 'multipart/related; boundary="{}"'.format(boundary),
                "customer_id": stepCfg.customer_id,
                "Gateway_Id": stepCfg.gateway_id,
                "nodename": stepCfg.nodename,
            }
            try:
                resp = await self._call_external_api(
                    url=self.BCM.endpoints.atlapi.url + "/image_reading",
                    payload=body,
                    payload_type='mime',
                    headers=headers,
                    verify="default",
                )
                self.logger.info(resp)
                if "ok" in resp:
                    return resp, None, 0, None
                else:
                    return None, resp, 1, None
            except Exception as err:
                # any exception in this block should be recoverable given proper handling of the metadata bundle
                self.logger.exception(err)
                return None, err, 1, None
        except Exception as err:
            # if some other exception is thrown and caught here, we have no recourse other than logging the event
            self.logger.exception(
                "file - {}: forward_to_api error: {}".format(stepCfg.filename, err)
            )
            return None, "ERROR: {}".format(err), 1, None

    async def prep_subjects(self, rd, subjects):
        to_process = []
        for subjName, subj in subjects:
            subjRec = AD(rd)
            subjRec.update(subj)
            subjRec.subject = subjName
            subjRec.processor_role = BCM.nodename
            if 'output' not in subjRec:
                subjRec.output = AD({'status': 'success', 'reason': None})
            if "nms_threshold" in subj:
                subjRec.nms_threshold = float(subj.nms_threshold)
            elif "nms.thresh" in subj:
                subjRec.nms_threshold = float(subj.nms_thresh)
            else:
                subjRec.nms_threshold = 0.4

            if "social_distance" in subjRec and 'version' in subjRec.social_distance:
                # self.logger.info(f"Enabling social distance v2 for {rd.nodename}")
                subjRec.sd_config = Social_Distance_Cfg_V2(**subjRec.social_distance)
                subjRec.social_enabled = subjRec.sd_config.version
            elif "social_distance" in subjRec:
                # self.logger.info(f"Enabling social distance v1 for {rd.nodename}")
                subjRec.sd_config = SocialDistanceCfg(**subjRec.social_distance)
                subjRec.social_enabled = 1
            else:
                subjRec.social_enabled = 0
                subjRec.sd_config = None

            to_process.append(subjRec)

        return to_process

    async def prep_config(self, recData):
        try:
            if "stepCfg" in recData:
                rd = recData.stepCfg
            else:
                rd = recData

            to_process = []
            frames, frame_times = None, None

            collection_time = to_datetime(rd.collection_time)
            rd.collection_time = collection_time.timestamp()
            rd.origins = rd.input_images[:] if 'input_images' in rd else []
            subjects = rd.subjects.items()
            del rd.subjects
            media_types = unique([fn.rsplit('.')[-1] for fn in rd.input_images])
            vcfg = rd.violation if "violation" in rd and isinstance(rd.violation, (AD, dict)) else None

            if 'regions' in rd and len(rd.regions):
                rd.regions = {
                    k: Zone.create(k, v, atype="region") for k, v in rd.regions.items()
                }

            # rest call with image payload
            if 'image' in rd:
                rd.image = np.asarray(bytearray(base64.b64decode(rd.image)), dtype=np.uint8)
            # queue-base request with video payload to be frame extracted
            elif rd.function == 'saferack_person' and vcfg and 'mp4' in media_types:
                fns = [fn for fn in rd.input_images if fn and 'mp4' in fn]
                if not len(fns):
                    # logger.info(f"mp4 list was zero length from: {rd.input_images}")
                    return []
                # logger.info(f"Selecting MP4s for detection: {fns}")
                seen = []
                selected = []
                for fn in fns:
                    ts = fn.rsplit('/', 1)[-1].split('_')[2]
                    if ts not in seen:
                        selected.append(fn)
                        seen.append(ts)
                for fn in selected:
                    collection_time = to_datetime(float(fn.rsplit('/', 1)[-1].split('_')[2]))
                    bucket, key = self.aws.s3.keyParts(fn)
                    if key.startswith("/"):
                        key = key[1:]
                    if 'extraction' not in rd:
                        rd.extraction = AD()
                    frames, frame_times, error = load_n_frames(await self.aws.s3.get(bucket, key),
                                                        color_space=rd.color_space,
                                                        min_fcount=rd.extraction.min_frames if 'min_frames' in rd.extraction else 12,
                                                        max_fcount=rd.extraction.max_frames if 'max_frames' in rd.extraction else 20,
                                                        min_duration=rd.extraction.min_duration if 'min_duration' in rd.extraction else 20,
                                                        # region = rd.regions.values()[0] if rd.region_filter else None,
                                                        scene_bridge=rd.extraction.scene_bridge if 'scene_bridge' in rd.extraction else 10,
                                                        scene_threshold=rd.extraction.scene_threshold if 'scene_threshold' in rd.extraction else None,
                                                        erate=rd.extraction.erate if 'erate' in rd.extraction else 3)
                    if error:
                        # logger.info(f"FRAME_EXTRACTION NOT COMPLETED: {error}")
                        continue
                    # logger.info(f"frame extracted {len(frames)}")
                    # if len(frames) != len(frame_times):
                    #     logger.info(f"FRAME_EXTRACTION LIST LENGTH MISMATCH: {len(frames)} != {len(frame_times)}")
                    logger.info(f"FRAME_EXTRACTION FRAME TIMES: {frame_times}")
                    for frame, time_offset in zip(frames, frame_times):
                        frd = AD(rd.as_dict())
                        frd.image = pack_image(frame)
                        frd.collection_time = (collection_time + timedelta(milliseconds=time_offset)).timestamp()
                        frd.bundle[1] = str(frd.collection_time)
                        frd.cid = ':'.join(frd.bundle[:5])
                        frd.scid = ':'.join(frd.bundle)
                        to_process.extend(await self.prep_subjects(frd, subjects))
                # queue-based request with standard image payload
            else:
                fn = rd.input_images[0]
                bucket, key = self.aws.s3.keyParts(fn)
                if key.startswith("/"):
                    key = key[1:]
                rd.image = np.asarray(bytearray(await self.aws.s3.get(bucket, key)), dtype=np.uint8)
                if len(rd.image):
                    to_process.extend(await self.prep_subjects(rd, subjects))

            to_process = [r for r in to_process if r.function == self.model]
            return to_process
        except Exception as err:
            self.logger.info(f"prep_data exception - {err}")
            return []

    async def prep_mv_results(self, res):
        """
        Encoding image data to be sent to atlapi happens here

        res.output.filedata has a tuple (image_type, io.BytesObject image)

        # sending images encoded in json requires images to first be base64 encodied,
        # then converted to a unicode string to be serialized into json.
        # On the other end, the data coming from the json as a string must be re-converted
        # into bytes before being base64.decoded back into its original format
        # 1) img_int8_bytes -> base64.encode(img_int8_bytes) -> image_b64_bytes
        # 2) image_b64_bytes -> decode_to(utf-8) -> image_unicode_str
        # 3) image_unicode_str -> payload_dict( key: image_unicode_str)
        # 4) json.dumps(payload_dict) -> json_unicode_str
        # 5) send to other host
        # 6) json_unicode_str -> json.parse() -> payload_dict
        # 7) payload_dict[key]=image_unicode_str -> encode_to(bytes)-> image_b64_bytes
        # 8) image_b64_bytes -> base64.decode(image_b64_bytes) = img_int8_bytes


        :param res: Modified res
        :return:
        """
        fdata = []
        for fname, image in res.output.filedata:
            fdata.append((fname, pack_image(image, b64=True, color_space=res.color_space)))
        res.output.filedata = fdata
        if 'image' in res:
            del res['image']
        return res

    def process_late_queue_messages(self, mcount=2):
        try:
            if not self.sync_run_async(self.ext_queue_late.empty(mcount=mcount)):
                late_requests = self.sync_run_async(
                    self.ext_queue_late.get_msg(mcount=mcount))
                if late_requests is not None and len(late_requests):
                    # self.logger.info(
                    #     "process_late_queue_messages {} - number of messages retrieved from late queue: {}".format(
                    #         self.model, len(late_requests)
                    #     )
                    # )
                    seen = []
                    for req in late_requests:
                        if req is not None and len(req.data.records):
                            recs = [AD(r) for r in req.data.records]
                            req.data.records = []
                            for rr in recs:
                                start = datetime.utcnow()
                                records = []
                                for r in self.sync_run_async(self.prep_config(rr)):
                                    r.source = "ext_queue"
                                    if "image" not in r:
                                        r["image"] = None
                                    records.append(
                                        self.sync_run_async(self.mv_function(r))
                                    )
                                if len(records):
                                    records = [r for r in records if isinstance(r, AD) and 'output' in r]
                                    if any(['evaluations' in r.output for r in records]):
                                        pre_vg_records = records[:]
                                        try:
                                            records = self.postprocess_violation_results(records)
                                        except Exception as err:
                                            records = pre_vg_records
                                            logger.exception(f"process_late_queue_messages postprocess exception: {repr(err)}")
                                    s3_start = datetime.utcnow()
                                    self.sync_run_async(self.save_s3_results(records))
                                    # self.logger.info(f"process_late_queue_messages total time saving {len(records)} s3 results {datetime.utcnow() - s3_start}")
                                    seen = []
                                    for rec in records:
                                        sig = (rec.customer_id, rec.gateway_id, rec.camera_id, rec.collection_time, rec.step_name)
                                        if sig in seen:
                                            logger.info(f"duplicate payload collison: {sig}")
                                            continue
                                        else:
                                            seen.append(sig)
                                        data = AD({
                                            "cid": rec.cid,
                                            "customer_id": rec.customer_id,
                                            "headers": {
                                                "Content-Type": "application/json",
                                                "gateway_id": rec.gateway_id,
                                                "nodename": rec.nodename,
                                                "customer_id": rec.customer_id,
                                            },
                                            "args": {},
                                            "records": [rec],
                                        })
                                        # self.logger.info(f"process_late_queue_messages mv_result payload size: {len(data.jstr())}")
                                        res = self.sync_run_async(self.mv_results_queue.put_msg(ATLEvent(data=data)))
                                        # self.logger.info(f"process_late_queue_messages return message to mv_result queue result: {res}")
                            # self.logger.info(f"process_late_queue_messages total time processing request {datetime.utcnow() - start}")
                        self.sync_run_async(self.ext_queue_late.delete(req))
            else:
                time.sleep(3)
        except Exception as err:
            self.logger.exception(f"process_late_queue_messages exception: {err}")

    def process_queue_messages(self, mcount=6):
        try:
            if not self.sync_run_async(self.ext_queue.empty(mcount=mcount)):
                requests = self.sync_run_async(self.ext_queue.get_msg(mcount=mcount))
                if requests is not None and len(requests):
                    # self.logger.info(
                    #     "{} - number of messages retrieved from queue: {}".format(
                    #         self.model, len(requests)
                    #     )
                    # )
                    seen = []
                    for req in requests:
                        if req is not None and len(req.data.records):
                            recs = [AD(r) for r in req.data.records]
                            req.data.records = []
                            for rr in recs:
                                start = datetime.utcnow()
                                records = []
                                for r in self.sync_run_async(self.prep_config(rr)):
                                    r.source = "ext_queue"
                                    if "image" not in r:
                                        r["image"] = None
                                    records.append(
                                        self.sync_run_async(self.mv_function(r))
                                    )
                                if len(records):
                                    records = [r for r in records if isinstance(r, AD) and 'output' in r]
                                    if any(['evaluations' in r.output for r in records]):
                                        pre_vg_records = records[:]
                                        try:
                                            records = self.postprocess_violation_results(records)
                                        except Exception as err:
                                            records = pre_vg_records
                                            logger.exception(f"process_queue_messages postprocess exception: {repr(err)}")
                                    s3_start = datetime.utcnow()
                                    self.sync_run_async(self.save_s3_results(records))
                                    # self.logger.info(f"process_queue_messages total time saving {len(records)} s3 results {datetime.utcnow() - s3_start}")
                                    seen = []
                                    for rec in records:
                                        sig = (rec.customer_id, rec.gateway_id, rec.camera_id, rec.collection_time, rec.step_name)
                                        if sig in seen:
                                            logger.info(f"duplicate payload collision: {sig}")
                                            continue
                                        data = AD({
                                            "cid": rec.cid,
                                            "customer_id": rec.customer_id,
                                            "headers": {
                                                "Content-Type": "application/json",
                                                "gateway_id": rec.gateway_id,
                                                "nodename": rec.nodename,
                                                "customer_id": rec.customer_id,
                                            },
                                            "args": {},
                                            "records": [rec],
                                        })
                                        # self.logger.info(f"process_late_queue_messages mv_result payload size: {len(data.jstr())}")
                                        res = self.sync_run_async(self.mv_results_queue.put_msg(ATLEvent(data=data)))
                                        # self.logger.info(f"process_queue_messages return message to mv_result queue result: {res}")
                            # self.logger.info(f"process_queue_messages total time processing request {datetime.utcnow() - start}")
                        self.sync_run_async(self.ext_queue.delete(req))

                    if not self.queue.sync_q.empty():
                        # USING REST Interface
                        req = self.queue.sync_q.get(block=False)
                        for r in self.sync_run_async(self.prep_config(req)):
                            r.source = "rest"
                            res = self.sync_run_async(self.mv_function(r))
                            # if res.output.status == "error":
                            #     self.logger.info("record error {}: {}".format(res.cid, res))
                        if self.BCM.scode == 'mvedge':
                            self.sync_run_async(self.forward_to_api(res))
                        else:
                            res = self.sync_run_async(self.prep_mv_rest_results(res))
                            self.sync_run_async(self.return_rest_mv_results(res))
                        self.queue.sync_q.task_done()
            else:
                time.sleep(3)
        except Exception as err:
            msg = f"objRecog.process_queue_messages exception: {repr(err)}"
            self.logger.exception(msg)

    def postprocess_violation_results(self, records):
        try:
            self.logger.info(f"postprocess_violation_results processing {len(records)} records with evaulation data")
            new_records = []
            rd = AD(records[0])
            del rd.output.evaulations
            rd.images = []
            rds = AD()
            v = AD()
            vd = AD()
            vcount = AD()
            for vgroup, vgcfg in rd.output.violation.items():
                vcount[vgroup] = 0
                vgrd = rds[vgroup] = AD(rd.as_dict())
                vgrd.bundle[-1] = f'{vgroup}_violation'
                vgrd.scid = ':'.join(vgrd.bundle)
                vgrd.step_name = f'{vgroup}_violation'
                vgrd.function = f'violation_processor'
                vgrd.violation_record = True
                gv = v[vgroup] = AD()
                gvd = vd[vgroup] = AD()
                violation_keys = AD()
                for category_name, category in vgcfg.categories.items():
                    gv[category_name] = []
                    gvd[category_name] = {violation_name: {'ctype': vdata.ctype if 'ctype' in vdata else 'individual',
                                                           'data': {},
                                                           'results': []} for violation_name, vdata in category.conditions.items()}
                    violation_keys[category_name] = [
                        violation_name for violation_name, violation in category.conditions.items()
                        if violation.enabled]

                for record in records:
                    if 'violations' not in record.output:
                        record.output.violations = AD()
                    record.output.violations[vgroup] = AD()
                    if 'violation_data' not in record.output:
                        record.output.violation_data = AD()
                    record.output.violation_data[vgroup] = AD()
                    record.output.violation_record = False
                    for category_name, violations in violation_keys.items():
                        record.output.violations[vgroup][category_name] = []
                        record.output.violation_data[vgroup][category_name] = []
                        for violation_name in violations:
                            if f'evaluations.{vgroup}.{category_name}.{violation_name}' in record.output:
                                if violation_name in record.output.evaluation_data[vgroup][category_name]:
                                    gvd[category_name][violation_name].data[str(record.collection_time).replace('.', '_')] = record.output.evaluation_data[vgroup][category_name][violation_name]
                                vstatus = bool(record.output.evaluations[vgroup][category_name][violation_name])
                                gvd[category_name][violation_name].results.append(vstatus)
                                if vstatus:
                                    record.output.violation_data[vgroup][category_name].append(violation_name)

                for category_name, violations in violation_keys.items():
                    vgc = vgcfg.categories[category_name]
                    gvc = gv[category_name]
                    gvdc = gvd[category_name]
                    for violation_name, gvdcc in gvdc.items():
                        vgcc = vgc.conditions[violation_name]
                        minc = vgcc.get('min_count', 1)
                        if 'ctype' in vgcc and vgcc.ctype == 'aggregate':
                            continue
                        else:
                            vgcc.ctype = 'individual'
                        count_indications = sum(gvdcc.results)
                        total_results = len(gvdcc.results)
                        percentage = (count_indications)/(total_results) if total_results else 0.0
                        is_over_min_count = total_results >= minc
                        is_over_threshold = percentage > (vgcc.get('threshold', 0.5) * (minc/total_results))

                        logger.info(f"indications: {count_indications}, results: {total_results}, percentage: {percentage}")

                        if is_over_min_count:
                            logger.info(f"over minimum count ({minc})")
                        in_violation = (is_over_min_count and is_over_threshold)
                        if in_violation:
                            gvc.append(violation_name)
                            vcount[vgroup] += 1

                        gvdc[violation_name] = AD({
                            "data": gvdcc.data,
                            "detections": gvdcc.results,
                            "in_violation": in_violation,
                            "indicaton_count": count_indications,
                            "is_over_min_count": is_over_min_count,
                            'is_over_threshold': is_over_threshold,
                            "min_count": vgcc.get('min_count', 1),
                            "score": percentage,
                            "threshold":  vgcc.get('threshold', 0.5),
                            "total_results": total_results
                        })

                    for violation_name, gvdcc in gvdc.items():
                        vgcc = vgc.conditions[violation_name]
                        if vgcc.ctype == 'individual':
                            continue
                        if violation_name == 'truck_alignment_violation':
                            gvdcc.data = AD(gvdc.cage_violation.data)
                            gvdcc.results = gvdc.cage_violation.detections
                            count_indications = sum(gvdcc.results)
                            total_results = len(gvdcc.results)
                            percentage = (count_indications)/(total_results) if total_results else 0.0
                            is_over_min_count = total_results >= vgcc.get('min_count', 1)
                            is_over_threshold = percentage > vgcc.get('threshold', 0.5)
                            in_violation = (is_over_min_count and is_over_threshold)

                            if in_violation:
                                gvc.append(violation_name)
                                vcount[vgroup] += 1

                            gvdc[violation_name] = AD({
                                "data": gvdcc.data,
                                "detections": gvdcc.results,
                                "in_violation": in_violation,
                                "indicaton_count": count_indications,
                                "is_over_min_count": is_over_min_count,
                                'is_over_threshold': is_over_threshold,
                                "min_count": vgcc.get('min_count', 1),
                                "score": percentage,
                                "threshold":  vgcc.get('threshold', 0.5),
                                "total_results": total_results
                            })

                logger.info(f"violation_data: {gv}")

            for vgroup, vgrd in rds.items():
                if vcount[vgroup] > 0:
                    gv = v[vgroup]
                    gvd = vd[vgroup]
                    # logger.info(f"violation summary: {gv}")
                    vgrd.output.filedata = []
                    vgrd.output.vcount = vcount[vgroup]
                    vgrd.output.violation_record = True
                    vgrd.output.violations = AD({vgroup: gv})
                    vgrd.output.violation_data = AD({vgroup: gvd})
                    vgrd.output.evaluation_data = {}

                    for record in records:
                        for category_name, violations in gv.items():
                            rvc = record.output.violations[vgroup][category_name] = [
                                violation_name
                                for violation_name in violations
                                if violation_name in record.output.violation_data[vgroup][category_name]
                            ]
                            if len(rvc):
                                has_people = (record.output.subjects.get('person', 0) > 0)
                                if has_people:
                                    vgrd.output.filedata.extend([
                                        image for image in record.output.filedata
                                        if image[0].endswith(f"{category_name}-violation.jpg")
                                    ])
                    if len(vgrd.output.filedata):
                        new_records.append(vgrd)
            records = records[:1]
            # for record in records:
                # record.output.filedata = [image for image in record.output.filedata
                #                           if not image[0].endswith(f'violation.jpg')]
            records.extend(unique(new_records))
            return records
        except Exception as err:
            logger.exception(f'VIOLATION_POSTPROCESS ERROR: {repr(err)}')
            raise

    async def return_rest_mv_results(self, step):
        _headers = {
            "customer_id": step.customer_id,
            "gateway_id": step.gateway_id,
            "nodename": step.nodename,
            "Content-Type": "application/json",
        }

        resp = await self._call_external_api(
            url=self.BCM.endpoints.atlapi.url + "/mvresult",
            payload= step,
            headers=_headers,
            verify="default",
        )

        # self.logger.debug("toMVResults - response: {}".format(resp))

    def run(self):
        self.logger.info("Object detection model {} - started".format(self.name))
        while not self.should_stop:
            try:
                #Checking if mvapi is running on the edge device.
                if self.queue:
                    if not self.aws:
                        # Note: For edge devices self.aws is None
                        # In future, if we decide to initialize self.aws for edge devices
                        # We will need another condition to differentiate between Cloud vs Edge mvapi
                        if not self.queue.sync_q.empty():
                            # USING REST Interface
                            req = self.queue.sync_q.get(block=False)
                            if req == "STOP":
                                self._stop_event.set()
                                self.stop()
                            else:
                                for r in self.sync_run_async(self.prep_config(req)):
                                    r.source = "rest"
                                    res = self.sync_run_async(self.mv_function(r))
                                    if res.output.status == "error":
                                        self.logger.info("record error {}: {}".format(res.cid, res))
                                if self.BCM.scode == 'mvedge':
                                    self.sync_run_async(self.forward_to_api(res))
                                else:
                                    res = self.sync_run_async(self.prep_mv_results(res))
                                    self.sync_run_async(self.return_rest_mv_results(res))
                                self.queue.sync_q.task_done()
                        else:
                            time.sleep(0.25)
                    else:
                        # USING AWS Queues
                        if 'backlog_processor_role' in self.BCM.svcs.mvapi and self.BCM.svcs.mvapi.backlog_processor_role:
                            self.process_late_queue_messages(mcount=self.pcnt)
                        else:
                            self.process_queue_messages(mcount=self.pcnt)
                            self.process_late_queue_messages(mcount=self.late_pcnt)
                else:
                    time.sleep(0.25)
            except Exception as err:
                self.logger.exception("objRecog run loop exception: {}".format(err))

    async def save_s3_results(self, records):
        # self.logger.info('In save_s3_results mvapi BEFORE input rd = {}'.format(rd))
        # Remove extensions if present
        for rd in records:
            if "images" not in rd:
                rd.images = []
            if 'image' in rd:
                del rd['image']
            if rd.output.status != 'error' and rd.output.filedata:
                filedata = rd.output.filedata[:]
                rd.output.filedata = []
                for img_type, image in filedata:
                    if ':' in img_type:
                        collection_time, img_type = img_type.split(':')
                        ftype, ext = img_type.rsplit('.', 1)
                        recId = f"{rd.nodename}/{rd.camera_id}/{rd.step_name}/{ftype}"
                        fName = f"{rd.gateway_id}_{rd.camera_id}_{collection_time}_{rd.step_name}_{collection_time.replace('.', '_')}_{img_type}"
                        bucket, key, path = self.aws.s3.createKey(
                            rd.customer_id, rd.function, recId, collection_time, fName
                        )
                        store_path = await self.aws.s3.put_customer_data(
                            bucket, key, pack_image(image, color_space=rd.color_space), recType=ftype, compress=False
                        )
                        rd.images.append(store_path)
                        # self.logger.info("save_s3_results image file s3 path: {}".format(rd.images[-1]))
                    else:
                        ftype, ext = img_type.rsplit('.', 1)
                        recId = f"{rd.nodename}/{rd.camera_id}/{rd.step_name}/{ftype}"
                        fName = f"{rd.gateway_id}_{rd.camera_id}_{rd.collection_time}_{rd.step_name}_{img_type}"
                        bucket, key, path = self.aws.s3.createKey(
                            rd.customer_id, rd.function, recId, rd.collection_time, fName
                        )
                        store_path = await self.aws.s3.put_customer_data(
                            bucket, key, pack_image(image, color_space=rd.color_space), recType=ftype, compress=False
                        )
                        rd.images.append(store_path)
                        # self.logger.info("save_s3_results image file s3 path: {}".format(rd.images[-1]))

    def stop(self):
        self.should_stop = True
        if self.loop.is_running():
            self.loop.stop()

    async def sync_process(self, req):
        try:
            recs = await self.prep_config(req)
            r = recs[0]
            r.source = "sync"
            res = await self.mv_function(r)
            if res and 'output.status' in res and res.output.status != "error":
                # self.logger.info("sync process success {}".format(res.cid))
                res = await self.prep_mv_results(res)
                return res
            else:
                # self.logger.exception(f"objRecog.sync_process exception: null result {res}")
                return None
        except Exception as err:
            self.logger.exception(f"objRecog.sync_process exception: {err}")
            return None

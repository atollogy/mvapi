#! /usr/bin/env python3.6

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.lines as lines
from matplotlib import path, transforms
from matplotlib import colors as mcolors
import matplotlib._color_data as mcd
plt.autoscale(enable=True, axis="both", tight=True)

from attribute_dict import AD
from common import *
from .image_functions import *
from .shapes import *

import cv2
import functools
import logging
logger = logging.getLogger('mvapi')
import math
import numpy as np
from threading import Lock
from typing import Tuple, Dict, List, Optional, Any, Union

ANNOTATION_LOCK = Lock()

CV2_FONTS = [f for f in dir(cv2) if 'FONT' in f]

def annotate(color_space_image, boxes, color_space="bgr", dpi=72):
    ANNOTATION_LOCK.acquire()
    res = do_annotation(color_space_image, boxes, color_space=color_space, dpi=dpi)
    ANNOTATION_LOCK.release()
    return res

def do_annotation(color_space_image, boxes, color_space="bgr", dpi=72):
    if not isinstance(color_space_image, np.ndarray) or color_space_image.ndim < 3:
        color_space_image = load_image(color_space_image, color_space=color_space)
    if color_space != 'rgb':
        color_space_image = color_space_convert(color_space_image, color_space, 'rgb')

    x_scale = color_space_image.shape[1]
    y_scale = color_space_image.shape[0]

    if isinstance(boxes, Zone):
        boxes = [boxes]

    for bbox in [b for b in boxes if isinstance(b, Zone)]:
        if not bbox.atype and bbox.atype_name:
            bbox.atype = annotation_types[bbox.atype_name]
        if bbox.atype.blur:
            color_space_image[bbox.y_slice, bbox.x_slice] = cv2.blur(
                color_space_image[bbox.y_slice, bbox.x_slice], bbox.atype.blur)

    # a reasonable annotated figure size
    x_fig = 800
    y_fig = 600
    scale_factor = 1.0
    if (x_scale < x_fig) and (y_scale < y_fig):
        xs = x_fig / x_scale
        ys = y_fig / y_scale
        scale_factor = min(xs, ys)
        color_space_image = cv2.resize(
            color_space_image, (0, 0), 0, scale_factor, scale_factor, cv2.INTER_LINEAR
        )

    y, x = color_space_image.shape[:2]
    x_scale = x / dpi
    y_scale = y / dpi

    figSize = (x_scale, y_scale)
    fig_shape = (y, x, 3)

    image_id = id(color_space_image)
    fig = plt.figure(num=image_id, figsize=figSize, dpi=dpi)
    ax = fig.add_axes([0.0, 0.0, 1.0, 1.0])
    ax.axis("off")
    ax.imshow(color_space_image, aspect="equal")

    if len(boxes):
        for annotation_zone in boxes:
            if isinstance(annotation_zone, Zone):
                annotate_box(ax, annotation_zone, scale_factor, y_scale)
            else:
                annotation_zone.atype = annotation_types[annotation_zone.atype_name]
                annotate_box(ax, annotation_zone, scale_factor, y_scale)

    fig.canvas.draw_idle()
    image = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8).reshape(fig_shape)
    plt.close(image_id)
    image = color_space_convert(image, 'rgb', color_space)
    return image

def annotate_box(ax, annotation_zone, scale_factor, y_scale):
    profile = annotation_zone.atype
    ax.axis(profile.axis)

    if profile.box:
        ax.add_patch(
            plt.Rectangle(
                (annotation_zone.x * scale_factor, annotation_zone.y * scale_factor),
                annotation_zone.w * scale_factor,
                annotation_zone.h * scale_factor,
                fill=profile.box_fill.mpl if profile.box_fill else False,
                edgecolor=profile.color.mpl,
                linewidth=profile.line_width,
            )
        )
    elif profile.line:
        (line_xs, line_ys) = zip(*annotation_zone.line)
        line = mpl_line(
                    line_xs * int(scale_factor),
                    line_ys * int(scale_factor),
                    linewidth=profile.line_width,
                    color=profile.color.label,
                    marker=profile.marker
                )
        line.set_label(annotation_zone.label, color=profile.text_color.label, size=profile.text_size)
        ax.add_line(line)

    if profile.text:
        ## Write the zone.label annotation
        text_size = profile.text_size + divmod(y_scale, 200)[0]
        if "text_in_box" in profile and profile.text_in_box:
            ax.text(
                annotation_zone.x * scale_factor,
                annotation_zone.y * scale_factor,
                annotation_zone.label,
                bbox=dict(
                    facecolor=profile.text_fill.mpl,
                    edgecolor=profile.color.mpl,
                    linewidth=profile.line_width,
                    alpha=profile.alpha,
                )
                if profile.text_fill
                else None,
                fontsize=text_size,
                color=profile.text_color.mpl,
            )
        else:
            x_out = annotation_zone.x + abs(profile.line_width + 1) if profile.box else annotation_zone.x
            y_out = (
                abs(annotation_zone.y - 2 * (profile.line_width + 1))
                if profile.box
                else annotation_zone.y
            )
            ax.text(
                x_out * scale_factor,
                y_out * scale_factor,
                annotation_zone.label,
                bbox=dict(
                    facecolor=profile.text_fill.mpl if profile.text_fill else False,
                    edgecolor=profile.color.mpl,
                    linewidth=profile.line_width,
                    alpha=profile.alpha,
                )
                if profile.box
                else None,
                fontsize=text_size,
                color=profile.text_color.mpl,
            )

    if annotation_zone.dset:
        (line_xs, line_ys) = zip(*annotation_zone.line)
        ax.add_line(
            lines.Line2D(
                line_xs * scale_factor,
                line_ys * scale_factor,
                linewidth=2,
                color=profile.color.mpl,
            )
        )

def build_box(o, t, l):
    box = AD({"tf": {}, "tr": {}, "bl": {}, "br": {}})
    inverse_slope = (o[0] - t[0]) / (o[1] - t[1])

    def line(x, y, l):
        p1 = AD()
        p2 = AD()
        if inverse_slope == 0:
            p1.x = x + l
            p1.y = y
            p2.x = x - l
            p2.y = y
        elif inverse_slope == math.inf:
            p1.x = x
            p1.y = y + l
            p2.x = x
            p2.y = y - l
        else:
            dx = l / math.sqrt(1 + (inverse_slope * inverse_slope))
            dy = inverse_slope * dx
            p1.x = x + dx
            p1.y = y + dy
            p2.x = x - dx
            p2.y = y - dy
        return ((p1.x, p1.y), (p2.x, p2.y))

    if o[0] > t[0]:
        box.tl, box.bl = line(t[0], t[1], l)
        box.tr, box.br = line(o[0], o[1], l)
    else:
        box.tl, box.bl = line(o[0], o[1], l)
        box.tr, box.br = line(t[0], t[1], l)
    return box

def crop_if_outside(color_space_image_shape, bbox):
    """
    if a bounding box is outside the image bounds crop it to the image edge

    Parameters
    ----------
    img_shape : shape of image in the following shape:
     [num rows, num columns, k channels]

    bbox : bounding box [x1, y1, x2, y2]

    Returns
    -------
    bounding box of object
    """
    bbox[0] = 0 if bbox[0] < 0 else bbox[0]
    bbox[1] = 0 if bbox[1] < 0 else bbox[1]
    bbox[2] = (
        color_space_image_shape[1] if bbox[2] > color_space_image_shape[1] else bbox[2]
    )  # compare width
    bbox[3] = (
        color_space_image_shape[0] if bbox[3] > color_space_image_shape[0] else bbox[3]
    )  # compare height
    return bbox

def save_annotation(rv, bb, origins, color_space):
    if not 'annotations' in rv:
        rv.annotations = AD()
    if bb.rtype not in rv.annotations:
        rv.annotations[bb.rtype] = AD()
    rv.annotations[bb.rtype][id(bb)] = bb.annotation
    rv.annotations[bb.rtype][id(bb)].origins = origins
    rv.annotations[bb.rtype][id(bb)].color_space = color_space

def save_annotations(rv, bboxes, origins, color_space):
    for bb in [b for b in bboxes if isinstance(b, Zone)]:
        save_annotation(rv, bb, origins, color_space)

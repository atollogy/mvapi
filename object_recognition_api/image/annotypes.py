
from attribute_dict import *
from common import *
from .color import *

### annotation functions  ############################################################################

annotation_types = AD()
annotation_types.anonymous_subject = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": (41,41),
        "box": True,
        "color": CT.light_green,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.light_green,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.blur_subject = AD(annotation_types.anonymous_subject)
annotation_types.box_only = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.red,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": False,
        "text_color": CT.white,
        "text_fill": CT.red,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.correction = AD(
    {
        "alpha": 1.0,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.navy_blue,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.navy_blue,
        "text_fill": CT.light_green,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.crop = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.red,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.white,
        "text_fill": CT.red,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.distance = AD(
    {
        "alpha": 1,
        "axis": "off",
        "blur": False,
        "box": False,
        "color": CT.yellow,
        "box_fill": CT.black,
        "line": True,
        "marker": 'o',
        "line_width": 4,
        "marker": 'o',
        "text": True,
        "text_color": CT.light_gray,
        "text_fill": CT.black,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.line = AD(
    {
        "alpha": 1,
        "axis": "off",
        "box": False,
        "color": CT.yellow,
        "box_fill": CT.black,
        "line": True,
        "marker": 'o',
        "line_width": 4,
        "text": True,
        "text_color": CT.light_gray,
        "text_fill": CT.black,
        "text_size": 12,
        "text_in_box": False,
    }
)
annotation_types.person = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.orange,
        "box_fill": CT.black,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.orange,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.region = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.red,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.red,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.result = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.light_green,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.light_green,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.condition = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.orange,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.orange,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.subject = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.light_green,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": CT.light_green,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.subject_no_text = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.green,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": False,
        "text_color": CT.black,
        "text_fill": CT.green,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.subject_no_box = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": False,
        "color": CT.green,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.black,
        "text_fill": False,
        "text_size": 10,
        "text_in_box": False,
    }
)
annotation_types.text = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": False,
        "color": CT.dark_blue,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.white,
        "text_fill": False,
        "text_size": 10,
        "text_in_box": True,
    }
)
annotation_types.text_in_box = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": False,
        "color": CT.dark_blue,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.white,
        "text_fill": CT.dark_blue,
        "text_size": 10,
        "text_in_box": True,
    }
)
annotation_types.zone = AD(
    {
        "alpha": 0.65,
        "axis": "off",
        "blur": False,
        "box": True,
        "color": CT.dark_blue,
        "box_fill": False,
        "line": False,
        "marker": None,
        "line_width": 2.5,
        "text": True,
        "text_color": CT.white,
        "text_fill": CT.dark_blue,
        "text_size": 10,
        "text_in_box": False,
    }
)

def make_annotation_type(
    name,
    alpha=0.65,
    axis="off",
    blur=False,
    box=True,
    color=CT.green,
    box_fill=False,
    line=False,
    marker='o',
    line_width=2.5,
    text=True,
    text_color=CT.white,
    text_in_box=False,
    text_fill=CT.green,
    text_size=10,
    force=False,
):
    if name not in annotation_types or force:
        annotation_types[name] = AD(
            {
                "alpha": alpha,
                "axis": axis,
                "blur": blur,
                "box": box,
                "color": color,
                "box_fill": box_fill,
                "line": line if not box else False,
                "marker": marker,
                "line_width": line_width,
                "text": text,
                "text_color": text_color,
                "text_fill": text_fill,
                "text_size": text_size,
            }
        )
    return annotation_types[name]

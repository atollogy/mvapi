
from attribute_dict import AD
from common import *

import cv2
import io
import logging
logger = logging.getLogger('mvapi')
import numpy as np
import os
from PIL import Image
from typing import Tuple, Dict, List, Optional, Any, Union


RGB_Color = Tuple[int, int, int]

COLOR_BLUE: RGB_Color = (0, 0, 255)
COLOR_GREEN: RGB_Color = (0, 255, 0)
COLOR_RED: RGB_Color = (255, 0, 0)
COLOR_YELLOW: RGB_Color = (255, 255, 0)
COLOR_CYAN: RGB_Color = (0, 255, 255)
COLOR_WHITE: RGB_Color = (255, 255, 255)
COLOR_LIGHT_GREEN: RGB_Color = (144, 238, 144)
COLOR_BLACK: RGB_Color = (0, 0, 0)

COLOR_PICK = {
    "BLUE": COLOR_BLUE,
    "GREEN": COLOR_GREEN,
    "RED": COLOR_RED,
    "YELLOW": COLOR_YELLOW,
    "CYAN": COLOR_CYAN,
    "WHITE": COLOR_WHITE,
    "LIGHT_GREEN": COLOR_LIGHT_GREEN,
    "BLACK": COLOR_BLACK
}

def rgb_from_color_pick(
        color: Union[str, RGB_Color],
    ) -> RGB_Color:

    # Pick RGB color value from  COLOR_PICK
    if isinstance(color, tuple):
        bgr_color = color
    else:
        color = color.upper()
        if color in COLOR_PICK:
            bgr_color = COLOR_PICK[color]
        else:
            raise Image_Utility_Exception("Undefined color={}".format(color))

    return bgr_color

def annotate_cv(
    image: np.ndarray,
    boxes: Dict[str, List[List[int]]],
    font: int = cv2.FONT_HERSHEY_SIMPLEX,
    font_color: Union[str, RGB_Color] = "GREEN",
    background_text_color: Union[str, RGB_Color] = "BLACK",
    font_size: int = 1,
    box_thickness: int = 2,
    box_color: Union[str, RGB_Color] = "BLUE",
    color_space: str = "rgb",
) -> io.IOBase:
    """
    boxes = {"label": [[x1,y1,w1,h1],[x2,y2,w2,h2]], "label_2": [[]], ...}
    """

    # Convert image to RGB color space if input image is not RGB format.
    if color_space != "rgb":
        image = color_space_convert(image, color_space, "rgb")

    # Find each RGB values for bouding box, text, text background.
    box_rgb = rgb_from_color_pick(box_color)
    font_rgb = rgb_from_color_pick(font_color)
    background_text_rgb = rgb_from_color_pick(background_text_color)

    text_front_space = 5
    background_gap = 2
    text_top_conner = 30
    annotate_img = image.copy()
    for label, bbox in boxes.items():

        if not isinstance(bbox[0], list):
            bbox = list(bbox)

        for b in bbox:
            x1 = b[0]
            y1 = b[1]
            x2 = b[0] + b[2]
            y2 = b[1] + b[3]

            # Draw bounding box from boxes coordinates.
            cv2.rectangle(annotate_img, (x1, y1), (x2, y2), box_rgb, box_thickness)

            # Put the back ground text from label.

            cv2.putText(
                annotate_img,
                label,
                (
                    x1 + text_front_space + background_gap,
                    y1 + text_top_conner + background_gap,
                ),
                fontFace=font,
                fontScale=font_size,
                color=background_text_rgb,
                thickness=2,
                lineType=cv2.LINE_AA,
            )

            # Put the label text.
            cv2.putText(
                annotate_img,
                label,
                (x1 + text_front_space, y1 + text_top_conner),
                fontFace=font,
                fontScale=font_size,
                color=font_rgb,
                thickness=2,
                lineType=cv2.LINE_AA,
            )

    # convert numpy image to bytes io format
    img_byte = Image.fromarray(annotate_img)
    export_img = io.BytesIO()
    img_byte.save(export_img, format="JPEG")

    return export_img

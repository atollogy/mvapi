#!/usr/bin/env python3

from api import Api
from attribute_dict import *
from aws import Aws
from common import *
from consul_kvs import CAD_Cache
from image.utilities import *
from log import logger, startLogger
from object_detection import ObjectDetection
from shell import shcmd

import argparse
import asyncio
import base64
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
import janus
import json
import functools

import logging
logger = logging.getLogger("mvapi")
import multiprocessing_logging
multiprocessing_logging.install_mp_handler()
import os
import pyconsul as consul
import queue
import random
import requests
from sanic import Sanic
from sanic.response import json as jresp
import signal
import sys
import threading
import time
from time import strftime
import traceback
import types

import uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

FUNCTION_VERSION = MODULE_VERSION = (
    shcmd('git log -n1 -p -- {}| grep "^commit"'.format(os.path.abspath(__file__)))[0][
        0
    ]
    .split(" ")[-1]
    .strip("\n")[0:8]
)
logger.info("{} version: {}".format(__file__, MODULE_VERSION))

app = Sanic('mvapi')
objDetect = AD({"load_list": [], "routes": {}})

#####################################################################################################
### api endpoints

class Worker_Pool(concurrent.futures.ThreadPoolExecutor):
    def __init__(self, max_workers=None, name='Worker_Pool'):
        if name is None:
            name = threading.currentThread().getName()
        concurrent.futures.ThreadPoolExecutor.__init__(self, max_workers=max_workers, thread_name_prefix=f'Worker_Pool-{name}')

worker_pool = Worker_Pool(max_workers=25)

async def getStats():
    td = {"status": "okay", "stats": {}}
    for mName, queue in objDetect.routes.items():
        if not isinstance(queue, janus.Queue):
            continue
        td["stats"][mName] = queue.async_q.qsize()
    return td


# health check endpoint
@app.route("/health")
async def health(request):
    resp = await getStats()
    return jresp(resp)


# main object detection endpoint
@app.route("/object_detection/<model>", methods=["POST"])
async def od_handler(request, model):
    recData = AD(json.loads(request.body.decode()))
    if model == 'anonymize':
        try:
            # logger.info(f"anonymize recdata: {AD({k: v for k, v in recData.items() if k != 'image'}).jstr()}")
            res = await objDetect.friends.instances.friends_sync.sync_process(recData)
            if res is not None:
                if 'image' in res:
                    del res['image']
                return jresp(res)
            else:
                return jresp(
                    {
                    "status": 300,
                    "message": f"Object Detection failed for model - {model}"
                    }
                )
        except Exception as err:
            logger.exception(f"mvapi.od_handler exception: {err}")
    else:
        await objDetect.routes[model].async_q.put(recData)
        return jresp({"message": "Object Detection queued for model: {model}"})

#####################################################################################################
### utility functions

async def load_queue(model=None, queue=None):
    blfile = "/data/mvapi_data/backlog/{}_backlog.json".format(model)
    if os.path.exists(blfile):
        logger.info(
            "Found backlog data file for model: {} - loading {}...".format(
                model, blfile
            )
        )
        try:
            async with open(blfile) as blf:
                bl_events = json.load(blf)
            logger.info(
                "Number of events in model{} backlog file: {}".format(
                    model, len(bl_events)
                )
            )
            for event in bl_events:
                event["image"] = base64.b64decode(event["image"])
                await queue.async_q.put(event)
            os.remove(blfile)
        except Exception as err:
            logger.exception("load_queue exception: {}".format(err))


async def save_queue(mdl):
    if not os.path.exists("/data/mvapi_data/backlog"):
        os.makedirs("/data/mvapi_data/backlog")

    to_save = []
    blfile = "/data/mvapi_data/backlog/{}_backlog.json".format(mdl)
    while not objDetect[mdl]["queue"].empty():
        event = await objDetect[mdl]["queue"].async_q.get()
        if event == "STOP":
            break
        event.image = base64.b64encode(event.image.tobytes()).decode("utf-8")
        to_save.apend(event)
        await objDetect[mdl]["queue"].sync_q.task_done()

    if len(to_save):
        logger.info(
            "For model {}: saving {} queued events to disk".format(mdl, len(to_save))
        )
        with open(blfile, "w") as blf:
            blf.write(json.dumps(to_save))


async def shutdown(signal, loop):
    logger.error("mvapi received signal: {} - exiting".format(signal))
    tasks = []
    for mn in objDetect.load_list:
        tasks.append(asyncio.ensure_future(save_queue(mn)))

    results = await asyncio.wait(tasks, timeout=10)

    worker_pool.shutdown(wait=False, cancel_futures=True)
    app.stop()
    sys.exit(signal)


#####################################################################################################
## spawn object recognition process threads

def spawn_models(ckv):
    load_models = AD()
    if ckv is not None and not BCM.is_local:
        _models = AD(ckv.get('_models'))
        model_cfg = _models.model_cfg
        kpath = f'regions.{BCM.cgr}.svcs.{BCM.scode}.load_models'
        if kpath in _models.envs[BCM.env]:
            models_to_load =_models.envs[BCM.env][kpath]
        else:
            models_to_load = _models.envs[BCM.env].svcs[BCM.scode].load_models
        logger.info("Loaded model info from consul")
    else:
        model_cfg = BCM.svcs[BCM.scode].model_cfg
        models_to_load = BCM.svcs[BCM.scode].load_models
        logger.info("Loaded model info from BCM")

    for mn, md in models_to_load.items():
        if mn in model_cfg:
            load_models[mn] = model_cfg[mn]
            load_models[mn].update(md)
            if 'cfg' in load_models[mn] and md['model_version'] in load_models[mn].cfg:
                load_models[mn].cfg = load_models[mn].cfg[md['model_version']]
            else:
                load_models[mn].cfg = AD()

    logger.warn('Models to load: {}'.format(load_models))

    loop = asyncio.get_event_loop()

    models_fail = []

    for model_name, model_cfg in load_models.items():
        if 'threads' in model_cfg and model_cfg.threads and model_cfg.threads > 0:
            logger.info(f"Loading {model_name} using: {model_cfg}")
            try:
                if model_cfg.model_version not in model_cfg.versions:
                    logger.exception(f'Cannot load model {model_name} version "{model_cfg.model_version}' +
                                     f'because it is not in the  approved versions - {model_cfg.versions}')
                    models_fail.append(model_name)
                    continue
                logger.warn("Constructing  model: {}".format(model_name))
                if model_name == 'friends':
                    pcnt = 10
                    model_cfg.threads = 4
                elif 'saferack' in model_name:
                    pcnt = 10
                    model_cfg.threads = 2
                else:
                    pcnt = 10
                if model_name in objDetect:
                    logger.exception(f'model_name already loaded: {model_name}')
                    continue
                objDetect[model_name] = AD()
                objDetect[model_name].aliases = [model_name]
                objDetect[model_name].aliases.extend(model_cfg.aliases)
                objDetect[model_name].instances = AD()
                objDetect[model_name].threads = []
                objDetect[model_name].queue = janus.Queue()

                for a in objDetect[model_name].aliases:
                    objDetect.routes[a] = objDetect[model_name].queue

                fut = asyncio.ensure_future(
                    load_queue(model=model_name, queue=objDetect[model_name].queue), loop=loop
                )
                asyncio.wait_for(fut, timeout=5, loop=loop)
                logger.info(objDetect[model_name].keys())
                objDetect.load_list.append(model_name)
                for t in range(1, model_cfg.threads + 1):
                    model_inst_name = model_name + f'_thread_{t}'
                    logger.info(f"model thread name: {model_inst_name}")
                    objDetect[model_name].instances[model_inst_name] = ObjectDetection(
                            model=model_name,
                            mcfg=model_cfg,
                            aws=AWS,
                            bcm=BCM,
                            name=model_inst_name,
                            pcnt=pcnt,
                            queue=objDetect[model_name].queue
                        )

                if model_name == 'friends' and 'friends_sync' not in objDetect[model_name].instances:
                    objDetect[model_name].instances['friends_sync'] = ObjectDetection(
                            model=model_name,
                            mcfg=model_cfg,
                            aws=AWS,
                            bcm=BCM,
                            name='friends_sync',
                            pcnt=0,
                            queue=None
                        )
            except Exception as err:
                logger.info("Error loading model: {} - {}".format(model_name, err))
        else:
            logger.info(f"Model {model_name} is configured not to load")

    workers = unique(*tuple([objDetect[mn].instances.values() for mn in objDetect.load_list]))
    running_workers = []
    for w in workers:
        running_workers.append(worker_pool.submit(w.run))
        logger.info(f"Started worker: {w.name}")
    return running_workers


#####################################################################################################
### main configuration


def setup_signal_handling():
    loop = asyncio.get_event_loop()
    for sig in [
        "SIGKILL",
        "SIGTERM",
        "SIGHUP",
        "SIGUSR1",
        "SIGUSR2",
        "SIGSEGV",
        "SIGSTOP",
        "SIGPWR",
        "SIGQUIT",
    ]:
        try:
            signum = getattr(signal, sig)
            loop.add_signal_handler(
                signum,
                functools.partial(
                    asyncio.ensure_future, functools.partial(shutdown, signum, loop)
                ),
            )

        except:
            logger.info("Error - skipping signal: {}".format(sig))


def load_config():
    ### load bcm metadata
    CACHE = {"gwid": {}, "_customerModel": {}}

    try:
        CWD = os.path.dirname(os.path.realpath(__file__))
        with open("{}/version.json".format(CWD)) as vf:
            VERSION = json.load(vf)["version"]

        parser = argparse.ArgumentParser(description="atollogy api server")
        parser.add_argument(
            "-l", "--log_level", default=None, help="set logging level (ex: 10, 20)"
        )
        parser.add_argument("-H", "--HELP", help="Display extended help documentation")

        ARGS = parser.parse_args()

        ### Set initial logging level and start logging
        if ARGS.log_level is not None:
            logger = startLogger(int(ARGS.log_level))
        elif "log_level" in BCM.svcs.mvapi:
            logger = startLogger(BCM.svcs.mvapi.log_level)
        else:
            logger = startLogger(10)

        if BCM.scode != 'mvedge':
            ### set up consul
            CONSUL_HOST = "consul.{}.at0l.io".format(BCM.env)
            CONSUL_PORT = BCM.endpoints.consul.port
            CKV = consul.Consul(
                host=CONSUL_HOST, port=CONSUL_PORT, scheme="https", verify=False
            ).kv

            # # load caches
            # for cgr in BCM.cgrsActive:
            #     CACHE["gwid"][cgr] = CAD_Cache(prefix=f"_nodes/{cgr}/by_gwid")
            #     if "_customerModel" in BCM.local and cgr in BCM.local._customerModel:
            #         CACHE["_customerModel"][cgr] = CAD_Cache(prefix=f"_customerModel/{cgr}/devices/gateways")
            #     else:
            #         CACHE["_customerModel"][cgr] = CAD_Cache(prefix=f"_customerModel/{cgr}/devices/gateways")

            AWS = Aws(
                env=BCM.env,
                cgr=BCM.cgr,
                ckv=CKV,
                cache=None,
                mvapi_models=BCM.svcs['mvapi'].load_models.keys() + ["mv_results"],
            )
        else:
            AWS = None
            CKV = None

        return AWS, None, CKV, VERSION

    except Exception as err:
        logger.info("MVAPI startup exception: {} - Exiting".format(err))
        sys.exit(1)


#####################################################################################################
if __name__ == "__main__":
    AWS, CACHE, CKV, VERSION = load_config()

    setup_signal_handling()

    running_workers = spawn_models(CKV)

    logger.info(
        "##################### Object Recognition Server starting - {} #####################".format(
            str(datetime.utcnow())
        )
    )

    app.run(host="0.0.0.0", port=BCM.endpoints.mvapi.port)

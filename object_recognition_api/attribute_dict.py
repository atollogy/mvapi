#!/usr/bin/python3

import collections
import copy
from datetime import datetime, date, timedelta, timezone
from functools import reduce, partial
import hashlib
import os
import re
import json
import sys
import time
import uuid

class BaseBaseString(type):
    def __instancecheck__(cls, instance):
        return isinstance(instance, (bytes, str))

    def __subclasshook__(cls, thing):
        # TODO: What should go here?
        raise NotImplemented

def with_metaclass(meta, *bases):
    class metaclass(meta):
        __call__ = type.__call__
        __init__ = type.__init__

        def __new__(cls, name, this_bases, d):
            if this_bases is None:
                return type.__new__(cls, name, (), d)
            return meta(name, bases, d)

    return metaclass('temporary_class', None, {})

class basestring(with_metaclass(BaseBaseString)):
    pass

def _recursive_repr(item):
    """Hack around python `repr` to deterministically represent dictionaries.
    This is able to represent more things than json.dumps, since it does not require things to be JSON serializable
    (e.g. datetimes).
    """

    if isinstance(item, basestring):
        result = str(item)

    elif isinstance(item, list):
        result = '[{}]'.format(', '.join([_recursive_repr(x) for x in item]))

    elif isinstance(item, (dict, AD, CAD)):
        kv_pairs = [
            '{}: {}'.format(_recursive_repr(k), _recursive_repr(item[k]))
            for k in sorted(item)
        ]
        result = '{' + ', '.join(kv_pairs) + '}'
    else:
        result = repr(item)
    return result


def get_hash(item):
    repr_ = _recursive_repr(item).encode('utf-8')
    return hashlib.md5(repr_).hexdigest()

def get_hash_int(item):
    return int(get_hash(item), base=16)

def escape_chars(text, chars):
    """Helper function to escape uncomfortable characters."""
    text = str(text)
    chars = list(set(chars))

    if '\\' in chars:
        chars.remove('\\')
        chars.insert(0, '\\')

    for ch in chars:
        text = text.replace(ch, '\\' + ch)

    return text


class JSONEncoder(json.JSONEncoder):
    """JSONEncoder to handle ``datetime`` and other problematic object values"""

    class EncodeError(Exception):
        """Raised when an error occurs during encoding"""

    def default(self, obj):
        try:
            if isinstance(obj, (datetime)):
                return obj.timestamp()
            elif isinstance(obj, bytes):
                return self.default(obj.decode("utf-8"))
            elif isinstance(obj, list):
                return [json.JSONEncoder.default(self, x) for x in obj]
            elif isinstance(obj, tuple):
                return tuple([json.JSONEncoder.default(self, x) for x in obj])
            elif hasattr(obj, "toJSON"):
                return obj.toJSON()
            elif hasattr(obj, "jstr"):
                return obj.jstr()
            else:
                try:
                    encoded_obj = json.JSONEncoder.default(self, obj)
                except Exception:
                    encoded_obj = _recursive_repr(obj)
                return encoded_obj
        except Exception:
            return _recursive_repr(obj)


### json object serializer
def json_safe(obj):
    """JSON dumper for objects not serializable by default json code"""
    return json.dumps(obj, cls=JSONEncoder, default=str, indent=4, separators=(",", ": "), sort_keys=True)



########################################################################################################
# AD - Persistent Attribute Accessible Dict Class
########################################################################################################

class Attribute_Dict_Exception(Exception):
    def __init__(self, message, payload=None):
        Exception.__init__(self)
        self.message = message
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

class Attribute_Dict_Error(Attribute_Dict_Exception):
    def __init__(self, message, payload=None):
        Attribute_Dict_Exception.__init__(self, message, payload)

def _to_x(d, tgt=None):
    if tgt is None:
        return d
    elif isinstance(d, tgt):
        return d
    elif hasattr(d, "items"):
        td = tgt()
        for k, v in d.items():
            k = str(k)
            if isinstance(k, bytes):
                k = k.decode()
            else:
                k = str(k)
            td[k] = _to_x(v, tgt=tgt)
        return td
    elif isinstance(d, list) and d.__class__ == 'list':
        return [_to_x(v, tgt=tgt) for v in d]
    return d

class AD(dict):
    meta = {}
    consul_value_sig = sorted(["CreateIndex", "ModifyIndex", "LockIndex", "Flags", "Key", "Value", "Session"])

    def __init__(self, *args, **kwargs):
        dict.__init__(self)

        if len(kwargs):
            if 'persistTGT' in kwargs:
                self.set_file_persistence(kwargs['persistTGT'], flush=kwargs['flush'] if 'flush' in kwargs else False)
                del kwargs['persistTGT']
            for item in kwargs.items():
                self.update(item)

        for a in args:
            if a:
                self.update(a)

    def __add__(self, tgt):
        tmp = AD()
        tmp.update(self)
        tmp.update(tgt)
        return tmp

    def __iadd__(self, tgt):
       self.update(tgt)

    def __cmp__(self, other):
        return id(self) == id(other)

    def __contains__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            if "." in key:
                path, key = key.split(".", 1)
                if path in dict.keys(self):
                    return key in dict.__getitem__(self, path)
                else:
                    return False
            else:
                return key in dict.keys(self)
        except Exception:
            return False

    has_key = __contains__

    def __delattr__(self, key):
        try:
            self.__delitem__(key)
        except:
            raise AttributeError(key)

    def __delitem__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            if "." in key:
                path, key = key.split(".", 1)
                del dict.__getitem__(self, path)[key]
            else:
                dict.__delitem__(self, key)
        except KeyError:
            pass

    def __deepcopy__(self):
        return AD(self.deep_items())
    copy = __deepcopy__

    def __getattr__(self, key):
        try:
            return self.__getitem__(key)
        except:
            raise AttributeError(key)

    def __getitem__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            if '.' in key:
                path, key = key.split('.', 1)
                return dict.__getitem__(self, path)[key]
            else:
                return dict.__getitem__(self, key)
        except:
            raise KeyError(key)

    def __hash__(self):
        return id(self)

    def __iter__(self):
        for k in self.keys():
            yield k

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        if isinstance(key, bytes):
            key = key.decode()
        else:
            key = str(key)
        value = _to_x(value, tgt=AD)
        if "." in key:
            path, key = key.split(".", 1)
            if isinstance(self.setdefault(path, AD()), (AD, dict)):
                if isinstance(dict.__getitem__(self, path), dict):
                    dict.__setitem__(self, path,  _to_x(dict.__getitem__(self, path), tgt=AD))
            else:
                dict.__setitem__(self, path, AD())
            dict.__getitem__(self, path).__setitem__(key, value)
        else:
            dict.__setitem__(self, key, value)

    def __setstate__(self, state):
        self.update(state)

    def __myself__(self, me, params=None):
        my_id = id(me)
        if my_id not in AD.meta:
            AD.meta[my_id] = {}
            if params and isinstance(params (dict, AD)):
                AD.meta[my_id].update(params)
        return AD.meta[my_id]

    @staticmethod
    def _deep_items(d):
        """Recursive item iterator"""
        _items = []
        if not hasattr(d, 'items'):
            return None
        for k, v in dict.items(d):
            if k == "__dict__":
                continue
            if hasattr(v, 'items'):
                _items.extend(
                    [(f'{k}.{str(_k)}', _v) for _k, _v in AD._deep_items(v)]
                )
            else:
                _items.append((k, v))
        return iter(sorted(_items, key=lambda x: x[0]))

    @staticmethod
    def _deep_keys(d):
        """Recursive key iterator"""
        kvs = []
        if not hasattr(d, 'items'):
            return []
        for k, v in dict.items(d):
            if k == '__dict__':
                continue
            elif hasattr(v, 'items'):
                kvs.extend(
                    [f'{k}.{str(_k)}' for _k in AD._deep_keys(v)]
                )
            else:
                kvs.append(k)
        return sorted(kvs)

    def _json_safe(self):
        """JSON dumper for objects not serializable by default json code"""
        return json.dumps(self, cls=JSONEncoder, default=str, indent=4, separators=(",", ": "), sort_keys=True)

    @staticmethod
    def _jvalue(value):
        try:
            if isinstance(value, bytes):
                return json.loads(value.decode())
            elif isinstance(value, str):
                return json.loads(value)
            else:
                return value
        except KeyError:
            print('Missing required key "Value"')
            return value
        except json.JSONDecodeError:
            return value

    def as_dict(self):
        return to_dict(self)

    def clear(self):
        [dict.__delitem__(self, key) for key in self.keys()]

    def delete(self, key):
        self.__delitem__(key)

    def deep_items(self):
        return AD._deep_items(self)
    deepItems = deep_items

    def deep_keys(self):
        return AD._deep_keys(self)
    deepKeys = deep_keys

    def delete_keys(self, keys):
        for k in keys:
            self.delete(k)

    def dumps(self):
        return self.jstr()

    def get(self, key, default=None):
        try:
            if default:
                return self.setdefault(key, default=default)
            elif AD.__contains__(self, key):
                return self.__getitem__(key)
            else:
                return None
        except Exception:
            return None

    def getlike(self, partial_key, multi=False):
        kmatch = re.compile(partial_key)
        if multi:
            res = AD()
            for key in self.deep_keys():
                if kmatch.match(key):
                    res[key] = self.__getitem__(key)
            return res
        else:
            for key in self.deep_keys():
                if kmatch.match(key):
                    return self.__getitem__(key)

    def items(self):
        return [(k, v) for k, v in dict.items(self) if k != "__dict__"]

    def iteritems(self):
        for k, v in self.items():
            yield (k, v)

    def iterkeys(self):
        return self.__iter__()

    def itervalues(self):
        for _, v in self.items():
            yield v

    def jstr(self):
        return self._json_safe()
    _for_json = jstr
    to_json = jstr

    def keys(self):
        return list(dict.keys(self))

    @classmethod
    def load(kls, path):
        """Reads json in from a file"""
        if os.path.exists(path):
            with open(path, "r") as fh:
                return kls.loads(fh.read())
        else:
            raise IOError(f"Path does not exists {path}")

    @classmethod
    def loads(kls, jstr):
        """Reads parses str_in as json, and updates from results """
        if isinstance(jstr, bytes):
            tgt = json.loads(jstr.decode('utf-8'))
        elif isinstance(jstr, str):
            tgt = json.loads(jstr)
        else:
            tgt = json.loads(str(jstr))
        if isinstance(tgt, dict):
            tgt = kls(tgt)
        return tgt

    def pop(self, key):
        value = self.__getitem__(key)
        self.__delitem__(key)
        return (key, value)

    def setdefault(self, key, default):
        try:
            return self.__getitem__(key)
        except KeyError:
            self.__setitem__(key, default)
            return default

    def set_file_persistence(self, path, flush=False):
        """Sets path for persistent json store"""
        myself = self.__myself__(self)
        if path in ['.', '..', './', '/']:
            raise IOError(f"attribute_dict.set_file_persistence exception invalid path: {path}")
        myself['persistence'] = {}
        myself['persistence']['mode'] = 'file'
        myself['persistence']['path'] = path
        myself['persistence']['fname'] = os.path.basename(path)
        myself['persistence']['dir'] = os.path.dirname(path)
        if not os.path.exists(myself['persistence']['dir']):
            os.system(f"mkdir -p {myself['persistence']['dir']}")
        if flush:
            with open(myself['persistence']['path'], 'w') as fh:
                fh.writelines(["{}"])
        if os.path.exists(myself['persistence']['path']):
            self.update(self.load(myself['persistence']['path']))

    setpersist = set_file_persistence

    def sync(self):
        """Writes text rendering of self to a file"""
        myself = self.__myself__(self)
        if (myself and
            'persistence' in myself and
            'mode' in myself['persistence'] and
             myself['persistence']['mode'] == 'file'):
            os.system(f"mkdir -p {myself['persistence']['dir']}")
            with open(myself['persistence']['path'], "w") as pf:
                pf.write(self.jstr())

    def to_dict(self):
        return to_dict(self)

    def update(self, item):
        if not item:
            return
        elif hasattr(item, 'keys') and hasattr(item, 'items'):
            for k, v in item.items():
                self.__setitem__(k,  v)
        elif isinstance(item, tuple) and len(item) == 2:
            self[item[0]] = item[1]
        elif (isinstance(item, list) and item.__class__ == 'list') or hasattr(item, '__next__'):
            for i in [e for e in item if len(e) == 2]:
                self.update(i)
        elif isinstance(item, bytes):
            try:
                if item[0] == b"[":
                    self.update(json.loads(item.decode("utf-8")))
                if item[0] == b'"' and len(item) > 4:
                    self.update(json.loads(item[1:-1].decode()))
                if len(item) > 0:
                    self.update(json.loads(item.decode()))
            except json.JSONDecodeError:
                pass
        elif isinstance(item, str):
            try:
                if item.startswith('''b'"'''):
                    self.update(json.loads(item[3:-3]))
                elif os.path.exists(item):
                    with open(item) as fh:
                        self.update(json.load(fh))
                else:
                    self.update(json.loads(item))
            except json.JSONDecodeError:
                pass

    def values(self):
        return list(self.itervalues())


class CAD(AD):
    """
    Dictionary subclass enabling attribute lookup/assignment of keys/values.

    kwargs is leveraged to configure  persistence behavior.
        AWS S3 based persistence
            To use aws s3 persistence, you must call the classmethod AD.set_s3_mgr to set the client up in the classes scope.
            This removes the burden of passing the s3 client object all over the code base.  This is typically set as close to
            module import time as possible.
            - kwargs:
                bucket: str = None -> "atl-<env>-<cgr>-<bucket-type>"
                can_s3_persist: bool = False -> autopopulated
                key_path: str = None -> "/<namespace>/some/additional/structure"
                open: bool = True -> used when initially creating a versioned resource (means pre-persisted state)
                s3_uri: str = None -> "s3://atl-<env>-<cgr>-<bucket-type>/<namespace>/some/additional/structure"
                s3_metadata: dict = {} -> hash of metadata to be stored as customer attributes with object
                                          limited to 2KB in size.  A helper function will compress/decompress values if the
                                          hash size exceeds the limit.
                tnumber: int = <integer timestamp or tnumber object -> tnumbers are the major version identifier value used in placing
                                                                       objects on a timeline.  <int major version>_<float minor version>
                tstamp: float = timestamp -> used with tnumber as minor version identifier

        Filesystem based persistence kwargs:
            older local filesystem persistence functionality writing json data to disk.
            - kwargs:
                dir: str = None -> "/filesystem/path" - auto populated from persistTGT
                flush: bool = False -> set to True to zero an existing persisted json object
                fname: str = None ->  "filename.ext" - auto populated from persistTGT
                persistTGT:  str = None -> "/local/filesystem/path"  if the file exists, load it, otherwise initialize it
    """
    consul_value_sig = sorted(["CreateIndex", "ModifyIndex", "LockIndex", "Flags", "Key", "Value", "Session"])
    meta = AD({'default': {
                    'persistence': {
                        'file': {
                            'dir': None,
                            'flush': None,
                            'fname': None,
                            'path': None
                        },
                        'locked': False,
                        'mode': False,
                        's3': {
                            'bucket': None,
                            'key_path': None,
                            'metadata': {
                                'tnumber': None,
                                'tstamp': None
                            },
                            'version': None,
                            'uri': None
                        },
                    },
                    'subscriptions': {}
                    }
            })
    s3_mgr = None

    def __init__(self, *args, **kwargs):
        AD.__init__(self)
        myself = self.__myself__(self)

        if len(kwargs):
            if 'persistTGT' in kwargs:
                self.set_file_persistence(kwargs['persistTGT'], flush=kwargs['flush'] if 'flush' in kwargs else False)
                del kwargs['persistTGT']
            elif 's3_params' in kwargs:
                self.set_s3_persistence(**kwargs['s3_params'])
                del kwargs['s3_params']
            if 'callbacks' in kwargs:
                for cb, key_path in kwargs['callbacks']:
                    self.register_callback(cb, key_path)
                del kwargs['callbacks']
            if 'callback' in kwargs:
                if isinstance(kwargs['callback'], tuple):
                    self.register_callback(kwargs['callback'][0], kwargs['callback'][1])
                else:
                    self.register_callback(kwargs['callback'], any)
                del kwargs['callback']
            if len(kwargs):
                self.update(kwargs.items())
            if not myself.persistence.s3.version:
                for a in args:
                    self.update(a)
        else:
            for a in args:
                self.update(a)

    def __delitem__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            key = key.replace("/", ".")
            key = key.replace("..", ".")
            if key[0] == ".":
                key = key[1:]
            if key[-1] == ".":
                key = key[:-1]
            if "." in key:
                path, key = key.split(".", 1)
                del dict.__getitem__(self, path)[key]
            else:
                dict.__delitem__(self, key)
        except KeyError:
            pass

    def __getattr__(self, key):
        try:
            return self.__getitem__(key)
        except:
            raise AttributeError(key)

    def __getitem__(self, key):
        try:
            if isinstance(key, bytes):
                key = key.decode()
            else:
                key = str(key)
            key = key.replace("/", ".")
            key = key.replace("..", ".")
            if key[0] == ".":
                key = key[1:]
            if key[-1] == ".":
                key = key[:-1]
            if '.' in key:
                path, key = key.split('.', 1)
                return dict.__getitem__(self, path)[key]
            else:
                return dict.__getitem__(self, key)
        except:
            raise KeyError(key)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        myself = self.__myself__(self)
        if isinstance(key, bytes):
            key = key.decode()
        else:
            key = str(key)
        key = key.replace("/", ".")
        key = key.replace("..", ".")
        if key[0] == ".":
            key = key[1:]
        if key[-1] == ".":
            key = key[:-1]
        value = _to_x(value, tgt=CAD)
        if "." in key:
            path, key = key.split(".", 1)
            if isinstance(self.setdefault(path, CAD()), (AD, CAD, dict)):
                if isinstance(dict.__getitem__(self, path), dict):
                    dict.__setitem__(self, path,  _to_x(dict.__getitem__(self, path), tgt=CAD))
            else:
                dict.__setitem__(self, path, CAD())
            dict.__getitem__(self, path).__setitem__(key, value)

        else:
            dict.__setitem__(self, key, value)

        if key in myself['subscriptions'] and len(myself['subscriptions'][key]):
            self.__notify__(key, value)

    def __myself__(self, me, params=None):
        my_id = id(me)
        if my_id not in CAD.meta:
            CAD.meta[my_id] = AD(CAD.meta.default)
            if params and isinstance(params (dict, AD, CAD)):
                CAD.meta[my_id].update(params)
        return CAD.meta[my_id]

    def __notify__(self, key, value):
        myself = self.__myself__(self)
        for k in [any, key]:
            if k in myself['subscriptions']:
                for cb in myself['subscriptions'][k]:
                    if hasattr(cb, '__call__'):
                        cb(*(key, value))

    @staticmethod
    def _ckvSig(rec):
        if hasattr(rec, "keys"):
            if len([k for k in rec.keys() if k in CAD.consul_value_sig]) >= 5 and "Key" in rec and "Value" in rec:
                return True
        return False

    def register_callback(self, cb, key_path):
        myself = self.__myself__(self)
        if key_path not in myself['subscriptions']:
            myself['subscriptions'][key_path] = [cb]
        elif cb not in myself['subscriptions'][key_path]:
            myself['subscriptions'][key_path].append(cb)

    def remove_callback(self, key_path, cb):
        myself = self.__myself__(self)
        if myself:
            if key_path in myself['subscriptions']:
                if cb in myself['subscriptions']:
                    myself['subscriptions'][key_path].remove(cb)

    def replace(self, replacement):
        self.clear()
        self.update(replacement)

    def set_file_persistence(self, path, flush=False):
        """Sets path for persistent json store"""
        myself = self.__myself__(self)
        myself['persistence.mode'] = 'file'
        myself['persistence.file.path'] = path
        myself['persistence.file.fname'] = os.path.basename(path)
        myself['persistence.file.dir'] = os.path.dirname(path)
        if not os.path.exists(myself.persistence.file.dir):
            os.system(f"mkdir -p {myself.persistence.file.dir}")
        if flush:
            with open(myself.persistence.file.path, 'w') as fh:
                fh.writelines(["{}"])
        if os.path.exists(myself.persistence.file.path):
            self.update(self.load(myself.persistence.file.path))

    setpersist = set_file_persistence

    def set_s3_mgr(self, s3_mgr):
        if CAD.s3_mgr is None:
            CAD.s3_mgr = s3_mgr

    def set_s3_persistence(self, bucket=None, key=None, metadata={}, uri=None):
        myself = self.__myself__(self)
        myself['persistence.mode'] = 's3'
        if bucket and key:
            myself['persistence.s3.bucket'] = bucket
            myself['persistence.s3.key'] = key
            myself['persistence.s3.uri'] = f"s3://{bucket}/{key}"
        else:
            myself['persistence.s3.uri'] = uri
            bucket, key = uri.split("://")[-1].split("/", 1)
            myself['persistence.s3.bucket'] = bucket
            myself['persistence.s3.key'] = f"/{key}"
        myself['persistence.s3.metadata'].update(metadata)
        if not myself['persistence.s3.metadata'].tnumber:
            myself['persistence.s3.metadata'].tnumber = int(time.time())
        if not myself['persistence.s3.metadata'].tstamp:
             myself['persistence.s3.metadata.tstamp'] = time.time()
        self.s3_load()

    def s3_load(self):
        myself = self.__myself__(self)
        if CAD.s3_mgr:
            try:
                myself['persistence.s3.version'] = CAD.s3_mgr.exists(myself['bucket'], myself['key_path'])
                if myself.persistence.s3.version:
                    myself['s3_metadata'] = CAD.s3_mgr.get_metadata(
                        myself.persistence.s3.bucket,
                        myself.persistence.s3.key
                    )
                    self.update(
                        CAD.s3_mgr.get(
                                    myself.persistence.s3.bucket,
                                    myself.persistence.s3.key
                                )
                            )
            except Exception as err:
                return Attribute_Dict_Exception(f"CAD.s3_load exception {myself.persistence.s3.uri}: {err}")
        else:
            return Attribute_Dict_Exception(f"CAD.s3_load error: s3_mgr has not been set")

    def sync(self):
        """Writes text rendering of self to a file"""
        myself = self.__myself__(self)
        if myself:
            if myself.persistence.mode == 'file':
                self.sync_file()
            elif myself.persistence.mode == 's3':
                self.sync_s3()

    def sync_file(self):
        myself = self.__myself__(self)
        if myself.persistence.mode == 'file':
            os.system(f"mkdir -p {myself.persistence.file.dir}")
            with open(myself.persistence.file.path, "w") as pme:
                pme.write(self.jstr())

    def sync_s3(self, force=False):
        myself = self.__myself__(self)
        try:
            if myself.persistence.mode == 's3' and not myself.persistence.s3.version or force:
                myself['persistence.s3.version'] = CAD.s3_mgr.put(
                    myself.persistence.s3.bucket,
                    myself.persistence.s3.key,
                    self.jstr(),
                    metadata=myself.persistence.s3.metadata
                    )
        except Exception as err:
            raise Attribute_Dict_Exception(f"AD.persist exception {myself['s3_uri']}: {err}")

    def update(self, item):
        if not item:
            return
        elif CAD._ckvSig(item):
            key = item["Key"].replace("/", ".")
            self.__setitem__(key, CAD._jvalue(item["Value"]))
        elif hasattr(item, 'keys') and hasattr(item, 'items'):
            for k, v in item.items():
                self.__setitem__(k,  v)
        elif isinstance(item, tuple) and len(item) == 2:
            self[item[0]] = item[1]
        elif (isinstance(item, list) and item.__class__ == 'list') or hasattr(item, '__next__'):
            for i in [e for e in item if len(e) == 2]:
                self.update(i)
        elif isinstance(item, bytes):
            try:
                if item[0] == b"[":
                    self.update(json.loads(item.decode("utf-8")))
                if item[0] == b'"' and len(item) > 4:
                    self.update(json.loads(item[1:-1].decode()))
                if len(item) > 0:
                    self.update(json.loads(item.decode()))
            except json.JSONDecodeError:
                pass
        elif isinstance(item, str):
            try:
                if item.startswith('''b'"'''):
                    self.update(json.loads(item[3:-3]))
                elif os.path.exists(item):
                    with open(item) as fh:
                        self.update(json.load(fh))
                else:
                    self.update(json.loads(item))
            except json.JSONDecodeError:
                pass

    def values(self):
        return list(AD.itervalues(self))

ConsulAD = CAD

to_ad = partial(_to_x, tgt=AD)
to_cad = partial(_to_x, tgt=CAD)
to_dict = partial(_to_x, tgt=dict)

def parse_name(s3_img_loc):
    """
    parse file name to build json response

    Parameters
    ----------
    image_name: name of image

    Returns
    -------
    sensor_id : Cam ID - 1,2,3.. from key name
    timestamp : Unix timestamp in key name
    recorder_name : recorder name grabbed from s3 path
    recorder_id : Gateway id from key name 
    cause : From key name. Can be snapshot or "00" 
    """
    s3_img_loc = s3_img_loc.strip("/")
    image_name = s3_img_loc.split("/")[-1]
    image_id = image_name.replace(".jpg", "")

    try:
        sensor_id = image_id.split("_")[1].replace("CAM", "")
    except:
        print("sensor id not found. Defaulting to None")
        sensor_id = None
    try:
        #  splitting on the - only necessary for < 16/06/17
        timestamp = image_id.split("_")[2].split("-")[0]
    except:
        print("timestamp not found. Defaulting to None")
        timestamp = None
    try:
        recorder_id = image_id.split("_")[0]
    except:
        print("recorder id not found. Defaulting to None")
        recorder_id = None
    try:
        #  splitting on the - only necessary for < 16/06/17
        cause = image_id.split("_")[-1].split("-")[-1]
    except:
        print("cause not found. Defaulting to None")
        cause = None
    try:
        recorder_name = s3_img_loc.split("/")[2]
    except:
        print("cause not found. Defaulting to None")
        recorder_name = None
    return sensor_id, timestamp, recorder_name, recorder_id, cause

#!/usr/bin/env python3

from attribute_dict import *
from common import *
import asyncio
from asyncio import ensure_future
import pyconsul as consul
import json
import logging
import os
from pprint import pprint as pp
import pyconsul as consul
import requests
from threading import Thread
import time
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger("mvapi")

class Consul_Monitor_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class Consul_Monitor(Thread):
    def __init__(self, cad_cache=None, prefix=None, read_write=False):
        Thread.__init__(self)
        self.cad_cache = cad_cache
        self.index = None
        self.prefix = prefix
        if self.prefix[0] == '/':
            self.prefix = self.prefix[:]
        if self.prefix[-1] == '/':
            self.prefix = self.prefix[:-1]
        self.read_write = read_write
        self.ckv = consul.Consul(host=f"consul.{BCM.env}.at0l.io", port=BCM.endpoints.consul.port, scheme="https", verify=False).kv
        self.cad_cache.update(self.ckv.get(self.prefix))

    def cancel(self, task):
        self.join()

    def stop(self):
        self.join()

    def run(self):
        self.watch()

    def cache_is_dirty(self):
        start = time.time()
        try:
            if self.index is None:
                try:
                    res = requests.get(f'https://consul.{BCM.env}.at0l.io/v1/kv/{self.prefix}?recurse=false')
                except Exception as err:
                    logger.exception(f"consul_cache.cache_is_dirty exception: {err}")
                    time.sleep(30)
                else:
                    self.index = int(res.raw.headers['X-Consul-Index'])
                    return True
            try:
                res = requests.get(f'https://consul.{BCM.env}.at0l.io/v1/kv/{self.prefix}?recurse=false&index={self.index}&wait=5m',timeout=300)
            except requests.exceptions.ReadTimeout:
                return False
            else:
                self.index = int(res.raw.headers['X-Consul-Index'])
                return True
        except Exception as err:
            logger.exception(f"consul_cache.cache_is_dirty exception: {err}")
            return False

    def cache_update(self):
        data = self.ckv.get(self.prefix)
        if data:
            self.cad_cache.update(data)

    def watch(self):
        logger.info(f"consul_cache.watch background worker starting {datetime.utcnow()}")
        while True:
            try:
                dirty = self.cache_is_dirty()
                if dirty:
                    self.cache_update()
            except Exception as err:
                logger.exception(f"consul_cache.watch exception: {err}")


class CAD_Cache_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class CAD_Cache(CAD):
    key_prefixes = AD()

    def __init__(self, prefix, *args, callback=None, read_write=False, **kwargs):
        """
        loop: :parent asycio.loop
        prefix: :string consul kv path scope
        """
        if prefix in CAD_Cache.key_prefixes:
            self = CAD_Cache.key_prefixes[prefix]
        else:
            CAD.__init__(self, *args, **kwargs)
            CAD_Cache.key_prefixes[prefix] = self
            myself = CAD_Cache.__myself__(self, params=kwargs)
            myself.prefix = prefix
            myself.read_write = read_write

            if read_write:
                self.register_callback(self.write_back, any)

            if callback and isinstance(callback, tuple) and len(callback) == 2:
                self.register_callback(*callback)

            myself.ckv = consul.Consul(host=f"consul.{BCM.env}.at0l.io",
                                       port=BCM.endpoints.consul.port,
                                       scheme="https", verify=False).kv

            start_data = myself.ckv.get(myself.prefix)
            logger.info(f"CAD_Cache prefix -{prefix} initial data: {start_data}")
            self.update(start_data)

            myself.monitor = Consul_Monitor(
                                    cad_cache=self,
                                    prefix=myself.prefix,
                                    read_write=read_write
                                  )
            myself.monitor.start()
            logger.debug(f"consul_cache: prefix initialized: {myself.prefix}")

    @staticmethod
    def __kparts(key):
        if isinstance(key, bytes):
            key = key.decode()
        dkey = key.replace("/", ".")
        if dkey[0] == ".":
            dkey = dkey[1:]
        if dkey[-1] == ".":
            dkey = dkey[:-1]
        skey = key.replace(".", "/")
        if skey[0] == "/":
            skey = skey[1:]
        if skey[-1] == "/":
            skey = skey[:-1]
        return dkey, skey

    def write_back(self, key, value):
        myself = CAD_Cache.__myself__(self)
        if myself.read_write:
            dkey, skey = CAD_Cache.__kparts(key)
            fkey = f"{myself.prefix}/{skey}"
            myself.ckv.put(fkey, value)
        else:
            raise CAD_Cache_Exception(f"CAD_Cache.write_back called on read_only cache for: ({key}, {value})")

Consul_Cache = CAD_Cache
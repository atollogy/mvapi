#!/usr/bin/env python3

import logging
from logging.handlers import RotatingFileHandler
import multiprocessing_logging
multiprocessing_logging.install_mp_handler()

import os

APP_LOGGER = "mvapi"

logger = logging.getLogger(APP_LOGGER)


def startLogger(logLevel):
    LOG_PATH = "/var/log/atollogy"
    LOG_NAME = "{}/mvapi.log".format(LOG_PATH)
    LOG_SIZE = 20000000  # 20MB
    LOG_ROTATION_COUNT = 10
    LOG_LEVEL = int(logLevel)

    # Create log folder
    if not os.path.exists(LOG_PATH):
        os.makedirs(LOG_PATH)

    formatter = logging.Formatter(
        '{"timestamp": "%(asctime)s", "thread": "%(threadName)s", "syslog.appname": "%(name)s", "level": "%(levelname)s", "message": "%(message)s"}'
    )
    floghandler = RotatingFileHandler(
        LOG_NAME, maxBytes=LOG_SIZE, backupCount=LOG_ROTATION_COUNT
    )
    floghandler.setLevel(LOG_LEVEL)
    floghandler.setFormatter(formatter)

    logger.addHandler(floghandler)
    logger.setLevel(LOG_LEVEL)
    return logger

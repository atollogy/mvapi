#!/usr/bin/env python3
from log import *
from attribute_dict import *
import asyncio
import cv2
from datadog import initialize
from datadog import api as ddapi
from datetime import datetime, date, timedelta, timezone
import functools
import io
import iso8601
import json
import logging
import os
import re
import signal
import sys
import time

logger = logging.getLogger('mvapi')

######### BCM LOADER ##########################################################
BCM = None
def get_bcm():
    global BCM
    cfg = None
    local = False
    if "local_bcm" in os.environ:
        cfg = os.environ["local_bcm"]
        local = True
    elif os.path.exists("/etc/kvhandler/bcm.json"):
        cfg = "/etc/kvhandler/bcm.json"

    if cfg:
        BCM = AD.load(cfg)
        BCM.is_local = local
        # print(f"BCM successfully loaded from: {cfg}")
        return BCM
    else:
        print("BCM cannot be found - exiting")
        sys.exit(1)

BCM = get_bcm()
# print(BCM)

######### Context Managers ####################################################
class GracefulInterruptHandler(object):
    def __init__(self, signals=(signal.SIGINT, signal.SIGTERM), funcs=[]):
        self.signals = signals
        self.original_handlers = {}
        self.funcs = funcs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        for sig in self.signals:
            self.original_handlers[sig] = signal.getsignal(sig)
            signal.signal(sig, self.handler)

        return self

    def handler(self, signum, frame):
        [f() for f in self.funcs]
        self.release()
        self.interrupted = True

    def __exit__(self, type, value, tb):
        self.release()

    def release(self):
        if self.released:
            return False

        for sig in self.signals:
            signal.signal(sig, self.original_handlers[sig])

        self.released = True
        return True

######### Exceptions ##########################################################
class Atl_Exception(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv

class API_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class CFG_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class DB_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class Metric_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class MV_Exceptionn(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class Time_Exception(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class MVError(Atl_Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)

class Image_Utility_Exception(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Atl_Exception.__init__(self, message, status_code=status_code, payload=payload)


######### FQDN and Nodename Parser with Cache #################################
FQDND_STORE = AD()
host_re = re.compile(r'(?P<scode>(^(\w){4,8})(?=(?P<inst>(\d{2,4}$))))')
def parse_fqdn(hostname):
    if hostname is None or not isinstance(hostname, str) or len(hostname) < 3:
        logger.info(f"parse_fqdn: cannot deal with - {hostname}")
        return None

    if '_' in hostname:
        hostname = f"{hostname.replace('_', '.')}.at0l.io"

    if hostname in FQDND_STORE:
        return FQDND_STORE[hostname]
    else:
        FQDND_STORE[hostname] = fqdnd = AD()

    nparts = hostname.split('.')
    fqdnd.fqdn = fqdnd.hostname = hostname
    fqdnd.cgr = fqdnd.customer_id = nparts[-4]
    fqdnd.dom = nparts[-2]
    fqdnd.domain = '.'.join(nparts[-2:])
    fqdnd.env = nparts[-3]
    fqdnd.host = nparts[0]
    fqdnd.nodename = '_'.join(nparts[:-2])
    fqdnd.nparts = nparts
    fqdnd.shost = '.'.join(nparts[:-2])
    fqdnd.site = None
    fqdnd.subdom = '.'.join(nparts[1:])
    fqdnd.tld = nparts[-1]
    fqdnd.scode = fqdnd.host
    fqdnd.inst = 0
    fqdnd.inum = 0
    fqdnd.slot = 0

    labels = ['host', 'site', 'cgr', 'env', 'dom', 'tld']
    splabels = labels[-(len(nparts) - 1):]
    nsubparts = nparts[-(len(nparts) - 1):]
    for idx, np in enumerate(nsubparts):
        fqdnd[splabels[idx]] = np

    try:
        res = host_re.match(fqdnd.host)
        if hasattr(res, 'groupdict'):
            hre_res = res.groupdict()
            fqdnd.scode = hre_res['scode']
            fqdnd.inst = hre_res['inst']
            fqdnd.inum = int(hre_res['inst'])
            fqdnd.slot = int(hre_res['inst'][-1])
        return fqdnd
    except Exception as err:
        logger.info(f'parse_fqdn exception: {err}')
        return fqdnd

######### color_space helpers ############################################

CV2_CONVERTERS = AD()
for k in [c for c in cv2.__dict__.keys() if c.startswith("COLOR_")]:
    kp = k.lower().split("_", 1)[-1].split("2", 1)
    if len(kp) == 2:
        kl = kp[0]
        fColor, toColor = kp
        if fColor not in CV2_CONVERTERS:
            CV2_CONVERTERS[fColor] = AD({toColor: getattr(cv2, k)})
        else:
            CV2_CONVERTERS[fColor][toColor] = getattr(cv2, k)


def color_space_convert(color_space_image, image_color_space, target_color_space):
    try:
        image_color_space = image_color_space.lower()
        target_color_space = target_color_space.lower()

        if image_color_space == target_color_space:
            return color_space_image.copy()
        elif image_color_space == "gray":
            raise Image_Utility_Exception("Image in grayscale cannot be color_space converted")
        elif image_color_space in CV2_CONVERTERS and target_color_space in CV2_CONVERTERS[image_color_space]:
            return cv2.cvtColor(color_space_image.copy(), CV2_CONVERTERS[image_color_space][target_color_space])
        else:
            raise Image_Utility_Exception(
                "Colorspaces not found: {} {}".format(image_color_space, target_color_space)
            )

    except Exception as err:
        msg = "Image_utilities color_space_convert error: {}".format(err)
        logger.exception(msg)
        return None

def get_color_space(color_space):
    if not isinstance(color_space, str):
        raise Image_Utility_Exception(
            "Color space must be lowercase string: {}".format(color_space)
        )
    else:
        color_space = color_space.lower()
        if color_space == "bgr":
            return "bgr"
        elif color_space not in CV2_CONVERTERS:
            raise Image_Utility_Exception("Color space not found: {}".format(color_space))
    return getattr(cv2, CV2_CONVERTERS[color_space])

######### Common Utility Functions ############################################

### JSON Serialization Helpers
class JSONEncoder(json.JSONEncoder):
    """JSONEncoder to handle ``datetime`` and other problematic object values"""

    class EncodeError(Exception):
        """Raised when an error occurs during encoding"""

    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                return obj.timestamp()
            elif isinstance(obj, bytes):
                return self.default(obj.decode("utf-8"))
            elif isinstance(obj, io.BytesIO):
                return "removed binary data"
            elif hasattr(obj, "toJSON"):
                return obj.toJSON()
            elif hasattr(obj, "jstr"):
                return obj.jstr()
            else:
                try:
                    encoded_obj = json.JSONEncoder.default(self, obj)
                except TypeError as err:
                    encoded_obj = repr(obj)
                return encoded_obj
        except Exception as err:
            raise JSONEncoder.EncodeError("JSON Encoder Error: {}".format(repr(err)))

### json object serializer
def json_safe(obj):
    """JSON dumper for objects not serializable by default json code"""
    return json.dumps(obj, cls=JSONEncoder, default=str, indent=4, separators=(",", ": "), sort_keys=True)


######### datetime handler ############################################
# this regex parses '20201111120151' and '20201111120151.123456'
DATETIME_RE = re.compile(r'(?P<year>(\d{4}))(?P<month>(\d{2}))(?P<day>(\d{2}))(?P<hour>(\d{2}))(?P<minute>(\d{2}))(?P<second>(\d{2}))\.{0,1}(?P<microsecond>(\d{0,6}))')
def to_datetime(rec_time):
    now = datetime.utcnow()
    now = now.replace(tzinfo=timezone.utc)
    if not rec_time:
        msg = f"to_datetime ERROR: rec_time is NULL"
        logger.error(msg)
        return rec_time
    elif isinstance(rec_time, str):
        dmatch = DATETIME_RE.match(rec_time)
        if any([c in rec_time for c in ['-', 'T']]):
            dt = iso8601.parse_date(rec_time)
            dt = dt.replace(tzinfo=timezone.utc)
        elif len(rec_time) in [14, 21] and hasattr(dmatch, 'groupdict'):
            dtm = AD({k: int(v) for k, v in dmatch.groupdict().items() if v.isnumeric()})
            if 'microsecond' not in dtm:
                dtm.microsecond = datetime.utcnow().microsecond
            dt = datetime(dtm.year, dtm.month, dtm.day, hour=dtm.hour, minute=dtm.minute, second=dtm.second, microsecond=dtm.microsecond, tzinfo=timezone.utc)
        elif '.' in rec_time:
            try:
                dt = datetime.fromtimestamp(float(rec_time), tz=timezone.utc)
            except Exception as err:
                msg = f"to_datetime float conversion EXCEPTION: {repr(err)}"
                logger.exception(msg)
                dt = datetime.fromtimestamp(int(rec_time.split('.', 1)[0]), tz=timezone.utc)
        elif rec_time.isnumeric() and len(rec_time) == 13:
            dt = datetime.fromtimestamp((int(rec_time)/1000), tz=timezone.utc)
        else:
            dt = datetime.fromtimestamp(int(rec_time), tz=timezone.utc)
    elif isinstance(rec_time, int):
        if len(str(rec_time)) == 13:
            dt = datetime.fromtimestamp(rec_time/1000, tz=timezone.utc)
        else:
            dt = datetime.fromtimestamp(rec_time, tz=timezone.utc)
    elif isinstance(rec_time, float):
        dt =  datetime.fromtimestamp(rec_time, tz=timezone.utc)
    else:
        dt = rec_time
    return dt

def mtime_int(tm):
    return int(to_datetime(tm).timestamp() * 1000)

def mtime_str(tm):
    return str(int(to_datetime(tm).timestamp() * 1000))

### array de-duplication
def unique(*args):
    temp = []
    for l in args:
        if isinstance(l, list):
            for i in l:
                if isinstance(i, list):
                    for x in unqiue(i):
                        if x not in temp:
                            temp.append(x)
                elif i not in temp:
                    temp.append(i)
    return temp

######### Metric decorators & helpers #########################################
### setup datadog agent
if BCM is not None:
    dd_options = AD({
        'api_key': BCM.endpoints.datadog.core_api_key,
        'app_key': BCM.endpoints.atlapi.dd_app_key
        })
    initialize(**dd_options)
    metric_prefix = f'atl.{BCM.env}.atlapi'
else:
    ddapi = None
    metric_prefix = f'atl.prd.atlapi'

def find_value(*args, _value_name='nodename', _hint={}, **kwargs):
    if not _value_name:
        return None

    hints = {
        'nodename': ['args', 'steps', 'requestHeaders._rawHeaders'],
        'step_name': ['records', 'steps']
    }
    hints.update(_hint)
    value = None

    def get_value_by_name(tgt, value_name):
        if not tgt or not value_name:
            return None
        value = None
        if hasattr(tgt, value_name):
            value = getattr(tgt, value_name)
        elif hasattr(tgt, 'keys') and value_name in tgt:
            value =  tgt[value_name]
        elif hasattr(tgt, 'values'):
            value = find_value(*tuple(tgt.values()), _value_name=value_name)
        elif hasattr(tgt, 'deepKeys'):
            for k in tgt.deepKeys():
                if k.rsplit('.', 1)[-1] == value_name:
                    value =  tgt[k]
        elif isinstance(tgt, (list, tuple)):
            value = find_value(*tuple(tgt), _value_name=value_name)
        return value

    for a in [v for v in (list(args) + list(kwargs.values()))]:
        value = get_value_by_name(a, _value_name)
        if value:
            return value
        elif _value_name in hints:
            for lf in hints[_value_name]:
                value = get_value_by_name(get_value_by_name(a, lf), _value_name)
                if value:
                    return value
    return  value




######### decorators ############################################
def execution_time(func, _ftype='sync', threshold=10):
    @functools.wraps(func)
    async def async_timer(*args, **kwargs):
        start = asyncio.get_event_loop().time()
        result = await func(*args, **kwargs)
        duration = time.time() - start
        if duration > threshold:
            logger.debug(f"EXCESSIVE EXECUTION TIME FOR {func.__name__.upper()} = {duration} SEC")
        return result

    @functools.wraps(func)
    def sync_timer(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        duration = time.time() - start
        if duration > threshold:
            logger.debug(f"EXCESSIVE EXECUTION TIME FOR {func.__name__.upper()} = {duration} SEC")
        return result

    if logger.level < 30:
        if _ftype == 'async':
            return async_timer
        else:
            return sync_timer
    else:
        return func

def exec_time_sync(func, threshold=10):
    return execution_time(func, _ftype='sync', threshold=threshold)

def exec_time_async(func, threshold=10):
    return execution_time(func, _ftype='async', threshold=threshold)

def track_metrics(func, _ftype='sync', _metric=None, _mtype='count', _nodename=None, _tags=[], _value=1):
    def sync_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value
        @functools.wraps(func)
        def sync_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = time.time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = func(*args, **kwargs)
                duration = (time.time() - start)
                if not nodename:
                    logger.error(f"sync_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                ensureDeferred(send_metric(nodename, failure, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
                msg = f"sync_metric_wrap {metric} EXCEPTION: {repr(err)}"
                logger.exception(msg)
                raise
            else:
                ensureDeferred(send_metric(nodename, metric, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
            return res
        return sync_metric_wrap

    def deferred_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value
        @functools.wraps(func)
        @inlineCallbacks
        def deferred_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = time.time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = yield func(*args, **kwargs)
                duration = (time.time() - start)
                if not nodename:
                    logger.error(f"deferred_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                ensureDeferred(send_metric(nodename, failure, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
                msg = f"deferred_metric_wrap {metric} EXCEPTION: {repr(err)}"
                logger.exception(msg)
                raise
            else:
                ensureDeferred(send_metric(nodename, metric, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
            return res
        return deferred_metric_wrap

    def async_metric_decorator(func):
        metric = _metric if _metric else func.__name__
        failure = f"{metric}.failure"
        __mtype = _mtype
        __nodename = _nodename
        __tags = _tags
        __value = _value
        @functools.wraps(func)
        async def async_metric_wrap(*args, **kwargs):
            tags = []
            duration = None
            start = asyncio.get_event_loop().time()
            nodename = __nodename if __nodename else find_value(args, kwargs)
            try:
                res = await func(*args, **kwargs)
                duration = (asyncio.get_event_loop().time() - start)
                if not nodename:
                    logger.error(f"async_metric_wrap {metric} ERROR: nodename is empty - aborting metric collection")
                    return res
                else:
                    if len(__tags):
                        for t in __tags:
                            if ':{}' in t:
                                tags.append(t.format(find_value(res, _value_name=t.split(':')[0])))
                            else:
                                tags.append(t)
            except Exception as err:
                ensureDeferred(send_metric(nodename, failure, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
                msg = f"async_metric_wrap {metric} EXCEPTION: {repr(err)}"
                logger.exception(msg)
                raise
            else:
                ensureDeferred(send_metric(nodename, metric, exec_time=duration, mtype=__mtype, tags=tags, value=__value))
            return res
        return async_metric_wrap

    if _ftype == 'defer':
        return deferred_metric_decorator(func)
    elif _ftype == 'async':
        return async_metric_decorator(func)
    else:
        return sync_metric_decorator(func)

async def send_metric(nodename, metric, add_prefix=True, exec_time=None, fqdnd=None, mtype='count', options=None, tags=None, value=1):
    if add_prefix and not metric.startswith(metric_prefix):
        metric = f"{metric_prefix}.{metric}"

    if not ddapi:
        logger.info("send_metric Datadog API not configured")
        return
    try:
        if not fqdnd:
            fqdnd = parse_fqdn(nodename)
        if not fqdnd:
            logger.error(f"send_metric fqdnd returned null for node: {nodename} - metric {metric}")
            return
        if not exec_time:
            exec_time = 1

        tags = unique([
                    f"env:{fqdnd.env}",
                    f"cgr:{fqdnd.cgr}",
                    f"scode:{fqdnd.scode}",
                    f"nodename:{fqdnd.nodename}"
                    ], tags)

        series = [
            {
                'host': BCM.fqdn,
                'interval': 1,
                'metric': metric,
                'points': [[time.time(), value]],
                'tags': tags,
                'type': mtype
            },
            {
                'host': BCM.fqdn,
                'name': metric + '.exec_time',
                'points': [[time.time(), exec_time]],
                'tags': tags
            }
        ]

        await deferToThread(
            functools.partial(
                ddapi.Metric.send,
                series
                )
            )
        # logger.info(f"send_metric response: {resp} - {series}")
    except Exception as err:
        msg = f"send_metric: EXCEPTION {nodename} {metric} - {err}"
        logger.exception(msg)

def track_activity(nodename, count=1):
    if not nodename:
        return None
    if count < 1:
        count = 1
    fqdnd = parse_fqdn(nodename)
    metric = '.'.join(['atl'] + [fqdnd[i] for i in ['env', 'cgr', 'site'] if fqdnd[i]] + ['ds', 'activity'])
    ensureDeferred(
                    send_metric(
                        nodename,
                        metric,
                        add_prefix=False,
                        fqdnd=fqdnd,
                        value=count
                    )
            )

def smetric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='sync', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

def dmetric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='defer', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

def ametric(func, *args, _nodename=None, _metric=None, _tags=[], _value=1, **kwargs):
    frapper = track_metrics(func, _ftype='async', _nodename=_nodename, _metric=_metric, _tags=_tags, _value=_value)
    return frapper(*args, **kwargs)

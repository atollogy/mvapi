#!/usr/bin/env python3


from attribute_dict import AD

import json
import os
import sys

### load bcm metadata
cfg = None
if "MVAPI_CONF" in os.environ:
    cfg = os.environ["MVAPI_CONF"]
elif os.path.exists("/etc/kvhandler/bcm.json"):
    cfg = "/etc/kvhandler/bcm.json"
elif os.path.exists("/etc/kvmanager/bcm.json"):
    cfg = "/etc/kvmanager/bcm.json"
else:
    sys.exit("Cannot locate bcm.json exiting")

print(("BCM loaded form path: " + cfg))
BCM = AD()
BCM.load(cfg)

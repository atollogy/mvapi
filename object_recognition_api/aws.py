#!/usr/bin/env python3

from api import Api
from attribute_dict import AD
from common import *
from image.utilities import *
from queuing import ATLEvent, AsyncAWSQueueManager

import asyncio
import boto3
import boto3.resources.base as base
import botocore
import concurrent
from concurrent.futures import ProcessPoolExecutor
import cv2
from datetime import datetime, timedelta
import functools
import gzip
import io
import iso8601
import json
import logging
logger = logging.getLogger("mvapi")
import mimetypes
import numpy as np
import multiprocessing as mp
import platform
import threading


# --------------------------------------------------------------------------------------------------------------
### AWS service coordinator class
class Aws(Api):
    class AWSException(Exception):
        """Raised when hard to identify AWS api errors occur"""

    def __init__(
        self, env=None, cgr=None, cache=None, ckv=None, mvapi_models=[]
    ):
        Api.__init__(self, cache=cache, cgr=cgr, ckv=ckv, env=env)
        # threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        # self.loop = asyncio.get_event_loop()
        # asyncio.set_event_loop(self.loop)
        cnt = mp.cpu_count()
        self.cpu_count = cnt * 5 if cnt > 0 else 5
        self.aws_creds = ckv.get("aws_creds")
        self.cgrMap = ckv.get("cgrMap")
        self.mvapi_models = mvapi_models
        self.sessions = {}
        self.sessions[env] = boto3.Session(region_name=self.cgrMap[cgr])
        self.services = {}
        self.services[env] = {}
        self.services[env]["s3"] = self.sessions[env].client(
            service_name="s3",
            region_name=self.cgrMap[cgr],
            aws_access_key_id=self.aws_creds[env].akid,
            aws_secret_access_key=self.aws_creds[env].sak,
            endpoint_url="https://s3.{}.amazonaws.com/".format(
                self.cgrMap[cgr]
            ),
        )

        self.services[env]["sqs"] = self.sessions[env].client(
            service_name="sqs",
            region_name=self.cgrMap[cgr],
            aws_access_key_id=self.aws_creds[env].akid,
            aws_secret_access_key=self.aws_creds[env].sak,
            endpoint_url="https://sqs.{}.amazonaws.com/".format(
                self.cgrMap[cgr]
            ),
        )

        self.s3 = S3(aws=self, env=env, cgr=cgr, cache=cache)
        self.sqs = SQS(aws=self, env=env, cgr=cgr, queues=mvapi_models)

    def __call__(self, svc, env=None, cgr=None):
        if env is None:
            env = self.ENV
        if cgr is None:
            cgr = self.CGR
        if env and env in self.sessions:
            session = self.sessions[env]
        elif env in self.aws_creds and cgr in self.cgrMap:
            session = self.sessions[env] = boto3.Session()
        elif self.ENV in self.sessions:
            session = self.sessions[self.ENV]
        else:
            session = self.sessions[self.ENV] = boto3.Session()

        if env not in self.services:
            self.services[env] = {}

        if svc not in self.services[env]:
            self.services[env][svc] = session.client(
                service_name=svc,
                region_name=self.cgrMap[cgr],
                aws_access_key_id=self.aws_creds[env].akid,
                aws_secret_access_key=self.aws_creds[env].sak,
                endpoint_url="https://{}.{}.amazonaws.com/".format(
                    svc, self.cgrMap[cgr]
                ),
            )

        return self.services[env][svc]


### Class to copy data from S3 to memory or local file
class S3(Api):
    def __init__(self, aws=None, env=None, cgr=None, cache=None):
        Api.__init__(self, aws=aws, env=env, cgr=cgr, cache=cache)

    def createKey(self, customer_id, category, recId, recTime, fName):
        if category in ["beacons", "image_readings"]:
            bucket = "atl-%s-%s-rawdata" % (self.ENV, customer_id)
        else:
            bucket = "atl-%s-%s-data" % (self.ENV, customer_id)
        d = to_datetime(recTime)
        date_hour = d.strftime("%Y/%m/%d/%H")
        key = "{}/{}/{}/{}".format(category, recId, date_hour, fName)
        path = "s3://{}/{}".format(bucket, key)
        return bucket, key, path

    async def get(self, bucket, key, store_path=None):
        """
        :param Bucket:
        :param Key:
        :param store_path:
        :return: if store_path is not None then return path of file saved to disk else return the data
        """
        logger.info("AWS Fetch: {} - {} - {}".format(bucket, key, store_path))

        try:
            d = await self.get_object(Bucket=bucket, Key=key)
            # print('raw get: {}'.format(d))
            kparts = key.rsplit(".", 1)
            data = d["Body"].read()
            logger.info("AWS Fetch data size: {}".format(len(data)))

            if store_path:
                with open(store_path, "wb") as f:
                    f.write(data)
                return store_path
            if kparts[-1] == "gz":
                data = gzip.decompress(data)
                kparts.pop()
            if kparts[-1] == "json":
                data = json.loads(data.decode("utf-8"))
                kparts.pop()
            return data
        except Exception as e:
            error = {
                "bucket": bucket,
                "error": repr(e),
                "key": key,
                "message": "AWS Fetch {}: {} - {}".format(
                    bucket, key, repr(e)
                ),
                "storePatch": store_path,
            }
            logger.debug(error)
            return None

    async def get_bucket(self, *args, **kwargs):
        _, env, cgr, btype = kwargs["Bucket"].split("-")
        resp = await self.AWS.async_loop_run(
            functools.partial(
                self.AWS("s3", env=env, cgr=cgr).Bucket,
                Bucket=kwargs["Bucket"],
            )
        )
        return resp

    async def get_object(self, *args, **kwargs):
        _, env, cgr, btype = kwargs["Bucket"].split("-")
        resp = await self.AWS.async_loop_run(
            functools.partial(
                self.AWS("s3", env=env, cgr=cgr).get_object,
                Bucket=kwargs["Bucket"],
                Key=kwargs["Key"],
            )
        )
        return resp

    async def get_path(self, uri, store_path=None):
        files = AD()
        try:
            bucket_name, path = self.keyParts(uri)
            bucket = await self.get_bucket(Bucket=bucket_name)
            if not path[-1] == "/":
                path += "/"
            for f in bucket.list(path, ""):
                files[f] = io.BytesIO(await self.get(bucket_name, f))
            if store_path:
                if not os.path.exists(store_path):
                    os.makedirs(store_path)
                for f in files:
                    _, fpath = self.keyParts(f)
                    fpath = fpath.strip(path)
                    if "/" in fpath:
                        subdir, fname = fpath.rsplit("/", 1)
                        subdir = os.path.join(store_path, subdir)
                        if not os.path.exists(subdir):
                            os.makedirs(subdir)
                    else:
                        subdir = store_path
                        fname = fpath
                    fullPath = os.path.join(subdir, fname)
                    with open(fullPath, "w") as fo:
                        fo.write(files[f])
            return files
        except botocore.exceptions.ClientError as e:
            logger.exception("aws.getPath - store_path not provided")

    def keyParts(self, uri):
        if "https://" in uri:
            uriProto, uriParts = uri.split("://")
            s3Endpoint, bucket, key = uriParts.split("/", 2)
            key = "/{}".format(key)
        elif "s3://" in uri:
            uriProto, uriParts = uri.split("://")
            bucket, key = uriParts.split("/", 1)
            key = "/{}".format(key)
        elif uri[0] == "/":
            uriProto = None
            bucket, key = uri[1:].split("/", 1)
            key = "/{}".format(key)
        else:
            uriProto = "path"
            bucket, key = uri.split("/", 1)
            key = "/{}".format(key)
        return bucket, key

    async def put(self, bucket, key, body):
        if self.ENV not in bucket:
            bparts = bucket.split("-")
            bparts[1] = self.ENV
            bucket = "-".join(bparts)
        d = await self.put_object(Bucket=bucket, Key=key, Body=body)
        logger.info("AWS Persist ({}) [FINISH]".format(key))
        if d is not None:
            return d
        else:
            return None

    async def put_customer_data(
        self,
        bucket,
        key,
        recData,
        recType="identity",
        compress=True,
        safe=False):

        # serialize object as json format string and compress
        if self.ENV not in bucket:
            bparts = bucket.split("-")
            bparts[1] = self.ENV
            bucket = "-".join(bparts)

        if recType == "json":
            recData = json.dumps(recData, default=str)

        if compress:
            if not key.endswith(".gz"):
                key += ".gz"
            if hasattr(recData, "encode"):
                data = gzip.compress(recData.encode())
            else:
                data = gzip.compress(recData)
        else:
            data = recData

        key_path = "s3://%s/%s" % (bucket, key)
        if safe:
            d = await self.safe_put(Bucket=bucket, Key=key, Body=data)
        else:
            d = await self.put_object(Bucket=bucket, Key=key, Body=data)

        logger.info("AWS Persist ({}) [FINISH]".format(key))
        return key_path

    async def put_object(self, *args, **kwargs):
        to_store = []
        bucket = kwargs['Bucket']
        key = kwargs['Key']
        body = kwargs['Body']

        if self.ENV not in bucket:
            bparts = bucket.split("-")
            bparts[1] = self.ENV
            bucket = "-".join(bparts)
        _, env, cgr, btype = bucket.split("-")
        path = f"s3://{bucket}/{key}"

        if body is None:
            raise MVError(f'AWS PUT ERROR: {path} [FAILURE] body was "None"')
        try:
            content_type = mimetypes.guess_type(key)[0]
            if content_type is None:
                logger.info(f"Asset didn't have a recognizable mimetype path: {path}, body_type: {type(body)}")
                content_type = 'application/octet-stream'
            to_store.append([bucket, key, body])
            # if key.split('.')[-1] == 'jpg' and 'violation' not in key:
            #     to_store.extend(self.thumbnail(bucket, key, body.copy()))

            res = None
            for ft in to_store:
                try:
                    if all([len(e) for e in ft]):
                        if isinstance(ft[2], np.ndarray):
                            ft[2] = bytearray(ft[2].tobytes())
                        d = await self.AWS.async_loop_run(
                            functools.partial(
                                self.AWS("s3", env=env, cgr=cgr).put_object,
                                Bucket=ft[0],
                                Key=ft[1],
                                Body=ft[2],
                                ContentType=content_type,
                            )
                        )
                        logger.info(f"AWS PUT {ft[0]}/{ft[1]} [FINISH]")
                        if res is None:
                            res = d
                except Exception as err:
                    logger.exception("AWS PUT EXCEPTION: {ft[0]}/{ft[1]} [FAILURE] - {err}")
            if res is not None and res["ResponseMetadata"]["HTTPStatusCode"] < 300:
                # logger.info(f"AWS PUT {bucket}/{key} [FINISH]")
                return path
            else:
                logger.error(f'AWS PUT ERROR: {path} [FAILURE] - {res}')
                return None
        except Exception as e:
            logger.exception("AWS PUT EXCEPTION: {path} [FAILURE] - {e}")
            return None

    async def put_path(self, locPath, uri, prefix=None, rm=False):
        bucket_name, path = self.keyParts(uri)
        if self.ENV not in bucket_name:
            bparts = bucket_name.split("-")
            bparts[1] = self.ENV
            bucket_name = "-".join(bparts)
        data = "fs"
        if isinstance(locPath, str):
            fList = shcmd("find {} -type f".format(locPath))
        elif isinstance(locPath, list):
            fList = locPath
        elif hasattr(locPath, "keys"):
            data = "args"
            fList = locPath.keys()
        for f in fList:
            if prefix:
                fkey = f.strip(prefix)
            else:
                fkey = f.split("/")[-1]
            if data == "fs":
                with open(f, "r") as fo:
                    d = await self.put_object(
                        None,
                        Bucket=bucket_name,
                        Key=os.path.join(path, fkey),
                        Body=fo.read(),
                    )
                if rm:
                    os.remove(f)
            else:
                d = await self.put_object(
                    None,
                    Bucket=bucket_name,
                    Key=os.path.join(path, fkey),
                    Body=locPath[f].read(),
                )

    async def list_objects(self, *args, **kwargs):
        _, env, cgr, btype = kwargs["Bucket"].split("-")
        resp = await self.AWS.async_loop_run(
            functools.partial(
                self.AWS("s3", env=env, cgr=cgr).list_objects_v2,
                Bucket=kwargs["Bucket"],
                Key=kwargs["Key"],
            )
        )
        return resp

    async def safe_put(self, *args, **kwargs):
        try:
            bList = await self.list_objects(
                Bucket=kwargs["Bucket"], Key=kwargs["Key"]
            )
            if bList["KeyCount"] == 0:
                resp =  await self.put_object(
                    Bucket=kwargs["Bucket"],
                    Key=kwargs["Key"],
                    Body=kwargs["Body"],
                )
                key_path = "s3://%s/%s" % (bucket, key)
                return key_path
        except Exception as err:
            return {"ResponseMetadata": {"HTTPStatusCode": 200}}

    def thumbnail(self, bucket, key, body):
        thumbs = []
        try:
            if isinstance(body, np.ndarray) and body.ndim >= 3:
                image = body.copy()
            else:
                image = load_image(body.copy(), color_space='bgr')
            fname, ext = key.rsplit('.', 1)
            y, x = image.shape[:2]
            if x >= 200:
                t200_dim = (200, int(y * 200/x))
                thumbs.append([bucket,
                               f"{fname}_thumbnail-200.{ext}",
                               pack_image(cv2.resize(image.copy(), t200_dim, interpolation=cv2.INTER_AREA), color_space='bgr')
                              ])
            if x >= 400:
                t400_dim = (400, int(y * 400/x))
                thumbs.append([bucket,
                               f"{fname}_thumbnail-400.{ext}",
                               pack_image(cv2.resize(image.copy(), t400_dim, interpolation=cv2.INTER_AREA), color_space='bgr')
                              ])
            return thumbs
        except Exception as err:
            logger.exception("aws.thumbnail exception: {repr(err)}")
            return thumbs


class SNS(Api):
    def __init__(self, aws: Aws, cgr: str, env: str) -> None:
        Api.__init__(self, aws=aws, cgr=cgr, env=env)

    async def publish_message(self, topic: str, message: str) -> None:
        resp = await self.AWS.async_loop_run(
            functools.partial(
                self.AWS("sns").publish,
                TopicArn=topic,
                Message=message
            )
        )

class SQS(Api):
    def __init__(self, aws=None, cgr=None, env=None, queues=[], wait=20):
        Api.__init__(self, aws=aws, cgr=cgr, env=env)
        self.queues = {}
        self.qinfo = AD()

        for qurl in self.AWS("sqs").list_queues()["QueueUrls"]:
            qname = qurl.split("/")[-1]
            if "." in qname:
                qname = qname.split(".")[0]
            qarn = self.AWS("sqs").get_queue_attributes(QueueUrl=qurl, AttributeNames=["QueueArn"])["Attributes"]["QueueArn"]
            self.qinfo[qname] = AD({
                                    'qurl': qurl,
                                    'qarn': qarn
                                   })

        for q in queues:
            self.queues[q] = AsyncAWSQueueManager(
                aws=aws, cgr=cgr, env=env, qname=q, wait=wait, qinfo=self.qinfo
            )
            print(self.queues[q].qurl)
            print(self.queues[q].qarn)

    def __call__(self, qname, fifo=True, dlq=True, wait=20):
        if qname not in self.queues:
            self.queues[qname] = AsyncAWSQueueManager(
                aws=self.AWS,
                cgr=self.CGR,
                env=self.ENV,
                qname=qname,
                qinfo=self.qinfo,
                fifo=fifo,
                dlq=dlq,
                wait=wait
            )
        return self.queues[qname]

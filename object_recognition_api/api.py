#!/usr/bin/env python3

from attribute_dict import AD
from exceptions import APIError

import aiohttp
import asyncio
from asyncio import ensure_future
import base64
import botocore
import concurrent
from concurrent.futures import ProcessPoolExecutor
from datetime import datetime, date, timedelta, timezone
import email
import email.message
from email import policy
from email.parser import BytesParser, Parser
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart, MIMEBase
from email.mime.application import MIMEApplication
import functools
import gzip
import io
import json

import logging

logger = logging.getLogger("mvapi")

import mimetypes
import numpy as np
import os
import random
import ssl
import tarfile
import time

import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


import zipfile

### load the version metadata
with open(os.path.dirname(__file__) + "/version.json") as vf:
    VERSION = json.load(vf)["version"]

with open(
    "{}/static/atl_favicon.ico".format(
        os.path.dirname(os.path.realpath(__file__))
    ),
    "rb",
) as _ico:
    FAVICON = _ico.read()


class Api(object):
    """Atollogy REST Api Class """

    API_VERSION = VERSION
    FAVICON = FAVICON

    def __init__(self, *args, **kwargs):
        super(Api, self).__init__()
        self.args = args
        for k, v in kwargs.items():
            if k != "APP" and k[0] != "_":
                self.__dict__[k.upper()] = v
        self.ca_certs = AD(
            {
                "default": {
                    "fname": "/etc/ssl/certs/ca-certificates.crt",
                    "context": ssl.create_default_context(
                        cafile="/etc/ssl/certs/ca-certificates.crt"
                    ),
                }
            }
        )

        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        # self.executor = loky.get_reusable_executor(max_workers=5, timeout=30, kill_workers=True)
        # self.loop.set_default_executor(self.executor)

    def _json_safe(self, obj):
        """JSON dumper for objects not serializable by default json code"""
        return json.dumps(obj, cls=JSONEncoder)

    def sync_run_async(self, cmd, timeout=30):
        try:
            if asyncio.iscoroutine(cmd):
                if self.loop.is_running():
                    resp = asyncio.run_coroutine_threadsafe(
                        cmd, self.loop
                    ).result(timeout)
                else:
                    resp = self.loop.run_until_complete(cmd)
            else:
                resp = cmd()
            return resp
        except Exception as e:
            raise APIError("sync_run_async exception: {}".format(e))

    async def async_loop_run(self, cmd, timeout=30):
        try:
            if asyncio.iscoroutine(cmd):
                if self.loop.is_running():
                    resp = await asyncio.run_coroutine_threadsafe(
                        cmd, self.loop
                    ).result(timeout)
                else:
                    resp = await self.loop.run_until_complete(cmd)
            else:
                resp = cmd()
            return resp
        except Exception as e:
            raise APIError(e)

    def backoff_with_jitter(self, attempt, base=10, cap=3000):
        return random.randrange(0, min(cap, base * 2 ** attempt))

    def get_ssl_context(self, tgt):
        if (
            tgt != self.ca_certs.default.fname
            and tgt not in self.ca_certs
            and os.path.exists(tgt)
        ):
            self.ca_certs[tgt] = AD(
                {
                    "fname": tgt,
                    "context": ssl.create_default_context(cafile=tgt),
                }
            )
            return self.ca_certs[tgt].context
        else:
            return self.ca_certs.default.context

    async def _get_response(self, resp):
        '''utility function to make json method usable by both async and sync evocations'''
        if resp.status < 300:
            jdata = await resp.json()

            def _json():
                return jdata

            resp.json = _json
        return resp

    async def _http_get_json(
        self, *args, retries=3, force_status=(500, 502, 504), **kwargs
    ):
        if "url" not in kwargs:
            raise APIError("_http_get_json Error: URL is a required parameter")
        elif len(kwargs["url"]) < 10:
            raise APIError(
                "_http_get_json Error: Invalid URL passed: {}".format(
                    kwargs["url"]
                )
            )
        else:
            if "params" not in kwargs or kwargs["params"] == None:
                kwargs["params"] = {}
            if "headers" not in kwargs or kwargs["headers"] == None:
                kwargs["headers"] = {}
            if "timeout" not in kwargs or kwargs["timeout"] == None:
                kwargs["timeout"] = 10

            kw = dict(headers=kwargs["headers"], params=kwargs["params"])
            if "verify" in kwargs and kwargs["verify"]:
                kw["ssl"] = self.get_ssl_context(kwargs["verify"])

            status = 999
            tries = 1
            async with aiohttp.ClientSession(loop=self.loop) as session:
                while tries <= retries and status > 299:
                    async with session.get(kwargs["url"], **kw) as resp:
                        resp = await self._get_response(resp)
                        status = resp.status
                        if status < 300 or status in force_status:
                            break
                        else:
                            time.sleep(self.backoff_with_jitter(tries))
                            tries += 1
            return resp

    async def _http_post(
        self, *args, retries=3, force_status=(500, 502, 504), **kwargs
    ):
        if "payload" not in kwargs:
            raise APIError("_http_post Error: Empty payload")
        elif "url" not in kwargs:
            raise APIError(
                "_http_post Error: URL is a required parameter"
            )
        elif len(kwargs["url"]) < 10:
            raise APIError(
                "_http_post Error: Invalid URL passed: {}".format(
                    kwargs["url"]
                )
            )
        else:
            if "params" not in kwargs or kwargs["params"] == None:
                kwargs["params"] = {}
            if "headers" not in kwargs or kwargs["headers"] == None:
                kwargs["headers"] = {}
            if "timeout" not in kwargs or kwargs["timeout"] == None:
                kwargs["timeout"] = 10

            kw = dict(
                headers=kwargs["headers"],
                params=kwargs["params"],
            )

            if 'payload_type' in kwargs and kwargs['payload_type'] == 'json':
                kw['json'] = kwargs['payload']
            else:
                kw['data'] = kwargs['payload']

            if "verify" in kwargs and kwargs["verify"]:
                kw["ssl"] = self.get_ssl_context(kwargs["verify"])

            status = 999
            tries = 1
            async with aiohttp.ClientSession(loop=self.loop) as session:
                while tries <= retries and status > 299:
                    async with session.post(kwargs["url"], **kw) as resp:
                        resp = await self._get_response(resp)
                        status = resp.status
                        if status < 300 or status in force_status:
                            break
                        else:
                            time.sleep(self.backoff_with_jitter(tries))
                            tries += 1
            return resp

    async def _call_external_api(
        self,
        method='post',
        url=None,
        payload=None,
        payload_type='json',
        headers=None,
        params=None,
        retries=3,
        timeout=10,
        verify="default",
    ):
        def json_error(msg, exceptionType=False):
            if exceptionType:
                logger.exception(msg)
            else:
                logger.error(msg)
            return {"api_error": msg}

        try:
            if method.lower() == 'post':
                resp = await self._http_post(
                    url=url,
                    payload=payload,
                    payload_type=payload_type,
                    headers=headers,
                    params=params,
                    retries=retries,
                    timeout=timeout,
                    verify=verify,
                )
                if resp.status < 300:
                    return resp.json()
                else:
                    logger.info("External API {} Call {} - Error: {}".format(method, url, resp))
                    return json_error("External API {} Call {} - Error: {}".format(method, url, resp))
            elif method.lower() == 'get':
                resp = await self._http_get_data(
                    url=url,
                    headers=headers,
                    params=params,
                    retries=retries,
                    timeout=timeout,
                    verify=verify,
                )
                if resp.status < 300:
                    return resp.json()
                else:
                    logger.info("External API {} Call {} - Error: {}".format(method, url, resp))
                    return json_error("External API {} Call {} - Error: {}".format(method, url, resp))
        except Exception as err:
            return json_error(
                "External API {} Call {} - Exception: {}".format(method, url, err), True
            )


    def _call_external_api_sync(self, *args, **kwargs):
        return self.sync_run_async(self._call_external_api(*args, **kwargs))

    def _reqData(self, rName, request):
        try:

            def itemize(d):
                if hasattr(d, "items"):
                    _td = {}
                    for k, val in d.items():
                        k = k.lower()
                        if isinstance(k, bytes):
                            k = k.decode("utf-8")
                        if isinstance(val, list) and len(val) == 1:
                            if isinstance(val[0], bytes):
                                _td[k] = val[0].decode("utf-8")
                            else:
                                _td[k] = val[0]
                        elif isinstance(val, list):
                            _td[k] = itemize(val)
                        else:
                            _td[k] = val
                    return _td
                elif isinstance(d, list):
                    _tl = []
                    for i in d:
                        if hasattr(i, "items"):
                            _tl.append(itemize(i))
                        elif isinstance(i, bytes):
                            _tl.append(i.decode("utf-8"))
                        else:
                            _tl.append(i)
                    return _tl
                elif isinstance(d, bytes):
                    return d.decode("utf-8")
                else:
                    return d

            hdrs = itemize(request.requestHeaders._rawHeaders)
            if "mv_routing" in hdrs:
                hdrs["mv_routing"] = json.loads(
                    gzip.decompress(
                        base64.b64decode(hdrs["mv_routing"].encode())
                    ).decode()
                )
            hdrs = AD(hdrs)
            if "content-encoding" not in hdrs:
                hdrs["content-encoding"] = "identity"

            if (
                "customer_id" in hdrs
                and hdrs["customer_id"]
                in self.CACHE["gwid"][hdrs["customer_id"]]
            ):
                hdrs["fqdn"] = self.CACHE["gwid"][hdrs["customer_id"]][
                    hdrs["gateway_id"]
                ].fqdn
                hdrs["nodename"] = self.CACHE["gwid"][hdrs["customer_id"]][
                    hdrs["gateway_id"]
                ].nodename

            args = itemize(request.args) if len(request.args) else {}
            # print(args)
            if request.method in [b"POST", b"PUT"]:
                raw_data = request.content.read()
                if (
                    "content-encoding" in hdrs
                    and hdrs["content-encoding"] == "gzip"
                ):
                    semi_raw_data = gzip.decompress(raw_data)
                elif (
                    "content-encoding" in hdrs
                    and hdrs["content-encoding"] == "zip"
                ):
                    semi_raw_data = zipfile.ZipFile(io.BytesIO(raw_data))
                else:
                    semi_raw_data = raw_data

                if (
                    "content-type" in hdrs
                    and hdrs["content-type"] == "application/json"
                ):
                    data = json.loads(semi_raw_data.decode("utf-8"))
                elif (
                    "content-type" in hdrs
                    and hdrs["content-type"] == "application/zip"
                ):
                    data = AD({"filelist": semi_raw_data.namelist()})
                    for fname in data.filelist:
                        if fname == "data.json":
                            data["metadata"] = json.loads(
                                semi_raw_data.read(fname).decode("utf-8")
                            )
                        else:
                            data[fname] = semi_raw_data.read(fname)
                elif (
                    "content-type" in hdrs
                    and hdrs["content-type"] == "application/x-tar"
                ):
                    semi_raw_data = tarfile.open(io.BytesIO(raw_data), "r")
                    data = AD({"filelist": semi_raw_data.getnames()})
                    for fname in data.filelist:
                        if fname == "data.json":
                            data["metadata"] = json.loads(
                                semi_raw_data.extractfile(fname).decode(
                                    "utf-8"
                                )
                            )
                        else:
                            data[fname] = semi_raw_data.extractfile(fname)
                elif (
                    "content-type" in hdrs
                    and "multipart" in hdrs["content-type"]
                ):
                    data = {"order": []}
                    try:
                        mver = (
                            hdrs["mime-version"].encode()
                            if "mime-version" in hdrs
                            else b"1.0"
                        )
                        semi_raw_data = (
                            b"Mime-Version: "
                            + mver
                            + b"\r\nContent-Type: "
                            + hdrs["content-type"].encode()
                            + b"\r\n"
                            + semi_raw_data
                        )
                        msg = email.message_from_bytes(semi_raw_data)
                        for p in msg.walk():
                            pcType = p.get_content_type()
                            if pcType == "multipart/related":
                                continue
                            elif "Content-ID" not in p.keys():
                                continue

                            cidParts = (
                                p["Content-ID"][1:-1].split("@")[0].split("/")
                            )
                            logger.debug("cid parts: {}".format(cidParts))

                            if (
                                len(cidParts) == 4
                                and pcType == "application/json"
                            ):
                                gateway_id, camera_id, collection_time, step_name = (
                                    cidParts
                                )
                                filename = "index.json"
                                fpath = "index/index.json"
                            else:
                                gateway_id, camera_id, collection_time, step_name, filename = (
                                    cidParts
                                )
                                fpath = "{}/{}".format(step_name, filename)

                            camera_id = "video{}".format(camera_id)
                            logger.debug("fpath: {}".format(fpath))

                            bundle = (
                                camera_id,
                                collection_time,
                                hdrs["customer_id"],
                                gateway_id,
                                hdrs["nodename"],
                            )
                            bDict = dict(
                                zip(
                                    [
                                        "camera_id",
                                        "collection_time",
                                        "customer_id",
                                        "gateway_id",
                                        "nodename",
                                    ],
                                    bundle,
                                )
                            )
                            bDict["cid"] = ":".join(bundle)
                            body = p.get_payload(decode=True)
                            if bundle not in data["order"]:
                                data["order"].append(bundle)
                                metadata = json.loads(body.decode())
                                data[bundle] = AD()
                                data[bundle].update(bDict)
                                data[bundle].update(metadata)
                                data[bundle].bundle = bundle

                                baseEvent = AD(data[bundle])
                                baseEvent.end_time = metadata["steps"][-1][
                                    "endTime"
                                ]
                                baseEvent.filedata = {
                                    "metadata.json": metadata
                                }
                                baseEvent.filelist = ["metadata.json"]
                                baseEvent.function = "metadata"
                                baseEvent.function_version = 0
                                baseEvent.images = []
                                baseEvent.inputParameters = {}
                                baseEvent.metadata = metadata
                                baseEvent.output = {}
                                baseEvent.start_time = metadata["steps"][0][
                                    "startTime"
                                ]
                                baseEvent.step_config_version = 0
                                baseEvent.step_name = "baseEvent"

                                data[bundle]["stepData"] = AD()
                                data[bundle]["stepData"][
                                    "baseEvent"
                                ] = baseEvent
                                for step in data[bundle]["steps"]:
                                    data[bundle]["stepData"][
                                        step["name"]
                                    ] = step
                                    data[bundle]["stepData"][
                                        step["name"]
                                    ].update(bDict)
                                    data[bundle]["stepData"][step["name"]][
                                        "bundle"
                                    ] = tuple(list(bundle) + [step["name"]])
                                    data[bundle]["stepData"][step["name"]][
                                        "scid"
                                    ] = ":".join(list(bundle) + [step["name"]])
                                    data[bundle]["stepData"][step["name"]][
                                        "filedata"
                                    ] = {}
                                    data[bundle]["stepData"][step["name"]][
                                        "filelist"
                                    ] = []
                                    data[bundle]["stepData"][step["name"]][
                                        "images"
                                    ] = []
                                    data[bundle]["stepData"][step["name"]][
                                        "processor_role"
                                    ] = hdrs["nodename"].split("_")[0][:-2]
                                    data[bundle]["stepData"][step["name"]][
                                        "step_name"
                                    ] = step["name"]
                                data[bundle]["steps"] = ["baseEvent"] + [
                                    step["name"]
                                    for step in data[bundle]["steps"]
                                ]
                            else:
                                data[bundle]["stepData"][step_name][
                                    "filelist"
                                ].append(filename)
                                if pcType == "image/jpeg":
                                    data[bundle]["stepData"][step_name][
                                        "filedata"
                                    ][filename] = np.asarray(
                                        bytearray(body), dtype=np.uint8
                                    )
                                else:
                                    data[bundle]["stepData"][step_name][
                                        "filedata"
                                    ][filename] = body

                        # upack bundle dictionary into array of values
                        data = [data[k] for k in data["order"]]
                    except Exception as err:
                        logger.exception(
                            "_reqData: multipart decode exception: {}".format(
                                err
                            )
                        )
                else:
                    data = semi_raw_data
            else:
                raw_data = None
                data = None

            return (
                hdrs["customer_id"] if "customer_id" in hdrs else None,
                AD(hdrs),
                args,
                data,
                raw_data,
            )

        except TypeError as e:
            logger.exception(
                "{} API [{}]: TypeError in _reqData - {}".format(
                    rName.upper(),
                    request.method.upper().decode("utf-8"),
                    repr(request)[0:1000],
                )
            )

    def _return_error(
        self,
        rName,
        request,
        scode=500,
        err=None,
        msg=None,
        reqHeaders=None,
        retHeaders={},
    ):
        request.setResponseCode(scode)
        _headers = {"Content-Type": "application/json"}
        _headers.update(retHeaders)
        for k, v in _headers.items():
            request.setHeader(k, v)

        out = {
            "apiVersion": Api.API_VERSION,
            "error_source": rName,
            "error": err,
            "message": msg,
            "requestHeaders": reqHeaders,
            "resultCode": scode,
            "resultType": "ERROR",
            "uri": request.uri.decode(),
        }

        logger.exception(out)
        return self._json_safe(out)

    def _return_failure(
        self,
        rName,
        request,
        scode=500,
        msg="Failed",
        payload=None,
        reqHeaders=None,
        retHeaders={},
    ):
        request.setResponseCode(scode)
        _headers = {"Content-Type": "application/json"}
        _headers.update(retHeaders)
        for k, v in _headers.items():
            request.setHeader(k, v)

        out = (
            payload
            if payload
            else {
                "apiVersion": Api.API_VERSION,
                "method": request.method,
                "requestHeaders": reqHeaders,
                "resultCode": scode,
                "resultType": "FAILURE",
                "uri": request.uri.decode(),
            }
        )

        logger.error(out)
        return self._json_safe(out)

    def _return_success(
        self,
        rName,
        request,
        scode=200,
        msg="Success",
        payload=None,
        reqHeaders=None,
        retHeaders={},
    ):
        request.setResponseCode(scode)
        _headers = {"Content-Type": "application/json"}
        _headers.update(retHeaders)
        for k, v in _headers.items():
            request.setHeader(k, v)

        out = {"status": "ok"}

        logger.debug(out)
        return self._json_safe(out)

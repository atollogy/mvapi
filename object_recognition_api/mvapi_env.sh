export MVAPI_DATA=/data/mvapi_data
export MVAPI_HOME=/opt/mvapi
export MVAPI_CONF=/etc/kvhandler/bcm.json
export s3_write=1
export PYTHONPATH=/opt/mvapi/object_recognition_api:$PYTHONPATH
export LD_LIBRARY_PATH=/home/ubuntu/src/cntk/bindings/python/cntk/libs:/usr/local/cuda/lib64:/usr/local/lib:/usr/lib:/usr/local/cuda/extras/CUPTI/lib64:/usr/local/mpi/lib:
export CUDA_HOME=/usr/local/cuda
export CUDA_ROOT=/usr/local/cuda

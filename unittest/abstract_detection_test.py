import os
import simplejson
import numpy as np
import cv2
import tempfile

import context
import object_recognition_api
import object_detection
#from which_zone import which_box
from utils.cython_bbox import bbox_overlaps
#from model.config import cfg

#  minimum over lap to be considered a success
SUCCESS_OVERLAP = 0.4


class AbstractDetectionTest:
    def setUp(self, test_cfg):
        self.test_cfg = test_cfg
        object_recognition_api.object_detection.app.config["API_NAME"] = tempfile.mkstemp()
        object_recognition_api.object_detection.app.config["API_NAME"] = self.test_cfg.API_NAME
        #self.app = object_recognition_api.object_detection.app.test_client()
        #with object_recognition_api.object_detection.app.app_context():
        #    object_recognition_api.load_model()

    def test_detection(self):
        """
        test to make sure that shape of response is correct
        """
        interest_objs = self.test_cfg.INTEREST_OBJS
        blur_objs = []
        blur_loc = ""
        test_file_dir = self.test_cfg.TEST_CASES_DIR
        for test_file in os.listdir(test_file_dir):
            if not test_file.endswith(".json"):
                continue
            with open(test_file_dir + test_file) as f:
                ground_truth = simplejson.load(f)
                object_detection_params = {
                    "img_loc": ground_truth["img_loc"],
                    "interest_objs": simplejson.dumps(interest_objs),
                    "blur_objs": simplejson.dumps(blur_objs),
                    "blur_loc": blur_loc,
                }
                objects = self.app.get(
                    "/object_detection", query_string=object_detection_params
                )
                data = simplejson.loads(objects.get_data(as_text=True))

                self.assertEqual(
                    ground_truth["event_type"], data["event_type"]
                )
                self.assertEqual(ground_truth["sensor_id"], data["sensor_id"])
                self.assertEqual(
                    ground_truth["recorder_name"], data["recorder_name"]
                )
                self.assertEqual(
                    ground_truth["derived_location"], data["derived_location"]
                )
                self.assertEqual(
                    ground_truth["attributes"]["input_loc"],
                    data["attributes"]["input_loc"],
                )
                self.assertEqual(
                    ground_truth["attributes"]["output_loc"],
                    data["attributes"]["output_loc"],
                )
                self.assertIn("lookup_path", list(data["attributes"].keys()))
                self.assertEqual(
                    ground_truth["attributes"]["api_name"],
                    data["attributes"]["api_name"],
                )
                self.assertEqual(
                    set(interest_objs),
                    set(data["attributes"]["bboxes"].keys()),
                )

                self.assertEqual(ground_truth["assets"], data["assets"])
                self.assertEqual(ground_truth["cause"], data["cause"])

import unittest
import os
import sys

import context
from tools.test_net import run_test
from tools.trainval_net import run_train
from test_cfgs import test_wider_cfg as test_cfg
from net_cfgs import wider_cfg as net_cfg
from abstract_model_test import AbstractModelTest


class TestWiderModel(unittest.TestCase):
    def setUp(self):
        AbstractModelTest.setUp(self, net_cfg, test_cfg)


if __name__ == "__main__":
    unittest.main()

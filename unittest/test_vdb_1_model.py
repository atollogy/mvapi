import unittest
import os
import sys

import context
from tools.test_net import run_test
from tools.trainval_net import run_train
from test_cfgs import test_vdb_1_cfg as test_cfg
from net_cfgs import vdb_1_cfg as net_cfg


class TestVdb1Model(unittest.TestCase):
    @classmethod
    def tearDown(self):
        """
        make sure the files generated from unit test training are delted
        """
        os.system("rm -rf {}".format(test_cfg.UNITTEST_MODEL_DIR))
        os.system("rm -rf {}".format(test_cfg.CACHE_DIR))
        os.system("rm -rf {}".format(test_cfg.ANNOTATION_CACHE_DIR))

    @classmethod
    def tearDown(self):
        """
        delete the files generated from unit test training
        """
        os.system("rm -rf {}".format(test_cfg.UNITTEST_MODEL_DIR))
        os.system("rm -rf {}".format(test_cfg.CACHE_DIR))
        os.system("rm -rf {}".format(test_cfg.ANNOTATION_CACHE_DIR))

    def test_vdb_1_train(self):
        """
        test training process doesn't crash

        in the future probably want a more intelligent way of doing this to
        make sure training is actually occurring.
        """
        run_train(
            cfg_file=net_cfg.CFG,
            weight=test_cfg.WEIGHTS_TRAIN,
            tag=test_cfg.TAG,
            imdb_name=test_cfg.IMDB_TRAIN_NAME,
            imdbval_name=test_cfg.IMDB_TEST_NAME,
            net=net_cfg.NET_NAME,
            max_iters=test_cfg.MAX_ITERS,
            set_cfgs=test_cfg.SET_TRAIN_CFGS,
            model_selection_flag=False,
        )

    def test_vdb_1_test(self):
        """
        test whether scoring above a threshold on the test set
        """
        print(
            net_cfg.CFG,
            net_cfg.MODEL,
            test_cfg.TAG,
            test_cfg.IMDB_TEST_NAME,
            test_cfg.COMP_MODE_TEST,
            net_cfg.NET_NAME,
            test_cfg.MAX_PER_IMAGE,
            test_cfg.SET_TEST_CFGS,
        )
        mAP = run_test(
            cfg_file=net_cfg.CFG,
            model=net_cfg.MODEL,
            tag=test_cfg.TAG,
            imdb_name=test_cfg.IMDB_TEST_NAME,
            comp_mode=test_cfg.COMP_MODE_TEST,
            net=net_cfg.NET_NAME,
            max_per_image=test_cfg.MAX_PER_IMAGE,
            set_cfgs=test_cfg.SET_TEST_CFGS,
        )

        self.assertGreater(mAP, test_cfg.MIN_MAP)


if __name__ == "__main__":
    unittest.main()

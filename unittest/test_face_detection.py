import unittest
from test_cfgs import test_wider_cfg as test_cfg
from abstract_detection_test import AbstractDetectionTest


class FaceDetectionTest(AbstractDetectionTest, unittest.TestCase):
    def setUp(self):
        AbstractDetectionTest.setUp(self, test_cfg)


if __name__ == "__main__":
    unittest.main()

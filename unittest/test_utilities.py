import os
import unittest
import cv2

import context
import object_detection
import api_utils
#from which_zone import which_box
#from s3_config import DEV_CONN
DEV_CONN = None

class UtilityTest(unittest.TestCase):
    def test_blur(self):
        """
        test to make sure that ONLY the pixels in the area given by
        bbox*k_percent_larger are zero'd out to be black
        """
        img_loc = "/atl-hub-apdx-image-data/person/test/office.jpg"
        img_name = img_loc.split("/")[-1]
        bboxes = [
            [398, 24, 596, 339],
            [842, 67, 1042, 519],
            [390, 527, 742, 893],
        ]
        object_detection.cp_from_s3(DEV_CONN, img_loc)
        im = cv2.imread(img_name)
        blurred_im = object_detection.blur_box(im, bboxes)

        #  make sure all bboxes are blacked out
        for box in bboxes:
            self.assertEqual(
                blurred_im[box[1] : box[3], box[0] : box[2]].all(), 0
            )
        #  assert that aside from blacked out boxes ims are equal
        object_detection.rm_file(img_name)

    def test_zone_detection(self):
        """
        check to make sure that 3 of the zones are filled and one is not
        """
        true_obj_zone = [1, 2, 3]
        bboxes = [
            [398, 24, 596, 339, 0.99],
            [842, 67, 1042, 519, 0.98],
            [390, 527, 742, 893, 0.99],
        ]
        zones = [
            [[388, 15], [703, 375]],
            [[804, 58], [1099, 388]],
            [[355, 490], [745, 920]],
            [[830, 400], [1175, 820]],
        ]
        objs_summary = [] #which_box(bboxes, zones)

        for i, obj in enumerate(objs_summary):
            self.assertEqual(obj["zone"], true_obj_zone[i])

    def test_parse_filename(self):
        f_name = "/atl-prd-mcroskey-rawdata/images/atlgw01-b827eb63ac2f_mcroskey_prd/2017-06-14/b827eb63ac2f_CAM1_20170614000000-snapshot.jpg"
        sensor_id, timestamp, recorder_name, recorder_id, cause = api_utils.parse_name(
            f_name
        )
        self.assertEqual(sensor_id, "1")
        self.assertEqual(timestamp, "20170614000000")
        self.assertEqual(recorder_name, "atlgw01-b827eb63ac2f_mcroskey_prd")
        self.assertEqual(recorder_id, "b827eb63ac2f")
        self.assertEqual(cause, "snapshot")

        f_name = "/atl-prd-mcroskey-rawdata/images/atlgw01-b827eb63ac2f_mcroskey_prd/\
                  2017-06-14/b827eb63ac2f_CAM1_20170614000000_snapshot.jpg"
        _, timestamp, _, _, cause = api_utils.parse_name(f_name)
        self.assertEqual(timestamp, "20170614000000")
        self.assertEqual(cause, "snapshot")


if __name__ == "__main__":
    unittest.main()

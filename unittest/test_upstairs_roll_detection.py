import unittest
from test_cfgs import test_upstairs_roll_cfg as test_cfg
from abstract_detection_test import AbstractDetectionTest


class UpstairsRollDetectionTest(AbstractDetectionTest, unittest.TestCase):
    def setUp(self):
        AbstractDetectionTest.setUp(self, test_cfg)


if __name__ == "__main__":
    unittest.main()

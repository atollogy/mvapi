"""Set up paths for Faster R-CNN tests"""

import os.path as osp
import sys


def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)


this_dir = osp.dirname(__file__)

# Add lib to PYTHONPATH
lib_path = osp.join(this_dir, "..", "tools")
add_path(lib_path)

api_path = osp.join(this_dir, "..", "object_recognition_api")
add_path(api_path)

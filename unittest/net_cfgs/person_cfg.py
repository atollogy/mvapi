import os

mvhome = os.path.join("", "opt", "mvapi")
if "MVAPI_HOME" in os.environ:
    mvhome = os.environ["MVAPI_HOME"]

NAME = "COCO"
NET_NAME = "res101"
CFG = os.path.join(mvhome, "experiments/cfgs/res101.yml")
ANCHOR_SCALES = [4, 8, 16, 32]
ANCHOR_RATIOS = [.5, 1, 2]

CLASSES = [
    "__background__",
    "person",
    "bicycle",
    "car",
    "motorcycle",
    "airplane",
    "bus",
    "train",
    "truck",
    "boat",
    "traffic light",
    "fire hydrant",
    "stop sign",
    "parking meter",
    "bench",
    "bird",
    "cat",
    "dog",
    "horse",
    "sheep",
    "cow",
    "elephant",
    "bear",
    "zebra",
    "giraffe",
    "backpack",
    "umbrella",
    "handbag",
    "tie",
    "suitcase",
    "frisbee",
    "skis",
    "snowboard",
    "sports ball",
    "kite",
    "baseball bat",
    "baseball glove",
    "skateboard",
    "surfboard",
    "tennis racket",
    "bottle",
    "wine glass",
    "cup",
    "fork",
    "knife",
    "spoon",
    "bowl",
    "banana",
    "apple",
    "sandwich",
    "orange",
    "broccoli",
    "carrot",
    "hot dog",
    "pizza",
    "donut",
    "cake",
    "chair",
    "couch",
    "potted plant",
    "bed",
    "dining table",
    "toilet",
    "tv",
    "laptop",
    "mouse",
    "remote",
    "keyboard",
    "cell phone",
    "microwave",
    "oven",
    "toaster",
    "sink",
    "refrigerator",
    "book",
    "clock",
    "vase",
    "scissors",
    "teddy bear",
    "hair drier",
    "toothbrush",
]

production_model_dir = os.path.join(
    mvhome,
    "output/res101/coco_2014_train+coco_2014_valminusminival/production_model/",
)

MODEL = os.path.join(production_model_dir, "res101_faster_rcnn_iter_31000.ckpt")
if os.path.isdir(production_model_dir):
    for file_name in os.listdir(production_model_dir):
        if file_name.endswith(".ckpt.meta"):
            MODEL = production_model_dir + file_name.replace(".meta", "")

S3_WARMUP_LOC = "/atl-hub-apdx-image-data/person/warmup/"
LOCAL_WARMUP_LOC = "person_warmup/"

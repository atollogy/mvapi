import os

mvhome = os.path.join("", "opt", "mvapi")
if "MVAPI_HOME" in os.environ:
    mvhome = os.environ["MVAPI_HOME"]

NAME = "auto_tufting"
NET_NAME = "res101"
CFG = os.path.join(mvhome, "experiments/cfgs/res101.yml")
ANCHOR_SCALES = [4, 8, 16, 32]
ANCHOR_RATIOS = [0.5, 1, 2]
CLASSES = ("__background__", "person", "door_closed", "door_open")

production_model_dir = os.path.join(
    mvhome,
    "output/res101/{}_train/production_model/".format(NAME),
)

MODEL = os.path.join(production_model_dir, "res101_faster_rcnn_iter_31000.ckpt")
if os.path.isdir(production_model_dir):
    for file_name in os.listdir(production_model_dir):
        if file_name.endswith(".ckpt.meta"):
            MODEL = production_model_dir + file_name.replace(".meta", "")
S3_WARMUP_LOC = "/atl-hub-apdx-image-data/{}/warmup/".format(NAME)
LOCAL_WARMUP_LOC = "{}_warmup/".format(NAME)

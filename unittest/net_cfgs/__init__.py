from . import auto_tufting_cfg as at_cfg
from . import auto_tufting_cam2_cfg as atc2_cfg
from . import manual_tufting_cfg as mt_cfg
from . import downstairs_roll_cfg as dsr_cfg
from . import upstairs_roll_cfg as usr_cfg
from . import service_table_cfg as srt_cfg
from . import friends_cfg as frd_cfg
from . import vdb_1_cfg as vd1_cfg
from . import vdb_2_cfg as vd2_cfg

cfgs = {
    "auto_tufting": at_cfg,
    "auto_tufting_cam2": atc2_cfg,
    "manual_tufting": mt_cfg,
    "downstairs_roll": dsr_cfg,
    "upstairs_roll": usr_cfg,
    "service_table": srt_cfg,
    "friends": frd_cfg,
    "vdb_1": vd1_cfg,
    "vdb_2": vd2_cfg,
}

API_NAME = "vdb_1"
INTEREST_OBJS = ["person"]
MIN_MAP = 0.8
IMDB_TEST_NAME = "{}_test".format(API_NAME)
IMDB_TRAIN_NAME = "{}_train".format(API_NAME)
COMP_MODE_TEST = True
COMP_MODE_TRAIN = False
TAG = "test"
MAX_PER_IMAGE = 100
SET_TRAIN_CFGS = [
    "TRAIN.STEPSIZE",
    "5000",
    "ANCHOR_SCALES",
    "[4,8,16,32]",
    "ANCHOR_RATIOS",
    "[0.5,1,2]",
]
SET_TEST_CFGS = ["ANCHOR_SCALES", "[4,8,16,32]", "ANCHOR_RATIOS", "[0.5,1,2]"]
# small number since we aren't actually testing the training process only that it
# doesn't crash
MAX_ITERS = 100

from .test_dirs import getDirs

WEIGHTS_TRAIN, TEST_CASES_DIR, UNITTEST_MODEL_DIR, CACHE_DIR, ANNOTATION_CACHE_DIR = getDirs(
    API_NAME, IMDB_TRAIN_NAME
)

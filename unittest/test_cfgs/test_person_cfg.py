API_NAME = "person"
INTEREST_OBJS = ["person"]
MIN_MAP = 0.27  # COCO mAP is very low(but this is standard in research)
# couldn't find annotations for wider dataset w/out matlab
IMDB_TEST_NAME = "coco_2014_minival"
IMDB_TRAIN_NAME = "coco_2014_train+coco_2014_valminusminival"
COMP_MODE_TEST = True
COMP_MODE_TRAIN = False
TAG = "test"
MAX_PER_IMAGE = 100
SET_TRAIN_CFGS = [
    "TRAIN.STEPSIZE",
    "50000",
    "ANCHOR_SCALES",
    "[4,8,16,32]",
    "ANCHOR_RATIOS",
    "[0.5,1,2]",
]
SET_TEST_CFGS = ["ANCHOR_SCALES", "[4,8,16,32]", "ANCHOR_RATIOS", "[0.5,1,2]"]
# small number since we aren't actually testing the training process only that it
# doesn't crash
MAX_ITERS = 100

from .test_dirs import getDirs

WEIGHTS_TRAIN, TEST_CASES_DIR, UNITTEST_MODEL_DIR, CACHE_DIR, ANNOTATION_CACHE_DIR = getDirs(
    API_NAME, IMDB_TRAIN_NAME
)

import os

mvhome = os.path.join("", "opt", "mvapi")
if "MVAPI_HOME" in os.environ:
    mvhome = os.environ["MVAPI_HOME"]

mvdata = os.path.join("", "data", "mvapi_data")
if "MVAPI_DATA" in os.environ:
    mvdata = os.environ["MVAPI_DATA"]

def getDirs(api_name, imdb_train_name):
    WEIGHTS_TRAIN = os.path.join(
        mvdata, "imagenet_weights/res101.ckpt"
    )

    TEST_CASES_DIR = os.path.join(
        mvhome, "tests/test_cases", api_name
    )

    UNITTEST_MODEL_DIR = os.path.join(
        mvdata, "output/res101", imdb_train_name, "test"
    )

    CACHE_DIR = os.path.join(mvdata, "cache")

    ANNOTATION_CACHE_DIR = os.path.join(
        mvdata, "input", api_name, "annnotations_cache"
    )

    return (
        WEIGHTS_TRAIN,
        TEST_CASES_DIR,
        UNITTEST_MODEL_DIR,
        CACHE_DIR,
        ANNOTATION_CACHE_DIR,
    )

import unittest
import os
import sys

import context
from tools.test_net import run_test
from tools.trainval_net import run_train


class AbstractModelTest:
    def setUp(self, net_cfg, test_cfg):
        self.test_cfg = test_cfg
        self.net_cfg = net_cfg
        os.system("rm -rf {}".format(self.test_cfg.UNITTEST_MODEL_DIR))
        os.system("rm -rf {}".format(self.test_cfg.CACHE_DIR))
        os.system("rm -rf {}".format(self.test_cfg.CACHE_DIR))
        os.system("rm -rf {}".format(self.test_cfg.ANNOTATION_CACHE_DIR))

    def tearDown(self):
        """
        delete the files generated from unit test training
        """
        os.system("rm -rf {}".format(self.test_cfg.UNITTEST_MODEL_DIR))
        os.system("rm -rf {}".format(self.test_cfg.CACHE_DIR))
        os.system("rm -rf {}".format(self.test_cfg.ANNOTATION_CACHE_DIR))

    def test_model_train(self):
        """
        test training process doesn't crash

        in the future probably want a more intelligent way of doing this to
        make sure training is actually occurring.
        """
        run_train(
            cfg_file=self.net_cfg.CFG,
            weight=self.test_cfg.WEIGHTS_TRAIN,
            tag=self.test_cfg.TAG,
            imdb_name=self.test_cfg.IMDB_TRAIN_NAME,
            imdbval_name=self.test_cfg.IMDB_TEST_NAME,
            net=self.net_cfg.NET_NAME,
            max_iters=self.test_cfg.MAX_ITERS,
            set_cfgs=self.test_cfg.SET_TRAIN_CFGS,
            model_selection_flag=False,
        )

    def test_model_test(self):
        """
        test whether scoring above a threshold on the test set
        """
        mAP = run_test(
            cfg_file=self.net_cfg.CFG,
            model=self.net_cfg.MODEL,
            tag=self.test_cfg.TAG,
            imdb_name=self.test_cfg.IMDB_TEST_NAME,
            comp_mode=self.test_cfg.COMP_MODE_TEST,
            net=self.net_cfg.NET_NAME,
            max_per_image=self.test_cfg.MAX_PER_IMAGE,
            set_cfgs=self.test_cfg.SET_TEST_CFGS,
        )

        self.assertGreater(mAP, self.test_cfg.MIN_MAP)

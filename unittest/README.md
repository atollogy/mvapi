# Object Recognition Testing
When a new model trained to detect a unique set of objects is introduced we create 2 separate unit testing scripts. 

## Model Evaluation
Model evaluation unit tests assure that minimum performance thresholds are being hit and also that the training process can be run 
without crashing. We are primarily concerned with mAP(mean average precision). mAP thresholds are set with respect to the underlying 
complexity of the dataset we are working with. For example, breaking 0.70 mAP on COCO dataset is much more difficult than breaking 70 
mAP on pascal voc. 

In the future it would likely make sense to evaluate the training process in a more intelligent manner. Ex) checking to make sure 
that model is actually learning. Although assuming we built a decent test set a model that is doing a poor job training would fail 
the testing process. However, thorough testing of the training process could help us with debugging.

## API Testing
API unit tests assure us that if valid params are passed in nothing will explode.

## TO DO
* Detection test is currently shit in terms of accuracy. We are only checking that detections show up in ground truth. It is not currently checking for duplicates or boxes that appear in ground truth that were not detected by our model. This isn't a big deal though b/c the purpose of this test is to make sure our api is responding w/ the right shape. The actual model tests are what verifies precision.
* make an abstract class for detection and model tests
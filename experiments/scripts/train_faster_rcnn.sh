#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"

GPU_ID=$1
DATASET=$2
NET=$3

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:3:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

case ${DATASET} in
  friends)
    TRAIN_IMDB="friends_train"
    VAL_IMDB="friends_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  vdb_1)
    TRAIN_IMDB="vdb_1_train"
    VAL_IMDB="vdb_1_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  vdb_2)
    TRAIN_IMDB="vdb_2_train"
    VAL_IMDB="vdb_2_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  service_table)
    TRAIN_IMDB="service_table_train"
    VAL_IMDB="service_table_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;	
  auto_tufting)
    TRAIN_IMDB="auto_tufting_train"
    VAL_IMDB="auto_tufting_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;	
  auto_tufting_cam2)
    TRAIN_IMDB="auto_tufting_cam2_train"
    VAL_IMDB="auto_tufting_cam2_val"
    STEPSIZE=50000
    ITERS=70000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;	
  manual_tufting)
    TRAIN_IMDB="manual_tufting_train"
    VAL_IMDB="manual_tufting_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  upstairs_roll)
    TRAIN_IMDB="upstairs_roll_train"
    VAL_IMDB="upstairs_roll_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]" 
    RATIOS="[0.5,1,2]"
    ;;
  roll)
    TRAIN_IMDB="roll_train"
    VAL_IMDB="roll_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]" 
    RATIOS="[0.5,1,2]"
    ;;
  downstairs_roll)
    TRAIN_IMDB="downstairs_roll_train"
    VAL_IMDB="downstairs_roll_val"
    STEPSIZE=50000
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  wider)
    TRAIN_IMDB="wider_train"
    VAL_IMDB="wider_val"
    STEPSIZE=50000
    ITERS=70000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  pascal_voc)
    TRAIN_IMDB="voc_2007_trainval"
    VAL_IMDB="voc_2007_test"
    STEPSIZE=50000
    ITERS=70000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  pascal_voc_0712)
    TRAIN_IMDB="voc_2007_trainval+voc_2012_trainval"
    VAL_IMDB="voc_2007_test"
    STEPSIZE=50000
    ITERS=70000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  coco)
    TRAIN_IMDB="coco_2014_train+coco_2014_valminusminival"
    VAL_IMDB="coco_2014_minival"
    STEPSIZE=350000
    ITERS=490000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  *)
    echo "No dataset given"
    exit
    ;;
esac

LOG="${MVAPI_DATA}/logs/${NET}_${TRAIN_IMDB}_${EXTRA_ARGS_SLUG}_${NET}.`date +'%Y-%m-%d_%H-%M-%S'`.txt"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

set +x
if [[ ! -z  ${EXTRA_ARGS_SLUG}  ]]; then
    NET_FINAL=${MVAPI_DATA}/output/${NET}/${TRAIN_IMDB}/${EXTRA_ARGS_SLUG}/${NET}_faster_rcnn_iter_${ITERS}.ckpt
else
    NET_FINAL=${MVAPI_DATA}/output/${NET}/${TRAIN_IMDB}/default/${NET}_faster_rcnn_iter_${ITERS}.ckpt
fi
set -x

if [ !  -f ${NET_FINAL}.index ]; then
    if [[ ! -z  ${EXTRA_ARGS_SLUG}  ]]; then
        CUDA_VISIBLE_DEVICES=${GPU_ID} time python ${MVAPI_HOME}/tools/trainval_net.py \
            --weight ${MVAPI_DATA}/imagenet_weights/${NET}.ckpt \
            --imdb ${TRAIN_IMDB} \
            --imdbval ${VAL_IMDB} \
            --iters ${ITERS} \
            --cfg ${MVAPI_HOME}/experiments/cfgs/${NET}.yml \
            --tag ${EXTRA_ARGS_SLUG} \
            --net ${NET} \
            --model_selection_flag True \
            --set TRAIN.STEPSIZE ${STEPSIZE} ANCHOR_SCALES ${ANCHORS} ANCHOR_RATIOS ${RATIOS} ${EXTRA_ARGS}
    else
        CUDA_VISIBLE_DEVICES=${GPU_ID} time python ${MVAPI_HOME}/tools/trainval_net.py \
            --weight ${MVAPI_DATA}/imagenet_weights/${NET}.ckpt \
            --imdb ${TRAIN_IMDB} \
            --imdbval ${VAL_IMDB} \
            --iters ${ITERS} \
            --cfg ${MVAPI_HOME}/experiments/cfgs/${NET}.yml \
            --net ${NET} \
            --model_selection_flag True \
            --set TRAIN.STEPSIZE ${STEPSIZE} ANCHOR_SCALES ${ANCHORS} ANCHOR_RATIOS ${RATIOS} ${EXTRA_ARGS}
    fi
fi

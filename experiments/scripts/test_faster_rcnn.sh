#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"

GPU_ID=$1
DATASET=$2
NET=$3

array=( $@ )
len=${#array[@]}
EXTRA_ARGS=${array[@]:3:$len}
EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}

case ${DATASET} in
  friends)
    TRAIN_IMDB="friends_train"
    TEST_IMDB="friends_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  vdb_1)
    TRAIN_IMDB="vdb_1_train"
    TEST_IMDB="vdb_1_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  vdb_2)
    TRAIN_IMDB="vdb_2_train"
    TEST_IMDB="vdb_2_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  service_table)
    TRAIN_IMDB="service_table_train"
    TEST_IMDB="service_table_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  auto_tufting)
    TRAIN_IMDB="auto_tufting_train"
    TEST_IMDB="auto_tufting_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  auto_tufting_cam2)
    TRAIN_IMDB="auto_tufting_cam2_train"
    TEST_IMDB="auto_tufting_cam2_test"
    ITERS=31000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  manual_tufting)
    TRAIN_IMDB="manual_tufting_train"
    TEST_IMDB="manual_tufting_test"
    ITERS=34000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  upstairs_roll)
    TRAIN_IMDB="upstairs_roll_train"
    TEST_IMDB="upstairs_roll_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  roll)
    TRAIN_IMDB="roll_train"
    TEST_IMDB="roll_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  wider)
    TRAIN_IMDB="wider_train"
    TEST_IMDB="wider_val"
    ITERS=70000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  downstairs_roll)
    TRAIN_IMDB="downstairs_roll_train"
    TEST_IMDB="downstairs_roll_test"
    ITERS=35000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;; 
  pascal_voc)
    TRAIN_IMDB="voc_2007_trainval"
    TEST_IMDB="voc_2007_test"
    ITERS=70000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  pascal_voc_0712)
    TRAIN_IMDB="voc_2007_trainval+voc_2012_trainval"
    TEST_IMDB="voc_2007_test"
    ITERS=110000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  coco)
    TRAIN_IMDB="coco_2014_train+coco_2014_valminusminival"
    TEST_IMDB="coco_2014_minival"
    ITERS=490000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  *)
    echo "No dataset given"
    exit
    ;;
esac

LOG="${MVAPI_DATA}/logs/test_${NET}_${TRAIN_IMDB}_${EXTRA_ARGS_SLUG}.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

set +x
if [[ ! -z  ${EXTRA_ARGS_SLUG}  ]]; then
  NET_FINAL=${MVAPI_DATA}/output/${NET}/${TRAIN_IMDB}/${EXTRA_ARGS_SLUG}/${NET}_faster_rcnn_iter_${ITERS}.ckpt
else
  NET_FINAL=${MVAPI_DATA}/output/${NET}/${TRAIN_IMDB}/default/${NET}_faster_rcnn_iter_${ITERS}.ckpt
fi
set -x

if [[ ! -z  ${EXTRA_ARGS_SLUG}  ]]; then
  CUDA_VISIBLE_DEVICES=${GPU_ID} time python .${MVAPI_HOME}/tools/test_net.py \
    --imdb ${TEST_IMDB} \
    --model ${NET_FINAL} \
    --cfg ${MVAPI_HOME}/experiments/cfgs/${NET}.yml \
    --tag ${EXTRA_ARGS_SLUG} \
    --net ${NET} \
    --set ANCHOR_SCALES ${ANCHORS} ANCHOR_RATIOS ${RATIOS} ${EXTRA_ARGS}
else
  CUDA_VISIBLE_DEVICES=${GPU_ID} time python .${MVAPI_HOME}/tools/test_net.py \
    --imdb ${TEST_IMDB} \
    --model ${NET_FINAL} \
    --cfg ${MVAPI_HOME}/experiments/cfgs/${NET}.yml \
    --net ${NET} \
    --set ANCHOR_SCALES ${ANCHORS} ANCHOR_RATIOS ${RATIOS} ${EXTRA_ARGS}
fi



# Synchronize 'atl-prd--zoo/.ml/model', 'atl-stg--zoo/.ml/model',
#  'atl-demo--zoo/.ml/model' buckets with 'atl-dev--zoo/.ml/model'

import boto3

####################
## Set Parameters
####################

aws = {
    "prd": {
    },
    "dev": {
    },

    "stg": {
    },

    "demo": {
    }

}

source_env = 'dev'
source_bucket = 'atl-dev--zoo'
model_folder = '.ml/model'

target_envs = ['prd', 'demo', 'stg']


def upload_to_bucket(dest_s3_obj, key):
    '''
    Upload files from S3 buckets
    :param dest_s3_obj: dest s3 resource bucket object
    :param key: Key of the file in S3 Key format. Ex:'.ml/model/ground_operations/0/pipeline.config'
    :return:
    '''
    file_name = '_'.join(key.split(sep='/')[len(model_folder.split('/')):])
    dest_s3_obj.upload_file('/tmp/'+file_name, key)


def download_from_bucket(src_s3_obj, key, file_name):
    '''
    Download files from S3 buckets
    :param src_s3_obj: source s3 resource bucket object
    :param key: Key of the file in S3 Key format. Ex:'.ml/model/ground_operations/0/pipeline.config'
    :param file_name: name of file when downloaded to local machine
    :return:
    '''
    src_s3_obj.download_file(key, '/tmp/'+file_name)
    pass


def create_s3_objects(environment, bucket):
    '''
    Creates boto3 resource bucket objects
    :param environment: environment Ex: 'prd','src' etc
    :param bucket: Bucket to point the S3 object. Ex: atl-dev--zoo
    :return: bucket_s3: boto3 resource bucket object pointing to a 'bucket' in 'environment'
    '''
    s3 = boto3.resource('s3',aws_access_key_id=aws[environment]['id'],
                  aws_secret_access_key=aws[environment]['key'],
                  region_name='us-west-2')

    bucket_s3 = s3.Bucket(bucket)

    return bucket_s3


def copy_s3_objects(file_name, bucket, dest_bucket_s3_obj):
    '''
    Copies files from one S3 bucket to another in same AWS credentials environment
    :param file_name: file_name in the format of S3 Key to be copied. Ex: '.ml/model/ground_operations/0/pipeline.config'
    :param bucket: source bucket from where filename is copied. Ex: 'atl-dev--zoo'
    :param dest_bucket_s3_obj: destination s3 bucket object where files need to be copied
    :return:
    '''

    copy_source = {'Bucket': bucket, 'Key': file_name}
    dest_bucket_s3_obj.copy(copy_source, file_name)

def get_files_modified(src, src_s3_obj, dest, dest_s3_obj):
    """
    Get list of files modified between source s3 bucket and dest s3 bucket
    :param src: list of files in src bucket
    :param src_s3_obj: source s3 bucket object
    :param dest: list of files in dest object
    :param dest_s3_obj: dest s3 bucket object
    :return:
    """

    modified = list(set(src) & set(dest))

    dt_src = [(object.last_modified, object.key) for object in src_s3_obj.objects.filter(Prefix=model_folder) if object.key in modified]
    dt_dest = [(object.last_modified, object.key) for object in dest_s3_obj.objects.filter(Prefix=model_folder) if
              object.key in modified]

    out = []

    for i, val in enumerate(dt_src):
        if val[0] > dt_dest[i][0]:
            out.append(val[1])

    return out


def main():

    # Source S3 is always 'dev'
    # Destination S3 can be any of envs
    bucket_src_obj = create_s3_objects(source_env, source_bucket)

    # Getting the files at the bottom level of the tree.
    obj_src_names = [object.key for object in bucket_src_obj.objects.filter(Prefix=model_folder) if object.key[-1] != '/']

    for env in target_envs:

        bucket_dest_obj = create_s3_objects(env, 'atl-{}--zoo'.format(env))

        # Getting the files at the bottom level of the tree.
        obj_dest_names = [object.key for object in bucket_dest_obj.objects.filter(Prefix=model_folder) if
                          object.key[-1] != '/']

        # Finding the files in atl-dev--zoo and not in atl-{}--zoo
        new_added = list(set(obj_src_names) - set(obj_dest_names))

        # Finding files modified in atl-dev--zoo and not in atl-{}--zoo
        modified_files = get_files_modified(obj_src_names, bucket_src_obj, obj_dest_names, bucket_dest_obj)

        if env not in ['prd', 'stg']:

            # If env is not prd, then copy files with S3 buckets. Else, download and upload files since
            # https: // medium.com / tensult / copy - s3 - bucket - objects - across - aws - accounts - e46c15c4b9e1

            # Copy each file across buckets

            for file_key in new_added+modified_files:
                copy_s3_objects(file_key, source_bucket, bucket_dest_obj)
                print('Copied to atl-{}--zoo/{}'.format(env,file_key))

        else:

            for file_key in new_added+modified_files:

                # Download files from dev S3 to /tmp
                file_name = '_'.join(file_key.split(sep='/')[len(model_folder.split('/')):])
                download_from_bucket(bucket_src_obj, file_key, file_name)
                print('Downloaded from atl-dev--zoo/{}'.format(file_key))

                # Upload files from /tmp to prd S3
                upload_to_bucket(bucket_dest_obj, file_key)
                print('Uploaded atl-{}--zoo/{}'.format(env,file_key))


if __name__ == '__main__':
    main()

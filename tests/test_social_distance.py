#!/usr/bin/env python3

import os
import numpy as np
import cv2

# import the system under test
from attribute_dict import AD
from image.utilities import *
from mv_functions.social_distance import *


SD_step_output = {
                'bboxes': {
                    'operator': [[179, 174, 112, 281, 1.0]],
                    'person': [[771, 56, 146, 183, 0.9999998807907104]]
                },
                'brightness': 102.11002900865343,
                'excluded': {},
                'inference_time': 0.45399999991059303,
                'object_group_list': [
                    {
                        'detected_object_class': {'text': 'person'},
                        'detected_object_list': [
                            {
                                'bbox': {
                                    'xmax': 291,
                                    'xmin': 179,
                                    'ymax': 455,
                                    'ymin': 174
                                },
                                'confidence_score': 1.0
                            },
                            {
                                'bbox': {
                                    'xmax': 917,
                                    'xmin': 771,
                                    'ymax': 239,
                                    'ymin': 56
                                },
                                'confidence_score': 0.9999998807907104
                            }
                        ]
                    }
                ],
                'reason': None,
                'regions': {
                    'Press_10_Main_station_operator': {
                        'xmax': 750,
                        'xmin': 70,
                        'ymax': 760,
                        'ymin': 10
                    }
                },
                'resolution': '(768, 1024)',
                'status': 'success',
                'subjects': {'non-operator': 1, 'person': 1},
                'total_mvapi_time': 0.776999999769032
            }

SD_step_config = {
        "target_objects": ["person", "operator"],
        "perspective_transform": {
            "origin_points" : {
                'top_left': [154, 118],
                'top_right': [735, 93],
                'bottom_left': [149, 661],
                'bottom_right': [787, 644]
            },
            "scale_translation": 200
        }
    }


def test_perspective_transform():

    sd_config = SocialDistanceCfg(**SD_step_config)

    origin_coordinate = [[433,219]]
    transform_cd = SocialDistanceATL().run_ppt(sd_config.perspective_transform, origin_coordinate)

    assert transform_cd[0][0] == 650, "Failed to transform x coordinate properly"
    assert transform_cd[0][1] == 420, "Failed to transform y coordinate properly"


def test_social_distance_atl():

    sd_config = SocialDistanceCfg(**SD_step_config)
    sd_atl = SocialDistanceATL()

    origin_centers = sd_atl.get_centerpoint(SD_step_output['bboxes'], sd_config.target_objects)
    transform_cd = sd_atl.run_ppt(sd_config.perspective_transform, origin_centers)

    assert int(transform_cd[0][0]) == 1100, "Failed to transform x coordinate properly"
    assert int(transform_cd[0][1]) == 360, "Failed to transform y coordinate properly"

    assert int(transform_cd[1][0]) == 440, "Failed to transform x coordinate properly"
    assert int(transform_cd[1][1]) == 510, "Failed to transform y coordinate properly"
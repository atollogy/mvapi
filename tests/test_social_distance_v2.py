#!/usr/bin/env python3

import os
import numpy as np
import cv2

# import the system under test
from attribute_dict import AD
from image.utilities import *
from mv_functions.social_distance_v2 import *

td_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, "testdata")

sdv2_step_config = AD({
            "center_shift": -1,
            "dtz_top_x_adjust": 0,
            "dtz_bottom_x_adjust": 0,
            "dtz_top_y_adjust": 1.25,
            "focal_length": 2.8,
            "height": 114,
            "x_angle": 4,
            "rotate": 2,
            "y_angle": 24.5,
            "version": 2
        })

def test_social_distance_v2(monkeypatch):
    cfg = Social_Distance_Cfg_V2(**sdv2_step_config)
    ref_image = cv2.imread(f'{td_dir}/9c146374998a_video0_1598537154_operators_20200827140554021807_activation_anonymous.jpg')
    sd = Social_Distance_V2('dw4cam07', cfg, ref_image)
    rd = AD.load(f'{td_dir}/dw4cam07_step_input.json')
    sd_params, undistorted, undistorted_with_trapezoid  = sd.apply_transformations()
    dimg = sd.filter_and_calculate_bboxes(rd, rd.output.bboxes, undistorted.copy())

    object_bottom_midpoints =  [
        (608,146),
        (172,270),
        (1056,623),
        (320,515)
    ]

    display_trapezoid = AD(
       {
            'bottom_left': [54, 720],
            'bottom_right': [1089, 720],
            'top_left': [177, 71],
            'top_right': [966, 71]
        }
    )

    distances = AD(
        {
            '0': [],
            '1': [[[(608, 146), (1056, 623)], 8.59],
                  [[(172, 270), (1056, 623)], 10.01]],
            '2': [[[(320, 515), (608, 146)], 6.39],
                  [[(320, 515), (1056, 623)], 7.15]],
            '3': [[[(172, 270), (608, 146)], 4.55],
                  [[(172, 270), (320, 515)], 4.09]],
            '4': []
        }
    )

    assert distances == rd.output.distances, "Failed to calculate correct distances"
    assert object_bottom_midpoints == rd.output.object_bottom_midpoints, "Failed to calc correct object_bottom_midpoints"
    assert display_trapezoid == rd.output.sd_params.display_trapezoid, "Failed to generate correct display_trapezoid"

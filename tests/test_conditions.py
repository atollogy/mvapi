try:
    import sys
    sys.path.append('object_recognition_api')
    from attribute_dict import *
    from mv_functions.conditions import evaluate_ppe
    from image.utilities import load_image
except:
    from object_recognition_api.mv_functions.conditions import evaluate_ppe

import numpy as np
'''
with open('testdata/cage_safe2.jpg', 'rb') as file:
    idata = file.read()

ppe_violation_cfg = AD({
        "iou_threshold": 0,
        "name": "ppe",
        "primary": "operator",
        "conditions": {
            "hard_hat": {
                "alert": True,
                "annotation": "no hard hat",
                "descriptors": {
                    "hard_hat": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6
            },
            "safety_vest": {
                "alert": True,
                "annotation": "no safety vest",
                "descriptors": {
                    "safety_garment": False,
                    "safety_vest": False
                },
                "enabled": True,
                "min_count": 6,
                "threshold": 0.6
            }
        }
    })

def test_incident_saferack_operator_vest_hat():
    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                    ],
                    "safety_vest": [
                        np.array([100, 300, 200, 200, 1.00]),
                    ],
                    "hard_hat": [
                        np.array([150, 100, 50, 50, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 0
    assert egc['safety_vest'] == 0


def test_incident_saferack_operator_garment_hat():
    rd = AD({"filedata": [],
            "collection_time": 1621892175.467852,
            "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                    ],
                    "safety_garment": [
                        np.array([100, 300, 200, 200, 1.00]),
                    ],
                    "hard_hat": [
                        np.array([150, 100, 50, 50, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    rd.image = idata
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 0
    assert egc['safety_vest'] == 0


def test_incident_saferack_operator_hat():
    rd = AD({"filedata": [],
            "collection_time": 1621892175.467852,
            "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                    ],
                    "hard_hat": [
                        np.array([150, 100, 50, 50, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 0
    assert egc['safety_vest'] == 1


def test_incident_saferack_operator_vest():
    rd = AD({"filedata": [],
            "collection_time": 1621892175.467852,
            "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                    ],
                    "safety_vest": [
                        np.array([100, 300, 200, 200, 1.00]),
                    ],
                    "hard_hat": [
                        np.array([150, 100, 50, 50, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 0
    assert egc['safety_vest'] == 0


def test_incident_saferack_operator():
    rd = AD({"filedata": [],
            "collection_time": 1621892175.467852,
            "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 1
    assert egc['safety_vest'] == 1


def test_incident_saferack_complex():
    rd = AD({"filedata": [],
            "collection_time": 1621892175.467852,
            "output":{
                "bboxes": {
                    "operator": [
                        np.array([100, 100, 200, 500, 1.00]),
                        np.array([500, 500, 200, 300, 1.00]),
                        np.array([700, 700, 200, 500, 1.00]),
                    ],
                    "safety_vest": [
                        np.array([100, 300, 200, 200, 1.00]),
                        np.array([700, 900, 200, 200, 1.00]),
                    ],
                    "hard_hat": [
                        np.array([150, 100, 50, 50, 1.00]),
                    ]
                },
                "filedata": [
                    ('anonymous.jpg', idata)
                ]
            },
            "region_filter": {}
        })
    egc = AD()
    edgc = AD({'zones': {}})
    evaluate_ppe('ppe', ppe_violation_cfg, egc, edgc, rd)

    assert egc['hard_hat'] == 2
    assert egc['safety_vest'] == 1
'''
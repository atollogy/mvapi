import os
try:
    import sys
    sys.path.append('object_recognition_api')
    from mv_functions.cage_violation import *
    from attribute_dict import AD
    from image.shapes import Point
    from image.utilities import load_image
except:
    from object_recognition_api.mv_functions.cage_violation import cage_violation
    from object_recognition_api.mv_functions.cage_violation import polygon_area
    from object_recognition_api.attribute_dict import AD
    from object_recognition_api.image.shapes import Point

__DRAW = (os.environ.get('ENABLE_DRAWING_TESTS') is not None)
__DRAW_VAR=os.environ.get("ENABLE_DRAWING_TESTS")

import numpy as np
'''
cage_violation_cfg = AD({
    "safe_cage": {
        "correlation": {
          "event_interval": 4,
          "name": "violation_truck",
          "sources": ["38af294f14bc"],
          "step_functions": [
            "lpr"
          ],
          "unknown_image_index": -2
        },
        "categories": {
          "cage": {
            "conditions": {
                "cage_violation": {
                    "alert": True,
                    "deployed_cage_bbox": [
                      630,
                      300,
                      256,
                      239
                    ],
                    "deployed_threshold": 0.75,
                    "deploying_threshold": 0.5,
                    "descriptors": {
                        "cage_violation": True
                    },
                    "enabled": True,
                    "min_count": 2,
                    "output_image": "anonymous.jpg",
                    "raised_cage_bbox": [
                      461,
                      134,
                      306,
                      237
                    ],
                    "threshold": 0.16
                },
                "extreme_cage_violation": {
                    "alert": True,
                    "annotation": "extreme cage violation",
                    "descriptors": {
                        "extreme_cage_violation": True
                    },
                    "deployed_threshold": 0.75,
                    "deploying_threshold": 0.4,
                    "enabled": True,
                    "min_count": 1,
                    "threshold": 0.05,
                    "extreme_displacement": 0.6,
                    "truck_alignment_count": 3
                }
            }
          }
        },
        "alert": {
          "active_alert_window": 7200,
          "atollogy_recipients": [
            "todd@atollogy.com"
          ],
          "use_recipient_field": "ToAddresses"
        }
    }
})



def test_cage_violation_ready():
    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
        "raised_cage_bbox": np.array([100, 100, 500, 500]),
        "deployed_cage_bbox": np.array([300, 300, 500, 500]),
        "deployed_threshold": 0.75,
        "deploying_threshold": 0.50,
    })
    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([100, 100, 500, 500])]
            },
            "filedata": []
        }
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.cage_status == 'ready'

def test_cage_violation_safelydeployed():
    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([100, 100, 500, 500]),
            "deployed_cage_bbox": np.array([300, 300, 500, 500]),
            "deployed_threshold": 0.75,
            "deploying_threshold": 0.50,
        })
    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{"bboxes": {
            "saferack_cage": [np.array([300, 300, 500, 500])]
            },
            "filedata": []
        },
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.cage_status == 'deployed-safely'


def test_cage_violation_deploying():
    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([100, 100, 300, 300]),
            "deployed_cage_bbox": np.array([300, 300, 300, 300]),
            "deployed_threshold": 0.75,
            "deploying_threshold": 0.50})
    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([300, 200, 300, 300])]
            },
            "filedata": []
        }
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.cage_status == 'deploying'


def test_cage_violation_drawing_safe():
    with open('testdata/cage_safe.jpg', 'rb') as file:
        idata = file.read()

    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([461, 134, 306, 237]),
            "deployed_cage_bbox": np.array([630, 300, 256, 239]),
            "deployed_threshold": 0.75,
            "deploying_threshold": 0.50,
        })
    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([461, 134, 306, 237])],
            },
            "filedata": [
                ('anonymous.jpg', idata)
            ]
        },

    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.status == 'ready'
    assert edgc.cage_status == 'ready'
    assert egc.cage_violation == False

    if __DRAW:
        cv2.imshow('undeployed1', load_image(rd.output.filedata[1][1]))

    with open('testdata/cage_safe2.jpg', 'rb') as file:
        idata = file.read()

    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([461, 134, 306, 237]),
            "deployed_cage_bbox": np.array([630, 300, 256, 239]),
            "deployed_threshold": 0.75,
            "deploying_threshold": 0.50,
        })

    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([496, 151, 389, 301])]
            },
            "filedata": [
                ('anonymous.jpg', idata)
            ]
        }
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.status == 'ready'
    assert edgc.cage_status == 'ready'
    assert egc.cage_violation == False

    if __DRAW:
        cv2.imshow('undeployed2', load_image(rd.output.filedata[1][1]))
        cv2.waitKey(0) # waits until a key is pressed
        cv2.destroyAllWindows() # destroys the window showing image

def test_cage_violation_drawing_dangerous():
    with open('testdata/cage_dangerous.jpg', 'rb') as file:
        idata = file.read()

    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([461, 134, 306, 237]),
            "deployed_cage_bbox": np.array([630, 300, 256, 239]),
            "deployed_threshold": 0.80,
            "deploying_threshold": 0.30,
        })

    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([603, 249, 288, 243])],
                "operator": [np.array([652, 311, 255, 250])],
            },
            "filedata": [
                ('anonymous.jpg', idata)
            ]
        }
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.status == 'extreme-cage-violation'
    assert edgc.cage_status == 'deploying'
    assert egc.extreme_cage_violation == True

    if __DRAW:
        cv2.imshow(edgc.cage_status, load_image(rd.output.filedata[1][1]))
        cv2.waitKey(0) # waits until a key is pressed
        cv2.destroyAllWindows() # destroys the window showing image

def test_cage_violation_drawing_deployed():
    with open('testdata/cage_deployed.jpg', 'rb') as file:
        idata = file.read()

    cfg = AD(cage_violation_cfg.safe_cage.categories.cage)
    cfg.conditions.cage_violation.update({
            "raised_cage_bbox": np.array([461, 134, 306, 237]),
            "deployed_cage_bbox": np.array([630, 300, 256, 239]),
            "deployed_threshold": 0.80,
            "deploying_threshold": 0.30,
        })

    rd = AD({
        "collection_time": 1621892175.467852,
        "output":{
            "bboxes": {
                "saferack_cage": [np.array([652, 311, 255, 250])],
            },
            "filedata": [
                ('anonymous.jpg', idata)
            ]
        }
    })
    egc = AD()
    edgc = AD()
    cage_violation('safe_cage', cfg, egc, edgc, rd)

    assert edgc.status == 'deployed-safely'
    assert edgc.cage_status == 'deployed-safely'
    assert egc.cage_violation == False

    if __DRAW:
        cv2.imshow(edgc.cage_status, load_image(rd.output.filedata[1][1]))
        cv2.waitKey(0) # waits until a key is pressed
        cv2.destroyAllWindows() # destroys the window showing image

def test_polygon_area():
    a1 = polygon_area(
        Point(100, 100), Point(200, 100), Point(200,200), Point(100,200)
    )

    a2 = polygon_area(
        Point(200, 100), Point(200,200), Point(100,200), Point(100, 100)
    )

    a3 = polygon_area(
        Point(200,200), Point(100,200), Point(100, 100), Point(200, 100)
    )

    a4 = polygon_area(
        Point(100,200), Point(100, 100), Point(200, 100), Point(200,200)
    )

    assert a1 == a2
    assert a2 == a3
    assert a3 == a4
    assert a4 == (100*100)

def test_polygon_area_nonsquare():
    def ordering_test(points, value):
        a, b, c, d = points

        a1 = polygon_area(a, b, c, d)
        a2 = polygon_area(b, c, d, a)
        a3 = polygon_area(c, d, a, b)
        a4 = polygon_area(d, a, b, c)

        assert a1 == value
        assert a1 == a2
        assert a2 == a3
        assert a3 == a4
        assert a4 == a1

    #     _____
    #   /     /
    #  /_____/
    #
    ordering_test([Point(0,0), Point(100, 100), Point(200, 100), Point(100, 0)], 10000)

    #   ______
    #   \     \
    #    \_____\
    #
    ordering_test([Point(0, 100), Point(100, 100), Point(200, 0), Point(100, 0)], 10000)

    #    _________
    #   /         \
    #  /___________\
    #
    ordering_test([Point(0,0), Point(50,100), Point(100, 100), Point(150, 0)], 10000)

    # unusual shape
    ordering_test([Point(0,0), Point(50,100), Point(150, 200), Point(200, 0)], 22500)
'''
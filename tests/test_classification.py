import os
import sys

import numpy as np
import cv2

# import the system under test
from attribute_dict import AD

### For monkeypatch need to call each function as a one class library.
from mv_functions.tf_inference_classification import *
from image.utilities import *


myPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, "testdata")

airport_config_json = {
    "airport_classifier": {
        "filelist": ["classification_test1.jpg", "classification_test2.jpg"],
        "filedata": {},
        "input_crop": [420, 10, 2128, 2128],
        "input_image_indexes": [0],
        "color_space": "rgb",
        "brightness": {"high_threshold": 170, "low_threshold": 7},
        "nodename": "atlgw01_atl_prd",
        "function": 'inception_imagenet',
        "subject": "airplane",
        "num_top_predictions": 20,
        "confidence_threshold": 0.3,
        "group_class": ["airliner", "warplane, military plane"],
        "debug": True
    }
}

mcfg = AD(
    {
    "inception_imagenet":{
        "name": "inception_imagenet",
        "lookup_path": "/data/mvapi_data/output/inception/inception_imagenet_train/production_model",
        "net_name": "inception_imagenet",
        "type": "tf_classifier",
        "current_version": 0,
        "versions": [0],
        "cfg": {
            "label_file": "imagenet_2012_challenge_label_map_proto.pbtxt",
            "uid_file": "imagenet_synset_to_human_label_map.txt"
        },
        "aliases": [],
        "model_version": 6,
        "threads": 1
    },
    "resnet50_v2_imagenet": {
        "name": "resnet50_v2_imagenet",
        "lookup_path": "/data/mvapi_data/output/resnet50/resnet50_v2_imagenet_train/production_model",
        "net_name": "resnet50_v2_imagenet",
        "type": "tf_classifier",
        "current_version": 2,
        "versions": [0, 1, 2],
        "cfg": {
            "classes_path": "/data/mvapi_data/output/resnet50/resnet50_v2_imagenet_train/production_model/label/resnet50_v2_imagenet_class.json",
            "classes_dir": "/data/mvapi_data/output/resnet50/resnet50_v2_imagenet_train/production_model/label",
            "batch_size": 64,
            "RGB_MEAN": [123.68, 116.78, 103.94]
        },
        "aliases": [],
        "model_version": 2,
        "threads": 1
    }
    }
)

rank_result = {
    "airliner": 0.426,
    "trailer truck, tractor trailer, trucking rig, rig, articulated lorry, semi": 0.3145,
    "wing": 0.0254,
    "aircraft carrier, carrier, flattop, attack aircraft carrier": 0.0173,
    "warplane, military plane": 0.0137,
    "missile": 0.0093,
    "tank, army tank, armored combat vehicle, armoured combat vehicle": 0.0086,
    "tow truck, tow car, wrecker": 0.0078,
    "crane": 0.0077,
    "cannon": 0.0067,
    "projectile, missile": 0.0042,
    "snowplow, snowplough": 0.002,
    "space shuttle": 0.002,
    "racer, race car, racing car": 0.0017,
    "garbage truck, dustcart": 0.0015,
    "forklift": 0.0015,
    "pier": 0.0014,
    "moving van": 0.0014,
    "stretcher": 0.0013,
    "steam locomotive": 0.0013,
}

rank_result2 = {
    "tow truck, tow car, wrecker": 0.0905,
    "aircraft carrier, carrier, flattop, attack aircraft carrier": 0.078,
    "stretcher": 0.0459,
    "dock, dockage, docking facility": 0.0428,
    "crane": 0.0381,
    "amphibian, amphibious vehicle": 0.0373,
    "wreck": 0.0336,
    "submarine, pigboat, sub, U-boat": 0.0332,
    "reel": 0.0287,
    "sandbar, sand bar": 0.0221,
    "forklift": 0.0169,
    "pole": 0.0159,
    "drilling platform, offshore rig": 0.0148,
    "trailer truck, tractor trailer, trucking rig, rig, articulated lorry, semi": 0.0147,
    "fireboat": 0.0142,
    "tank, army tank, armored combat vehicle, armoured combat vehicle": 0.013,
    "hook, claw": 0.0081,
    "warplane, military plane": 0.008,
    "catamaran": 0.0079,
    "missile": 0.0078,
}


### need monkeypatch because init function for class is called (which calls create_graph, which calls tf)
def test_result_confidence(monkeypatch):

    group_class = ["airliner", "warplane, military plane"]
    rd = AD(airport_config_json["airport_classifier"])

    def mockreturn(self):
        return None

    monkeypatch.setattr(ClassificationInference, "create_graph", mockreturn)

    mv_function =  ClassificationInference(mcfg=mcfg.inception_imagenet)
    class_result ,confidence_score = mv_function.result_confidence(rd, rank_result)

    assert confidence_score == 0.426, "\nadd_result_class task failed"


def test_inception_inference(monkeypatch):
    def mockreturn(self):
        return None

    monkeypatch.setattr(ClassificationInference, "create_graph", mockreturn)
    monkeypatch.setattr(ClassificationInference, "setup_class_list", mockreturn)

    def mock_inception_inference(self, color_sapce_image, rd):
        return rank_result, None

    monkeypatch.setattr(
        ClassificationInference, "inception_inference", mock_inception_inference
    )

    mv_function = ClassificationInference(mcfg=mcfg.inception_imagenet)
    stepCfg = AD(airport_config_json["airport_classifier"])
    img_path = os.path.join(myPath, stepCfg.filelist[0])
    file_bytes = get_np_image(img_path)
    stepCfg.origins = [img_path]

    stepCfg.image = file_bytes
    result_classification = mv_function.run_classification(stepCfg)

    assert (
        result_classification.output.airplane == 0.426
    ), "\ninception_inference task failed"


def test_inception_inference_notairplane(monkeypatch):
    def mockreturn(self):
        return None

    monkeypatch.setattr(ClassificationInference, "create_graph", mockreturn)
    monkeypatch.setattr(ClassificationInference, "setup_class_list", mockreturn)

    def mock_inception_inference(self, color_sapce_image, rd):
        return rank_result2, None

    monkeypatch.setattr(
        ClassificationInference, "inception_inference", mock_inception_inference
    )

    mv_function = ClassificationInference(mcfg=mcfg.inception_imagenet)
    stepCfg = AD(airport_config_json["airport_classifier"])

    img_path = os.path.join(myPath, stepCfg.filelist[1])
    file_bytes = get_np_image(img_path)
    stepCfg.origins = [img_path]

    stepCfg.image = file_bytes
    result_classification = mv_function.run_classification(stepCfg)

    assert (
        result_classification.output.airplane == 0.0
    ), "\ninception_inference task with no airplane image failed"

    stepCfg.image = file_bytes
    stepCfg.confidence_threshold = 0.001
    result_classification = mv_function.run_classification(stepCfg)

    assert (
        result_classification.output.airplane == 0.008
    ), "\ninception_inference task with low threshold failed"

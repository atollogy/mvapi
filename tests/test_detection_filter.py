import os
import sys
from atollogy.data.schemas.mv_results.mv_image_results import DetectedObject

import numpy as np
import cv2

# import the system under test
import sys
sys.path.append('object_recognition_api')
from attribute_dict import AD

### For monkeypatch need to call each function as a one class library.
from mv_functions.tf_inference_detection import *
from image.utilities import *

myPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, "testdata")

####### input parameters #######
airport_config_json = AD({
    "airplane_detection": {
        "filelist": [],
        "filedata": {},
        "color_space": "rgb",
        "function": "ground_operations",
        "input_crop": None,
        "input_image_indexes": [0],
        "input_step": "mvedge",
        "region_filter": True,
        "regions": {"stand_101": [400, 290, 1200, 540]},
        "annotate_after_filter": True,
        "subjects": {
            "active": {
                "confidence_thresh": 0.6,
                "nms_threshold": 0.4,
                "roi_ratio": {"airplane": 0.2},
                "overlap": {
                    "airplane": {
                        "ConveyorBelt": None,
                        "LuggageTrailer": None,
                        "FuelTruck": None,
                    }
                },
                "multiclass": True,
                "states": {"all": [["bbox", None], True, True, False]},
            }
        },
    }
})

mcfg = AD(
    {
    "ground_operations": {
        "name": "ground_operations",
        "lookup_path": "/data/mvapi_data/output/faster_rcnn_resnet101/ground_operations_train/production_model",
        "net_name": "faster_rcnn_resnet101",
        "type": "tf_faster_rcnn",
        "current_version":  6,
        "versions": [0, 1, 2, 3, 4, 5, 6],
        "cfg": {},
        "aliases": [],
        "model_version": 6,
        "threads": 1
    },
    }
)

categories = [
    {'id': 1, 'name': "person"},
    {'id': 2, 'name': "airplane"},
    {'id': 3, 'name': "truck"},
    {'id': 4, 'name': "Stairs"},
    {'id': 5, 'name': "PassengerDoor"},
    {'id': 6, 'name': "CargoDoor"},
    {'id': 7, 'name': "LuggageTrailer"},
    {'id': 8, 'name': "PushbackTug"},
    {'id': 9, 'name': "FuelTruck"},
    {'id': 10, 'name': "CateringTruck"},
    {'id': 11, 'name': "ConveyorBelt"},
    {'id': 12, 'name': "PalletLoader"},
    {'id': 13, 'name': "JetBridgeHood"},
    {'id': 14, 'name': "Bus"},
    {'id': 15, 'name': "TugBarWheel"},
    {'id': 16, 'name': "ParkWheel"},
    {'id': 17, 'name': "EmergencyVehicle"},
    {'id': 18, 'name': "TugConnected"}
]

category_index = {
    '1': {'id': 1, 'name': 'person'},
    '2': {'id': 2, 'name': "airplane"},
    '3': {'id': 3, 'name': "truck"},
    '4': {'id': 4, 'name': "Stairs"},
    '5': {'id': 5, 'name': "PassengerDoor"},
    '6': {'id': 6, 'name': "CargoDoor"},
    '7': {'id': 7, 'name': "LuggageTrailer"},
    '8': {'id': 8, 'name': "PushbackTug"},
    '9': {'id': 9, 'name': "FuelTruck"},
    '10': {'id': 10, 'name': "CateringTruck"},
    '11': {'id': 11, 'name': "ConveyorBelt"},
    '12': {'id': 12, 'name': "PalletLoader"},
    '13': {'id': 13, 'name': "JetBridgeHood"},
    '14': {'id': 14, 'name': "Bus"},
    '15': {'id': 15, 'name': "TugBarWheel"},
    '16': {'id': 16, 'name': "ParkWheel"},
    '17': {'id': 17, 'name': "EmergencyVehicle"},
    '18': {'id': 18, 'name': "TugConnected"}
}

###################################

####### test case 1 #######
output_bbxes_test1 = {
    "truck": [],
    "airplane": [[0, 100, 2000, 2000, 0.9999986886978149]],
    "Stairs": [],
    "TugBarWheel": [],
    "CargoDoor": [],
    "ParkWheel": [],
    "FuelTruck": [[1300, 1000, 400, 400, 0.92], [1950, 1900, 600, 600, 0.93]],
    "TugConnected": [],
    "PassengerDoor": [],
    "CateringTruck": [],
    "LuggageTrailer": [[1202, 368, 785, 546, 0.6057034134864807]],
    "PushbackTug": [],
    "JetBridgeHood": [[1195, 1000, 200, 200, 0.9683179259300232]],
    "PalletLoader": [],
    "ConveyorBelt": [[1900, 1900, 300, 500, 0.92]],
    "EmergencyVehicle": [],
    "Bus": [],
    "person": [],
    "operator": [],
}



subject_count_test1 = {
    "truck": 0,
    "airplane": 1,
    "Stairs": 0,
    "TugBarWheel": 0,
    "CargoDoor": 0,
    "ParkWheel": 0,
    "FuelTruck": 2,
    "TugConnected": 0,
    "PassengerDoor": 0,
    "CateringTruck": 0,
    "LuggageTrailer": 1,
    "PushbackTug": 0,
    "JetBridgeHood": 1,
    "PalletLoader": 0,
    "ConveyorBelt": 1,
    "EmergencyVehicle": 0,
    "Bus": 0,
    "person": 0,
    "non-operator": 0,
}

excluded_bbxes1 = {}
###########################

####### test case 2 #######
output_bbxes_test2 = {
    "truck": [],
    "airplane": [[0, 178, 1546, 1008, 0.9999986886978149]],
    "Stairs": [],
    "TugBarWheel": [],
    "CargoDoor": [],
    "ParkWheel": [],
    "FuelTruck": [],
    "TugConnected": [],
    "PassengerDoor": [],
    "CateringTruck": [],
    "LuggageTrailer": [[1202, 368, 3785, 1246, 0.6057034134864807]],
    "PushbackTug": [[1512, 650, 4874, 2135, 0.9999567270278931]],
    "JetBridgeHood": [[1195, 364, 3770, 1236, 0.9683179259300232]],
    "PalletLoader": [],
    "ConveyorBelt": [],
    "EmergencyVehicle": [],
    "Bus": [],
    "person": [],
    "operator": [],
}

subject_count_test2 = {
    "truck": 0,
    "airplane": 1,
    "Stairs": 0,
    "TugBarWheel": 0,
    "CargoDoor": 0,
    "ParkWheel": 0,
    "FuelTruck": 0,
    "TugConnected": 0,
    "PassengerDoor": 0,
    "CateringTruck": 0,
    "LuggageTrailer": 1,
    "PushbackTug": 1,
    "JetBridgeHood": 1,
    "PalletLoader": 0,
    "ConveyorBelt": 0,
    "EmergencyVehicle": 0,
    "Bus": 0,
    "person": 0,
    "non-operator": 0,
}

excluded_bbxes2 = {}
###################################

recData_1 = {
            "output":
                {"bboxes":
                        {"bird":[[0,0,100,100,1.0]], "car":[[1,1,3,4,0.9]], "train":[[2,1,3,4,0.9]], "truck": [[3,1,3,4,0.9]]},
                "subjects":
                        {"bird": 1, "car": 0, "train":0, "truck": 3},
                "excluded":
                        {}
                }
        }

recData_2 = {
            "output":
                {"bboxes":
                        { "car":[], "operator":[], "person":[], "train":[],  "truck":[]},
                "subjects":
                        {"car":0, "operator":0, "person":0, "train":0, "truck":0},
                "excluded":
                        {}
                }
        }

### need monkeypatch because init function for class is called (which calls create_graph, which calls tf)
def test_parents_overlap_filter(monkeypatch):

    overlap_config = airport_config_json["airplane_detection"]["subjects"]["active"]["overlap"]

    def mock_load_model(self):
        return None

    def mock_create_session(self):
        return None

    ### Any identifier of the form __value (at least two leading underscores, at most one trailing underscore)
    ### is textually replaced with _classname__value, where classname is the current class name with leading underscore(s) stripped.
    def mock_createCategoryMap(self, pathToLabelFile):
        self._DetectionInference__categories = categories
        self._DetectionInference__classList = [x["name"] for x in categories]
        self._DetectionInference__category_index = category_index

    monkeypatch.setattr(DetectionInference, "load_tf_model", mock_load_model)
    monkeypatch.setattr(DetectionInference, "create_session", mock_create_session)
    monkeypatch.setattr(DetectionInference, "createCategoryMap", mock_createCategoryMap)
    DetectionInference.load_success = True
    DetectionInference.model_pbtxt = ""

    mv_function = DetectionInference(mcfg=mcfg.ground_operations)

    for class_key, bbox_list in output_bbxes_test1.items():
        output_bbxes_test1[class_key] = [Zone.create(class_key, bb[:4], threshold=bb[4]) for bb in bbox_list]

    bboxes, subjects, excluded = mv_function.parents_overlap_filter(overlap_config, output_bbxes_test1, subject_count_test1, excluded_bbxes1)

    assert bboxes["FuelTruck"] == [[1300, 1000, 400, 400, 0.92]], "\nparents_overlap_filter task failed, bboxes result is incorrect"
    assert subjects["FuelTruck"] == 1 and subjects["ConveyorBelt"] == 0, "\nparents_overlap_filter task failed, subjects result is incorrect"
    assert len(excluded) == 2, "\nparents_overlap_filter task failed, excluded_bbxes result is incorrect"


def test_multiclass_filter(monkeypatch):

    def mock_load_model(self):
        return None

    def mock_create_session(self):
        return None

    ### Any identifier of the form __value (at least two leading underscores, at most one trailing underscore)
    ### is textually replaced with _classname__value, where classname is the current class name with leading underscore(s) stripped.
    def mock_createCategoryMap(self, pathToLabelFile):
        self._DetectionInference__categories = categories
        self._DetectionInference__classList = [x["name"] for x in categories]
        self._DetectionInference__category_index = category_index


    monkeypatch.setattr(DetectionInference, "load_tf_model", mock_load_model)
    monkeypatch.setattr(DetectionInference, "create_session", mock_create_session)
    monkeypatch.setattr(DetectionInference, "createCategoryMap", mock_createCategoryMap)
    DetectionInference.load_success = True
    DetectionInference.model_pbtxt = ""

    mv_function = DetectionInference(mcfg=mcfg.ground_operations)

    bboxes, subjects, excluded = mv_function.multiclass_filter(output_bbxes_test2, subject_count_test2, excluded_bbxes2, 0.6)

    assert bboxes["LuggageTrailer"] == [], "\nparents_overlap_filter task failed, bboxes result is incorrect"
    assert subjects["LuggageTrailer"] == 0, "\nparents_overlap_filter task failed, subjects result is incorrect"
    assert excluded["LuggageTrailer"] == [[1202, 368, 3785, 1246, 0.6057034134864807]], "\nparents_overlap_filter task failed, excluded_bbxes result is incorrect"


def test_post_group_class_bboxes_subject(monkeypatch):

    def mock_load_model(self):
        return None

    def mock_create_session(self):
        return None

    ### Any identifier of the form __value (at least two leading underscores, at most one trailing underscore)
    ### is textually replaced with _classname__value, where classname is the current class name with leading underscore(s) stripped.
    def mock_createCategoryMap(self, pathToLabelFile):
        self._DetectionInference__categories = categories
        self._DetectionInference__classList = [x["name"] for x in categories]
        self._DetectionInference__category_index = category_index


    monkeypatch.setattr(DetectionInference, "load_tf_model", mock_load_model)
    monkeypatch.setattr(DetectionInference, "create_session", mock_create_session)
    monkeypatch.setattr(DetectionInference, "createCategoryMap", mock_createCategoryMap)
    DetectionInference.load_success = True
    DetectionInference.model_pbtxt = ""

    mv_function = DetectionInference(mcfg=mcfg.ground_operations)

    group_class = {"truck": ["truck", "car", "train"]}

    rd = AD(recData_1)

    for key, bbox_list in rd.output.bboxes.items():
        rd.output.bboxes[key] = [Zone.create(key, bb[:4], threshold=bb[4]) for bb in bbox_list]
    print(rd.output.bboxes)
    mv_function.post_group_class(group_class, rd.output, "bboxes", [], save_grouped=True)
    mv_function.post_group_class(group_class, rd.output, "subjects", 0)

    assert rd.output.bboxes == { 'bird': [[0, 0, 100, 100, 1.0]], 'car': [], 'train': [], 'truck': [[3, 1, 3, 4, 0.9], [1, 1, 3, 4, 0.9], [2, 1, 3, 4, 0.9]]}, "\nFail to post_group_class bboxes combine"
    assert rd.output.subjects == {'bird': 1, 'car': 0, 'train': 0, 'truck': 3}, "\nFail to post_group_class subjects combine"
    assert rd.output.grouped_class == {'car':[[1,1,3,4,0.9]], 'train':[[2,1,3,4,0.9]]}, "\nFail to post_group_class createe combined key"

def test_post_group_class_no_detection(monkeypatch):

    def mock_load_model(self):
        return None

    def mock_create_session(self):
        return None

    def mock_createCategoryMap(self, pathToLabelFile):
        self._DetectionInference__categories = categories
        self._DetectionInference__classList = [x["name"] for x in categories]
        self._DetectionInference__category_index = category_index

    monkeypatch.setattr(DetectionInference, "load_tf_model", mock_load_model)
    monkeypatch.setattr(DetectionInference, "create_session", mock_create_session)
    monkeypatch.setattr(DetectionInference, "createCategoryMap", mock_createCategoryMap)
    DetectionInference.load_success = True
    DetectionInference.model_pbtxt = ""

    mv_function = DetectionInference(mcfg=mcfg.ground_operations)

    group_class = {"truck": ["truck", "car", "train"]}

    rd = AD(recData_2)

    mv_function.post_group_class(group_class, rd.output, "bboxes", [], save_grouped=True)
    mv_function.post_group_class(group_class, rd.output, "subjects", 0)

    assert rd.output.bboxes == {"car": [], "operator":[], "person":[], "train": [], "truck":[]}, "\nFail to post_group_class bboxes combine"
    assert rd.output.subjects == {"car": 0, "operator":0, "person":0, "train": 0, "truck":0}, "\nFail to post_group_class subjects combine"
    assert rd.output.grouped_class == {"car": [], "train": []}, "\nFail to post_group_class createe combined key"


def test_process_with_exclusion_filter(monkeypatch):
    def mock_load_model(self):
        return None
    def mock_create_session(self):
        return None
    def mock_createCategoryMap(self, pathToLabelFile):
        self._DetectionInference__categories = categories
        self._DetectionInference__classList = [x["name"] for x in categories]
        self._DetectionInference__category_index = category_index

    monkeypatch.setattr(DetectionInference, "load_tf_model", mock_load_model)
    monkeypatch.setattr(DetectionInference, "create_session", mock_create_session)
    monkeypatch.setattr(DetectionInference, "createCategoryMap", mock_createCategoryMap)
    DetectionInference.load_success = True
    DetectionInference.model_pbtxt = ""

    mv_function = DetectionInference(mcfg=mcfg.ground_operations)
    from image.shapes import Zone


    excluded = AD({'person': [[936, 133], [864, 320]], 'operator': [[936, 133], [864, 320]]})
    detectedBoxes = AD({
        "person": [Zone.create("person", [752, 53, 227, 485, 0.9994]), Zone.create("person", [824, 22, 183, 335, 0.9265])]
    })
    results_bboxes = AD({})
    results = AD({})
    zone_filter = True
    interest_objects = ['person', 'operator']

    
    detections, counts, excluded_obj = mv_function.process_with_exclusion_filter(excluded, detectedBoxes, results_bboxes, results, zone_filter, interest_objects)
    
    assert len(detections['person']) == 0
    assert len(detections['operator']) == 0
    assert counts['person'] == 0
    assert counts['operator'] == 0
    assert counts['non-operator'] == 0
    assert len(excluded_obj['person']) == len(detectedBoxes.person)
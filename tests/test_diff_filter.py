#!/usr/bin/env python3

import os
import numpy as np
import cv2

# import the system under test
from attribute_dict import AD
from mv_functions.diff_filter import *

myPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, "testdata")

diffconfig = "img_diff"

diff_filter_config_json = {
    "img_diff": {
        "filelist": ["img_diff_test1.jpg", "img_diff_test2.jpg", "img_diff_test3.jpg"],
        "filedata": {},
        "color_space": "bgr",
        "brightness": {"high_threshold": 170, "low_threshold": 7},
        "collection_time": 1551247348,
        "subjects": {
            "localALPR": {
                "zone": [850, 500, 1800, 1000],
                "color_space": "gray",
                "pixel_threshold": 20,
                "lesspixel": 1000,
                "padding": 0,
            }
        },
    }
}


def test_img_absdiff():
    stepCfg = AD(diff_filter_config_json[diffconfig])
    img_path = os.path.join(myPath, stepCfg.filelist[0])
    previous_path = os.path.join(myPath, stepCfg.filelist[1])

    img = cv2.imread(img_path)
    previous_img = cv2.imread(previous_path)

    gray_img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur_img1 = cv2.GaussianBlur(gray_img1, (5, 5), 0)

    gray_img2 = cv2.cvtColor(previous_img, cv2.COLOR_BGR2GRAY)
    blur_img2 = cv2.GaussianBlur(gray_img2, (5, 5), 0)

    diff_img = cv2.absdiff(blur_img1, blur_img2)
    diff_img[diff_img <= 20] = 0

    assert np.count_nonzero(diff_img) < 80000, "\nimg_absdiff task failed"


def test_connected_pixel_threshold():
    diff_filter = DiffFilter()
    stepCfg = AD(diff_filter_config_json[diffconfig])
    img_path = os.path.join(myPath, stepCfg.filelist[0])
    previous_path = os.path.join(myPath, stepCfg.filelist[1])

    img = cv2.imread(img_path)
    previous_img = cv2.imread(previous_path)

    gray_img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur_img1 = cv2.GaussianBlur(gray_img1, (5, 5), 0)

    gray_img2 = cv2.cvtColor(previous_img, cv2.COLOR_BGR2GRAY)
    blur_img2 = cv2.GaussianBlur(gray_img2, (5, 5), 0)

    diff_img = cv2.absdiff(blur_img1, blur_img2)
    diff_img[diff_img <= 20] = 0

    filtered_img, changed_blobs, changed_segments, region_pixels, largest_segment, segment_percent = diff_filter.connected_pixel_threshold(
        diff_img, 1000
    )

    assert np.count_nonzero(filtered_img) < np.count_nonzero(
        diff_img
    ), "\nconnected_pixel_threshold task failed"


def test_region_of_interest():
    diff_filter = DiffFilter()
    stepCfg = AD(diff_filter_config_json[diffconfig])
    img_path = os.path.join(myPath, stepCfg.filelist[0])
    previous_path = os.path.join(myPath, stepCfg.filelist[1])

    img = cv2.imread(img_path)
    previous_img = cv2.imread(previous_path)

    gray_img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur_img1 = cv2.GaussianBlur(gray_img1, (5, 5), 0)

    gray_img2 = cv2.cvtColor(previous_img, cv2.COLOR_BGR2GRAY)
    blur_img2 = cv2.GaussianBlur(gray_img2, (5, 5), 0)

    diff_img = cv2.absdiff(blur_img1, blur_img2)
    diff_img[diff_img <= 20] = 0
    filtered_img, changed_blobs, changed_segments, region_pixels, largest_segment, segment_percent = diff_filter.connected_pixel_threshold(
        diff_img, 1000
    )
    roi_result = diff_filter.region_of_interest(filtered_img, 0)

    assert roi_result == [85, 186, 387, 335], "\nregion_of_interest task failed"

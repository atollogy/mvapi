# MVAPI

### Quick Setup
  * The easiest way would be to use Atollogy mvlab or mvapi instances. MV instances derived from mvbase have all the dependencies installed. The standard path for mvapi is /opt/recog and the mvapi service can be managed using systemctl.
  * Always run `source env_setup` before using this repo. See the script file for more details, parameters and defaults.
  * To run mvapi the production models should be downloaded. Run `./download_models`
  * To run mvapi cd into object\_recognition\_api and do `python object_recognition_api.py`

**Note** MV processes depend on following environment variables which are set by the env\_setup script\(default values in parenthesis\):
  1. MVAPI_HOME (repo home directory)
  2. MVAPI_DATA (/dataXX/mvapi_data)
  3. s3_write (0)
  4. MVAPI_CONF (/etc/kvhandler/bcm.json)
  
### Good to know
  * The training data and model snapshots are stored at path \[DATA\_VOLUME\]/recog\_data. If a seperate data volume is not present all data just goes to /recog\_data.
  * The systemctl service instance of mvapi resides at location /opt/recog
  * Chef runs every 20 mins and configures the mvapi service to conform to whatever configuration defined in consul
  * The brnach, commit and install command can be managed at consul path /versions/[ENV]/[CGR]/mvapi
  * The log level can be managed from consul path /envs/[ENV]/svcs/mvapi
  * If you stop the system service the next chef run is going to restart the service. To not have chef to touch your manual configuration set commit to NONE in consul.
  * The NONE commit stops chef from to do any git updates, service restarts and environment changes. Though BCM will update but those changes might not be loaded by mvapi as service is not restarted.
  * mvapi writes the blur and annotated images to path supplied by atlapi. If those paths are not in mvapi's environment or if buckets are not there then expect S3ResponseError or S3AccessError.
  * If you do not want setup your own instance of mvapi and want to use one in /opt. The environment variables for systemd service can be affected by install\_cmd at consul path /versions/[ENV]/[CGR]/mvapi. See env\_setup script for parameter details.
  * If you need to know the environment variables used by systemd service. See in file /etc/atollogy/mvapi_environment


## About
This is a repurposed repo of Xinlei Chen's [Faster RCNN](https://github.com/endernewton/tf-faster-rcnn). The original repo is based on the python Caffe implementation of faster RCNN available [here](https://github.com/rbgirshick/py-faster-rcnn).

**Note**: Several minor modifications are made when reimplementing the framework, which give potential improvements. For details about the modifications and ablative analysis, please refer to the technical report [An Implementation of Faster RCNN with Study for Region Sampling](https://arxiv.org/pdf/1702.02138.pdf). If you are seeking to reproduce the results in the original paper, please use the [official code](https://github.com/ShaoqingRen/faster_rcnn) or maybe the [semi-official code](https://github.com/rbgirshick/py-faster-rcnn). For details about the faster RCNN architecture please refer to the paper [Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks](http://arxiv.org/pdf/1506.01497.pdf).

### Detection Performance
The current code supports **VGG16** and **Resnet V1** models. However, Zac recommends sticking with resnet models.

With Resnet101 (last ``conv4``):

  * Train on VOC 2007 trainval and test on VOC 2007 test, **75.2** (person detection **79.5**).
  * Train on COCO 2014 trainval35k and test on VOC 2007 test(only for person detection), **86.3**.
  * Train on VOC 2007+2012 trainval and test on VOC 2007 test (R-FCN schedule), **79.3**.
  * Train on COCO 2014 trainval35k and test on minival (*old*, 900k/1290k), **34.0**.
  * Train on COCO 2014 trainval35k and test on minival with approximate [FPN](https://arxiv.org/abs/1612.03144) *baseline* [setup](https://github.com/endernewton/tf-faster-rcnn/blob/master/experiments/cfgs/res101-lg.yml) (*old*, 900k/1290k), **35.8**.
  * Train on filtered(images containing faces smaller than 25 width or height were dropped) [WIDER](http://mmlab.ie.cuhk.edu.hk/projects/WIDERFace/), **95.44**
  * Train on McRoskey Brown and Blue Roll(upstairs and downstairs) and test/val on held out set from downstairs w/ mAP of **94.8** where blue roll AP 99.4 and brown roll AP 90.1
  * Train on McRoskey Brown and Blue Roll(upstairs and downstairs) and test/val on held out set from upstairs w/ mAP of **89.6** where brown roll AP 95.9 and blue AP 83.3
  * Autotufting test mAP **96.5** where person AP 89.5, door_closed AP 100.0, and door_open 100.0
  * Autotufting cam2 test mAP **94.6** where person AP 89.7 and mattress AP 99.5
  * manual tufting test mAP **fill in**

**Note**:

  * Due to the randomness in GPU training with Tensorflow espeicially for VOC, the best numbers are reported (with 2-3 attempts) here. According to my experience, for COCO you can almost always get a very close number (within 0.2%) despite the randomness.
  * **All** the numbers are obtained with a different testing scheme without selecting region proposals using non-maximal suppression (TEST.MODE top), the default and original testing scheme (TEST.MODE nms) will likely result in slightly worse performance (see [report](https://arxiv.org/pdf/1702.02138.pdf), for COCO it drops 0.X AP).
  * Since we keep the small proposals (\< 16 pixels width/height), our performance is especially good for small objects.
  * For other minor modifications, please check the [report](https://arxiv.org/pdf/1702.02138.pdf). Notable ones include using ``crop_and_resize``, and excluding ground truth boxes in RoIs during training.
  * For COCO, we find the performance improving with more iterations (VGG16 350k/490k: 26.9, 600k/790k: 28.3, 900k/1190k: 29.5), and potentially better performance can be achieved with even more iterations.
  * For Resnet101, we fix the first block (total 4) when fine-tuning the network, and only use ``crop_and_resize`` to resize the RoIs (7x7) without max-pool. The final feature maps are average-pooled for classification and regression. All batch normalization parameters are fixed. Weight decay is set to Renset101 default 1e-4. Learning rate for biases is not doubled.
  * For approximate [FPN](https://arxiv.org/abs/1612.03144) baseline setup we simply resize the image with 800 pixels, add 32^2 anchors, and take 1000 proposals during testing.
  * Check out [here](http://ladoga.graphics.cs.cmu.edu/xinleic/tf-faster-rcnn/)/[here](http://gs11655.sp.cs.cmu.edu/xinleic/tf-faster-rcnn/)/[here](https://drive.google.com/open?id=0B1_fAEgxdnvJSmF3YUlZcHFqWTQ) for the latest models, including longer COCO VGG16 models and Resnet101 ones.

# Adding a New Data Set
A new data set should follow the structure below:
```
data/dummy_dataset/
----imgs/
----annotations/
----train.txt
----val.txt
----test.txt
```
train-env-setup script can be used to setup all folders before training. It requires the annotation csv file to be at location s3://model-zoo-[env]/MODEL-NAME/VERSION/datasets/MODEL.csv. The script should be called with follwing parameters from the project root.

```
./train-env-setup MODEL-NAME VERSION TO_BACKUP

./train-env-setup manual_tufting 1 0
```

**NOTE**
 VERSION should be kept as whole numbers increasing by 1 on each revision
 The VERSION for each model is used to create the s3 path for object detection output
 When creating a new version update the same in ./object\_recognition_api/model_cfg.py and ./download\_model for deploying proper versions of model.
 
If annotations were generated using the VGG tool look inside utility_scripts for data_setup.py.
```
python data_setup.py MODEL-NAME
```

Next call crossval_setup from utility_scripts/build_crossval.py and pass to it the path to the dataset_name directory, training set size(percent), validation set size(percent), testing set size(percent), and percent similarity threshold (used for making sure images w/ too similar rgb histograms are in same train/ val/test fold)

```
python build_crossval.py MODEL-NAME
```
an annotation file should be named like image_id.json and look like:
```
[[{"width": 132, "top": 750, "height": 157, "label": "blue_roll", "left": 1203}, {"width": 128, "top": 814, "height": 120, "label": "red_roll", "left": 1079}, {"width": 143, "top": 713, "height": 142, "label": "blue_roll", "left": 966},
.
.
.,
{"width": 141, "top": 795, "height": 119, "label": "blue_roll", "left": 854}]
```
an image sitting in the imgs folder should be named like image_id.jpg

train.txt, val.txt, and test.txt all follow the example shape below:
```
image_id_0
.
.
image_id_i
.
.
image_id_n
```

# Training/Testing Files to Update/Create
The following files need to be created/updated in order to introduce a new model. Look for what was done to either manual_tufting, auto_tufting, upstairs_roll, downstairs_roll to follow as an example of how to implement.

* extend mturk.py by creating extended class in to lib/datasets/
* edit lib/datasets/factory.py
* edit experiments/scripts/train_faster_rcnn.sh
* edit experiments/scripts/test_faster_rcnn.sh
* add api config to object_recognition_api/net_cfgs/
* edit object_recognition_api/object_recognition_api.py

# Launching a Training or Testing Process
run a training Process where gpu_id could be 0 dataset could be manual_tufing
and net_name res101
```
./experiments/scripts/train_faster_rcnn {gpu_id} {dataset_name} {net_name}
```

testing example:
```
./experiments/scripts/test_faster_rcnn 0 manual_tufting res101
```

After training use upload model script to upload the snapshots, datasets csv and log to s3.
```
./upload_trained_model MODEL-NAME VERSION
```

# Launching an API
When launching the api you must decide which model you would like to work with, the number of APIs you will launch and the port you
want the API to sit. See in object_recognition_api/model_cfg.py for all available models. When launching the API all the models defined
in this file will be loaded, given that corresponding endpoints are declared in object_recognition_api.py file.

```
python object_recognition_api.py
```

# Object Recognition API

The goal of the object recognition API is to perform various tasks such as locating where people are in an image along with deciding if
a person is in any defined zones.

## 1.1 Object Detection
build bounding boxes and confidence scores of objects of interest in an image.

### 1.1.1 Parameters

```
img_loc : location of the image file. Below is an example path /atl-hub-apdx-image-data/office_image_test/office.jpg
interest_objs : list of objects interested in locating in image
json_loc: s3 location for output json file
blur_objs : list of objects to blur
blur_loc : s3 location of where blurred image should be stored. Below is an example path /atl-hub-apdx-image-data/blur/blur_image.jpg
annotate_loc : s3 location where annotated image will be stored.
annotate_sq : bool value to draw annotation squares
annotate_text : bool value to put annotation text to iamges
confidence_thresh : don't return objects below this confidence level(0.0-1.0)
nms_thresh : join boxes of same class if they overlap by greater than this threshold
```

### 1.1.2 Response

Response is in the following shape:

```
{
event_type: 'computer vision'
sensor_id : Cam ID - 1,2,3.. from key name
timestamp : Unix timestamp in key name
recorder_name : recorder name grabbed from s3 path
recorder_id : Gateway id from key name
cause : From key name. Can be snapshot or "00"
subject_name: auto-tufting-annotations, manual-tufting-annotations.. from key name.
derived_location: null
assets: null
attributes: JSON payload seen in following shape:
            {
            'input_loc': '/d1/d2/f1.jpg',
            'output_loc': '/d3/d4/f2.jpg',
            'model': '/d5/god_box3000.ckpt',
            'api_name': 'tufting',
            'bboxes': boxes along w/ confidence level
                      {
                      interest_obj_1' : [[x1, y1, x2, y2, object_confidence], ...]
                      .
                      .
                      .
                      'interest_obj_k : [[x1, y1, x2, y2, object_confidence], ...]
                      }
            }
}
```

### 1.1.3 Example
```Python
import requests
import simplejson

interest_objs = ['person']  # list of objects to keep track of
blur_objs = []
blur_loc = ''
object_detection_endpoint = 'http://public_ip:5000/object_detection/model_name'  # public ip address of wherever api is hosted
object_detection_params = {'img_loc': '/atl-hub-apdx-image-data/person/test/office.jpg', 'interest_objs': simplejson.dumps(interest_objs), 'blur_objs': simplejson.dumps(blur_objs), 'blur_loc': blur_loc}
resp = requests.get(object_detection_endpoint, params=object_detection_params)
```

# Future
## Implementation
* async processing of images for object detection
* tensorflow and python update to latest versions
* combine all machine vision prcesses to single service. (vision.py from atlapi)
* seperate training/testing from api.
* good way to pull all most recent models from model-zoo and training sets from model-training-sets
* more friendly way to add new datasets
* the data_setup should read image paths from a file that could comes from imageset builder tool, rather listing out whole bucket to find a image file.
* the mvapi configuration - training, testing, detection. This configuration can be found at consul path envs/[ENV]/regions/[REGION]/svcs/mvapi/functions
* gateway configuration - what needs to be done on a image. This configuration can be found at consul path _customerModel/[CGR]/devices/gateways/[GATEWAY NAME]/mv_routing
* Theobject\_recognition\_api/model\_cfg.py is the static representation of above two configurations. The code base needs to be changed to fetch these configuration items from consul


## Research
* detecting small objects([FPN](https://arxiv.org/abs/1612.03144))
* improved network backbone([RESNEXT](https://arxiv.org/abs/1611.05431))
* object segmentation([Mask-RCNN](https://arxiv.org/abs/1703.06870))
* [human object interaction](https://arxiv.org/pdf/1704.07333.pdf)
* check out google's TF obj recognition materials recently released [here](https://github.com/tensorflow/models/tree/master/object_detection)

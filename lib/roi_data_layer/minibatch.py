# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick and Xinlei Chen
# --------------------------------------------------------

"""Compute minibatch blobs for training a Fast R-CNN network."""


import numpy as np
import numpy.random as npr
import cv2
from model.config import cfg
from utils.blob import prep_im_for_blob, im_list_to_blob

from imgaug import augmenters as iaa


def get_minibatch(roidb, num_classes):
    """Given a roidb, construct a minibatch sampled from it."""
    num_images = len(roidb)
    # Sample random scales to use for each image in this batch
    random_scale_inds = npr.randint(
        0, high=len(cfg.TRAIN.SCALES), size=num_images
    )
    assert (
        cfg.TRAIN.BATCH_SIZE % num_images == 0
    ), "num_images ({}) must divide BATCH_SIZE ({})".format(
        num_images, cfg.TRAIN.BATCH_SIZE
    )

    # Get the input image blob, formatted for caffe
    im_blob, im_scales = _get_image_blob(roidb, random_scale_inds)

    blobs = {"data": im_blob}

    assert len(im_scales) == 1, "Single batch only"
    assert len(roidb) == 1, "Single batch only"

    # gt boxes: (x1, y1, x2, y2, cls)
    if cfg.TRAIN.USE_ALL_GT:
        # Include all ground truth boxes
        gt_inds = np.where(roidb[0]["gt_classes"] != 0)[0]
    else:
        # For the COCO ground truth boxes, exclude the ones that are ''iscrowd''
        gt_inds = np.where(
            roidb[0]["gt_classes"]
            != 0 & np.all(roidb[0]["gt_overlaps"].toarray() > -1.0, axis=1)
        )[0]
    gt_boxes = np.empty((len(gt_inds), 5), dtype=np.float32)
    gt_boxes[:, 0:4] = roidb[0]["boxes"][gt_inds, :] * im_scales[0]
    gt_boxes[:, 4] = roidb[0]["gt_classes"][gt_inds]
    blobs["gt_boxes"] = gt_boxes
    blobs["im_info"] = np.array(
        [[im_blob.shape[1], im_blob.shape[2], im_scales[0]]], dtype=np.float32
    )

    return blobs


def _get_image_blob(roidb, scale_inds):
    """Builds an input blob from the images in the roidb at the specified
  scales.
  """
    sometimes = lambda aug: iaa.Sometimes(0.5, aug)

    # Define our sequence of augmentation steps that will be applied to every image
    # All augmenters with per_channel=0.5 will sample one value _per image_
    # in 50% of all cases. In all other cases they will sample new values
    # _per channel_.
    seq = iaa.Sequential(
        [
            # execute 0 to 5 of the following (less important) augmenters per image
            # don't execute all of them, as that would often be way too strong
            iaa.SomeOf(
                (0, 5),
                [
                    sometimes(
                        iaa.Superpixels(
                            p_replace=(0, 1.0), n_segments=(20, 200)
                        )
                    ),  # convert images into their superpixel representation
                    iaa.OneOf(
                        [
                            iaa.GaussianBlur(
                                (0, 3.0)
                            ),  # blur images with a sigma between 0 and 3.0
                            iaa.AverageBlur(
                                k=(2, 7)
                            ),  # blur image using local means with kernel sizes between 2 and 7
                            iaa.MedianBlur(
                                k=(3, 11)
                            ),  # blur image using local medians with kernel sizes between 2 and 7
                        ]
                    ),
                    iaa.Sharpen(
                        alpha=(0, 1.0), lightness=(0.75, 1.5)
                    ),  # sharpen images
                    iaa.Emboss(
                        alpha=(0, 1.0), strength=(0, 2.0)
                    ),  # emboss images
                    # search either for all edges or for directed edges
                    sometimes(
                        iaa.OneOf(
                            [
                                iaa.EdgeDetect(alpha=(0, 0.7)),
                                iaa.DirectedEdgeDetect(
                                    alpha=(0, 0.7), direction=(0.0, 1.0)
                                ),
                            ]
                        )
                    ),
                    iaa.AdditiveGaussianNoise(
                        loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5
                    ),  # add gaussian noise to images
                    iaa.OneOf(
                        [
                            iaa.Dropout(
                                (0.01, 0.1), per_channel=0.5
                            ),  # randomly remove up to 10% of the pixels
                            iaa.CoarseDropout(
                                (0.03, 0.15),
                                size_percent=(0.02, 0.05),
                                per_channel=0.2,
                            ),
                        ]
                    ),
                    iaa.Add(
                        (-10, 10), per_channel=0.5
                    ),  # change brightness of images (by -10 to 10 of original value)
                    iaa.Multiply(
                        (0.5, 1.5), per_channel=0.5
                    ),  # change brightness of images (50-150% of original value)
                    iaa.ContrastNormalization(
                        (0.5, 2.0), per_channel=0.5
                    ),  # improve or worsen the contrast
                    sometimes(
                        iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)
                    ),  # move pixels locally around (with random strengths)
                    sometimes(
                        iaa.PiecewiseAffine(scale=(0.01, 0.05))
                    ),  # sometimes move parts of the image around
                ],
                random_order=True,
            )
        ],
        random_order=True,
    )

    num_images = len(roidb)
    processed_ims = []
    im_scales = []
    for i in range(num_images):
        im = cv2.imread(roidb[i]["image"])
        if roidb[i]["flipped"]:
            im = im[:, ::-1, :]
        target_size = cfg.TRAIN.SCALES[scale_inds[i]]
        im = seq.augment_images([im])[0]
        im, im_scale = prep_im_for_blob(
            im, cfg.PIXEL_MEANS, target_size, cfg.TRAIN.MAX_SIZE
        )
        im_scales.append(im_scale)
        processed_ims.append(im)

    # Create a blob to hold the input images
    blob = im_list_to_blob(processed_ims)

    return blob, im_scales

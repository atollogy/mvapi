import os
import simplejson
import numpy as np
import scipy

from datasets.mturk import mturk
from model.config import cfg


class manual_tufting(mturk):
    def __init__(self, image_set, devkit_path=None):
        mturk.__init__(self, "manual_tufting", image_set, devkit_path)
        self._classes = (
            "__background__",  # always index 0
            "person",
            "horizontal_tufting",
            "vertical_tufting",
            "slanted_tufting",
            "mattress",
        )
        self._class_to_ind = dict(
            list(zip(self.classes, list(range(self.num_classes))))
        )
        self._devkit_path = (
            self._get_default_path() if devkit_path is None else devkit_path
        )
        self._data_path = self._devkit_path
        self._image_index = self._load_image_set_index()

        assert os.path.exists(
            self._devkit_path
        ), "VOCdevkit path does not exist: {}".format(self._devkit_path)
        assert os.path.exists(
            self._data_path
        ), "Path does not exist: {}".format(self._data_path)

    def _get_default_path(self):
        return os.path.join(cfg.DATA_DIR, "manual_tufting")

    def _load_annotation(self, index):
        """
        Load image and bounding boxes info
        """
        filename = os.path.join(
            self._data_path, "annotations", index + ".json"
        )
        with open(filename) as json_annotation:
            print(filename)
            objs = simplejson.load(json_annotation)

        num_objs = len(objs)

        boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)

        # Load object bounding boxes into a data frame.
        for ix, obj in enumerate(objs):
            # Make pixel indexes 0-based
            x1 = obj["left"]
            y1 = obj["top"]
            x2 = obj["left"] + obj["width"]
            y2 = obj["top"] + obj["height"]
            cls = self._class_to_ind[obj["label"]]
            boxes[ix, :] = [x1, y1, x2, y2]
            gt_classes[ix] = cls
            overlaps[ix, cls] = 1.0
            seg_areas[ix] = (x2 - x1 + 1) * (y2 - y1 + 1)

        overlaps = scipy.sparse.csr_matrix(overlaps)

        return {
            "boxes": boxes,
            "gt_classes": gt_classes,
            "gt_overlaps": overlaps,
            "flipped": False,
            "seg_areas": seg_areas,
        }

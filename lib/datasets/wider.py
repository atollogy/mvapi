import os
from datasets.imdb import imdb
import numpy as np
import scipy.sparse
import scipy.io as sio
import utils.cython_bbox
import pickle
import uuid
from .voc_eval import voc_eval
from model.config import cfg
import PIL


class wider(imdb):
    def __init__(self, image_set, split, devkit_path=None):
        imdb.__init__(self, "wider_" + image_set)
        self._image_set = image_set  # {'train', 'test'}
        self._split = split  # {1, 2, ..., 10}
        self._devkit_path = (
            self._get_default_path() if devkit_path is None else devkit_path
        )
        self._data_path = self._devkit_path
        self._classes = ("__background__", "face")  # always index 0
        self._class_to_ind = dict(
            list(zip(self.classes, list(range(self.num_classes))))
        )
        self._image_ext = [".png"]
        self._image_index, self._gt_roidb = self._load_image_set_index()
        # Default to roidb handler
        self._roidb_handler = self.gt_roidb

        self._salt = str(uuid.uuid4())
        self._comp_id = "comp4"

        # Specific config options
        self.config = {
            "cleanup": True,
            "use_salt": True,
            "use_diff": False,
            "matlab_eval": False,
            "rpn_file": None,
            "min_size": 2,
        }

        assert os.path.exists(
            self._devkit_path
        ), "Devkit path does not exist: {}".format(self._devkit_path)
        assert os.path.exists(
            self._data_path
        ), "Path does not exist: {}".format(self._data_path)

    def image_path_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        return self.image_path_from_index(self._image_index[i])

    def image_path_from_index(self, index):
        """
        Construct an image path from the image's "index" identifier.
        """
        for ext in self._image_ext:
            image_path = os.path.join(
                self._data_path, "WIDER_" + self._image_set, index + ".jpg"
            )
            if os.path.exists(image_path):
                break
        assert os.path.exists(image_path), "Path does not exist: {}".format(
            image_path
        )

        return image_path

    def _boxes_to_keep(self, boxes, k_pixels=5):
        """
        remove bounding boxes that are less than k_pixels width or height

        Parameters
        ----------
            boxes : bounding boxes
            k_pixels : minimum width/height of bounding box

        Returns
        -------
            indices of bounding boxes to keep
        """
        widths = boxes[:, 2] - boxes[:, 0] + 1
        heights = boxes[:, 3] - boxes[:, 1] + 1
        keep_idx = np.where(
            np.bitwise_and(widths > k_pixels, heights > k_pixels)
        )
        return keep_idx

    def _load_image_set_index(self):
        """
        Load the indexes listed in this dataset's image set file.
        """
        # # Example path to image set file:
        # # self._data_path + /ImageSets/val.txt
        # # read from file
        # image_set_file = 'split%d/%s_%d_annot.txt' % (self._fold, self._image_set, self._fold)
        # # image_set_file = os.path.join(self._devkit_path, image_set_file)
        # image_set_file = os.path.join('/home/hzjiang/Code/py-faster-rcnn/CS3-splits', image_set_file)

        image_set_file = self._image_set + ".txt"
        image_set_file = os.path.join(self._devkit_path, image_set_file)

        # image_set_file = 'cs3_rand_train_annot.txt'
        # image_set_file = 'wider_dets_annot_from_cs3_model.txt'
        # image_set_file = 'wider_manual_annot.txt'

        assert os.path.exists(
            image_set_file
        ), "Path does not exist: {}".format(image_set_file)

        image_index = []
        gt_roidb = []

        with open(image_set_file) as f:
            # print len(f.lines())
            lines = f.readlines()

            idx = 0
            while idx < len(lines):
                image_name = lines[idx].split("\n")[0]

                image = PIL.Image.open(
                    os.path.join(
                        self._data_path,
                        "WIDER_" + self._image_set,
                        image_name + ".jpg",
                    )
                )
                imw = image.size[0]
                imh = image.size[1]

                idx += 1

                im_annotation_file = os.path.join(
                    self._data_path,
                    self._image_set + "_annotations",
                    image_name + ".csv",
                )
                boxes = np.genfromtxt(im_annotation_file, delimiter=",")
                if len(boxes.shape) == 1:
                    boxes = np.array([boxes])
                num_boxes = boxes.shape[0]
                # print num_boxes

                gt_classes = np.zeros((num_boxes), dtype=np.int32)
                overlaps = np.zeros(
                    (num_boxes, self.num_classes), dtype=np.float32
                )

                gt_classes[:] = self._class_to_ind["face"]
                overlaps[:, self._class_to_ind["face"]] = 1.0

                keep_idx = self._boxes_to_keep(boxes)

                if len(keep_idx[0]) <= 0:
                    continue

                boxes = boxes[keep_idx]
                gt_classes = gt_classes[keep_idx[0]]
                overlaps = overlaps[keep_idx[0], :]

                if not (boxes[:, 2] >= boxes[:, 0]).all():
                    print(boxes)
                    print(image_name)

                # print boxes
                assert (boxes[:, 2] >= boxes[:, 0]).all()
                assert (boxes[:, 3] >= boxes[:, 1]).all()

                overlaps = scipy.sparse.csr_matrix(overlaps)
                gt_roidb.append(
                    {
                        "boxes": boxes,
                        "gt_classes": gt_classes,
                        "gt_overlaps": overlaps,
                        "flipped": False,
                        "image_name": image_name,
                    }
                )
                image_index.append(image_name)

            assert idx == len(lines)

        return image_index, gt_roidb

    def _get_default_path(self):
        """
        Return the default path where PASCAL VOC is expected to be installed.
        """
        return os.path.join(cfg.DATA_DIR, "WIDER")

    def gt_roidb(self):
        """
        Return the database of ground-truth regions of interest.
        This function loads/saves from/to a cache file to speed up future calls.
        """
        cache_file = os.path.join(self.cache_path, self.name + "_gt_roidb.pkl")
        if os.path.exists(cache_file):
            with open(cache_file, "rb") as fid:
                roidb = pickle.load(fid)
            print("{} gt roidb loaded from {}".format(self.name, cache_file))
            return roidb

        with open(cache_file, "wb") as fid:
            pickle.dump(self._gt_roidb, fid, pickle.HIGHEST_PROTOCOL)
        print("wrote gt roidb to {}".format(cache_file))

        return self._gt_roidb

    def selective_search_roidb(self):
        """
        Return the database of selective search regions of interest.
        Ground-truth ROIs are also included.
        This function loads/saves from/to a cache file to speed up future calls.
        """
        cache_file = os.path.join(
            self.cache_path, self.name + "_selective_search_roidb.pkl"
        )

        if os.path.exists(cache_file):
            with open(cache_file, "rb") as fid:
                roidb = pickle.load(fid)
            print("{} ss roidb loaded from {}".format(self.name, cache_file))
            return roidb

        if self._image_set != "test":
            gt_roidb = self.gt_roidb()
            ss_roidb = self._load_selective_search_roidb(gt_roidb)
            roidb = imdb.merge_roidbs(gt_roidb, ss_roidb)
        else:
            roidb = self._load_selective_search_roidb(None)
            print(len(roidb))

        with open(cache_file, "wb") as fid:
            pickle.dump(roidb, fid, pickle.HIGHEST_PROTOCOL)
        print("wrote ss roidb to {}".format(cache_file))

        return roidb

    def _load_selective_search_roidb(self, gt_roidb):
        filename = os.path.abspath(
            os.path.join(self._devkit_path, self.name + ".mat")
        )
        assert os.path.exists(
            filename
        ), "Selective search data not found at: {}".format(filename)

        raw_data = sio.loadmat(filename)["all_boxes"].ravel()

        box_list = []
        for i in range(raw_data.shape[0]):
            boxes = raw_data[i][:, (1, 0, 3, 2)] - 1
            assert (boxes[:, 2] >= boxes[:, 0]).all()
            box_list.append(boxes)

        return self.create_roidb_from_box_list(box_list, gt_roidb)

    def _load_face_annotation(self, index):
        """
        Load image and bounding boxes info from txt files of face.
        """
        filename = os.path.join(self._data_path, "Annotations", index + ".mat")

        data = sio.loadmat(filename)

        num_objs = data["gt"].shape[0]

        boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)

        # Load object bounding boxes into a data frame.
        for ix in range(num_objs):
            # Make pixel indexes 0-based
            coor = data["gt"][ix, :]
            x1 = float(coor[0]) - 1
            y1 = float(coor[1]) - 1
            x2 = float(coor[2]) - 1
            y2 = float(coor[3]) - 1
            cls = self._class_to_ind["face"]
            boxes[ix, :] = [x1, y1, x2, y2]
            gt_classes[ix] = cls
            overlaps[ix, cls] = 1.0

        overlaps = scipy.sparse.csr_matrix(overlaps)

        if not (boxes[:, 2] >= boxes[:, 0]).all():
            print(boxes)
            print(filename)

        assert (boxes[:, 2] >= boxes[:, 0]).all()

        return {
            "boxes": boxes,
            "gt_classes": gt_classes,
            "gt_overlaps": overlaps,
            "flipped": False,
        }

    def _get_comp_id(self):
        comp_id = (
            self._comp_id + "_" + self._salt
            if self.config["use_salt"]
            else self._comp_id
        )
        return comp_id

    def _get_voc_results_file_template(self):
        # VOCdevkit/results/VOC2007/Main/<comp_id>_det_test_aeroplane.txt
        filename = (
            self._get_comp_id() + "_det_" + self._image_set + "_{:s}.txt"
        )
        path = os.path.join(
            self._devkit_path, "results", self._image_set, filename
        )
        return path

    def _write_voc_results_file(self, all_boxes):
        for cls_ind, cls in enumerate(self.classes):
            if cls == "__background__":
                continue
            print("Writing {} VOC results file".format(cls))
            filename = self._get_voc_results_file_template().format(cls)
            with open(filename, "wt") as f:
                for im_ind, index in enumerate(self.image_index):
                    dets = all_boxes[cls_ind][im_ind]
                    if dets == []:
                        continue
                    # the VOCdevkit expects 1-based indices
                    for k in range(dets.shape[0]):
                        f.write(
                            "{:s} {:.3f} {:.1f} {:.1f} {:.1f} {:.1f}\n".format(
                                index,
                                dets[k, -1],
                                dets[k, 0] + 1,
                                dets[k, 1] + 1,
                                dets[k, 2] + 1,
                                dets[k, 3] + 1,
                            )
                        )

    def _do_python_eval(self, output_dir="output"):
        annopath = os.path.join(
            self._devkit_path, self._image_set + "_annotations", "{:s}.csv"
        )
        imagesetfile = os.path.join(
            self._devkit_path, self._image_set + ".txt"
        )
        cachedir = os.path.join(self._devkit_path, "annotations_cache")
        aps = []
        # The PASCAL VOC metric changed in 2010
        use_07_metric = False
        print("VOC07 metric? " + ("Yes" if use_07_metric else "No"))
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        for i, cls in enumerate(self._classes):
            if cls == "__background__":
                continue
            filename = self._get_voc_results_file_template().format(cls)
            rec, prec, ap = voc_eval(
                filename,
                annopath,
                imagesetfile,
                cls,
                cachedir,
                ovthresh=0.5,
                use_07_metric=use_07_metric,
            )
            aps += [ap]
            print(("AP for {} = {:.4f}".format(cls, ap)))
            with open(os.path.join(output_dir, cls + "_pr.pkl"), "w") as f:
                pickle.dump({"rec": rec, "prec": prec, "ap": ap}, f)
        print(("Mean AP = {:.4f}".format(np.mean(aps))))
        print("~~~~~~~~")
        print("Results:")
        for ap in aps:
            print(("{:.3f}".format(ap)))
        print(("{:.3f}".format(np.mean(aps))))
        return np.mean(aps)

    def evaluate_detections(self, all_boxes, output_dir):
        self._write_voc_results_file(all_boxes)
        mAP = self._do_python_eval(output_dir)
        if self.config["matlab_eval"]:
            self._do_matlab_eval(output_dir)
        if self.config["cleanup"]:
            for cls in self._classes:
                if cls == "__background__":
                    continue
                filename = self._get_voc_results_file_template().format(cls)
                os.remove(filename)
        return mAP

    def competition_mode(self, on):
        if on:
            self.config["use_salt"] = False
            self.config["cleanup"] = False
        else:
            self.config["use_salt"] = True
            self.config["cleanup"] = True


if __name__ == "__main__":
    d = datasets.inria("train", "")
    res = d.roidb
    from IPython import embed

    embed()

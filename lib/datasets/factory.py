# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Factory method for easily getting imdbs by name."""


__sets = {}
from datasets.pascal_voc import pascal_voc
from datasets.wider import wider
from datasets.upstairs_roll import upstairs_roll
from datasets.downstairs_roll import downstairs_roll
from datasets.manual_tufting import manual_tufting
from datasets.auto_tufting import auto_tufting
from datasets.service_table import service_table
from datasets.auto_tufting_cam2 import auto_tufting_cam2
from datasets.vdb_1 import vdb_1
from datasets.vdb_2 import vdb_2
from datasets.friends import friends
from datasets.roll import roll

# from datasets.coco import coco
import numpy as np

# Set up voc_<year>_<split>
for year in ["2007", "2012"]:
    for split in ["train", "val", "trainval", "test"]:
        name = "voc_{}_{}".format(year, split)
        __sets[name] = lambda split=split, year=year: pascal_voc(split, year)

# Set up for face detection
for split in ["train", "val"]:
    name = "wider_{}".format(split)
    __sets[name] = lambda split=split: wider(split, 0)

# # Set up coco_2014_<split>
# for year in ['2014']:
#   for split in ['train', 'val', 'minival', 'valminusminival', 'trainval']:
#     name = 'coco_{}_{}'.format(year, split)
#     __sets[name] = (lambda split=split, year=year: coco(split, year))

# # Set up coco_2015_<split>
# for year in ['2015']:
#   for split in ['test', 'test-dev']:
#     name = 'coco_{}_{}'.format(year, split)
#     __sets[name] = (lambda split=split, year=year: coco(split, year))

# Set up McCroskey roll identifier
for split in ["train", "val", "test"]:
    name = "upstairs_roll_{}".format(split)
    __sets[name] = lambda split=split: upstairs_roll(split)

# Set up McCroskey roll identifier
for split in ["train", "val", "test"]:
    name = "downstairs_roll_{}".format(split)
    __sets[name] = lambda split=split: downstairs_roll(split)

# Set up manual_tufting
for split in ["train", "val", "test"]:
    name = "manual_tufting_{}".format(split)
    __sets[name] = lambda split=split: manual_tufting(split)

# Set up auto_tufting
for split in ["train", "val", "test"]:
    name = "auto_tufting_{}".format(split)
    __sets[name] = lambda split=split: auto_tufting(split)

# Set up auto_tufting cam2
for split in ["train", "val", "test"]:
    name = "auto_tufting_cam2_{}".format(split)
    __sets[name] = lambda split=split: auto_tufting_cam2(split)

# Set up service_table
for split in ["train", "val", "test"]:
    name = "service_table_{}".format(split)
    __sets[name] = lambda split=split: service_table(split)

# Set up vdb_1
for split in ["train", "val", "test"]:
    name = "vdb_1_{}".format(split)
    __sets[name] = lambda split=split: vdb_1(split)

# Set up vdb_2
for split in ["train", "val", "test"]:
    name = "vdb_2_{}".format(split)
    __sets[name] = lambda split=split: vdb_2(split)

# Set up friends
for split in ["train", "val", "test"]:
    name = "friends_{}".format(split)
    __sets[name] = lambda split=split: friends(split)

# Set up roll
for split in ["train", "val", "test"]:
    name = "roll_{}".format(split)
    __sets[name] = lambda split=split: roll(split)


def get_imdb(name):
    """Get an imdb (image database) by name."""
    if name not in __sets:
        raise KeyError("Unknown dataset: {}".format(name))
    return __sets[name]()


def list_imdbs():
    """List all registered imdbs."""
    return list(__sets.keys())

# --------------------------------------------------------
# Tensorflow Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Zheqi he, Xinlei Chen, based on code from Ross Girshick
# --------------------------------------------------------


#import _init_paths
from model.test import test_net
from model.config import cfg, cfg_from_file, cfg_from_list
from datasets.factory import get_imdb
import argparse
import pprint
import time, os, sys

import tensorflow as tf
from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1


def parse_args():
    """
  Parse input arguments
  """
    parser = argparse.ArgumentParser(description="Test a Fast R-CNN network")
    parser.add_argument(
        "--cfg",
        dest="cfg_file",
        help="optional config file",
        default=None,
        type=str,
    )
    parser.add_argument(
        "--model", dest="model", help="model to test", default=None, type=str
    )
    parser.add_argument(
        "--imdb",
        dest="imdb_name",
        help="dataset to test",
        default="voc_2007_test",
        type=str,
    )
    parser.add_argument(
        "--comp",
        dest="comp_mode",
        help="competition mode",
        action="store_true",
    )
    parser.add_argument(
        "--num_dets",
        dest="max_per_image",
        help="max number of detections per image",
        default=100,
        type=int,
    )
    parser.add_argument(
        "--tag", dest="tag", help="tag of the model", default="", type=str
    )
    parser.add_argument(
        "--net",
        dest="net",
        help="vgg16, res50, res101, res152",
        default="res50",
        type=str,
    )
    parser.add_argument(
        "--set",
        dest="set_cfgs",
        help="set config keys",
        default=None,
        nargs=argparse.REMAINDER,
    )

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args


def run_test(**kwargs):

    print("kwargs: {}\n".format(kwargs))
    if kwargs["cfg_file"] is not None:
        cfg_from_file(kwargs["cfg_file"])
    if kwargs["set_cfgs"] is not None:
        cfg_from_list(kwargs["set_cfgs"])

    print("Using config:")
    pprint.pprint(cfg)

    # if has model, get the name from it
    # if does not, then just use the inialization weights
    if kwargs["model"]:
        filename = os.path.splitext(os.path.basename(kwargs["model"]))[0]
    else:
        filename = os.path.splitext(os.path.basename(kwargs["weight"]))[0]

    tag = kwargs["tag"]
    tag = tag if tag else "default"
    filename = tag + "/" + filename

    imdb = get_imdb(kwargs["imdb_name"])
    imdb.competition_mode(kwargs["comp_mode"])

    tf.reset_default_graph()
    tfconfig = tf.ConfigProto(allow_soft_placement=True)
    tfconfig.gpu_options.allow_growth = True

    # init session
    sess = tf.Session(config=tfconfig)
    # load network
    if kwargs["net"] == "vgg16":
        net = vgg16(batch_size=1)
    elif kwargs["net"] == "res50":
        net = resnetv1(batch_size=1, num_layers=50)
    elif kwargs["net"] == "res101":
        net = resnetv1(batch_size=1, num_layers=101)
    elif kwargs["net"] == "res152":
        net = resnetv1(batch_size=1, num_layers=152)
    else:
        raise NotImplementedError

    # load model
    net.create_architecture(
        sess,
        "TEST",
        imdb.num_classes,
        tag="default",
        anchor_scales=cfg.ANCHOR_SCALES,
        anchor_ratios=cfg.ANCHOR_RATIOS,
    )

    if kwargs["model"]:
        print(("Loading model check point from {:s}").format(kwargs["model"]))
        saver = tf.train.Saver()
        saver.restore(sess, kwargs["model"])
        print("Loaded.")
    else:
        print(("Loading initial weights from {:s}").format(kwargs["weight"]))
        sess.run(tf.global_variables_initializer())
        print("Loaded.")

    mAP = test_net(
        sess, net, imdb, filename, max_per_image=kwargs["max_per_image"]
    )

    sess.close()
    return mAP


if __name__ == "__main__":
    args = parse_args()

    print("Called with args:")
    print(args)
    run_test(**vars(args))

import simplejson
import os

#import _init_paths
from model.config import cfg, get_output_dir
import glob
from tools.test_net import run_test


def get_snapshots(output_dir):
    """
   grab all the stored models and build a dictionary where the iteration maps to
   the snapshot file location
   """
    print(("outputdir: {}".format(output_dir)))
    sfiles = os.path.join(
        output_dir, cfg.TRAIN.SNAPSHOT_PREFIX + "_iter_*.ckpt.meta"
    )
    sfiles = glob.glob(sfiles)
    sfiles.sort(key=os.path.getmtime)
    model_dict = {}
    for s in sfiles:
        s = s.replace(".meta", "")
        iter_i = s.split("iter_")[-1].replace(".ckpt", "")
        print((iter_i, s))
        model_dict[iter_i] = s
    return model_dict


def test_models(imdb, cfg_file, imdbval_name, net_name, weight_path):
    snapshots = get_snapshots(get_output_dir(imdb, None))
    mAPs = {}
    best_mAP = 0
    best_model = None
    for iter_i in snapshots:
        mAP = run_test(
            cfg_file=cfg_file,
            model=snapshots[iter_i],
            weight=weight_path,
            tag="test",
            imdb_name=imdbval_name,
            comp_mode=True,
            net=net_name,
            max_per_image=100,
            set_cfgs=[],
        )
        mAPs[iter_i] = mAP
        if best_mAP < mAP:
            best_mAP = mAP
            best_model = snapshots[iter_i]
    results_file = os.path.abspath(
        os.path.join(
            cfg.ROOT_DATA_DIR, "output", cfg.EXP_DIR, imdb.name, "results.json"
        )
    )
    with open(results_file, "w") as f:
        simplejson.dump(mAPs, f, sort_keys=True)
    os.system("rm {}/*".format(get_output_dir(imdb, "production_model")))
    os.system(
        "cp {}* {}/.".format(
            best_model.replace(".ckpt", ""),
            get_output_dir(imdb, "production_model"),
        )
    )

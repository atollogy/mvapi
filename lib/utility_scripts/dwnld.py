import boto.s3.connection
import re
import datetime

AWS_ACCESS_KEY_ID = ""
AWS_SECRET_ACCESS_KEY = ""
name_regex = r"(?P<gateway_id>[A-Za-z0-9]{12})[_-](?P<sensor_id>CAM\d+)[_-](?P<reading_time>\d+)[_-](?P<cause>.*).jpg"
device = "atlgw02-b827ebd43775_mcroskey_prd"

CONN = boto.s3.connect_to_region(
    "us-west-2",
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
    is_secure=True,
    calling_format=boto.s3.connection.OrdinaryCallingFormat(),
)

for key_file in ["train.txt", "val.txt", "test.txt"]:
    with open("/home/ubuntu/datasets/manual_tufting/" + key_file) as f:
        for key_name in f.readlines():
            img_name = key_name.strip() + ".jpg"
            bucket = CONN.get_bucket("atl-prd-mcroskey-rawdata")
            m = re.match(name_regex, img_name)
            matches = m.groupdict()
            d = datetime.datetime.fromtimestamp(
                int(matches["reading_time"])
            ) - datetime.timedelta(hours=7)
            datehour = d.strftime("/%Y/%m/%d/%H/")

            try:  # only reason thsi exists is b/c we didn't use to separate by date
                key = bucket.new_key(
                    "images/" + device + "/CAM1" + datehour + img_name
                )
                key.get_contents_to_filename(
                    "/home/ubuntu/datasets/manual_tufting/imgs/" + img_name
                )
            except:
                device = "atlgw02-b827ebd43775_mcroskey_prd"
                date_dir = img_name.split("_")[-1][:8]
                date_dir = "{}-{}-{}".format(
                    date_dir[:4], date_dir[4:6], date_dir[6:]
                )
                key = bucket.new_key(
                    "images/" + device + "/CAM1/" + date_dir + "/" + img_name
                )
                key.get_contents_to_filename(
                    "/home/ubuntu/datasets/manual_tufting/imgs/" + img_name
                )

import sys
import ast
import operator

with open(sys.argv[1]) as f:
    scores = ast.literal_eval(f.readline().strip())
    best = max(iter(scores.items()), key=operator.itemgetter(1))
    print(("{}: {}".format(*best)))

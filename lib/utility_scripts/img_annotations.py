#from . import _init_paths
import argparse
import requests
import json
from boto.s3.connection import S3Connection
import re
import logging
from datetime import datetime
from logging.handlers import RotatingFileHandler

# Logger settings
APP_LOGGER = "img_annotation"
LOG_NAME = "img_annotation"
LOG_SIZE = 20000000  # 10MB
LOG_ROTATION_COUNT = 10
LOG_LEVEL = logging.DEBUG
customer_name = "vdb"

FNAME_REGEX = r"(?P<recorder_id>[A-Za-z0-9]{12})[-_](?P<sensor_id>CAM\d+)[_-](?P<reading_time>\d+)[-_](?P<cause>.*)\.j.+$"
MODELS = {"mills_6_8": "friends", "mills_5_7": "friends"}

dates = ["2017/08/14/10", "2017/08/14/11"]

OBJ_DICT = {
    "upstairs_roll": (["brown_roll", "blue_roll"], []),
    "downstairs_roll": (["brown_roll", "blue_roll"], []),
    "manual_tufting": (
        [
            "person",
            "horizontal_tufting",
            "vertical_tufting",
            "slanted_tufting",
            "mattress",
        ],
        ["person"],
    ),
    "auto_tufting": (["person", "door_open", "door_closed"], ["person"]),
    "auto_tufting_cam2": (["person", "mattress"], ["person"]),
    "mills_6_8": (["person"], ["person"]),
    "mills_5_7": (["person"], ["person"]),
}

GATEWAY_DICT = {
    "mills_6_8": "atlgw03-b827eb2236ac_vdb_prd",
    "mills_5_7": "atlgw04-b827eb316da6_vdb_prd",
    "upstairs_roll": "atlgw04-b827eb6c888a_mcroskey_prd",
    "downstairs_roll": "atlgw03-b827eb34575a_mcroskey_prd",
    "manual_tufting": "atlgw02-b827ebd43775_mcroskey_prd",
    "auto_tufting": "atlgw01-b827eb63ac2f_mcroskey_prd",
    "auto_tufting_cam2": "atlgw05-b827ebcbc510_mcroskey_prd",
}


def get_name_features(f_name):
    m = re.match(FNAME_REGEX, f_name)
    gateway = m.groupdict()["recorder_id"]
    cam = m.groupdict()["sensor_id"]
    timest = m.groupdict()["reading_time"]
    if timest.startswith("2017"):
        timestamp = datetime.strptime(timest, "%Y%m%d%H%M%S")
    else:
        timestamp = datetime.utcfromtimestamp(int(timest))

    return (
        gateway,
        cam,
        timestamp.year,
        "{:02d}".format(timestamp.month),
        "{:02d}".format(timestamp.day),
        "{:02d}".format(timestamp.hour),
    )


def s3_json_path(f_name):
    gateway, cam, year, month, day, hour = get_name_features(f_name)
    json_path = "/atl-prd-{}-data/imageresults/{}/{}/{}/{}/{}/{}/{}".format(
        customer_name, gateway, cam, year, month, day, hour, f_name
    )
    return json_path


def s3_img_ann_path(f_name):
    gateway, cam, year, month, day, hour = get_name_features(f_name)
    img_path = "/atl-prd-{}-data/annotated_images/{}/{}/{}/{}/{}/{}/{}".format(
        customer_name, gateway, cam, year, month, day, hour, f_name
    )
    return img_path


def s3_img_blur_path(f_name):
    gateway, cam, year, month, day, hour = get_name_features(f_name)
    img_path = "/atl-prd-{}-data/blur_images/{}/{}/{}/{}/{}/{}/{}".format(
        customer_name, gateway, cam, year, month, day, hour, f_name
    )
    return img_path


def mv_to_s3(local_loc, s3_loc):
    cp_to_s3(conn, local_loc, s3_loc)
    rm_file(local_loc)


def draw_and_save(img_loc, annotations, text_bool, save_loc):
    im = cv2.imread(img_loc)
    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect="equal")
    for class_name in annotations["attributes"]["bboxes"]:
        for annotation in annotations["attributes"]["bboxes"][class_name]:
            bbox = annotation[:4]
            score = annotation[-1]
            ax.add_patch(
                plt.Rectangle(
                    (bbox[0], bbox[1]),
                    bbox[2] - bbox[0],
                    bbox[3] - bbox[1],
                    fill=False,
                    edgecolor="red",
                    linewidth=3.5,
                )
            )
            if text_bool:
                ax.text(
                    bbox[0],
                    bbox[1] - 2,
                    "{:s} {:.3f}".format(class_name, score),
                    bbox=dict(facecolor="blue", alpha=0.5),
                    fontsize=14,
                    color="white",
                )
    plt.tight_layout()
    plt.draw()
    plt.savefig(save_loc)
    plt.close()


def annotate_and_blur_demo(
    conn, interest_objs, blur_objs, endpoint, raw_bucket_name, s3_img_dir
):
    """
    use this to make demo images that can blur and draw the annotation on an image.
    JSON responses can also be stored

    Parameters
    ----------
    conn : boto connection object
    interest_objs : objects interested in annotating
    blur_objs : subset of interest_objs that we want to blur
    object_detection_endpoint : api endpoint
    raw_bucket_name : name of bucket where images are stored
    data_bucket: name of bucket where results will be stored
    s3_img_dir : directory path where images are stored
    s3_blur_dir : directory path where blurred image should be stored
    json_dir : local directory where json annotations should be stored

    Returns
    -------
    None
    """
    try:
        bucket = conn.get_bucket(raw_bucket_name)
        img_locs = [i.name for i in bucket.list(s3_img_dir)]
        for iter_i, img_loc in enumerate(img_locs):
            print(img_loc)

            img_name = img_loc.split("/")[-1]
            s3_img_path = "/" + raw_bucket_name + "/" + img_loc
            logger.info("Image: " + s3_img_path)

            blur_s3_path = s3_img_blur_path(img_name)
            s3_annotated_img_path = s3_img_ann_path(img_name)

            json_name = img_name.replace(".jpg", ".json")
            s3_json_res_path = s3_json_path(json_name)

            params = {
                "img_loc": s3_img_path,
                "interest_objs": json.dumps(interest_objs),
                "blur_objs": json.dumps(blur_objs),
                "blur_loc": blur_s3_path,
                "annotate_loc": s3_annotated_img_path,
                "json_loc": s3_json_res_path,
                "annotate_sq": True,
                "annotate_text": True,
                "nms_thresh": "50",
                "confidence_thresh": "60",
            }

            resp = requests.get(endpoint, params=params)

            print(("Status code: " + str(resp.status_code)))
            logger.info(resp)
    except Exception as err:
        logger.exception(err)
        print("ERROR")


if __name__ == "__main__":
    # parser = argparse.ArgumentParser(description="Atollogy beacon aggregator")
    # parser.add_argument('-H', '--HELP',
    #                     help="Display extended help documentation")
    # parser.add_argument('-a', '--api',
    #                     default=None,
    #                     required=True,
    #                     help="Api name to process")
    #
    # args = parser.parse_args()
    # model = args.api

    # Set up logging
    LOG_NAME = "-".join(["annotation", "1"]) + ".log"

    logger = logging.getLogger(LOG_NAME)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    floghandler = RotatingFileHandler(
        LOG_NAME, maxBytes=LOG_SIZE, backupCount=LOG_ROTATION_COUNT
    )
    floghandler.setLevel(LOG_LEVEL)
    floghandler.setFormatter(formatter)
    logger.addHandler(floghandler)
    logger.setLevel(LOG_LEVEL)

    conn = S3Connection(
    )

    raw_bucket_name = "atl-prd-{}-rawdata".format(customer_name)

    for model in list(MODELS.keys()):
        for d in dates:

            blur_objs = OBJ_DICT[model][1]
            interest_objs = OBJ_DICT[model][0]
            gateway = GATEWAY_DICT[model]

            endpoint = (
                "http://localhost:5000/object_detection/" + MODELS[model]
            )

            gateway = GATEWAY_DICT[model]

            s3_img_dir = "images/{}/CAM1/{}".format(gateway, d)

            print(
                (
                    conn,
                    interest_objs,
                    blur_objs,
                    endpoint,
                    raw_bucket_name,
                    s3_img_dir,
                )
            )

            annotate_and_blur_demo(
                conn,
                interest_objs,
                blur_objs,
                endpoint,
                raw_bucket_name,
                s3_img_dir,
            )

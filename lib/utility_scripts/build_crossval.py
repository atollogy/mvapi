import cv2
import numpy as np
import os
import sys


def img_similarity(img1, img2):
    img1_hist = cv2.calcHist(
        [img1], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256]
    )
    img2_hist = cv2.calcHist(
        [img2], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256]
    )
    return cv2.compareHist(img1_hist, img2_hist, 0)


def organize(distinct_ims, train_size, val_size, test_size):
    """
    take desired cross-val split sizes and fill from largest to smallest bins
    """
    id_size = [(i, len(distinct_ims[i])) for i in distinct_ims]
    id_size.sort(key=lambda x: x[1], reverse=True)

    cv_set = {"train": [], "val": [], "test": []}

    cv_space = [["train", train_size], ["val", val_size], ["test", test_size]]
    for bin, bin_size in id_size:
        cv_space.sort(key=lambda x: x[1], reverse=True)
        cv_set[cv_space[0][0]].append(bin)
        cv_space[0][1] -= bin_size
    return cv_set


def assemble_folds(distinct_ims, cv_bin_ids):
    cv = {"train": [], "val": [], "test": []}
    for cv_fold in cv_bin_ids:
        for bin_id in cv_bin_ids[cv_fold]:
            cv[cv_fold] += distinct_ims[bin_id]
    return cv


def crossval_setup(
    data_dir, train_sz=0.6, val_sz=0.15, test_sz=0.25, sim_thresh=0.99
):
    img_dir = os.path.join(data_dir, "imgs")
    distinct_ims = {}
    ims = os.listdir(img_dir)
    for i in ims:
        print(i)
        img_i = cv2.imread(os.path.join(img_dir, i))
        unique = True
        for img_bin in distinct_ims:
            ran_samp = cv2.imread(
                os.path.join(img_dir, np.random.choice(distinct_ims[img_bin]))
            )
            if img_similarity(img_i, ran_samp) > sim_thresh:
                distinct_ims[img_bin].append(i)
                unique = False
                break
        if unique:
            distinct_ims[i] = [i]

    train_size = int(len(ims) * train_sz)
    val_size = int(len(ims) * val_sz)
    test_size = int(len(ims) * test_sz)
    cv_ids = organize(distinct_ims, train_size, val_size, test_size)
    cv = assemble_folds(distinct_ims, cv_ids)

    for fold in cv:
        with open(os.path.join(data_dir, fold + ".txt"), "w") as f:
            for i in cv[fold]:
                f.write("{}\n".format(i.replace(".jpg", "")))


mvdata = os.path.join("", "data", "mvapi_data")
if "MVAPI_DATA" in os.environ:
    mvdata = os.environ["MVAPI_DATA"]

if __name__ == "__main__":
    if len(sys.argv) is not 2:
        print("Print give the model you want to build data for")
        exit

    model = sys.argv[1]
    crossval_setup(os.path.join(mvdata, "input", model))

import pandas as pd
import simplejson
import cv2


def csv_to_dicts(mturk_data):
    annotations = {}
    #  parse csv block
    for _, r in mturk_data.iterrows():
        jsonfile_name = r["#filename"].strip()
        try:
            obj_name = simplejson.loads(r["region_attributes"])[
                "class"
            ].strip()
            attributes = simplejson.loads(r["region_shape_attributes"])
            annotation = {
                "left": attributes["x"],
                "width": attributes["width"],
                "top": attributes["y"],
                "height": attributes["height"],
                "label": obj_name,
            }
        except:  # if nothing is labeled
            continue
        if jsonfile_name in annotations.keys():
            annotations[jsonfile_name].append(annotation)
        else:
            annotations[jsonfile_name] = [annotation]
    return annotations


# csv file path for additional annotation.
# csv_path='/data/mvapi_data/input/friends/v4/friends_old_bak 90/friends_90.csv'

folder_path = csv_path[: -len(csv_path.split("/")[-1]) - 1]

annotation_dir = "/data/mvapi_data/input/friends/annotations/"
image_dir = "/data/mvapi_data/input/friends/imgs/"

mturk_data = pd.read_csv(csv_path)
annotations = csv_to_dicts(mturk_data)

missing_count = 0
missing_img = {}

for key, value in annotations.items():
    file_path = folder_path + "/" + key
    img = cv2.imread(file_path)
    if img is not None:
        h, w, _ = img.shape
        cv2.imwrite(image_dir + "/" + key, img)

        jss = annotations[key]

        for js in jss:
            if js["left"] < 0:
                js["left"] = 0
                print("adjust left: " + key)
            if js["top"] < 0:
                js["top"] = 0
                print("adjust top: " + key)
            if js["left"] + js["width"] > w:
                js["width"] = w - js["left"] - 1
                print("adjust width: " + key)
            if js["top"] + js["height"] > h:
                js["height"] = h - js["top"] - 1
                print("adjust height: " + key)

        with open(annotation_dir + key[:-4] + ".json", "w") as f:
            simplejson.dump(jss, f)

        print("processing:", key)
    else:
        missing_count = missing_count + 1
        missing_img[key] = 0
        print("missing image:", key)

print("Missing images are:\n", sorted(missing_img.keys()))
print("----------------------------------------------------- ")
print("Number of annotations missing an image: ", missing_count)

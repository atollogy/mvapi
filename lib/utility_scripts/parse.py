import numpy as np
import pandas as pd
import simplejson

#  cross val split sizes
TRAIN_SIZE, VAL_SIZE, TEST_SIZE = 0.6, 0.15, 0.25

a = pd.read_csv("/home/ubuntu/datasets/auto_tufting/auto_tufting.csv")
annotations = {}

#  parse csv block
for _, r in a.iterrows():
    jsonfile_name = r["#filename"].strip(".jpg") + ".json"
    try:
        obj_name = simplejson.loads(r["region_attributes"])["class"].strip()
        attributes = simplejson.loads(r["region_shape_attributes"])

        annotation = {
            "left": attributes["x"],
            "width": attributes["width"],
            "top": attributes["y"],
            "height": attributes["height"],
            "label": obj_name,
        }
    except:  # if nothing is labeled
        continue
    if jsonfile_name in list(annotations.keys()):
        annotations[jsonfile_name].append(annotation)
    else:
        annotations[jsonfile_name] = [annotation]

#  dump so each image in the csv has it's own corresponding json
for jsonfile_name in annotations:
    with open(
        "/home/ubuntu/datasets/auto_tufting/annotations/" + jsonfile_name, "a+"
    ) as f:
        simplejson.dump(annotations[jsonfile_name], f)

#  train/val/test split
file_keys = [i.strip(".json") for i in list(annotations.keys())]
num_files = len(file_keys)
train_files = np.random.choice(
    file_keys, int(TRAIN_SIZE * num_files), replace=False
)

file_keys = list(set(file_keys) - set(train_files))
val_files = np.random.choice(
    file_keys, int(VAL_SIZE * num_files), replace=False
)

test_files = list(set(file_keys) - set(val_files))

#  build txt files to relate images and annotations to crossval sets
crossval_set = {"train": train_files, "val": val_files, "test": test_files}
for split_name in crossval_set:
    with open(
        "/home/ubuntu/datasets/auto_tufting/" + split_name + ".txt", "w"
    ) as f:
        for file_key in crossval_set[split_name]:
            f.write(file_key + "\n")

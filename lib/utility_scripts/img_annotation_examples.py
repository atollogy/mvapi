#from . import _init_paths
from object_detection import cp_from_s3, cp_to_s3, rm_file

import requests
import os
import simplejson
from boto.s3.connection import S3Connection
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import cv2
from datetime import datetime

OBJ_DICT = {
    "upstairs_roll": ["brown_roll", "blue_roll"],
    "downstairs_roll": ["brown_roll", "blue_roll"],
    "manual_tufting": [
        "person",
        "horizontal_tufting",
        "vertical_tufting",
        "slanted_tufting",
    ],
    "auto_tufting": ["person", "door_open", "door_closed"],
}
GATEWAY_DICT = {
    "upstairs_roll": "atlgw04-b827eb6c888a_mcroskey_prd",
    "downstairs_roll": "atlgw03-b827eb34575a_mcroskey_prd",
    "manual_tufting": "atlgw02-b827ebd43775_mcroskey_prd",
    "auto_tufting": "atlgw01-b827eb63ac2f_mcroskey_prd",
}


def get_name_features(f_name):
    gateway, cam = f_name.split("_")[:2]
    if "-" in f_name:
        ts_str = "%Y%m%d%H%M%S"
        timestamp = f_name.split("_")[-1].split("-")[0]
        timestamp = datetime.strptime(timestamp, ts_str)
    else:
        timestamp = f_name.split("_")[2]
        timestamp = datetime.fromtimestamp(int(timestamp))
    return (
        gateway,
        cam,
        timestamp.year,
        "{:02d}".format(timestamp.month),
        "{:02d}".format(timestamp.day),
        "{:02d}".format(timestamp.hour),
    )


def s3_json_path(f_name):
    gateway, cam, year, month, day, hour = get_name_features(f_name)
    json_path = "/atl-prd-mcroskey-data/imageresults/{}/{}/{}/{}/{}/{}/{}".format(
        gateway, cam, year, month, day, hour, f_name
    )
    return json_path


def s3_img_ann_path(f_name):
    gateway, cam, year, month, day, hour = get_name_features(f_name)
    img_path = "/atl-prd-mcroskey-data/images/{}/{}/{}/{}/{}/{}/{}".format(
        gateway, cam, year, month, day, hour, f_name
    )
    return img_path


def mv_to_s3(local_loc, s3_loc):
    cp_to_s3(conn, local_loc, s3_loc)
    rm_file(local_loc)


def draw_and_save(img_loc, annotations, text_bool, save_loc):
    im = cv2.imread(img_loc)
    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect="equal")
    for class_name in annotations["attributes"]["bboxes"]:
        for annotation in annotations["attributes"]["bboxes"][class_name]:
            bbox = annotation[:4]
            score = annotation[-1]
            ax.add_patch(
                plt.Rectangle(
                    (bbox[0], bbox[1]),
                    bbox[2] - bbox[0],
                    bbox[3] - bbox[1],
                    fill=False,
                    edgecolor="red",
                    linewidth=3.5,
                )
            )
            if text_bool:
                ax.text(
                    bbox[0],
                    bbox[1] - 2,
                    "{:s} {:.3f}".format(class_name, score),
                    bbox=dict(facecolor="blue", alpha=0.5),
                    fontsize=14,
                    color="white",
                )
    plt.tight_layout()
    plt.draw()
    plt.savefig(save_loc)
    plt.close()


def annotate_and_blur_demo(
    conn,
    interest_objs,
    blur_objs,
    object_detection_endpoint,
    bucket_name,
    s3_img_dir,
    s3_blur_dir,
    json_dir,
    localdir,
):
    """
    use this to make demo images that can blur and draw the annotation on an image.
    JSON responses can also be stored
    
    Parameters
    ----------
    conn : boto connection object
    interest_objs : objects interested in annotating
    blur_objs : subset of interest_objs that we want to blur
    object_detection_endpoint : api endpoint
    bucket_name : name of bucket where images are stored
    s3_img_dir : directory path where images are stored
    s3_blur_dir : directory path where blurred image should be stored
    json_dir : local directory where json annotations should be stored
    
    Returns
    -------
    None
    """
    bucket = conn.get_bucket(bucket_name)
    img_locs = [i.name for i in bucket.list(s3_img_dir, "/")]
    for iter_i, img_loc in enumerate(img_locs):
        print(("iter {}/{}".format(iter_i, len(img_locs))))
        img_name = img_loc.split("/")[-1]
        s3_img_path = "/" + bucket_name + "/" + img_loc
        blur_s3_path = "/" + bucket_name + "/" + s3_blur_dir + img_name
        object_detection_params = {
            "img_loc": s3_img_path,
            "interest_objs": simplejson.dumps(interest_objs),
            "blur_objs": simplejson.dumps(blur_objs),
            "blur_loc": blur_s3_path,
        }
        resp = requests.get(
            object_detection_endpoint, params=object_detection_params
        )
        if json_dir:  #  store json response
            json_loc = json_dir + img_name.replace(".jpg", ".json")
            with open(json_loc, "w") as f:
                simplejson.dump(resp.json(), f)
            s3_json_res_path = s3_json_path(json_loc.split("/")[-1])
            mv_to_s3(json_loc, s3_json_res_path)
        if localdir:  # build and store annotated image
            local_path = localdir + img_name
            s3_annotated_img_path = s3_img_ann_path(img_name)
            cp_from_s3(conn, s3_img_path, local_path)
            draw_and_save(local_path, resp.json(), True, local_path)
            mv_to_s3(local_path, s3_annotated_img_path)


if __name__ == "__main__":
    api_name = "auto_tufting"
    dates = ["2017-06-17", "2017-06-18"]
    object_detection_endpoint = "http://172.31.0.106:5000/object_detection"

    json_dir = "/home/ubuntu/{}_json/".format(api_name)
    annotated_img_dir = "/home/ubuntu/{}_annotated_imgs/".format(api_name)
    rm_file(json_dir)
    rm_file(annotated_img_dir)
    os.mkdir(json_dir)
    os.mkdir(annotated_img_dir)

    interest_objs = OBJ_DICT[api_name]
    gateway = GATEWAY_DICT[api_name]

    conn = S3Connection(
    )
    blur_objs = []
    bucket_name = "atl-prd-mcroskey-rawdata"
    for d in dates:
        s3_img_dir = "images/{}/{}/".format(gateway, d)
        s3_blur_dir = ""
        annotate_and_blur_demo(
            conn,
            interest_objs,
            blur_objs,
            object_detection_endpoint,
            bucket_name,
            s3_img_dir,
            s3_blur_dir,
            json_dir,
            annotated_img_dir,
        )
